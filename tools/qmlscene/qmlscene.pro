TEMPLATE = app
TARGET = qmlscene
DESTDIR=$$PWD/../../bin

include($$PWD/../../src/scenegraph_include.pri)

macx: CONFIG -= app_bundle

SOURCES += main.cpp

CONFIG += console

CONFIG(bundle) | symbian: include(bundle/bundle.pri)

symbian {
    TARGET.EPOCHEAPSIZE = 0x20000 0x5000000
}

DEFINES += QML_RUNTIME_TESTING
