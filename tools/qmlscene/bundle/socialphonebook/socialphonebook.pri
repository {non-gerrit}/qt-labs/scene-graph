symbian {
	socialphonebook.sources = $$DECLARATIVE_UI_EXAMPLES/socialphonebook
	socialphonebook.path = ./qmlscene-resources

	DEPLOYMENT += socialphonebook
	
} else {
	RESOURCES += $$PWD/socialphonebook.qrc
}