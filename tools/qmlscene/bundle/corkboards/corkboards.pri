symbian {


	corkboards.sources = $$[QT_INSTALL_PREFIX]/examples/declarative/toys/corkboards
	corkboards.path = ./qmlscene-resources

	DEPLOYMENT += corkboards

} else {

	RESOURCES += $$PWD/corkboards.qrc
	
}