/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DEFAULT_VIDEONODE_H
#define DEFAULT_VIDEONODE_H

#include "texturematerial.h"
#include "node.h"
#include "qxvideoplayer_p.h"

#include <phonon/phononnamespace.h>
#include <private/qgl_p.h>
#include <QtOpenGL/qglbuffer.h>
#include <QtCore/qobject.h>

class VideoNodeInterface : public QObject, public GeometryNode
{
    Q_OBJECT
public:
    VideoNodeInterface()
        : m_opacity(1)
        , m_current_time(0)
        , m_volume(1)
        , m_fill_mode(QxVideoPlayer::Stretch)
        , m_linear_filtering(false)
        , m_playing(false)
        , m_mute(false)
    {
    }

    virtual void setRect(const QRectF &rect) = 0;
    QRectF rect() const { return m_rect; }

    virtual void setOpacity(qreal opacity) = 0;
    qreal opacity() const { return m_opacity; }

    virtual void setLinearFiltering(bool linearFiltering) = 0;
    bool linearFiltering() const { return m_linear_filtering; }

    virtual void setSource(const QUrl &source) = 0;
    QUrl source() const { return m_source; }

    virtual void setPlaying(bool playing) = 0;
    bool isPlaying() const { return m_playing; }

    virtual void setCurrentTime(qint64 millis) = 0;
    virtual qint64 currentTime() const { return m_current_time; }

    virtual qint64 length() = 0;

    virtual QxVideoPlayer::State state() const = 0;

    virtual void setSourceSize(const QSize &size) = 0;
    QSize sourceSize() const { return m_source_size; }

    virtual QSize nativeSize() const = 0;

    virtual void setFillMode(QxVideoPlayer::FillMode mode) = 0;
    QxVideoPlayer::FillMode fillMode() const { return m_fill_mode; }

    virtual void setMute(bool mute) = 0;
    bool isMute() const { return m_mute; }

    virtual void setVolume(qreal volume) = 0;
    qreal volume() const { return m_volume; }

Q_SIGNALS:
    void currentTimeChanged();
    void stateChanged();
    void lengthChanged();
    void nativeSizeChanged();

protected:
    QRectF m_rect;
    QSize m_source_size;
    qreal m_opacity;
    QUrl m_source;
    qint64 m_current_time;
    qreal m_volume;
    QxVideoPlayer::FillMode m_fill_mode;
    bool m_linear_filtering;
    bool m_playing;
    bool m_mute;
};

namespace Phonon {
    class MediaObject;
    class AudioOutput;
    class VideoWidget;
}

class AbstractBuffer
{
public:
    virtual ~AbstractBuffer() { }
    virtual void setSize(const QSize &size) = 0;
    virtual QSize size() = 0;
    virtual QGLTexture2DPtr texture() = 0;
    virtual QPaintDevice *paintDevice() = 0; // Lock, map or whatever.
    virtual void updateTexture() = 0; // Unlock, unmap or unwhatever.
};

class TextureBuffer : public AbstractBuffer
{
public:
    TextureBuffer() : m_guard(0) { }
    virtual ~TextureBuffer();
    virtual void setSize(const QSize &size);
    virtual QSize size();
    virtual QGLTexture2DPtr texture();

protected:
    void genTexture();

    QGLTexture2DPtr m_texture;
    QGLSharedResourceGuard m_guard;
    QSize m_size;
};

class SystemMemoryBuffer : public TextureBuffer
{
public:
    SystemMemoryBuffer();
    virtual void updateTexture();
    virtual QPaintDevice *paintDevice();

private:
    QImage m_image;
    bool m_dirtyTextureSize;
};

#if !defined(QT_OPENGL_ES_2)

// A buffer buffer class!
class PixelBufferObjectBuffer : public TextureBuffer
{
public:
    PixelBufferObjectBuffer();
    virtual void setSize(const QSize &size);
    virtual void updateTexture();
    virtual QPaintDevice *paintDevice();

private:
    QImage m_image;
    QGLBuffer m_pbo;
    bool m_dirtyTextureSize;
};

// A buffer buffer buffer class!!
// DoublePixelBufferObjectBuffer lags one frame behind.
class DoublePixelBufferObjectBuffer : public TextureBuffer
{
public:
    DoublePixelBufferObjectBuffer();
    virtual void setSize(const QSize &size);
    virtual void updateTexture();
    virtual QPaintDevice *paintDevice();

private:
    QImage m_image;
    QGLBuffer m_pbo0;
    QGLBuffer m_pbo1;
    int m_currentPbo;
    bool m_dirtyTextureSize;
};

#endif

class DefaultVideoNode : public VideoNodeInterface
{
    Q_OBJECT
public:
    enum UploadMethod
    {
        SystemMemoryUpload, // glTexImage2D() from system memory.
        PixelBufferObjectUpload, // glTexImage2D() from PBO.
        DoublePixelBufferObjectUpload, // glTexImage2D() from two PBO.
        LockSurfaceUpload // eglLockSurfaceKHR(), requires KHR_lock_surface2 EGL extension.
    };

    DefaultVideoNode(UploadMethod method = SystemMemoryUpload);
    ~DefaultVideoNode();

    virtual void setRect(const QRectF &rect);
    virtual void setOpacity(qreal opacity);
    virtual void setLinearFiltering(bool linearFiltering);

    virtual void setSource(const QUrl &source);
    virtual void setPlaying(bool playing);
    virtual void setCurrentTime(qint64 millis);
    virtual qint64 length();
    virtual QxVideoPlayer::State state() const;
    virtual void setSourceSize(const QSize &size);
    virtual QSize nativeSize() const;
    virtual void setFillMode(QxVideoPlayer::FillMode mode);
    virtual void setMute(bool mute);
    virtual void setVolume(qreal volume);

    virtual void update(uint updateFlags);

private Q_SLOTS:
    void changeCurrentTime(qint64 millis);
    void changeState(Phonon::State toState, Phonon::State fromState);

private:
    enum UpdateFlag
    {
        UpdateTexture = 0x01,
        UpdateGeometry = 0x02
    };

    void setSourceToNativeSize();
    void updateTexture();
    void updateGeometry();

    TextureMaterial m_material;
    TextureMaterialWithOpacity m_materialO;

    Phonon::MediaObject *m_player;
    Phonon::AudioOutput *m_audioOut;
    Phonon::VideoWidget *m_videoOut;
    AbstractBuffer *m_buffer;
};

#endif
