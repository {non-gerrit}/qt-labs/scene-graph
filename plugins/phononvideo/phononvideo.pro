TARGET  = PhononVideo
TEMPLATE = lib
CONFIG += qt plugin
DEPENDPATH += .
INCLUDEPATH += .
DESTDIR=../../imports/Qt/labs/PhononVideo

include(../../src/scenegraph_include.pri)

QT += declarative phonon opengl

HEADERS += \
        default_videonode.h \
        phononvideoplugin.h \
        qxvideoplayer_p.h \

SOURCES += \
        default_videonode.cpp \
        phononvideoplugin.cpp \
        qxvideoplayer.cpp \

macx: CONFIG -= app_bundle

!contains(QT_CONFIG, egl):DEFINES += QT_NO_EGL
