/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXVIDEOPLAYER_P_H
#define QXVIDEOPLAYER_P_H

#include "qxitem.h"

class VideoNodeInterface;

class QxVideoPlayer : public QxItem
{
    Q_OBJECT
    Q_PROPERTY(QUrl source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(bool playing READ isPlaying WRITE setPlaying NOTIFY playingChanged)
    Q_PROPERTY(int time READ currentTime WRITE setCurrentTime NOTIFY currentTimeChanged)
    Q_PROPERTY(int length READ length NOTIFY lengthChanged)
    Q_PROPERTY(State state READ state NOTIFY stateChanged)
    Q_PROPERTY(int sourceWidth READ sourceWidth WRITE setSourceWidth NOTIFY sourceWidthChanged)
    Q_PROPERTY(int sourceHeight READ sourceHeight WRITE setSourceHeight NOTIFY sourceHeightChanged)
    Q_PROPERTY(int nativeWidth READ nativeWidth NOTIFY nativeWidthChanged)
    Q_PROPERTY(int nativeHeight READ nativeHeight NOTIFY nativeHeightChanged)
    Q_PROPERTY(FillMode fillMode READ fillMode WRITE setFillMode NOTIFY fillModeChanged)
    Q_PROPERTY(bool mute READ isMute WRITE setMute NOTIFY muteChanged)
    Q_PROPERTY(qreal volume READ volume WRITE setVolume NOTIFY volumeChanged)
    Q_ENUMS(State)
    Q_ENUMS(FillMode)
public:
    enum State // = Phonon::State
    {
        Loading,
        Stopped,
        Playing,
        Buffering,
        Paused,
        Error
    };

    enum FillMode
    {
        Stretch,
        PreserveAspectFit,
        PreserveAspectCrop
    };

    QxVideoPlayer(QxItem *parent = 0);
    ~QxVideoPlayer();

    void setSource(const QUrl &url);
    QUrl source() const;

    void setPlaying(bool playing);
    bool isPlaying() const;

    void setCurrentTime(int time);
    int currentTime() const;

    int length() const;

    State state() const;

    void setSourceWidth(int width);
    void setSourceHeight(int height);
    int sourceWidth() const;
    int sourceHeight() const;

    int nativeWidth() const;
    int nativeHeight() const;

    void setFillMode(FillMode mode);
    FillMode fillMode() const;

    void setMute(bool mute);
    bool isMute() const;

    void setVolume(qreal volume);
    qreal volume() const;

Q_SIGNALS:
    void sourceChanged();
    void playingChanged();
    void currentTimeChanged();
    void lengthChanged();
    void stateChanged();
    void sourceWidthChanged();
    void sourceHeightChanged();
    void nativeWidthChanged();
    void nativeHeightChanged();
    void fillModeChanged();
    void muteChanged();
    void volumeChanged();

protected:
    void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry);
    void renderOpacityChanged(qreal newOpacity, qreal oldOpacity);
    void smoothChange(bool newSmooth, bool oldSmooth);

private:
    VideoNodeInterface *m_node;
};

#endif
