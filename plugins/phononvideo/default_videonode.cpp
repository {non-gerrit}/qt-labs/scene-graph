/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "default_videonode.h"

#include "utilities.h"

#include <phonon/mediaobject.h>
#include <phonon/audiooutput.h>
#include <phonon/videowidget.h>

//----------------------------------------------------------------------------//
//                               TextureBuffer                                //
//----------------------------------------------------------------------------//

TextureBuffer::~TextureBuffer()
{
    const QGLContext *ctx = m_guard.context();
    if (ctx) {
        QGLShareContextScope scope(ctx);
        GLuint id = m_guard.id();
        glDeleteTextures(1, &id);
    }
}

void TextureBuffer::setSize(const QSize &size)
{
    m_size = size;
}

QSize TextureBuffer::size()
{
    return m_size;
}

QGLTexture2DPtr TextureBuffer::texture()
{
    if (m_texture.isNull())
        genTexture();
    return m_texture;
}

void TextureBuffer::genTexture()
{
    const QGLContext *ctx = QGLContext::currentContext();
    GLuint id;
    glGenTextures(1, &id);
    m_guard.setContext(ctx);
    m_guard.setId(id);
    m_texture = QGLTexture2DPtr(QGLTexture2D::fromTextureId(id, QSize(1, 1)));
    m_texture->setBindOptions(QGLContext::InternalBindOption);
    m_texture->setHorizontalWrap(QGL::ClampToEdge);
    m_texture->setVerticalWrap(QGL::ClampToEdge);
}

//----------------------------------------------------------------------------//
//                             SystemMemoryBuffer                             //
//----------------------------------------------------------------------------//

SystemMemoryBuffer::SystemMemoryBuffer()
    : m_dirtyTextureSize(true)
{
}

void SystemMemoryBuffer::updateTexture()
{
    if (m_texture.isNull())
        genTexture();
    m_texture->bind();
    if (m_image.isNull()) {
        QRgb black = 0;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1, 1, 0, GL_BGRA, GL_UNSIGNED_BYTE, &black);
        m_texture->setSize(QSize(1, 1));
    } else if (m_dirtyTextureSize) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_image.width(), m_image.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, m_image.constBits());
        m_texture->setSize(m_size);
        m_dirtyTextureSize = false;
    } else {
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_image.width(), m_image.height(), GL_BGRA, GL_UNSIGNED_BYTE, m_image.constBits());
    }
}

QPaintDevice *SystemMemoryBuffer::paintDevice()
{
    if (m_image.size() != m_size) {
        m_image = QImage(m_size, QImage::Format_RGB32);
        m_dirtyTextureSize = true;
    }
    return &m_image;
}

#if !defined(QT_OPENGL_ES_2)
//----------------------------------------------------------------------------//
//                          PixelBufferObjectBuffer                           //
//----------------------------------------------------------------------------//

PixelBufferObjectBuffer::PixelBufferObjectBuffer()
    : m_pbo(QGLBuffer::PixelUnpackBuffer), m_dirtyTextureSize(true)
{
}

void PixelBufferObjectBuffer::setSize(const QSize &size)
{
    if (size == m_size)
        return;
    TextureBuffer::setSize(size);
    m_dirtyTextureSize = true;
}

void PixelBufferObjectBuffer::updateTexture()
{
    m_pbo.unmap();
    m_image = QImage();
    if (m_texture.isNull())
        genTexture();
    glBindTexture(GL_TEXTURE_2D, m_texture->textureId());
    if (m_dirtyTextureSize) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_size.width(), m_size.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, 0);
        m_texture->setSize(m_size);
        m_dirtyTextureSize = false;
    } else {
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_size.width(), m_size.height(), GL_BGRA, GL_UNSIGNED_BYTE, 0);
    }
    m_pbo.release();
}

QPaintDevice *PixelBufferObjectBuffer::paintDevice()
{
    if (!m_pbo.isCreated())
        m_pbo.create();
    m_pbo.bind();
    m_pbo.allocate(m_size.width() * m_size.height() * sizeof(QRgb)); // Avoid stalling.
    m_image = QImage((uchar *)m_pbo.map(QGLBuffer::WriteOnly), m_size.width(), m_size.height(), QImage::Format_RGB32);
    return &m_image;
}

//----------------------------------------------------------------------------//
//                       DoublePixelBufferObjectBuffer                        //
//----------------------------------------------------------------------------//

DoublePixelBufferObjectBuffer::DoublePixelBufferObjectBuffer()
    : m_pbo0(QGLBuffer::PixelUnpackBuffer)
    , m_pbo1(QGLBuffer::PixelUnpackBuffer)
    , m_currentPbo(0)
    , m_dirtyTextureSize(true)
{
}

void DoublePixelBufferObjectBuffer::setSize(const QSize &size)
{
    if (size == m_size)
        return;
    TextureBuffer::setSize(size);
    m_dirtyTextureSize = true;
}

void DoublePixelBufferObjectBuffer::updateTexture()
{
    QGLBuffer *current = m_currentPbo == 0 ? &m_pbo0 : &m_pbo1;
    current->unmap();
    current->release();
    m_currentPbo ^= 1;
    m_image = QImage();
}

QPaintDevice *DoublePixelBufferObjectBuffer::paintDevice()
{
    if (m_texture.isNull())
        genTexture();
    if (!m_pbo0.isCreated()) {
        m_pbo0.create();
        m_pbo1.create();
    }
    if (m_dirtyTextureSize) {
        m_pbo0.bind();
        m_pbo0.allocate(m_size.width() * m_size.height() * sizeof(QRgb));
        m_pbo1.bind();
        m_pbo1.allocate(m_size.width() * m_size.height() * sizeof(QRgb));
        qMemSet(m_pbo1.map(QGLBuffer::WriteOnly), 0, m_pbo1.size());
        m_pbo1.unmap();
        m_currentPbo = 0;
    }

    // Start uploading previous frame.
    QGLBuffer *prev = m_currentPbo == 0 ? &m_pbo1 : &m_pbo0;
    QGLBuffer *current = m_currentPbo == 0 ? &m_pbo0 : &m_pbo1;
    prev->bind();

    glBindTexture(GL_TEXTURE_2D, m_texture->textureId());
    if (m_dirtyTextureSize) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_size.width(), m_size.height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, 0);
        m_texture->setSize(m_size);
        m_dirtyTextureSize = false;
    } else {
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_size.width(), m_size.height(), GL_BGRA, GL_UNSIGNED_BYTE, 0);
    }

    current->bind();
    m_image = QImage((uchar *)current->map(QGLBuffer::WriteOnly), m_size.width(), m_size.height(), QImage::Format_RGB32);
    return &m_image;
}
#endif

//----------------------------------------------------------------------------//
//                              DefaultVideoNode                              //
//----------------------------------------------------------------------------//

DefaultVideoNode::DefaultVideoNode(UploadMethod method)
{
    m_player = new Phonon::MediaObject;
    m_audioOut = new Phonon::AudioOutput(Phonon::VideoCategory);
    m_videoOut = new Phonon::VideoWidget;
    Phonon::createPath(m_player, m_audioOut);
    Phonon::createPath(m_player, m_videoOut);
    m_videoOut->setAttribute(Qt::WA_DontShowOnScreen);
    //m_videoOut->show();
    m_videoOut->move(0, 0);
    m_videoOut->resize(320, 240); // Set to some random size for now.
    m_player->setTickInterval(1);
    m_videoOut->setScaleMode(Phonon::VideoWidget::FitInView);
    m_videoOut->setAspectRatio(Phonon::VideoWidget::AspectRatioWidget);

    connect(m_player, SIGNAL(tick(qint64)), this, SLOT(changeCurrentTime(qint64)));
    connect(m_player, SIGNAL(totalTimeChanged(qint64)), this, SIGNAL(lengthChanged()));
    connect(m_player, SIGNAL(stateChanged(Phonon::State, Phonon::State)), this, SLOT(changeState(Phonon::State, Phonon::State)));

    switch (method) {
    case SystemMemoryUpload:
        m_buffer = new SystemMemoryBuffer;
        break;
#if !defined(QT_OPENGL_ES_2)
    case PixelBufferObjectUpload:
        m_buffer = new PixelBufferObjectBuffer;
        break;
    case DoublePixelBufferObjectUpload:
        m_buffer = new DoublePixelBufferObjectBuffer;
        break;
#endif
    default:
        Q_ASSERT(!"Upload method not implemented.");
        break;
    }
    m_buffer->setSize(m_videoOut->size());

    setMaterial(&m_material);
    setGeometry(Utilities::createTexturedRectGeometry(QRectF(0, 0, 1, 1), QSize(1, 1), QRectF(0, 0, 1, 1)));

    setOpacity(m_opacity);
    setVolume(m_volume);
    setFillMode(m_fill_mode);
    setLinearFiltering(m_linear_filtering);
    setPlaying(m_playing);
    setMute(m_mute);
}

DefaultVideoNode::~DefaultVideoNode()
{
    delete m_videoOut;
    delete m_audioOut;
    delete m_player;
    delete m_buffer;
}

void DefaultVideoNode::setRect(const QRectF &rect)
{
    m_rect = rect;
    setBoundingRect(rect);
    setUpdateFlag(UpdateGeometry);
}

void DefaultVideoNode::setOpacity(qreal opacity)
{
    m_materialO.setOpacity(opacity);
    m_opacity = opacity;
    setMaterial(opacity == 1 ? &m_material : &m_materialO); // Indicate that the material state has changed.
}

void DefaultVideoNode::setLinearFiltering(bool linearFiltering)
{
    m_material.setLinearFiltering(linearFiltering);
    m_materialO.setLinearFiltering(linearFiltering);
    m_linear_filtering = linearFiltering;
    setMaterial(m_opacity == 1 ? &m_material : &m_materialO); // Indicate that the material state has changed.
}

void DefaultVideoNode::setSource(const QUrl &source)
{
    m_source = source;
    m_current_time = 0;
    m_player->setCurrentSource(source);
    if (m_playing)
        m_player->play();
    emit nativeSizeChanged();
    emit stateChanged();
    emit lengthChanged();
    emit currentTimeChanged();

    setUpdateFlag(UpdateTexture | UpdateGeometry);
}

void DefaultVideoNode::setPlaying(bool playing)
{
    if ((m_playing = playing))
        m_player->play();
    else
        m_player->pause();
}

void DefaultVideoNode::setCurrentTime(qint64 millis)
{
    m_current_time = millis;
    m_player->seek(millis);
    if (m_playing)
        m_player->play();
    setUpdateFlag(UpdateTexture);
    emit currentTimeChanged();
}

qint64 DefaultVideoNode::length()
{
    return m_player->totalTime();
}

QxVideoPlayer::State DefaultVideoNode::state() const
{
    return QxVideoPlayer::State(m_player->state());
}

void DefaultVideoNode::setSourceSize(const QSize &size)
{
    m_source_size = size;
    if (size.isEmpty())
        setSourceToNativeSize();
    else
        setUpdateFlag(UpdateTexture);
}

QSize DefaultVideoNode::nativeSize() const
{
    return m_videoOut->sizeHint();
}

void DefaultVideoNode::setFillMode(QxVideoPlayer::FillMode mode)
{
    m_fill_mode = mode;
    setUpdateFlag(UpdateGeometry);
}

void DefaultVideoNode::setMute(bool mute)
{
    m_mute = mute;
    m_audioOut->setMuted(mute);
}

void DefaultVideoNode::setVolume(qreal volume)
{
    m_volume = volume;
    m_audioOut->setVolume(volume);
}

void DefaultVideoNode::changeCurrentTime(qint64 millis)
{
    m_current_time = millis;
    setUpdateFlag(UpdateTexture);
    emit currentTimeChanged();
}

void DefaultVideoNode::changeState(Phonon::State toState, Phonon::State fromState)
{
    if (fromState == Phonon::LoadingState && toState != Phonon::ErrorState) {
        if (m_source_size.isEmpty())
            setSourceToNativeSize();
        emit nativeSizeChanged();
        emit lengthChanged();
        setUpdateFlag(UpdateGeometry);
    }
    emit stateChanged();
}

void DefaultVideoNode::update(uint updateFlags)
{
    if (updateFlags & UpdateTexture)
        updateTexture();
    if (updateFlags & UpdateGeometry)
        updateGeometry();
}

void DefaultVideoNode::updateTexture()
{
    if (m_material.texture().isNull()) {
        m_material.setTexture(m_buffer->texture(), true);
        m_materialO.setTexture(m_buffer->texture(), true);
    }
    if (!m_source_size.isEmpty() && m_buffer->size() != m_source_size) {
        m_buffer->setSize(m_source_size);
        m_videoOut->resize(m_source_size);
    }
    m_videoOut->render(m_buffer->paintDevice());
    m_buffer->updateTexture();
}

void DefaultVideoNode::updateGeometry()
{
    Geometry *g = geometry();
    QRectF src(0, 0, 1, 1);
    QRectF dst = m_rect;
    QSizeF nativeSize = m_videoOut->sizeHint();
    if (!nativeSize.isEmpty()) {
        if (m_fill_mode == QxVideoPlayer::PreserveAspectFit) {
            nativeSize.scale(dst.size(), Qt::KeepAspectRatio);
            qreal dx = qreal(0.5) * (dst.width() - nativeSize.width());
            qreal dy = qreal(0.5) * (dst.height() - nativeSize.height());
            dst.adjust(dx, dy, -dx, -dy);
        } else if (m_fill_mode == QxVideoPlayer::PreserveAspectCrop) {
            nativeSize.scale(dst.size(), Qt::KeepAspectRatioByExpanding);
            qreal dx = qreal(0.5) * (nativeSize.width() - dst.width()) / nativeSize.width();
            qreal dy = qreal(0.5) * (nativeSize.height() - dst.height()) / nativeSize.height();
            src.adjust(dx, dy, -dx, -dy);
        }
    }

    Utilities::setupRectGeometry(g, dst, QSize(1, 1), src);
    markDirty(DirtyGeometry);
}

void DefaultVideoNode::setSourceToNativeSize()
{
    QSize size = m_videoOut->sizeHint();
    if (size.isEmpty())
        size = QSize(320, 240);
    if (size != m_buffer->size()) {
        m_videoOut->resize(size);
        m_buffer->setSize(size);
        setUpdateFlag(UpdateTexture);
    }
}
