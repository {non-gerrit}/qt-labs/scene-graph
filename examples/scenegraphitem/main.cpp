/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui/qapplication.h>
#include <QtGui/qgraphicsview.h>
#include <QtGui/qgraphicsscene.h>
#include <QtGui/qfiledialog.h>
#include <QtDeclarative/qdeclarativeengine.h>
#include <QtOpenGL/qgl.h>

#include "scenegraphitem.h"

class MyGraphicsView : public QGraphicsView
{
public:
    MyGraphicsView(const QUrl &file, QWidget *parent = 0) : QGraphicsView(parent)
    {
        setViewport(new QGLWidget);
        setScene(&scene);
        scene.addItem(&item);
        item.setFlag(QGraphicsItem::ItemClipsToShape, false);
        item.setSource(file);
        QObject::connect(item.engine(), SIGNAL(quit()), QCoreApplication::instance(), SLOT(quit()));
    }

protected:
    QGraphicsScene scene;
    SceneGraphItem item;
};

static QUrl displayFileDialog()
{
    QString fileName = QFileDialog::getOpenFileName(0, "Open QML file", QString(), "QML Files (*.qml)");
    return fileName.isEmpty() ? QUrl() : QUrl::fromLocalFile(QFileInfo(fileName).canonicalFilePath());
}

int main(int argc, char ** argv)
{
    QUrl file;
    for (int i = 1; i < argc; ++i) {
        if (*argv[i] != '-' && QFileInfo(argv[i]).exists())
            file = QUrl::fromLocalFile(argv[i]);
    }

    QApplication app(argc, argv);

    if (file.isEmpty())
        file = displayFileDialog();

    if (file.isEmpty())
        return 0;

    MyGraphicsView view(file);
    view.show();
    view.raise();

    return app.exec();
}
