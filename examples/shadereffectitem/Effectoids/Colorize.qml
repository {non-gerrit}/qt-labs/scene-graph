/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0

ShaderEffectItem
{
    fragmentShader:
            "varying highp vec2 qt_TexCoord0;                                       \n" +
            "uniform lowp sampler2D source;                                         \n" +
            "uniform lowp vec4 color;                                               \n" +
            "uniform lowp float intensity;                                          \n" +
            "uniform lowp float qt_Opacity;                                         \n" +
            "void main() {                                                          \n" +
            "    lowp vec4 pix = texture2D(source, qt_TexCoord0);                   \n" +
            "    lowp float gray = dot(pix.xyz, vec3(0.5, 0.5, 0.5));               \n" +
            "    gl_FragColor = qt_Opacity * mix(pix, vec4(color.xyz * gray, pix.w), intensity);    \n" +
            "}"

    vertexShader:
            "attribute highp vec4 qt_Vertex;                                        \n" +
            "attribute highp vec2 qt_MultiTexCoord0;                                \n" +
            "uniform highp mat4 qt_ModelViewProjectionMatrix;                       \n" +
            "varying highp vec2 qt_TexCoord0;                                       \n" +
            "void main() {                                                          \n" +
            "    qt_TexCoord0 = qt_MultiTexCoord0;                                  \n" +
            "    gl_Position = qt_ModelViewProjectionMatrix * qt_Vertex;            \n" +
            "}"

    property color color;
    property real intensity;
    active: intensity > 0
    property variant source
}
