/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0
import "Effectoids"


Item {
    width: 360
    height: 540

    Colorize {
        anchors.fill: effectRoot
        source: effectRoot
        id: colorizeEffect
        color: "#c0c0ff"
        intensity: 0
        blending: false
    }

    Item {
        width: 360
        height: 540
        id: effectRoot

        Image {
            anchors.fill: parent
            source: "bg.png"
            smooth: true
        }

        MovingBox {}
    }

    Rectangle {
        id: popup
        anchors.centerIn: colorizeEffect
        gradient: Gradient {
            GradientStop { position: 0; color: "white" }
            GradientStop { position: 1; color: "#ffa080" }
        }
        width: 250
        height: 100
        radius: 40
        border.color: "black"
        border.width: 2
        smooth: true
        scale: 0
        opacity: 0
        rotation: 90
        Text {
            anchors.centerIn: parent
            text: "SMS!!!!"
            font.pixelSize: 60
        }
    }

    property real transitionTime: 800;

    SequentialAnimation {
        loops: Animation.Infinite
        ParallelAnimation { // fade in
            NumberAnimation { target: popup; property: "opacity"; to: 0.7; duration: transitionTime }
            NumberAnimation { target: popup; property: "scale"; to: 1; duration: transitionTime * 3; easing.type: Easing.OutElastic}
            NumberAnimation { target: colorizeEffect; property: "intensity"; to: 1; duration: transitionTime }
        }
        PauseAnimation { duration: 2000 }
        ParallelAnimation { // fade out
            NumberAnimation { target: popup; property: "opacity"; to: 0; duration: transitionTime / 5 }
            NumberAnimation { target: popup; property: "scale"; to: 0; duration: transitionTime / 5; easing.type: Easing.InOutCubic }
            NumberAnimation { target: colorizeEffect; property: "intensity"; to: 0; duration: transitionTime / 5 }
        }
        running: true
    }

    Rectangle { x: 360; width: 1000; height: 1000; color: "black" }
    Rectangle { y: 540; width: 1000; height: 1000; color: "black" }

}
