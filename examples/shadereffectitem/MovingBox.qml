/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0

Item {
    id: root
    anchors.fill: parent

    Rectangle {
        width: 100
        height: 100
        color: "steelblue"
        radius: 15

        x: 100
        y: 50

        Rectangle {
            anchors.verticalCenter: parent.verticalCenter;
            height: 3
            anchors.left: parent.left
            anchors.right: parent.right;
            anchors.margins: 10
            color: "black"
        }
        Rectangle {
            anchors.horizontalCenter: parent.horizontalCenter;
            width: 3
            anchors.top: parent.top
            anchors.bottom: parent.bottom;
            anchors.margins: 10
            color: "black"
        }
        Rectangle {
            anchors.verticalCenter: parent.verticalCenter;
            height: 1
            anchors.left: parent.left
            anchors.right: parent.right;
            anchors.margins: 11
            color: "white"
        }

        Rectangle {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.margins: 11
            width: 1
            color: "white";
        }

        SequentialAnimation on x {
            NumberAnimation { to: root.width - 100; duration: 15000 }
            NumberAnimation { to: 0; duration: 15000 }
            loops: Animation.Infinite
        }
        SequentialAnimation on y {
            NumberAnimation { to: root.height - 100; duration: 20070 }
            NumberAnimation { to: 0; duration: 20070 }
            loops: Animation.Infinite
        }
        NumberAnimation on rotation {
            loops: Animation.Infinite
            duration: 60000
            from: 0
            to: -360
        }
    }
}
