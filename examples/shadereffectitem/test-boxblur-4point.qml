/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0
import "Effectoids"


Item {
    width: 360
    height: 540

    Box4PointBlur {
        source: ShaderEffectSource {
            sourceItem: box
            filtering: ShaderEffectSource.Linear
        }
        anchors.fill: box
        xOffset: knob.progress / width;
        yOffset: knob.progress / height;
        smooth: true
    }

    MovingBox {
        id: box
    }

    Rectangle {
        id: slider
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: 32
        color: "black"
        opacity: 0.5
    }

    Item {
        id: knob
        x: 0.5 * (slider.width - knob.width)
        anchors.top: slider.top
        anchors.bottom: slider.bottom
        width: height * 1.5
        Rectangle {
            anchors.fill: parent
            anchors.margins: 2
            radius: height / 2
            color: "white"
            border.width: 2
            border.color: "black"
        }
        property real progress: x / (slider.width - knob.width)
        MouseArea {
            anchors.fill: parent
            drag.target: parent
            drag.axis: Drag.XAxis
            drag.minimumX: 0
            drag.maximumX: slider.width - knob.width
        }
    }
}
