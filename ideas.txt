- Set the rectangle geometry to [0,1]x[0,1] and pass a vec4 attribute
  with top, left, bottom and right to the vertex shader.

- Look into instancing.

- Being able to sort materials of the same type based on
  varying properties might be usefule. For instance, if 
  three texturematerials come in sequence and two of 
  them source the same texture, then we want those to sort
  together. This can be acheived by adding some virtual
  compareTo function to Material and/or by passing the old
  material to the updateEffect() function so that the effect
  can decide to update only some states.

- We want to investigate different ways of transferring vertex
  data to the screen. Current options:
  * Use the same pointer and memcopy into it seems to be favorable
    on SGX.
  * Use of a  VBO with streaming needs to be investigated

- Rather than having Geometry own the data, it could contain
  a float* and a ushort* which could be put directly into the
  Node as POD blocks. This would mean that memory becomes better
  packed and would potentially lead to fewer cachemisses. It also
  means two less allocations pr nodes for the nodes that have 
  predictable vertex size, such as ImageNode and RectNode (non-round)

- Use scissoring instead of stencil clip when possible. Either we need
  a special node type for scissoring, a special mode in the existing
  clip node, or some code to detect whether a clip is rectangular.

- If there are eight or fewer clip nodes, each clip can be rendered
  into its own stencil bit. Changing the clip is then just a matter of
  setting the right stencil test mask.

