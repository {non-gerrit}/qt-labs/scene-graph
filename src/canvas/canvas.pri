INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/qxgraphicsview.h \
    $$PWD/qxgraphicsview_p.h \
    $$PWD/qxclipnode_p.h \
    $$PWD/qvsyncanimationdriver_p.h

SOURCES += \
    $$PWD/qxgraphicsview.cpp \
    $$PWD/qxclipnode.cpp \
    $$PWD/qvsyncanimationdriver.cpp
