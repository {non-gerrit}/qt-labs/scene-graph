TEMPLATE = lib
TARGET = QtSceneGraph
DEPENDPATH += .
INCLUDEPATH += .
DESTDIR=../lib
DLLDESTDIR=../bin

macx: CONFIG -= app_bundle

target.path = $$[QT_INSTALL_LIBS]
INSTALLS += target

symbian {
    contains(QT_EDITION, OpenSource) {
        TARGET.CAPABILITY = LocalServices NetworkServices ReadUserData UserEnvironment WriteUserData
    } else {
        TARGET.CAPABILITY = All -Tcb
    }

    TARGET.EPOCALLOWDLLDATA = 1

    scenegraphlibrary.sources = $$QMAKE_LIBDIR_QT/QtScenegraph.dll
    scenegraphlibrary.path = c:/sys/bin
    DEPLOYMENT += scenegraphlibrary

    MOC_DIR = tmp/moc
    OBJECTS_DIR = tmp/obj
} else {
    MOC_DIR = .moc
    OBJECTS_DIR = .obj
}

QT += opengl declarative script network

include(canvas/canvas.pri)
include(graphicsitems/graphicsitems.pri)
include(scenegraph/scenegraph.pri)
include(adaptationlayers/adaptationlayers.pri)
include(effects/effects.pri)
include(scenegraphitem/scenegraphitem.pri)

DEFINES += QML_RUNTIME_TESTING
DEFINES += QT_BUILD_SCENEGRAPH_LIBRARY

HEADERS += \
    qmlscene_global.h

SOURCES += \
    qmlscene_global.cpp \
    scenegraph/coreapi/qsgcontext.cpp
