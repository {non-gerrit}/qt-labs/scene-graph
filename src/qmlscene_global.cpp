/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qmlscene_global.h"

#include <QtDeclarative/qdeclarativecontext.h>
#include <QtDeclarative/qdeclarativeengine.h>


#include <canvas/qxgraphicsview.h>
#include <graphicsitems/qxitem.h>
#include <graphicsitems/qxitem_p.h>
#include <graphicsitems/qxanchors_p.h>
#include <graphicsitems/qxevents_p.h>
#include <graphicsitems/qxmousearea_p.h>
#include <graphicsitems/qxrectangle_p.h>
#include <graphicsitems/qximage_p.h>
#include <graphicsitems/qxborderimage_p.h>
#include <graphicsitems/qxtext_p.h>
#include <graphicsitems/qxflickable_p.h>
#include <graphicsitems/qxflickable_p_p.h>
#include <graphicsitems/qxlistview_p.h>
#include <graphicsitems/qxgridview_p.h>
#include <graphicsitems/qxpathview_p.h>
#include <graphicsitems/qxpositioners_p.h>
#include <graphicsitems/qxrepeater_p.h>
#include <graphicsitems/qxloader_p.h>
#include <graphicsitems/qxvisualitemmodel_p.h>
#include <graphicsitems/qxstateoperations_p.h>
#include <graphicsitems/qxanchoranimation_p.h>
#include <graphicsitems/qxparentanimation_p.h>
#include <graphicsitems/qxfocusscope_p.h>
#include <graphicsitems/qxflipable_p.h>
#include <graphicsitems/qxtextedit_p.h>
#include <graphicsitems/qxtextinput_p.h>

#include <effects/shadereffectitem.h>
#include "node.h"
#include "utilities.h"


static QDeclarativePrivate::AutoParentResult QxItem_autoParent(QObject *obj, QObject *parent)
{
    QxItem* gobj = qobject_cast<QxItem*>(obj);
    if (!gobj)
        return QDeclarativePrivate::IncompatibleObject;

    QxItem* gparent = qobject_cast<QxItem*>(parent);
    if (!gparent)
        return QDeclarativePrivate::IncompatibleParent;

    gobj->setParentItem(gparent);
    return QDeclarativePrivate::Parented;
}

void qt_scenegraph_register_types()
{
    static bool initialized = false;
    if (initialized)
        return;
    initialized = true;

    // ### Remove before final integration...
    QByteArray mode = qgetenv("QMLSCENE_IMPORT_NAME");
    QByteArray name = "QtQuick";
    int majorVersion = 2;
    int minorVersion = 0;
    if (mode == "quick1") {
        majorVersion = 1;
    } else if (mode == "qt") {
        name = "Qt";
        majorVersion = 4;
        minorVersion = 7;
    }

    QDeclarativePrivate::RegisterAutoParent autoparent = { 0, &QxItem_autoParent };
    QDeclarativePrivate::qmlregister(QDeclarativePrivate::AutoParentRegistration, &autoparent);


    Q_ASSERT(QLatin1String(name) == QLatin1String("QtQuick"));

    qmlRegisterType<QxBorderImage>(name, majorVersion, minorVersion,"BorderImage");
    qmlRegisterType<QxColumn>(name, majorVersion, minorVersion,"Column");
    qmlRegisterType<QxDrag>(name, majorVersion, minorVersion,"Drag");
    qmlRegisterType<QxFlickable>(name, majorVersion, minorVersion,"Flickable");
    qmlRegisterType<QxFlipable>(name, majorVersion, minorVersion,"Flipable");
    qmlRegisterType<QxFlow>(name, majorVersion, minorVersion,"Flow");
    //    qmlRegisterType<QDeclarativeFocusPanel>(name, majorVersion, minorVersion,"FocusPanel");
    qmlRegisterType<QxFocusScope>(name, majorVersion, minorVersion,"FocusScope");
    qmlRegisterType<QxGradient>(name, majorVersion, minorVersion,"Gradient");
    qmlRegisterType<QxGradientStop>(name, majorVersion, minorVersion,"GradientStop");
    qmlRegisterType<QxGrid>(name, majorVersion, minorVersion,"Grid");
    qmlRegisterType<QxGridView>(name, majorVersion, minorVersion,"GridView");
    qmlRegisterType<QxImage>(name, majorVersion, minorVersion,"Image");
    qmlRegisterType<QxItem>(name, majorVersion, minorVersion,"Item");
    qmlRegisterType<QxListView>(name, majorVersion, minorVersion,"ListView");
    qmlRegisterType<QxLoader>(name, majorVersion, minorVersion,"Loader");
    qmlRegisterType<QxMouseArea>(name, majorVersion, minorVersion,"MouseArea");
    qmlRegisterType<QxPathView>(name, majorVersion, minorVersion,"PathView");
    qmlRegisterType<QxRectangle>(name, majorVersion, minorVersion,"Rectangle");
    qmlRegisterType<QxRepeater>(name, majorVersion, minorVersion,"Repeater");
    qmlRegisterType<QxRow>(name, majorVersion, minorVersion,"Row");
    qmlRegisterType<QxText>(name, majorVersion, minorVersion,"Text");
    qmlRegisterType<QxTextEdit>(name, majorVersion, minorVersion,"TextEdit");
    qmlRegisterType<QxTextInput>(name, majorVersion, minorVersion,"TextInput");
    qmlRegisterType<QxViewSection>(name, majorVersion, minorVersion,"ViewSection");
    qmlRegisterType<QxVisualDataModel>(name, majorVersion, minorVersion,"VisualDataModel");
    qmlRegisterType<QxVisualItemModel>(name, majorVersion, minorVersion,"VisualItemModel");

    qmlRegisterType<QxAnchors>();
    qmlRegisterType<QxKeyEvent>();
    qmlRegisterType<QxMouseEvent>();
    qmlRegisterType<QxVisualModel>();
    qmlRegisterType<QxPen>();
    qmlRegisterType<QxFlickableVisibleArea>();

    qmlRegisterUncreatableType<QxKeyNavigationAttached>(name, majorVersion, minorVersion,"KeyNavigation",QxKeyNavigationAttached::tr("KeyNavigation is only available via attached properties"));
    qmlRegisterUncreatableType<QxKeysAttached>(name, majorVersion, minorVersion,"Keys",QxKeysAttached::tr("Keys is only available via attached properties"));

    qmlRegisterType<QxAnchorChanges>(name, majorVersion, minorVersion,"AnchorChanges");
    qmlRegisterType<QxAnchors>();
    qmlRegisterType<QxAnchorSet>();
    qmlRegisterType<QxParentChange>(name, majorVersion, minorVersion,"ParentChange");
    qmlRegisterType<QxAnchorAnimation>(name, majorVersion, minorVersion,"AnchorAnimation");
    qmlRegisterType<QxParentAnimation>(name, majorVersion, minorVersion,"ParentAnimation");

//    qmlRegisterType<QxParticleMotion>();
//    qmlRegisterType<QxParticleMotionGravity>("Qt.particles",1,0,"ParticleMotionGravity");
//    qmlRegisterType<QxParticleMotionLinear>("Qt.particles",1,0,"ParticleMotionLinear");
//    qmlRegisterType<QxParticleMotionWander>("Qt.particles",1,0,"ParticleMotionWander");
//    qmlRegisterType<QxParticleMotionRandomDrift>("Qt.particles",1,0,"ParticleMotionRandomDrift");
//    qmlRegisterType<QxParticleKillZone>("Qt.particles",1,0,"ParticleKillZone");
//    qmlRegisterType<QxParticleMotionAcceleration>("Qt.particles",1,0,"ParticleMotionAcceleration");
//    qmlRegisterType<QxParticleMotionFriction>("Qt.particles",1,0,"ParticleMotionFriction");
//    qmlRegisterType<QxParticleMotionGravityWell>("Qt.particles",1,0,"ParticleMotionGravityWell");
//    qmlRegisterType<QxParticleMotionAntiGravity>("Qt.particles",1,0,"ParticleMotionAntiGravity");
//    qmlRegisterType<QxParticles>("Qt.particles",1,0,"ParticleEmitter");
//    qmlRegisterType<QxParticlesPainter>("Qt.particles",1,0,"ParticlePainter");
//    qmlRegisterType<QxParticleField>("Qt.particles",1,0,"ParticleField");

    qmlRegisterType<ShaderEffectItem>("QtQuick", 2, 0, "ShaderEffectItem");
    qmlRegisterType<ShaderEffectSource>("QtQuick", 2, 0, "ShaderEffectSource");
}
