#ifndef QSGCONTEXT_H
#define QSGCONTEXT_H

#include <QObject>

#include <QGLContext>

#include "node.h"
#include "qmlscene_global.h"

class QSGContextPrivate;
class RectangleNodeInterface;
class TextureNodeInterface;
class GlyphNodeInterface;
class Renderer;
class QSGTextureManager;

class QT_SCENEGRAPH_EXPORT QSGContext : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QSGContext)

public:
    explicit QSGContext(QObject *parent = 0);

    void initialize(QGLContext *context);

    Renderer *renderer() const;

    QSGTextureManager *textureManager() const;

    void setRootNode(RootNode *node);
    RootNode *rootNode() const;

    QGLContext *glContext() const;

    static QSGContext *current; // Evil nasty hack!! Get rid of this!

    bool isReady() const;

    virtual void renderNextFrame();

    virtual RectangleNodeInterface *createRectangleNode();
    virtual TextureNodeInterface *createTextureNode();
    virtual GlyphNodeInterface *createGlyphNode();
    virtual Renderer *createRenderer();
    virtual QSGTextureManager *createTextureManager();

signals:
    void ready();

    void aboutToRenderNextFrame();
};

#endif // QSGCONTEXT_H
