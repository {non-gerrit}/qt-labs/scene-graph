/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QMLRENDERER_H
#define QMLRENDERER_H

#include "renderer.h"

class QMLRenderer : public Renderer
{
    Q_OBJECT
public:
    QMLRenderer();

    void render();

    void nodeChanged(Node *node, Node::DirtyFlags flags);

private:
    void buildLists(Node *node);
    void renderNodes(const QVector <GeometryNode *> &list);

    const ClipNode *m_currentClip;
    AbstractEffect *m_currentMaterial;
    AbstractEffectProgram *m_currentProgram;
    const QMatrix4x4 *m_currentMatrix;
    QMatrix4x4 m_renderOrderMatrix;
    QVector<GeometryNode *> m_opaqueNodes;
    QVector<GeometryNode *> m_transparentNodes;
    QVector<GeometryNode *> m_tempNodes;

    bool m_rebuild_lists;
    bool m_needs_sorting;
    int m_currentRenderOrder;

#ifdef QML_RUNTIME_TESTING
    bool m_render_opaque_nodes;
    bool m_render_alpha_nodes;
#endif
};

#endif // QMLRENDERER_H
