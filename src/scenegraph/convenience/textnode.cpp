/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "textnode.h"
#include "solidrectnode.h"
#include "adaptationlayer.h"

#include "qsgcontext.h"

#include <qmath.h>
#include <qtextdocument.h>
#include <qtextlayout.h>
#include <qabstracttextdocumentlayout.h>
#include <qxmlstream.h>
#include <private/qdeclarativestyledtext_p.h>
#include <private/qfont_p.h>
#include <private/qfontengine_p.h>

/*!
  Creates an empty TextNode
*/
TextNode::TextNode(QSGContext *context)
    : m_opacity(1.0),
      m_usePixmapCache(false), m_richText(false), m_linearFiltering(false), m_layoutDirty(true),
      m_textFormat(Qt::AutoText), m_wrapMode(QTextOption::NoWrap), m_elideMode(Qt::ElideNone),
      m_alignment(Qt::AlignTop | Qt::AlignLeft), m_textStyle(TextNode::NormalTextStyle),
      m_textDocument(0), m_textLayout(0),
      m_idealContentsWidth(0.0), m_widthRestriction(-1.0), m_heightRestriction(-1.0)
    , m_context(context)
{
#if defined(QML_RUNTIME_TESTING)
    description = "text";
#endif
}

TextNode::~TextNode()
{
    delete m_textDocument;
    delete m_textLayout;
}

void TextNode::detectRichText()
{
    m_richText = m_textFormat == Qt::RichText ||
            (m_textFormat == Qt::AutoText && Qt::mightBeRichText(m_text));
}

void TextNode::setUsePixmapCache(bool on)
{
    m_usePixmapCache = on;
    setUpdateFlag(UpdateNodes);
}

void TextNode::setWidthRestriction(qreal widthRestriction)
{
    if (m_elideMode != Qt::ElideNone && widthRestriction < m_idealContentsWidth)
        setUpdateFlag(UpdateText);
    else if (m_wrapMode != QTextOption::NoWrap && widthRestriction < m_idealContentsWidth)
        setUpdateFlag(UpdateLayout);

    m_widthRestriction = widthRestriction;
}

void TextNode::setHeightRestriction(qreal heightRestriction)
{
    // ### Am I supposed to clip to this?
    m_heightRestriction = heightRestriction;
}

void TextNode::setLinearFiltering(bool on)
{
    m_linearFiltering = on;

    if (m_usePixmapCache)
        setUpdateFlag(UpdateNodes);
}

void TextNode::setText(const QString &text)
{
    m_text = text;

    detectRichText();
    setUpdateFlag(UpdateText);
    m_layoutDirty = true;
}

void TextNode::setTextFormat(Qt::TextFormat textFormat)
{
    m_textFormat = textFormat;

    bool wasRichText = m_richText;
    detectRichText();
    if (wasRichText != m_richText) {
        m_layoutDirty = true;
        setUpdateFlag(UpdateLayout);
    }
}

void TextNode::setGeometry(const QRectF &geometry)
{
    if (m_geometry.size() != geometry.size()) {
        bool needsRelayout =
            (m_geometry.width() < m_idealContentsWidth || geometry.width() < m_idealContentsWidth)
         && (m_wrapMode != QTextOption::NoWrap || m_elideMode != Qt::ElideNone);

        int hAlign = m_alignment & Qt::AlignHorizontal_Mask;
        int vAlign = m_alignment & Qt::AlignVertical_Mask;

        bool needsRealignment =
            (m_geometry.height() != geometry.height() && vAlign != Qt::AlignTop)
         || (m_geometry.width() != geometry.width() && hAlign != Qt::AlignLeft);

        if (needsRelayout) {
            m_layoutDirty = true;
            setUpdateFlag(UpdateLayout);
        } else if (needsRealignment) {
            setUpdateFlag(UpdateTransform);
        }
    }

    m_geometry = geometry;
}

void TextNode::setElideMode(Qt::TextElideMode elideMode)
{
    m_elideMode = elideMode;
    if (m_geometry.width() < m_idealContentsWidth && m_wrapMode == QTextOption::NoWrap) {
        m_layoutDirty = true;
        setUpdateFlag(UpdateText);
    }
}

void TextNode::setTextDocument(QTextDocument *textDocument)
{
    if (m_textDocument != 0)
        delete m_textDocument;

    m_textDocument = textDocument;
    if (m_richText) {
        m_layoutDirty = true;
        setUpdateFlag(UpdateText);
    }
}

void TextNode::setColor(const QColor &color)
{
    if (m_usePixmapCache) {
        setUpdateFlag(UpdateNodes);
    } else {
        for (int i=0; i<childCount(); ++i) {
            Node *childNode = childAtIndex(i);
            if (childNode->subType() == GlyphNodeSubType) {
                GlyphNodeInterface *glyphNode = static_cast<GlyphNodeInterface *>(childNode);
                if (glyphNode->color() == m_color)
                    glyphNode->setColor(color);
            } else if (childNode->subType() == SolidRectNodeSubType) {
                SolidRectNode *solidRectNode = static_cast<SolidRectNode *>(childNode);
                if (solidRectNode->color() == m_color)
                    solidRectNode->setColor(color);
            }
        }
    }
    m_color = color;
}

void TextNode::setStyleColor(const QColor &styleColor)
{
    if (m_textStyle != TextNode::NormalTextStyle) {
        if (m_usePixmapCache) {
            setUpdateFlag(UpdateNodes);
        } else {
            for (int i=0; i<childCount(); ++i) {
                Node *childNode = childAtIndex(i);
                if (childNode->subType() == GlyphNodeSubType) {
                    GlyphNodeInterface *glyphNode = static_cast<GlyphNodeInterface *>(childNode);
                    if (glyphNode->color() == m_styleColor)
                        glyphNode->setColor(styleColor);
                } else if (childNode->subType() == SolidRectNodeSubType) {
                    SolidRectNode *solidRectNode = static_cast<SolidRectNode *>(childNode);
                    if (solidRectNode->color() == m_styleColor)
                        solidRectNode->setColor(styleColor);
                }
            }
        }
    }
    m_styleColor = styleColor;
}

void TextNode::setFont(const QFont &font)
{
    m_font = font;

    m_layoutDirty = true;
    setUpdateFlag(UpdateLayout);
}

void TextNode::setWrapMode(QTextOption::WrapMode wrapMode)
{
    m_wrapMode = wrapMode;
    if (m_geometry.width() < m_idealContentsWidth) {
        m_layoutDirty = true;
        setUpdateFlag(UpdateLayout);
    }
}

void TextNode::setAlignment(Qt::Alignment alignment)
{
    m_alignment = alignment;
    setUpdateFlag(UpdateAlignment);
}

void TextNode::setTextStyle(TextStyle textStyle)
{
    m_textStyle = textStyle;
    setUpdateFlag(UpdateNodes);
}

QString TextNode::elidedText() const
{
    QString tempText = m_text;
    tempText.replace(QLatin1Char('\n'), QChar::LineSeparator);
    if (!tempText.contains(QChar::LineSeparator)
            && m_elideMode != Qt::ElideNone
            && widthRestriction() >= 0.0) {
        return QFontMetricsF(m_font).elidedText(tempText, m_elideMode, widthRestriction());
    } else {
        return tempText;
    }
}

void TextNode::updateLayout()
{
    if (!m_layoutDirty || m_text.isEmpty())
        return;

    m_layoutDirty = false;

    if (m_richText) {
        m_textDocument->setDefaultFont(m_font);

        if (widthRestriction() > 0.0)
            m_textDocument->setTextWidth(widthRestriction());

        m_contentsSize = m_textDocument->size();
        m_idealContentsWidth = m_textDocument->idealWidth();
    } else {
        m_textLayout->clearLayout();
        m_textLayout->setCacheEnabled(true);
        m_textLayout->setFont(m_font);

        QTextOption textOption;
        textOption.setWrapMode(m_wrapMode);
        m_textLayout->setTextOption(textOption);

        qreal lineWidth = 0.0;
        if ((m_wrapMode != QTextOption::NoWrap || m_elideMode != Qt::ElideNone)
                && widthRestriction() > 0.0) {
            lineWidth = widthRestriction();
        }

        QFontMetricsF fm(m_font);
        m_idealContentsWidth = fm.boundingRect(m_text).width();

        m_textLayout->beginLayout();
        while (true) {
            QTextLine line = m_textLayout->createLine();
            if (!line.isValid())
                break;

            if (lineWidth > 0.0)
                line.setLineWidth(lineWidth);
        }
        m_textLayout->endLayout();

        // We need to do the calculation of the implicit width in a separate pass after
        // finishing the layout.
        int implicitWidth = 0;
        int implicitHeight = 0;
        for (int i=0; i<m_textLayout->lineCount(); ++i) {
            QTextLine line = m_textLayout->lineAt(i);

            implicitWidth = qMax(implicitWidth, qCeil(line.naturalTextWidth()));
            line.setPosition(QPointF(0.0, implicitHeight));
            implicitHeight += qFloor(line.height());
        }

        m_contentsSize = QSizeF(implicitWidth, implicitHeight);
    }
}

void TextNode::updateText()
{
    if (!m_layoutDirty || m_text.isEmpty())
        return;

    if (m_richText) {
        if (m_textDocument == 0)
            m_textDocument = new QTextDocument();
        m_textDocument->setHtml(m_text);
        m_usePixmapCache = isComplexRichText();
    } else {
        if (m_textLayout == 0)
            m_textLayout = new QTextLayout();

        m_textLayout->clearLayout();
        // ### Small hack to support QxText::StyledText. Should be done a little bit more
        // explicitly.
        if (m_textFormat == 4)
            QDeclarativeStyledText::parse(m_text, *m_textLayout);
        else
            m_textLayout->setText(elidedText());
    }
}

QSizeF TextNode::contentsSize()
{
    updateText();
    updateLayout();

    if (!m_contentsSize.isValid())
        return QSizeF(0.0, QFontMetricsF(m_font).height());
    else
        return m_contentsSize;
}

void TextNode::update(uint updateFlags)
{
    if ((updateFlags & UpdateText) == UpdateText)
        updateText();

    if ((updateFlags & UpdateLayout) == UpdateLayout)
        updateLayout();

    if ((updateFlags & UpdateAlignment) == UpdateAlignment)
        updateAlignment();

    if ((updateFlags & UpdateNodes) == UpdateNodes)
        updateNodes();

    if ((updateFlags & UpdateTransform) == UpdateTransform)
        updateTransform();
}

void TextNode::setOpacity(qreal opacity)
{
    m_opacity = opacity;

    for (int i=0; i<childCount(); ++i) {
        Node *node = childAtIndex(i);
        if (node->subType() == GlyphNodeSubType) {
            GlyphNodeInterface *glyphNode = static_cast<GlyphNodeInterface *>(node);
            glyphNode->setOpacity(opacity);
        } else if (node->subType() == SolidRectNodeSubType) {
            SolidRectNode *solidRectNode = static_cast<SolidRectNode *>(node);
            solidRectNode->setOpacity(opacity);
        } else if (node->subType() == PixmapNodeSubType) {
            TextureNodeInterface *pixmapNode = static_cast<TextureNodeInterface *>(node);
            pixmapNode->setOpacity(opacity);
        }
    }
}

void TextNode::addTextDecorations(const QPointF &position, const QFont &font, const QColor &color,
                                  qreal width)
{
    QFontPrivate *dptrFont = QFontPrivate::get(font);
    QFontEngine *fontEngine = dptrFont->engineForScript(QUnicodeTables::Common);

    qreal lineThickness = fontEngine->lineThickness().toReal();

    QRectF line(position.x(), position.y() - lineThickness / 2.0, width, lineThickness);

    if (font.underline()) {
        int underlinePosition = fontEngine->underlinePosition().ceil().toInt();
        QRectF underline(line);
        underline.translate(0.0, underlinePosition);
        appendChildNode(new SolidRectNode(underline, color, m_opacity));
    }

    qreal ascent = fontEngine->ascent().toReal();
    if (font.overline()) {
        QRectF overline(line);
        overline.translate(0.0, -ascent);
        appendChildNode(new SolidRectNode(overline, color, m_opacity));
    }

    if (font.strikeOut()) {
        QRectF strikeOut(line);
        strikeOut.translate(0.0, ascent / -3.0);
        appendChildNode(new SolidRectNode(strikeOut, color, m_opacity));
    }
}

GlyphNodeInterface *TextNode::addGlyphs(const QPointF &position, const QGlyphs &glyphs, const QColor &color)
{
    GlyphNodeInterface *node = m_context->createGlyphNode();
    node->setGlyphs(position, glyphs);
    node->setColor(color);
    node->setOpacity(m_opacity);

    appendChildNode(node);

    return node;
}

void TextNode::addTextLayout(const QPointF &position, QTextLayout *textLayout, const QColor &color)
{
    QList<QGlyphs> glyphsList(textLayout->glyphs());
    for (int i=0; i<glyphsList.size(); ++i)
        addGlyphs(position, glyphsList.at(i), color);

    QFont font = textLayout->font();
    if (font.strikeOut() || font.underline() || font.overline())
        addTextDecorations(position, font, color, textLayout->boundingRect().width());
}

QRectF TextNode::boundingRect() const
{
    QRectF brect;
    for (int i=0; i<childCount(); ++i) {
        Node *node = childAtIndex(i);
        if (node != 0 && node->type() == Node::GeometryNodeType)
            brect |= static_cast<GeometryNode *>(node)->boundingRect();
    }
    return brect;
}

/*!
  Returns true if \a text contains any HTML tags, attributes or CSS properties which are unrelated
   to text, fonts or text layout. Otherwise the function returns false. If the return value is
  false, \a text is considered to be easily representable in the scenegraph. If it returns true,
  then the text should be prerendered into a pixmap before it's displayed on screen.
*/
bool TextNode::isComplexRichText() const
{
    if (m_textDocument == 0)
        return false;

    static QSet<QString> supportedTags;
    if (supportedTags.isEmpty()) {
        supportedTags.insert(QLatin1String("i"));
        supportedTags.insert(QLatin1String("b"));
        supportedTags.insert(QLatin1String("u"));
        supportedTags.insert(QLatin1String("div"));
        supportedTags.insert(QLatin1String("big"));
        supportedTags.insert(QLatin1String("blockquote"));
        supportedTags.insert(QLatin1String("body"));
        supportedTags.insert(QLatin1String("br"));
        supportedTags.insert(QLatin1String("center"));
        supportedTags.insert(QLatin1String("cite"));
        supportedTags.insert(QLatin1String("code"));
        supportedTags.insert(QLatin1String("tt"));
        supportedTags.insert(QLatin1String("dd"));
        supportedTags.insert(QLatin1String("dfn"));
        supportedTags.insert(QLatin1String("em"));
        supportedTags.insert(QLatin1String("font"));
        supportedTags.insert(QLatin1String("h1"));
        supportedTags.insert(QLatin1String("h2"));
        supportedTags.insert(QLatin1String("h3"));
        supportedTags.insert(QLatin1String("h4"));
        supportedTags.insert(QLatin1String("h5"));
        supportedTags.insert(QLatin1String("h6"));
        supportedTags.insert(QLatin1String("head"));
        supportedTags.insert(QLatin1String("html"));
        supportedTags.insert(QLatin1String("meta"));
        supportedTags.insert(QLatin1String("nobr"));
        supportedTags.insert(QLatin1String("p"));
        supportedTags.insert(QLatin1String("pre"));
        supportedTags.insert(QLatin1String("qt"));
        supportedTags.insert(QLatin1String("s"));
        supportedTags.insert(QLatin1String("samp"));
        supportedTags.insert(QLatin1String("small"));
        supportedTags.insert(QLatin1String("span"));
        supportedTags.insert(QLatin1String("strong"));
        supportedTags.insert(QLatin1String("sub"));
        supportedTags.insert(QLatin1String("sup"));
        supportedTags.insert(QLatin1String("title"));
        supportedTags.insert(QLatin1String("var"));
        supportedTags.insert(QLatin1String("style"));
    }

    static QSet<QCss::Property> supportedCssProperties;
    if (supportedCssProperties.isEmpty()) {
        supportedCssProperties.insert(QCss::Color);
        supportedCssProperties.insert(QCss::Float);
        supportedCssProperties.insert(QCss::Font);
        supportedCssProperties.insert(QCss::FontFamily);
        supportedCssProperties.insert(QCss::FontSize);
        supportedCssProperties.insert(QCss::FontStyle);
        supportedCssProperties.insert(QCss::FontWeight);
        supportedCssProperties.insert(QCss::Margin);
        supportedCssProperties.insert(QCss::MarginBottom);
        supportedCssProperties.insert(QCss::MarginLeft);
        supportedCssProperties.insert(QCss::MarginRight);
        supportedCssProperties.insert(QCss::MarginTop);
        supportedCssProperties.insert(QCss::TextDecoration);
        supportedCssProperties.insert(QCss::TextIndent);
        supportedCssProperties.insert(QCss::TextUnderlineStyle);
        supportedCssProperties.insert(QCss::VerticalAlignment);
        supportedCssProperties.insert(QCss::Whitespace);
        supportedCssProperties.insert(QCss::Padding);
        supportedCssProperties.insert(QCss::PaddingLeft);
        supportedCssProperties.insert(QCss::PaddingRight);
        supportedCssProperties.insert(QCss::PaddingTop);
        supportedCssProperties.insert(QCss::PaddingBottom);
        supportedCssProperties.insert(QCss::PageBreakBefore);
        supportedCssProperties.insert(QCss::PageBreakAfter);
        supportedCssProperties.insert(QCss::Width);
        supportedCssProperties.insert(QCss::Height);
        supportedCssProperties.insert(QCss::MinimumWidth);
        supportedCssProperties.insert(QCss::MinimumHeight);
        supportedCssProperties.insert(QCss::MaximumWidth);
        supportedCssProperties.insert(QCss::MaximumHeight);
        supportedCssProperties.insert(QCss::Left);
        supportedCssProperties.insert(QCss::Right);
        supportedCssProperties.insert(QCss::Top);
        supportedCssProperties.insert(QCss::Bottom);
        supportedCssProperties.insert(QCss::Position);
        supportedCssProperties.insert(QCss::TextAlignment);
        supportedCssProperties.insert(QCss::FontVariant);
    }

    QXmlStreamReader reader(m_textDocument->toHtml("utf-8"));
    while (!reader.atEnd()) {
        reader.readNext();

        if (reader.isStartElement()) {
            if (!supportedTags.contains(reader.name().toString().toLower()))
                return true;

            QXmlStreamAttributes attributes = reader.attributes();
            if (attributes.hasAttribute(QLatin1String("bgcolor")))
                return true;
            if (attributes.hasAttribute(QLatin1String("style"))) {
                QCss::StyleSheet styleSheet;
                QCss::Parser(attributes.value(QLatin1String("style")).toString()).parse(&styleSheet);

                QVector<QCss::Declaration> decls;
                for (int i=0; i<styleSheet.pageRules.size(); ++i)
                    decls += styleSheet.pageRules.at(i).declarations;

                QVector<QCss::StyleRule> styleRules =
                        styleSheet.styleRules
                        + styleSheet.idIndex.values().toVector()
                        + styleSheet.nameIndex.values().toVector();
                for (int i=0; i<styleSheet.mediaRules.size(); ++i)
                    styleRules += styleSheet.mediaRules.at(i).styleRules;

                for (int i=0; i<styleRules.size(); ++i)
                    decls += styleRules.at(i).declarations;

                for (int i=0; i<decls.size(); ++i) {
                    if (!supportedCssProperties.contains(decls.at(i).d->propertyId))
                        return true;
                }

            }
        }
    }

    return reader.hasError();
}

QPixmap TextNode::wrappedTextImage(const QColor &color) const
{
    //do layout
    QSize size = m_contentsSize.toSize();

    QPixmap pixmap(size);
    if (!size.isEmpty()) {
        pixmap.fill(Qt::transparent);

        {
            QPainter p(&pixmap);
            p.setPen(color);
            p.setFont(m_font);
            m_textLayout->draw(&p, QPointF(0, 0));
        }
    }

    return pixmap;
}

QPixmap TextNode::richTextImage(const QColor &color, bool suppressColors) const
{
    QSize size = m_contentsSize.toSize();

    QPixmap pixmap(size);
    if (!size.isEmpty()) {
        pixmap.fill(Qt::transparent);

        {
            QPainter p(&pixmap);
            QAbstractTextDocumentLayout::PaintContext context;
            QTextOption oldOption = m_textDocument->defaultTextOption();

            context.palette.setColor(QPalette::Text, color);
            if (suppressColors) {
                QTextOption option = m_textDocument->defaultTextOption();
                option.setFlags(QTextOption::SuppressColors);

                m_textDocument->setDefaultTextOption(option);
            }

            m_textDocument->documentLayout()->draw(&p, context);
            m_textDocument->setDefaultTextOption(oldOption);
        }
    }

    return pixmap;
}

QPixmap TextNode::generatedPixmap() const
{
    if (m_text.isEmpty())
        return QPixmap();

    QPixmap mainPixmap;
    QPixmap styledPixmap;

    if (m_richText) {
        mainPixmap = richTextImage(m_color, false);
        if (m_textStyle != NormalTextStyle)
            styledPixmap = richTextImage(m_styleColor, true);
    } else {
        mainPixmap = wrappedTextImage(m_color);
        if (m_textStyle != NormalTextStyle)
            styledPixmap = wrappedTextImage(m_styleColor);
    }

    if (mainPixmap.isNull())
        return QPixmap();

    switch (m_textStyle) {
    case SunkenTextStyle:
    case RaisedTextStyle:
    case OutlineTextStyle: {
            Q_ASSERT(!styledPixmap.isNull());
            QPixmap compositePixmap(styledPixmap.width() + 2, styledPixmap.height() + 2);
            compositePixmap.fill(Qt::transparent);

            {
                QPainter p(&compositePixmap);

                switch (m_textStyle) {
                case OutlineTextStyle:
                    p.drawPixmap(QPointF(-1, 0), styledPixmap);
                    p.drawPixmap(QPointF(1, 0), styledPixmap);
                    p.drawPixmap(QPointF(0, -1), styledPixmap);
                    p.drawPixmap(QPointF(0, 1), styledPixmap);
                    break;
                case SunkenTextStyle:
                    p.drawPixmap(QPointF(0, -1), styledPixmap);
                    break;
                case RaisedTextStyle:
                    p.drawPixmap(QPointF(0, 1), styledPixmap);
                    break;
                case NormalTextStyle:
                    break;
                };
                p.drawPixmap(QPoint(0, 0), mainPixmap);
            }

            return compositePixmap;
        }

    default:
        return mainPixmap;
    };
}

void TextNode::addTextBlock(const QPointF &position, const QTextBlock &block,
                            const QColor &overrideColor)
{
    if (!block.isValid())
        return;

    QPointF blockPosition = m_textDocument->documentLayout()->blockBoundingRect(block).topLeft();

    QTextBlock::iterator it = block.begin();
    while (!it.atEnd()) {
        QTextFragment fragment = it.fragment();
        if (!fragment.text().isEmpty()) {
            QTextCharFormat charFormat = fragment.charFormat();
            QColor color = overrideColor.isValid()
                    ? overrideColor
                    : charFormat.foreground().color();

            QFontMetricsF fm(fragment.charFormat().font());
            QPointF ascent(0, fm.ascent());

            QList<QGlyphs> glyphsList = fragment.glyphs();
            for (int i=0; i<glyphsList.size(); ++i) {
                QGlyphs glyphs = glyphsList.at(i);
                GlyphNodeInterface *glyphNode = addGlyphs(position + blockPosition + ascent, glyphs, color);

                QFont font = glyphs.font();
                QPointF baseLine = glyphNode->baseLine();
                qreal width = glyphNode->boundingRect().width();
                addTextDecorations(baseLine, font, color, width);
            }
        }

        ++it;
    }
}

void TextNode::updateAlignment()
{
    if (m_text.isEmpty())
        return;

    if (m_richText) {
        QTextOption textOption = m_textDocument->defaultTextOption();
        textOption.setAlignment(m_alignment);
        m_textDocument->setDefaultTextOption(textOption);
    } else {
        for (int i=0; i<m_textLayout->lineCount(); ++i) {
            QTextLine line = m_textLayout->lineAt(i);
            qreal x = 0.0;
            if (m_alignment & Qt::AlignHCenter) {
                x = (m_contentsSize.width() - line.naturalTextWidth()) / 2;
            } else {
                x = (m_contentsSize.width() - line.naturalTextWidth());
            }
            line.setPosition(QPointF(x, line.y()));
        }
    }
}

void TextNode::deleteContent()
{
    while (childCount() > 0)
        delete childAtIndex(0);
}

void TextNode::updateTransform()
{
    if (m_text.isEmpty())
        return;

    qreal x = 0;
    qreal y = 0;

    int hAlign = int(m_alignment & Qt::AlignHorizontal_Mask);
    int vAlign = int(m_alignment & Qt::AlignVertical_Mask);

    QSizeF size = contentsSize();
    switch (hAlign) {
    case Qt::AlignRight:
        x = m_geometry.width() - size.width();
        break;
    case Qt::AlignHCenter:
        x = m_geometry.width() / 2.0 - size.width() / 2.0;
        break;
    default:
        break;
    };

    switch (vAlign) {
    case Qt::AlignBottom:
        y = m_geometry.height() - size.height();
        break;
    case Qt::AlignVCenter:
        y = m_geometry.height() / 2.0 - size.height() / 2.0;
        break;
    case Qt::AlignTop:
        break;
    };

    if (!m_richText)
        y += QFontMetricsF(m_font).ascent();

    setMatrix(QMatrix4x4());
    translate(x, y);
}

void TextNode::updateNodes()
{
    deleteContent();
    if (m_text.isEmpty())
        return;

    if (m_usePixmapCache) {
        // ### gunnar: port properly
//        QPixmap pixmap = generatedPixmap();
//        if (pixmap.isNull())
//            return;

//        TextureNodeInterface *pixmapNode = m_context->createTextureNode();
//        pixmapNode->setRect(pixmap.rect());
//        pixmapNode->setSourceRect(pixmap.rect());
//        pixmapNode->setOpacity(m_opacity);
//        pixmapNode->setClampToEdge(true);
//        pixmapNode->setLinearFiltering(m_linearFiltering);

//        appendChildNode(pixmapNode);
    } else {
        if (m_text.isEmpty())
            return;

        // Implement styling by drawing text several times at slight shifts. shiftForStyle
        // contains the sequence of shifted positions at which to draw the text. All except
        // the last will be drawn with styleColor.
        QList<QPointF> shiftForStyle;
        switch (m_textStyle) {
        case OutlineTextStyle:
            // ### Should be made faster by implementing outline material
            shiftForStyle << QPointF(-1, 0);
            shiftForStyle << QPointF(0, -1);
            shiftForStyle << QPointF(1, 0);
            shiftForStyle << QPointF(0, 1);
            break;
        case SunkenTextStyle:
            shiftForStyle << QPointF(0, -1);
            break;
        case RaisedTextStyle:
            shiftForStyle << QPointF(0, 1);
            break;
        default:
            break;
        }

        shiftForStyle << QPointF(0, 0); // Regular position
        while (!shiftForStyle.isEmpty()) {
            QPointF shift = shiftForStyle.takeFirst();

            // Use styleColor for all but last shift
            if (m_richText) {
                QColor overrideColor = shiftForStyle.isEmpty() ? QColor() : m_styleColor;

                QTextFrame *textFrame = m_textDocument->rootFrame();
                QPointF p = m_textDocument->documentLayout()->frameBoundingRect(textFrame).topLeft();

                QTextFrame::iterator it = textFrame->begin();
                while (!it.atEnd()) {
                    addTextBlock(shift + p, it.currentBlock(), overrideColor);
                    ++it;
                }
            } else {
                addTextLayout(shift, m_textLayout, shiftForStyle.isEmpty()
                                                   ? m_color
                                                   : m_styleColor);
            }
        }
    }
}

