/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QDECLARATIVEITEM_P_H
#define QDECLARATIVEITEM_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "qxitem.h"
#include <private/qobject_p.h>
#include <node.h>

#include "qxanchors_p.h"
#include "qxanchors_p_p.h"

#include "qxitemchangelistener_p.h"
#include <private/qpodvector_p.h>

#include <private/qdeclarativestate_p.h>
#include <private/qdeclarativenullablevalue_p_p.h>
#include <private/qdeclarativenotifier_p.h>
#include <private/qdeclarativeglobal_p.h>

#include <QtCore/qelapsedtimer.h>

#include <qdeclarative.h>
#include <qdeclarativecontext.h>

#include <QtCore/qlist.h>
#include <QtCore/qdebug.h>
#include "qxgraphicsview.h"
#include "qsgcontext.h"

QT_BEGIN_NAMESPACE

class QNetworkReply;
class QxItemKeyFilter;
class QxClipNode;

//### merge into private?
class QxContents : public QObject, public QxItemChangeListener
{
    Q_OBJECT
public:
    QxContents();
    ~QxContents();

    QRectF rectF() const;

    void setItem(QxItem *item);

    void childRemoved(QxItem *item);
    void childAdded(QxItem *item);

Q_SIGNALS:
    void rectChanged(QRectF);

protected:
    void itemGeometryChanged(QxItem *item, const QRectF &newGeometry, const QRectF &oldGeometry);
    void itemDestroyed(QxItem *item);
    //void itemVisibilityChanged(QxItem *item)

private:
    void calcHeight(QxItem *changed = 0);
    void calcWidth(QxItem *changed = 0);

    QxItem *m_item;
    qreal m_x;
    qreal m_y;
    qreal m_width;
    qreal m_height;
};

class QxItemPrivate : public QObjectPrivate
{
    Q_DECLARE_PUBLIC(QxItem)

public:
    QxItemPrivate()
        : parentItem(0)
        , view(0)
        , firstChild(0)
        , lastChild(0)
        , nextSibling(0)
        , prevSibling(0)
        , dirtyInView(0)
        , widthValid(0)
        , heightValid(0)
        , transformNodeAttached(0)
        , enabled(1)
        , visible(1)
        , clip(0)
        , filtersChildMouse(0)
        , isFilteringChildMouse(0)
        , isMouseGrabber(0)
        , isFocusScope(0)
        , inActiveFocusChain(1)
        , _componentComplete(1)
        , _keepMouse(0)
        , smooth(0)
        , transformOriginDirty(1)
        , doneEventPreHandler(0)
        , implicitSize(0,0)
        , zValue(0)
        , opacity(1)
        , scale(1)
        , rotation(0)
        , transformOrigin(QxItem::Center)
        , acceptedButtons(Qt::NoButton)
        , focusItem(0)
        , renderOpacity(1)
        , clipNode(0)
        , localNode(0)
        , effectRefCount(0)
        , rootNode(0)
        , _anchors(0), _contents(0)
        ,  _baselineOffset(0)
        , _anchorLines(0)
        , _stateGroup(0)
        , keyHandler(0)
    {
        transformNode.setFlag(Node::OwnedByParent, false);
        groupNode.setFlag(Node::OwnedByParent, false);
        transformNode.appendChildNode(&groupNode);

#ifdef QML_RUNTIME_TESTING
        groupNode.description = QLatin1String("group");
        transformNode.description = QLatin1String("item transform");
#endif
    }

    void init(QxItem *parent)
    {
        Q_Q(QxItem);
        if (parent) {
            QDeclarative_setParent_noEvent(q, parent);
            q->setParentItem(parent);
        }
        _baselineOffset.invalidate();
    }

    QxItem *parentItem;
    QxGraphicsView *view;
    void setViewRecursive(QxGraphicsView *);

    QxItem *firstChild;
    QxItem *lastChild;
    QxItem *nextSibling;
    QxItem *prevSibling;
    inline void setParentItem(QxItem *);

    quint32 dirtyInView:1;
    quint32 widthValid:1;
    quint32 heightValid:1;
    quint32 transformNodeAttached:1;
    quint32 enabled:1;
    quint32 visible:1;
    quint32 clip:1;
    quint32 filtersChildMouse:1;
    quint32 isFilteringChildMouse:1;
    quint32 isMouseGrabber:1;
    quint32 isFocusScope:1;
    quint32 inActiveFocusChain:1;
    quint32 _componentComplete:1;
    quint32 _keepMouse:1;
    quint32 smooth:1;
    quint32 transformOriginDirty : 1;
    quint32 doneEventPreHandler : 1;
    quint32 padding: 15;

    void attachTransformNode();
    void detachTransformNode();
    void geometryChanged(const QRectF &, const QRectF &);
    void invalidateRenderOrderData();
    virtual QRectF renderOrderBoundingRect() const;

    // ref- and derefFromEffectItem() are used by effect items. When an effect is activated,
    // the effect calls refFromEffectItem() which causes this to detach from the main scene graph.
    // The effect then renders the sub-scenegraph to an FBO. When the effect is deactivated, it
    // will call derefFromEffectNode() which causes this to reattach to the main scene graph,
    // unless it is still referenced from other effects.
    void refFromEffectItem();
    void derefFromEffectItem();

    bool childrenDoNotOverlap() const { return groupNode.flags() & Node::ChildrenDoNotOverloap; }
    void setChildrenDoNotOverlap(bool noOverlap) { groupNode.setFlag(Node::ChildrenDoNotOverloap, noOverlap); }

    QPointF position;
    QSizeF size;
    QSizeF implicitSize;
    qreal zValue;

    qreal opacity;
    qreal scale;
    qreal rotation;
    QxItem::TransformOrigin transformOrigin;

    Qt::MouseButtons acceptedButtons;

    QPointF transformOriginPoint() const;
    QTransform mapToGlobalTransform() const;
    QTransform mapFromGlobalTransform(bool *ok = 0) const;

    QxItem *focusItem;

    // scenegraph stuff
    qreal renderOpacity;
    void updateRenderOpacity();
    void updateClip();

    QMatrix4x4 computeMatrixFromProperties() const;
    void updateGeometry();
    QxClipNode *clipNode;
    TransformNode transformNode;
    QMatrix4x4 transform;
    Node groupNode;
    Node *localNode;

    int effectRefCount;
    RootNode *rootNode;

    // transform property
    static int transform_count(QDeclarativeListProperty<QGraphicsTransform> *list);
    static void transform_append(QDeclarativeListProperty<QGraphicsTransform> *list, QGraphicsTransform *);
    static QGraphicsTransform *transform_at(QDeclarativeListProperty<QGraphicsTransform> *list, int);
    static void transform_clear(QDeclarativeListProperty<QGraphicsTransform> *list);

    QVector<QGraphicsTransform *> m_transformations;

    virtual void clipChanged(bool) {}

    // Private Properties
    QDeclarativeListProperty<QObject> data();
    QDeclarativeListProperty<QObject> resources();

    QDeclarativeListProperty<QDeclarativeState> states();
    QDeclarativeListProperty<QDeclarativeTransition> transitions();

    QString state() const;
    void setState(const QString &);

    QxAnchorLine left() const;
    QxAnchorLine right() const;
    QxAnchorLine horizontalCenter() const;
    QxAnchorLine top() const;
    QxAnchorLine bottom() const;
    QxAnchorLine verticalCenter() const;
    QxAnchorLine baseline() const;

    // data property
    static void data_append(QDeclarativeListProperty<QObject> *, QObject *);

    // resources property
    static QObject *resources_at(QDeclarativeListProperty<QObject> *, int);
    static void resources_append(QDeclarativeListProperty<QObject> *, QObject *);
    static int resources_count(QDeclarativeListProperty<QObject> *);

    static QxItemPrivate* get(QxItem *item) { return item->d_func(); }

    // Accelerated property accessors
    QDeclarativeNotifier parentNotifier;
    static void parentProperty(QObject *o, void *rv, QDeclarativeNotifierEndpoint *e);

    QxAnchors *anchors() {
        if (!_anchors) {
            Q_Q(QxItem);
            _anchors = new QxAnchors(q);
            if (!_componentComplete)
                _anchors->classBegin();
        }
        return _anchors;
    }
    QxAnchors *_anchors;
    QxContents *_contents;

    QDeclarativeNullableValue<qreal> _baselineOffset;

    struct AnchorLines {
        AnchorLines(QxItem *);
        QxAnchorLine left;
        QxAnchorLine right;
        QxAnchorLine hCenter;
        QxAnchorLine top;
        QxAnchorLine bottom;
        QxAnchorLine vCenter;
        QxAnchorLine baseline;
    };
    mutable AnchorLines *_anchorLines;
    AnchorLines *anchorLines() const {
        Q_Q(const QxItem);
        if (!_anchorLines) _anchorLines =
            new AnchorLines(const_cast<QxItem *>(q));
        return _anchorLines;
    }

    enum ChangeType {
        Geometry = 0x01,
        SiblingOrder = 0x02,
        Visibility = 0x04,
        Opacity = 0x08,
        Destroyed = 0x10
    };

    Q_DECLARE_FLAGS(ChangeTypes, ChangeType)

    struct ChangeListener {
        ChangeListener(QxItemChangeListener *l, QxItemPrivate::ChangeTypes t) : listener(l), types(t) {}
        QxItemChangeListener *listener;
        QxItemPrivate::ChangeTypes types;
        bool operator==(const ChangeListener &other) const { return listener == other.listener && types == other.types; }
    };

    void addItemChangeListener(QxItemChangeListener *listener, ChangeTypes types) {
        changeListeners.append(ChangeListener(listener, types));
    }
    void removeItemChangeListener(QxItemChangeListener *, ChangeTypes types);
    QPODVector<ChangeListener,4> changeListeners;

    QDeclarativeStateGroup *_states();
    QDeclarativeStateGroup *_stateGroup;

    QxItemKeyFilter *keyHandler;

    // XXX akennedy
#if 0
    // Reimplemented from QGraphicsItemPrivate
    virtual void subFocusItemChange()
    {
        emit q_func()->wantsFocusChanged(subFocusItem != 0);
    }

    // Reimplemented from QGraphicsItemPrivate
    virtual void siblingOrderChange()
    {
        Q_Q(QxItem);
        for(int ii = 0; ii < changeListeners.count(); ++ii) {
            const QxItemPrivate::ChangeListener &change = changeListeners.at(ii);
            if (change.types & QxItemPrivate::SiblingOrder) {
                change.listener->itemSiblingOrderChanged(q);
            }
        }
    }
#endif

    virtual void focusChanged(bool);

    static qint64 consistentTime;
    static void setConsistentTime(qint64 t);
    static void start(QElapsedTimer &);
    static qint64 elapsed(QElapsedTimer &);
    static qint64 restart(QElapsedTimer &);
};

/*
    Key filters can be installed on a QxItem, but not removed.  Currently they
    are only used by attached objects (which are only destroyed on Item
    destruction), so this isn't a problem.  If in future this becomes any form
    of public API, they will have to support removal too.
*/
class QxItemKeyFilter
{
public:
    QxItemKeyFilter(QxItem * = 0);
    virtual ~QxItemKeyFilter();

    virtual void keyPressed(QKeyEvent *event, bool post);
    virtual void keyReleased(QKeyEvent *event, bool post);
    virtual void inputMethodEvent(QInputMethodEvent *event, bool post);
    virtual QVariant inputMethodQuery(Qt::InputMethodQuery query) const;
    virtual void componentComplete();

    bool m_processPost;

private:
    QxItemKeyFilter *m_next;
};

class QxKeyNavigationAttachedPrivate : public QObjectPrivate
{
public:
    QxKeyNavigationAttachedPrivate()
        : QObjectPrivate(), left(0), right(0), up(0), down(0), tab(0), backtab(0) {}

    QxItem *left;
    QxItem *right;
    QxItem *up;
    QxItem *down;
    QxItem *tab;
    QxItem *backtab;
};

class QxKeyNavigationAttached : public QObject, public QxItemKeyFilter
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QxKeyNavigationAttached)

    Q_PROPERTY(QxItem *left READ left WRITE setLeft NOTIFY changed)
    Q_PROPERTY(QxItem *right READ right WRITE setRight NOTIFY changed)
    Q_PROPERTY(QxItem *up READ up WRITE setUp NOTIFY changed)
    Q_PROPERTY(QxItem *down READ down WRITE setDown NOTIFY changed)
    Q_PROPERTY(QxItem *tab READ tab WRITE setTab NOTIFY changed)
    Q_PROPERTY(QxItem *backtab READ backtab WRITE setBacktab NOTIFY changed)
    Q_PROPERTY(Priority priority READ priority WRITE setPriority NOTIFY priorityChanged)

    Q_ENUMS(Priority)

public:
    QxKeyNavigationAttached(QObject * = 0);

    QxItem *left() const;
    void setLeft(QxItem *);
    QxItem *right() const;
    void setRight(QxItem *);
    QxItem *up() const;
    void setUp(QxItem *);
    QxItem *down() const;
    void setDown(QxItem *);
    QxItem *tab() const;
    void setTab(QxItem *);
    QxItem *backtab() const;
    void setBacktab(QxItem *);

    enum Priority { BeforeItem, AfterItem };
    Priority priority() const;
    void setPriority(Priority);

    static QxKeyNavigationAttached *qmlAttachedProperties(QObject *);

Q_SIGNALS:
    void changed();
    void priorityChanged();

private:
    virtual void keyPressed(QKeyEvent *event, bool post);
    virtual void keyReleased(QKeyEvent *event, bool post);
};

class QxKeysAttachedPrivate : public QObjectPrivate
{
public:
    QxKeysAttachedPrivate()
        : QObjectPrivate(), inPress(false), inRelease(false)
        , inIM(false), enabled(true), imeItem(0), item(0)
    {}

    bool isConnected(const char *signalName);

    QxItem *finalFocusProxy(QxItem *item) const
    {
        QxItem *fp;
        while ((fp = item->focusItem()))
            item = fp;
        return item;
    }

    //loop detection
    quint32 inPress:1;
    quint32 inRelease:1;
    quint32 inIM:1;
    quint32 enabled : 1;
    quint32 padding: 28;

    QGraphicsItem *imeItem;
    QList<QxItem *> targets;
    QxItem *item;
};

class QxKeysAttached : public QObject, public QxItemKeyFilter
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QxKeysAttached)

    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(QDeclarativeListProperty<QxItem> forwardTo READ forwardTo)
    Q_PROPERTY(Priority priority READ priority WRITE setPriority NOTIFY priorityChanged)

    Q_ENUMS(Priority)

public:
    QxKeysAttached(QObject *parent=0);
    ~QxKeysAttached();

    bool enabled() const { Q_D(const QxKeysAttached); return d->enabled; }
    void setEnabled(bool enabled) {
        Q_D(QxKeysAttached);
        if (enabled != d->enabled) {
            d->enabled = enabled;
            emit enabledChanged();
        }
    }

    enum Priority { BeforeItem, AfterItem};
    Priority priority() const;
    void setPriority(Priority);

    QDeclarativeListProperty<QxItem> forwardTo() {
        Q_D(QxKeysAttached);
        return QDeclarativeListProperty<QxItem>(this, d->targets);
    }

    virtual void componentComplete();

    static QxKeysAttached *qmlAttachedProperties(QObject *);

Q_SIGNALS:
    void enabledChanged();
    void priorityChanged();
    void pressed(QxKeyEvent *event);
    void released(QxKeyEvent *event);
    void digit0Pressed(QxKeyEvent *event);
    void digit1Pressed(QxKeyEvent *event);
    void digit2Pressed(QxKeyEvent *event);
    void digit3Pressed(QxKeyEvent *event);
    void digit4Pressed(QxKeyEvent *event);
    void digit5Pressed(QxKeyEvent *event);
    void digit6Pressed(QxKeyEvent *event);
    void digit7Pressed(QxKeyEvent *event);
    void digit8Pressed(QxKeyEvent *event);
    void digit9Pressed(QxKeyEvent *event);

    void leftPressed(QxKeyEvent *event);
    void rightPressed(QxKeyEvent *event);
    void upPressed(QxKeyEvent *event);
    void downPressed(QxKeyEvent *event);
    void tabPressed(QxKeyEvent *event);
    void backtabPressed(QxKeyEvent *event);

    void asteriskPressed(QxKeyEvent *event);
    void numberSignPressed(QxKeyEvent *event);
    void escapePressed(QxKeyEvent *event);
    void returnPressed(QxKeyEvent *event);
    void enterPressed(QxKeyEvent *event);
    void deletePressed(QxKeyEvent *event);
    void spacePressed(QxKeyEvent *event);
    void backPressed(QxKeyEvent *event);
    void cancelPressed(QxKeyEvent *event);
    void selectPressed(QxKeyEvent *event);
    void yesPressed(QxKeyEvent *event);
    void noPressed(QxKeyEvent *event);
    void context1Pressed(QxKeyEvent *event);
    void context2Pressed(QxKeyEvent *event);
    void context3Pressed(QxKeyEvent *event);
    void context4Pressed(QxKeyEvent *event);
    void callPressed(QxKeyEvent *event);
    void hangupPressed(QxKeyEvent *event);
    void flipPressed(QxKeyEvent *event);
    void menuPressed(QxKeyEvent *event);
    void volumeUpPressed(QxKeyEvent *event);
    void volumeDownPressed(QxKeyEvent *event);

private:
    virtual void keyPressed(QKeyEvent *event, bool post);
    virtual void keyReleased(QKeyEvent *event, bool post);
    virtual void inputMethodEvent(QInputMethodEvent *, bool post);
    virtual QVariant inputMethodQuery(Qt::InputMethodQuery query) const;

    const QByteArray keyToSignal(int key) {
        QByteArray keySignal;
        if (key >= Qt::Key_0 && key <= Qt::Key_9) {
            keySignal = "digit0Pressed";
            keySignal[5] = '0' + (key - Qt::Key_0);
        } else {
            int i = 0;
            while (sigMap[i].key && sigMap[i].key != key)
                ++i;
            keySignal = sigMap[i].sig;
        }
        return keySignal;
    }

    struct SigMap {
        int key;
        const char *sig;
    };

    static const SigMap sigMap[];
};

Q_DECLARE_OPERATORS_FOR_FLAGS(QxItemPrivate::ChangeTypes);

QT_END_NAMESPACE

QML_DECLARE_TYPE(QxKeysAttached)
QML_DECLARE_TYPEINFO(QxKeysAttached, QML_HAS_ATTACHED_PROPERTIES)
QML_DECLARE_TYPE(QxKeyNavigationAttached)
QML_DECLARE_TYPEINFO(QxKeyNavigationAttached, QML_HAS_ATTACHED_PROPERTIES)

#endif // QDECLARATIVEITEM_P_H
