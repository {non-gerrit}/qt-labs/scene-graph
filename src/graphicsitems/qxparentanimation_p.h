/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXPARENTANIMATION_P_H
#define QXPARENTANIMATION_P_H

#include "qxitem.h"

#include <private/qdeclarativeanimation_p.h>
#include <private/qdeclarativetransition_p.h>
#include <private/qdeclarativestate_p.h>
#include <QtGui/qvector3d.h>

#include <QtDeclarative/qdeclarative.h>
#include <QtDeclarative/qdeclarativescriptstring.h>

#include <QtCore/qvariant.h>
#include <QtCore/qeasingcurve.h>
#include <QtCore/QAbstractAnimation>
#include <QtGui/qcolor.h>

class QxParentAnimationPrivate;
class QxParentAnimation : public QDeclarativeAnimationGroup
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(QxParentAnimation)

    Q_PROPERTY(QxItem *target READ target WRITE setTarget NOTIFY targetChanged)
    Q_PROPERTY(QxItem *newParent READ newParent WRITE setNewParent NOTIFY newParentChanged)
    Q_PROPERTY(QxItem *via READ via WRITE setVia NOTIFY viaChanged)

public:
    QxParentAnimation(QObject *parent=0);
    virtual ~QxParentAnimation();

    QxItem *target() const;
    void setTarget(QxItem *);

    QxItem *newParent() const;
    void setNewParent(QxItem *);

    QxItem *via() const;
    void setVia(QxItem *);

Q_SIGNALS:
    void targetChanged();
    void newParentChanged();
    void viaChanged();

protected:
    virtual void transition(QDeclarativeStateActions &actions,
                            QDeclarativeProperties &modified,
                            TransitionDirection direction);
    virtual QAbstractAnimation *qtAnimation();
};

QML_DECLARE_TYPE(QxParentAnimation)

#endif // QXPARENTANIMATION_P_H
