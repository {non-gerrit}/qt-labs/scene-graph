/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXBORDERIMAGE_P_H
#define QXBORDERIMAGE_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "qximagebase_p_p.h"

#include <private/qdeclarativescalegrid_p_p.h>

class QxNinePatchNode;
class QNetworkReply;
class QxBorderImagePrivate : public QxImageBasePrivate
{
    Q_DECLARE_PUBLIC(QxBorderImage)

public:
    QxBorderImagePrivate()
    : border(0), sciReply(0), 
      horizontalTileMode(QxBorderImage::Stretch),
      verticalTileMode(QxBorderImage::Stretch),
      redirectCount(0), node(0), texture(0)
    {
    }

    QDeclarativeScaleGrid *getScaleGrid()
    {
        Q_Q(QxBorderImage);
        if (!border)
            border = new QDeclarativeScaleGrid(q);
        return border;
    }

    QDeclarativeScaleGrid *border;
    QUrl sciurl;
    QNetworkReply *sciReply;
    QxBorderImage::TileMode horizontalTileMode;
    QxBorderImage::TileMode verticalTileMode;
    int redirectCount;

    void updateNode();
    void updatePixmap();
    QxNinePatchNode *node;
    QSGTextureRef texture;
};

#endif // QXBORDERIMAGE_P_H
