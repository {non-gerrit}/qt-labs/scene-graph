INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/qxrectangle_p.h \
    $$PWD/qxrectangle_p_p.h \
    $$PWD/qxitem.h \
    $$PWD/qxitem_p.h \
    $$PWD/qxanchors_p.h \
    $$PWD/qxanchors_p_p.h \
    $$PWD/qxitemchangelistener_p.h \
    $$PWD/qxmousearea_p.h \
    $$PWD/qxmousearea_p_p.h \
    $$PWD/qxevents_p.h \
    $$PWD/qximagebase_p.h \
    $$PWD/qximagebase_p_p.h \
    $$PWD/qximage_p.h \
    $$PWD/qximage_p_p.h \
    $$PWD/qxborderimage_p.h \
    $$PWD/qxborderimage_p_p.h \
    $$PWD/qxtext_p.h \
    $$PWD/qxtext_p_p.h \
    $$PWD/qxflickable_p.h \
    $$PWD/qxflickable_p_p.h \
    $$PWD/qxflipable_p.h \
    $$PWD/qxvisualitemmodel_p.h \
    $$PWD/qxlistview_p.h \
    $$PWD/qxgridview_p.h \
    $$PWD/qxpathview_p.h \
    $$PWD/qxpathview_p_p.h \
    $$PWD/qxpositioners_p.h \
    $$PWD/qxpositioners_p_p.h \
    $$PWD/qxtextedit_p_p.h \
    $$PWD/qxtextedit_p.h \
    $$PWD/qxtextinput_p_p.h \
    $$PWD/qxtextinput_p.h \
    $$PWD/qxloader_p.h \
    $$PWD/qxloader_p_p.h \
    $$PWD/qxrepeater_p.h \
    $$PWD/qxrepeater_p_p.h \
    $$PWD/qxstateoperations_p.h \
    $$PWD/qxanchoranimation_p.h \
    $$PWD/qxanchoranimation_p_p.h \
    $$PWD/qxparentanimation_p.h \
    $$PWD/qxparentanimation_p_p.h \
    $$PWD/qxfocusscope_p.h \
    $$PWD/qxpaintitem.h \
    $$PWD/qxpaintitem_p.h \
    $$PWD/nodes/qxninepatchnode_p.h \
    $$PWD/nodes/qxpainternode.h

SOURCES += \
    $$PWD/qxrectangle.cpp \
    $$PWD/qxitem.cpp \
    $$PWD/qxanchors.cpp \
    $$PWD/qxmousearea.cpp \
    $$PWD/qximagebase.cpp \
    $$PWD/qximage.cpp \
    $$PWD/qxborderimage.cpp \
    $$PWD/qxtext.cpp \
    $$PWD/qxflickable.cpp \
    $$PWD/qxflipable.cpp \
    $$PWD/qxvisualitemmodel.cpp \
    $$PWD/qxlistview.cpp \
    $$PWD/qxgridview.cpp \
    $$PWD/qxpathview.cpp \
    $$PWD/qxpositioners.cpp \
    $$PWD/qxtextedit.cpp \
    $$PWD/qxtextinput.cpp \
    $$PWD/qxloader.cpp \
    $$PWD/qxrepeater.cpp \
    $$PWD/qxstateoperations.cpp \
    $$PWD/qxanchoranimation.cpp \
    $$PWD/qxparentanimation.cpp \
    $$PWD/qxfocusscope.cpp \
    $$PWD/qxpaintitem.cpp \
    $$PWD/nodes/qxninepatchnode.cpp \
    $$PWD/nodes/qxpainternode.cpp
