/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxitem.h"
#include "qxitem_p.h"
#include "qxevents_p.h"
#include "qxgraphicsview.h"
#include "qxgraphicsview_p.h"
#include <private/qdeclarativetranslate_p.h>
#include "qxclipnode_p.h"

#include <private/qdeclarativeengine_p.h>

#include <QtDeclarative/qdeclarativeengine.h>
#include <private/qdeclarativeopenmetaobject_p.h>
#include <private/qdeclarativestate_p.h>
#include <QtDeclarative/qdeclarativeview.h>
#include <private/qdeclarativestategroup_p.h>
#include <QtDeclarative/qdeclarativecomponent.h>
#include <QtDeclarative/qdeclarativeinfo.h>

// XXX akennedy (used in data_append to catch insertion of declarative items)
#include <QtDeclarative/qdeclarativeitem.h>

#include <QDebug>
#include <QPen>
#include <QFile>
#include <QEvent>
#include <QGraphicsSceneMouseEvent>
#include <QtCore/qnumeric.h>
#include <QtScript/qscriptengine.h>
#include <QtGui/qapplication.h>

#ifndef FLT_MAX
#define FLT_MAX 1E+37
#endif

QxContents::QxContents() : m_x(0), m_y(0), m_width(0), m_height(0)
{
}

QxContents::~QxContents()
{
    QList<QxItem *> children = m_item->childItems();
    for (int i = 0; i < children.count(); ++i) {
        QxItem *child = qobject_cast<QxItem *>(children.at(i));
        if(!child)//### Should this be ignoring non-QxItem graphicsobjects?
            continue;
        QxItemPrivate::get(child)->removeItemChangeListener(this, QxItemPrivate::Geometry | QxItemPrivate::Destroyed);
    }
}

QRectF QxContents::rectF() const
{
    return QRectF(m_x, m_y, m_width, m_height);
}

void QxContents::calcHeight(QxItem *changed)
{
    qreal oldy = m_y;
    qreal oldheight = m_height;

    if (changed) {
        qreal top = oldy;
        qreal bottom = oldy + oldheight;
        qreal y = changed->y();
        if (y + changed->height() > bottom)
            bottom = y + changed->height();
        if (y < top)
            top = y;
        m_y = top;
        m_height = bottom - top;
    } else {
        qreal top = FLT_MAX;
        qreal bottom = 0;
        QList<QxItem *> children = m_item->childItems();
        for (int i = 0; i < children.count(); ++i) {
            QxItem *child = qobject_cast<QxItem *>(children.at(i));
            if(!child)//### Should this be ignoring non-QxItem graphicsobjects?
                continue;
            qreal y = child->y();
            if (y + child->height() > bottom)
                bottom = y + child->height();
            if (y < top)
                top = y;
        }
        if (!children.isEmpty())
            m_y = top;
        m_height = qMax(bottom - top, qreal(0.0));
    }

    if (m_height != oldheight || m_y != oldy)
        emit rectChanged(rectF());
}

void QxContents::calcWidth(QxItem *changed)
{
    qreal oldx = m_x;
    qreal oldwidth = m_width;

    if (changed) {
        qreal left = oldx;
        qreal right = oldx + oldwidth;
        qreal x = changed->x();
        if (x + changed->width() > right)
            right = x + changed->width();
        if (x < left)
            left = x;
        m_x = left;
        m_width = right - left;
    } else {
        qreal left = FLT_MAX;
        qreal right = 0;
        QList<QxItem *> children = m_item->childItems();
        for (int i = 0; i < children.count(); ++i) {
            QxItem *child = qobject_cast<QxItem *>(children.at(i));
            if(!child)//### Should this be ignoring non-QxItem graphicsobjects?
                continue;
            qreal x = child->x();
            if (x + child->width() > right)
                right = x + child->width();
            if (x < left)
                left = x;
        }
        if (!children.isEmpty())
            m_x = left;
        m_width = qMax(right - left, qreal(0.0));
    }

    if (m_width != oldwidth || m_x != oldx)
        emit rectChanged(rectF());
}

void QxContents::setItem(QxItem *item)
{
    m_item = item;
    //### optimize
    connect(this, SIGNAL(rectChanged(QRectF)), m_item, SIGNAL(childrenRectChanged(QRectF)));

    QList<QxItem *> children = m_item->childItems();
    for (int i = 0; i < children.count(); ++i) {
        QxItem *child = qobject_cast<QxItem *>(children.at(i));
        if(!child)//### Should this be ignoring non-QxItem graphicsobjects?
            continue;
        QxItemPrivate::get(child)->addItemChangeListener(this, QxItemPrivate::Geometry | QxItemPrivate::Destroyed);
        //###what about changes to visibility?
    }

    //### defer until componentComplete
    calcHeight();
    calcWidth();
}

void QxContents::itemGeometryChanged(QxItem *changed, const QRectF &newGeometry, const QRectF &oldGeometry)
{
    if (newGeometry.width() != oldGeometry.width())
        calcWidth(changed);
    if (newGeometry.height() != oldGeometry.height())
        calcHeight(changed);
}

void QxContents::itemDestroyed(QxItem *item)
{
    if (item)
        QxItemPrivate::get(item)->removeItemChangeListener(this, QxItemPrivate::Geometry | QxItemPrivate::Destroyed);
    calcWidth();
    calcHeight();
}

void QxContents::childRemoved(QxItem *item)
{
    if (item)
        QxItemPrivate::get(item)->removeItemChangeListener(this, QxItemPrivate::Geometry | QxItemPrivate::Destroyed);
    calcWidth();
    calcHeight();
}

void QxContents::childAdded(QxItem *item)
{
    if (item)
        QxItemPrivate::get(item)->addItemChangeListener(this, QxItemPrivate::Geometry | QxItemPrivate::Destroyed);
    calcWidth(item);
    calcHeight(item);
}

QxItemKeyFilter::QxItemKeyFilter(QxItem *item)
: m_processPost(false), m_next(0)
{
    QxItemPrivate *p = item?QxItemPrivate::get(item):0;
    if (p) {
        m_next = p->keyHandler;
        p->keyHandler = this;
    }
}

QxItemKeyFilter::~QxItemKeyFilter()
{
}

void QxItemKeyFilter::keyPressed(QKeyEvent *event, bool post)
{
    if (m_next) m_next->keyPressed(event, post);
}

void QxItemKeyFilter::keyReleased(QKeyEvent *event, bool post)
{
    if (m_next) m_next->keyReleased(event, post);
}

void QxItemKeyFilter::inputMethodEvent(QInputMethodEvent *event, bool post)
{
    if (m_next) m_next->inputMethodEvent(event, post);
}

QVariant QxItemKeyFilter::inputMethodQuery(Qt::InputMethodQuery query) const
{
    if (m_next) return m_next->inputMethodQuery(query);
    return QVariant();
}

void QxItemKeyFilter::componentComplete()
{
    if (m_next) m_next->componentComplete();
}

QxKeyNavigationAttached::QxKeyNavigationAttached(QObject *parent)
: QObject(*(new QxKeyNavigationAttachedPrivate), parent),
  QxItemKeyFilter(qobject_cast<QxItem*>(parent))
{
    m_processPost = true;
}

QxKeyNavigationAttached *
QxKeyNavigationAttached::qmlAttachedProperties(QObject *obj)
{
    return new QxKeyNavigationAttached(obj);
}

QxItem *QxKeyNavigationAttached::left() const
{
    Q_D(const QxKeyNavigationAttached);
    return d->left;
}

void QxKeyNavigationAttached::setLeft(QxItem *i)
{
    Q_D(QxKeyNavigationAttached);
    d->left = i;
    emit changed();
}

QxItem *QxKeyNavigationAttached::right() const
{
    Q_D(const QxKeyNavigationAttached);
    return d->right;
}

void QxKeyNavigationAttached::setRight(QxItem *i)
{
    Q_D(QxKeyNavigationAttached);
    d->right = i;
    emit changed();
}

QxItem *QxKeyNavigationAttached::up() const
{
    Q_D(const QxKeyNavigationAttached);
    return d->up;
}

void QxKeyNavigationAttached::setUp(QxItem *i)
{
    Q_D(QxKeyNavigationAttached);
    d->up = i;
    emit changed();
}

QxItem *QxKeyNavigationAttached::down() const
{
    Q_D(const QxKeyNavigationAttached);
    return d->down;
}

void QxKeyNavigationAttached::setDown(QxItem *i)
{
    Q_D(QxKeyNavigationAttached);
    d->down = i;
    emit changed();
}

QxItem *QxKeyNavigationAttached::tab() const
{
    Q_D(const QxKeyNavigationAttached);
    return d->tab;
}

void QxKeyNavigationAttached::setTab(QxItem *i)
{
    Q_D(QxKeyNavigationAttached);
    d->tab = i;
    emit changed();
}

QxItem *QxKeyNavigationAttached::backtab() const
{
    Q_D(const QxKeyNavigationAttached);
    return d->backtab;
}

void QxKeyNavigationAttached::setBacktab(QxItem *i)
{
    Q_D(QxKeyNavigationAttached);
    d->backtab = i;
    emit changed();
}

QxKeyNavigationAttached::Priority QxKeyNavigationAttached::priority() const
{
    return m_processPost ? AfterItem : BeforeItem;
}

void QxKeyNavigationAttached::setPriority(Priority order)
{
    bool processPost = order == AfterItem;
    if (processPost != m_processPost) {
        m_processPost = processPost;
        emit priorityChanged();
    }
}

void QxKeyNavigationAttached::keyPressed(QKeyEvent *event, bool post)
{
    Q_D(QxKeyNavigationAttached);
    event->ignore();

    if (post != m_processPost) {
        QxItemKeyFilter::keyPressed(event, post);
        return;
    }

    switch(event->key()) {
    case Qt::Key_Left:
        if (d->left) {
            d->left->setFocus(true);
            event->accept();
        }
        break;
    case Qt::Key_Right:
        if (d->right) {
            d->right->setFocus(true);
            event->accept();
        }
        break;
    case Qt::Key_Up:
        if (d->up) {
            d->up->setFocus(true);
            event->accept();
        }
        break;
    case Qt::Key_Down:
        if (d->down) {
            d->down->setFocus(true);
            event->accept();
        }
        break;
    case Qt::Key_Tab:
        if (d->tab) {
            d->tab->setFocus(true);
            event->accept();
        }
        break;
    case Qt::Key_Backtab:
        if (d->backtab) {
            d->backtab->setFocus(true);
            event->accept();
        }
        break;
    default:
        break;
    }

    if (!event->isAccepted()) QxItemKeyFilter::keyPressed(event, post);
}

void QxKeyNavigationAttached::keyReleased(QKeyEvent *event, bool post)
{
    Q_D(QxKeyNavigationAttached);
    event->ignore();

    if (post != m_processPost) {
        QxItemKeyFilter::keyReleased(event, post);
        return;
    }

    switch(event->key()) {
    case Qt::Key_Left:
        if (d->left) {
            event->accept();
        }
        break;
    case Qt::Key_Right:
        if (d->right) {
            event->accept();
        }
        break;
    case Qt::Key_Up:
        if (d->up) {
            event->accept();
        }
        break;
    case Qt::Key_Down:
        if (d->down) {
            event->accept();
        }
        break;
    case Qt::Key_Tab:
        if (d->tab) {
            event->accept();
        }
        break;
    case Qt::Key_Backtab:
        if (d->backtab) {
            event->accept();
        }
        break;
    default:
        break;
    }

    if (!event->isAccepted()) QxItemKeyFilter::keyReleased(event, post);
}

const QxKeysAttached::SigMap QxKeysAttached::sigMap[] = {
    { Qt::Key_Left, "leftPressed" },
    { Qt::Key_Right, "rightPressed" },
    { Qt::Key_Up, "upPressed" },
    { Qt::Key_Down, "downPressed" },
    { Qt::Key_Tab, "tabPressed" },
    { Qt::Key_Backtab, "backtabPressed" },
    { Qt::Key_Asterisk, "asteriskPressed" },
    { Qt::Key_NumberSign, "numberSignPressed" },
    { Qt::Key_Escape, "escapePressed" },
    { Qt::Key_Return, "returnPressed" },
    { Qt::Key_Enter, "enterPressed" },
    { Qt::Key_Delete, "deletePressed" },
    { Qt::Key_Space, "spacePressed" },
    { Qt::Key_Back, "backPressed" },
    { Qt::Key_Cancel, "cancelPressed" },
    { Qt::Key_Select, "selectPressed" },
    { Qt::Key_Yes, "yesPressed" },
    { Qt::Key_No, "noPressed" },
    { Qt::Key_Context1, "context1Pressed" },
    { Qt::Key_Context2, "context2Pressed" },
    { Qt::Key_Context3, "context3Pressed" },
    { Qt::Key_Context4, "context4Pressed" },
    { Qt::Key_Call, "callPressed" },
    { Qt::Key_Hangup, "hangupPressed" },
    { Qt::Key_Flip, "flipPressed" },
    { Qt::Key_Menu, "menuPressed" },
    { Qt::Key_VolumeUp, "volumeUpPressed" },
    { Qt::Key_VolumeDown, "volumeDownPressed" },
    { 0, 0 }
};

bool QxKeysAttachedPrivate::isConnected(const char *signalName)
{
    return isSignalConnected(signalIndex(signalName));
}

QxKeysAttached::QxKeysAttached(QObject *parent)
: QObject(*(new QxKeysAttachedPrivate), parent),
  QxItemKeyFilter(qobject_cast<QxItem*>(parent))
{
    Q_D(QxKeysAttached);
    m_processPost = false;
    d->item = qobject_cast<QxItem*>(parent);
}

QxKeysAttached::~QxKeysAttached()
{
}

QxKeysAttached::Priority QxKeysAttached::priority() const
{
    return m_processPost ? AfterItem : BeforeItem;
}

void QxKeysAttached::setPriority(Priority order)
{
    bool processPost = order == AfterItem;
    if (processPost != m_processPost) {
        m_processPost = processPost;
        emit priorityChanged();
    }
}

void QxKeysAttached::componentComplete()
{
    Q_D(QxKeysAttached);
    if (d->item) {
        for (int ii = 0; ii < d->targets.count(); ++ii) {
            // XXX akennedy
            /*
            QGraphicsItem *targetItem = d->finalFocusProxy(d->targets.at(ii));
            if (targetItem && (targetItem->flags() & QGraphicsItem::ItemAcceptsInputMethod)) {
                d->item->setFlag(QGraphicsItem::ItemAcceptsInputMethod);
                break;
            }
            */
        }
    }
}

void QxKeysAttached::keyPressed(QKeyEvent *event, bool post)
{
    Q_D(QxKeysAttached);
    if (post != m_processPost || !d->enabled || d->inPress) {
        event->ignore();
        QxItemKeyFilter::keyPressed(event, post);
        return;
    }

    // first process forwards
    // XXX akennedy
    if (d->item) {
        d->inPress = true;
        for (int ii = 0; ii < d->targets.count(); ++ii) {
            QxItem *i = d->finalFocusProxy(d->targets.at(ii));
            if (i) {
                QCoreApplication::sendEvent(i, event);
                if (event->isAccepted()) {
                    d->inPress = false;
                    return;
                }
            }
        }
        d->inPress = false;
    }

    QxKeyEvent ke(*event);
    QByteArray keySignal = keyToSignal(event->key());
    if (!keySignal.isEmpty()) {
        keySignal += "(QxKeyEvent*)";
        if (d->isConnected(keySignal)) {
            // If we specifically handle a key then default to accepted
            ke.setAccepted(true);
            int idx = QxKeysAttached::staticMetaObject.indexOfSignal(keySignal);
            metaObject()->method(idx).invoke(this, Qt::DirectConnection, Q_ARG(QxKeyEvent*, &ke));
        }
    }
    if (!ke.isAccepted())
        emit pressed(&ke);
    event->setAccepted(ke.isAccepted());

    if (!event->isAccepted()) QxItemKeyFilter::keyPressed(event, post);
}

void QxKeysAttached::keyReleased(QKeyEvent *event, bool post)
{
    Q_D(QxKeysAttached);
    if (post != m_processPost || !d->enabled || d->inRelease) {
        event->ignore();
        QxItemKeyFilter::keyReleased(event, post);
        return;
    }

    // XXX akennedy
    if (d->item) {
        d->inRelease = true;
        for (int ii = 0; ii < d->targets.count(); ++ii) {
            QxItem *i = d->finalFocusProxy(d->targets.at(ii));
            if (i) {
                QCoreApplication::sendEvent(i, event);
                if (event->isAccepted()) {
                    d->inRelease = false;
                    return;
                }
            }
        }
        d->inRelease = false;
    }

    QxKeyEvent ke(*event);
    emit released(&ke);
    event->setAccepted(ke.isAccepted());

    if (!event->isAccepted()) QxItemKeyFilter::keyReleased(event, post);
}

void QxKeysAttached::inputMethodEvent(QInputMethodEvent *event, bool post)
{
    // XXX akennedy
    /*
    Q_D(QxKeysAttached);
    if (post == m_processPost && d->item && !d->inIM && d->item->scene()) {
        d->inIM = true;
        for (int ii = 0; ii < d->targets.count(); ++ii) {
            QGraphicsItem *i = d->finalFocusProxy(d->targets.at(ii));
            if (i && (i->flags() & QGraphicsItem::ItemAcceptsInputMethod)) {
                d->item->scene()->sendEvent(i, event);
                if (event->isAccepted()) {
                    d->imeItem = i;
                    d->inIM = false;
                    return;
                }
            }
        }
        d->inIM = false;
    }
    */
    if (!event->isAccepted()) QxItemKeyFilter::inputMethodEvent(event, post);
}

class QxItemAccessor : public QGraphicsItem
{
public:
    QVariant doInputMethodQuery(Qt::InputMethodQuery query) const {
        return QGraphicsItem::inputMethodQuery(query);
    }
};

QVariant QxKeysAttached::inputMethodQuery(Qt::InputMethodQuery query) const
{
    Q_D(const QxKeysAttached);
    if (d->item) {
        for (int ii = 0; ii < d->targets.count(); ++ii) {
            // XXX akennedy
            /*
            QGraphicsItem *i = d->finalFocusProxy(d->targets.at(ii));
            if (i && (i->flags() & QGraphicsItem::ItemAcceptsInputMethod) && i == d->imeItem) { //### how robust is i == d->imeItem check?
                QVariant v = static_cast<QxItemAccessor *>(i)->doInputMethodQuery(query);
                if (v.userType() == QVariant::RectF)
                    v = d->item->mapRectFromItem(i, v.toRectF());  //### cost?
                return v;
            }
            */
        }
    }
    return QxItemKeyFilter::inputMethodQuery(query);
}

QxKeysAttached *QxKeysAttached::qmlAttachedProperties(QObject *obj)
{
    return new QxKeysAttached(obj);
}

// ### Must fix
struct RegisterAnchorLineAtStartup {
    RegisterAnchorLineAtStartup() {
        qRegisterMetaType<QxAnchorLine>("QxAnchorLine");
    }
};
static RegisterAnchorLineAtStartup registerAnchorLineAtStartup;


#ifndef QT_NO_DEBUG
static int qt_graphics_object_count = 0;

static void qt_print_graphics_object_count()
{
    qDebug("Number of leaked graphics objects: %i", qt_graphics_object_count);
    qt_graphics_object_count = -1;
}

static void qt_inc_graphics_object_count()
{
    ++qt_graphics_object_count;
    static bool atexit_registered = false;
    if (!atexit_registered) {
        atexit(qt_print_graphics_object_count);
        atexit_registered = true;
    }
}
#endif

QTransform QxItem::itemTransform(const QxItem *other, bool *ok) const
{
    Q_D(const QxItem);

    bool _ok;
    if (!ok) ok = &_ok;

    QTransform t = d->mapToGlobalTransform();
    QTransform o = other->d_func()->mapFromGlobalTransform(ok);

    if (*ok)
        return t * o;
    else
        return QTransform();
}

QPointF QxItem::mapFromScene(const QPointF &p) const
{
    Q_D(const QxItem);
    QTransform t = d->mapFromGlobalTransform();
    return t.map(p);
}

QPointF QxItem::mapToScene(const QPointF &p) const
{
    Q_D(const QxItem);
    QTransform t = d->mapToGlobalTransform();
    return t.map(p);
}

QRectF QxItem::mapToScene(const QRectF &r) const
{
    Q_D(const QxItem);
    QTransform t = d->mapToGlobalTransform();
    return t.map(r).boundingRect();
}

QPointF QxItem::mapToItem(QxItem *item, const QPointF &point) const
{
    if (item)
        return itemTransform(item).map(point);
    return mapToScene(point);
}

QPointF QxItem::mapFromItem(QxItem *item, const QPointF &point) const
{
    if (item)
        return item->itemTransform(this).map(point);
    return mapFromScene(point);
}

bool QxItem::isEnabled() const
{
    Q_D(const QxItem);
    return d->enabled;
}

void QxItem::setEnabled(bool e)
{
    Q_D(QxItem);
    if (e == d->enabled)
        return;

    d->enabled = e;
    emit enabledChanged();
}

bool QxItem::isVisible() const
{
    Q_D(const QxItem);
    return d->visible;
}

void QxItem::setVisible(bool v)
{
    Q_D(QxItem);
    if (v == d->visible)
        return;

    d->visible = v;
    if (d->visible && d->opacity != 0 && !d->rootNode)
        d->attachTransformNode();
    else
        d->detachTransformNode();

    itemVisibleHasChanged();
    emit visibleChanged();
}

QRectF QxItem::geometry() const
{
    Q_D(const QxItem);
    return QRectF(d->position, QSizeF(width(), height()));
}

qreal QxItem::x() const
{
    Q_D(const QxItem);
    return d->position.x();
}

qreal QxItem::y() const
{
    Q_D(const QxItem);
    return d->position.y();
}

void QxItem::setX(qreal v)
{
    Q_D(QxItem);
    if (d->position.x() == v)
        return;

    QRectF old = geometry();

    d->position.setX(v);

    d->geometryChanged(geometry(), old);

    emit xChanged();

    d->updateGeometry();
}

void QxItem::setY(qreal v)
{
    Q_D(QxItem);
    if (d->position.y() == v)
        return;

    QRectF old = geometry();

    d->position.setY(v);

    d->geometryChanged(geometry(), old);

    emit yChanged();

    d->updateGeometry();
}

qreal QxItem::zValue() const
{
    Q_D(const QxItem);
    return d->zValue;
}

void QxItem::setZValue(qreal z)
{
    Q_D(QxItem);
    if (d->zValue == z)
        return;
    if (d->transformNodeAttached) {
        d->detachTransformNode();
        d->zValue = z;
        d->attachTransformNode();
    } else {
        d->zValue = z;
    }
    emit zChanged();
}

qreal QxItem::width() const
{
    Q_D(const QxItem);
    return d->widthValid?d->size.width():d->implicitSize.width();
}

qreal QxItem::implicitWidth() const
{
    Q_D(const QxItem);
    return d->implicitSize.width();
}

void QxItem::setWidth(qreal v)
{
    Q_D(QxItem);

    if (d->widthValid && d->size.width() == v)
        return;

    QRectF old = geometry();

    d->size.setWidth(v);
    d->widthValid = true;

    d->geometryChanged(geometry(), old);

    emit widthChanged();

    d->updateGeometry();
}

void QxItem::setImplicitWidth(qreal v)
{
    Q_D(QxItem);
    if (d->implicitSize.width() == v)
        return;

    if (!d->widthValid) {
        QRectF old = geometry();

        d->implicitSize.setWidth(v);

        d->geometryChanged(geometry(), old);

        emit widthChanged();

        d->updateGeometry();
    } else {
        d->implicitSize.setWidth(v);
    }
}

bool QxItem::widthValid() const
{
    Q_D(const QxItem);
    return d->widthValid;
}

void QxItem::resetWidth()
{
    Q_D(QxItem);

    if (d->widthValid) {
        QRectF old = geometry();

        d->widthValid = false;

        QRectF newg = geometry();

        if (newg != old) {
            d->geometryChanged(newg, old);
            emit widthChanged();
            d->updateGeometry();
        }
    }
}

qreal QxItem::height() const
{
    Q_D(const QxItem);
    return d->heightValid?d->size.height():d->implicitSize.height();
}

qreal QxItem::implicitHeight() const
{
    Q_D(const QxItem);
    return d->implicitSize.height();
}

bool QxItem::heightValid() const
{
    Q_D(const QxItem);
    return d->heightValid;
}

void QxItem::setHeight(qreal v)
{
    Q_D(QxItem);
    if (d->heightValid && d->size.height() == v)
        return;

    QRectF old = geometry();

    d->size.setHeight(v);
    d->heightValid = true;

    d->geometryChanged(geometry(), old);

    emit heightChanged();

    d->updateGeometry();
}

void QxItem::setImplicitHeight(qreal v)
{
    Q_D(QxItem);
    if (d->implicitSize.height() == v)
        return;

    if (!d->heightValid) {
        QRectF old = geometry();

        d->implicitSize.setHeight(v);

        d->geometryChanged(geometry(), old);

        emit heightChanged();

        d->updateGeometry();
    } else {
        d->implicitSize.setHeight(v);
    }
}

void QxItem::resetHeight()
{
    Q_D(QxItem);

    if (d->heightValid) {
        QRectF old = geometry();

        d->heightValid = false;

        QRectF newg = geometry();

        if (newg != old) {
            d->geometryChanged(newg, old);
            emit heightChanged();
            d->updateGeometry();
        }
    }
}

bool QxItem::clip() const
{
    Q_D(const QxItem);
    return d->clip;
}

void QxItem::setClip(bool c)
{
    Q_D(QxItem);
    if (c == d->clip)
        return;

    d->clip = c;

    if (d->clipNode)
        d->updateClip();

    emit clipChanged();
}

void QxItemPrivate::updateClip()
{
    Q_Q(QxItem);

    if (clipNode) {
        if (clipNode->parent() != 0) {
            Q_ASSERT(clipNode);
            Q_ASSERT(clipNode->parent() == &transformNode);
            Q_ASSERT(clipNode->childCount() == 1);
            Q_ASSERT(clipNode->childAtIndex(0) == &groupNode);

            // Remove the clip node from the scene graph.
            transformNode.removeChildNode(clipNode);
            clipNode->removeChildNode(&groupNode);
            transformNode.prependChildNode(&groupNode);
        }
    } else {
        clipNode = new QxClipNode(QRectF(0, 0, 0, 0));
        clipNode->setFlag(Node::OwnedByParent, false);
    }

    if (clip) {
        QRectF clipRect = (localNode ? localNode->subtreeBoundingRect() : QRectF(0, 0, q->width(), q->height()));
        clipNode->setRect(clipRect);
        Q_ASSERT(clipNode->parent() == 0);
        Q_ASSERT(clipNode->childCount() == 0);

        // Wedge the clip node between the transform node and the group node.
        transformNode.removeChildNode(&groupNode);
        clipNode->appendChildNode(&groupNode);
        transformNode.prependChildNode(clipNode);
    }

    clipChanged(clip);
}

QPointF QxItem::pos() const
{
    Q_D(const QxItem);
    return d->position;
}

QSizeF QxItem::size() const
{
    return QSizeF(width(), height());
}

void QxItem::setPos(const QPointF &pos)
{
    Q_D(QxItem);

    if (d->position == pos)
        return;

    QRectF old = geometry();

    d->position = pos;

    d->geometryChanged(geometry(), old);

    if (old.x() != pos.x())
        emit xChanged();
    if (old.y() != pos.y())
        emit yChanged();

    d->updateGeometry();
}

void QxItem::setSize(const QSizeF &size)
{
    Q_D(QxItem);

    if (d->widthValid && d->heightValid && d->size == size)
        return;

    QRectF old = geometry();

    d->size = size;
    d->widthValid = true;
    d->heightValid = true;

    d->geometryChanged(geometry(), old);

    if (size.width() != old.width())
        emit widthChanged();
    if (size.height() != old.height())
        emit heightChanged();

    d->updateGeometry();
}

QxItem::TransformOrigin QxItem::transformOrigin() const
{
    Q_D(const QxItem);
    return d->transformOrigin;
}

void QxItem::setTransformOrigin(QxItem::TransformOrigin origin)
{
    Q_D(QxItem);
    if (d->transformOrigin == origin)
        return;

    d->transformOrigin = origin;

    bool update = d->scale != 1. || d->rotation != 0.;

    emit transformOriginChanged();

    if (update)
        d->updateGeometry();
}

qreal QxItem::renderOpacity() const
{
    Q_D(const QxItem);
    return d->renderOpacity;
}

qreal QxItem::opacity() const
{
    Q_D(const QxItem);
    return d->opacity;
}

void QxItem::setOpacity(qreal v)
{
    Q_D(QxItem);
    if (d->opacity == v)
        return;

    if (v != 0 && d->visible && !d->rootNode)
        d->attachTransformNode();
    else
        d->detachTransformNode();

    d->opacity = v;

    itemOpacityHasChanged();
    emit opacityChanged();

    d->updateRenderOpacity();
}

qreal QxItem::scale() const
{
    Q_D(const QxItem);
    return d->scale;
}

void QxItem::setScale(qreal v)
{
    Q_D(QxItem);
    if (d->scale == v)
        return;

    d->scale = v;

    emit scaleChanged();

    d->updateGeometry();
}

qreal QxItem::rotation() const
{
    Q_D(const QxItem);
    return d->rotation;
}

void QxItem::setRotation(qreal v)
{
    Q_D(QxItem);
    if (d->rotation == v)
        return;

    d->rotation = v;

    emit rotationChanged();

    d->updateGeometry();
}

QTransform QxItem::transform() const
{
    return QTransform();
}

void QxItem::setTransform(const QTransform &)
{
}

// This returns the value previously calculated in computeMatrixFromProperties().
QTransform QxItem::combinedTransform() const
{
    Q_D(const QxItem);
    return d->transform.toTransform();
}

void QxItem::stackBefore(QxItem *)
{
}

QPointF QxItem::transformOriginPoint() const
{
    Q_D(const QxItem);
    return d->transformOriginPoint();
}

QxItem *QxItem::parentItem() const
{
    Q_D(const QxItem);
    return d->parentItem;
}

void QxItem::setParentItem(QxItem *parent)
{
    Q_D(QxItem);

    if (parent == d->parentItem)
        return;

    if (d->parentItem)
        d->parentItem->itemChange(QGraphicsItem::ItemChildRemovedChange, QVariant::fromValue(this));

    d->setParentItem(parent);

    QxGraphicsView *parentView = parent?QxItemPrivate::get(parent)->view:0;
    if (parentView != d->view) {
        d->setViewRecursive(parentView);
    }

    if (d->parentItem) {
        d->parentItem->itemChange(QGraphicsItem::ItemChildAddedChange, QVariant::fromValue(this));
        d->updateRenderOpacity();
    }
    itemParentHasChanged();
}

QList<QxItem*> QxItem::childItems() const
{
    Q_D(const QxItem);

    QList<QxItem *> rv;

    QxItem *c = d->firstChild;
    while (c) {
        rv.prepend(c);
        c = QxItemPrivate::get(c)->nextSibling;
    }

    return rv;
}

Qt::MouseButtons QxItem::acceptedMouseButtons() const
{
    Q_D(const QxItem);
    return d->acceptedButtons;
}

void QxItem::setAcceptedMouseButtons(Qt::MouseButtons b)
{
    Q_D(QxItem);
    d->acceptedButtons = b;
}

void QxItem::setFiltersChildEvents(bool e)
{
    Q_D(QxItem);
    if (e == d->filtersChildMouse)
        return;

    if (!e) {
        if (d->isFilteringChildMouse)
            QxGraphicsViewPrivate::get(d->view)->mouseFilters.removeAll(this);
    }

    d->filtersChildMouse = e;
}

void QxItem::grabMouse()
{
    Q_D(QxItem);

    QxGraphicsView *view = d->view;
    if (!view && qApp)
        view = qobject_cast<QxGraphicsView *>(QApplication::focusWidget());

    QxGraphicsViewPrivate *v = QxGraphicsViewPrivate::get(view);

    if (QxItem *grabber = mouseGrabberItem()) {
        v->mouseFilters.clear();
        QEvent e(QEvent::UngrabMouse);
        grabber->sceneEvent(&e);
    }

    v->mouseGrabber = this;
}

void QxItem::ungrabMouse()
{
    Q_D(QxItem);

    if (this == mouseGrabberItem()) {
        QxGraphicsView *view = d->view;
        if (!view && qApp)
            view = qobject_cast<QxGraphicsView *>(QApplication::focusWidget());
        QxGraphicsViewPrivate *v = QxGraphicsViewPrivate::get(view);
        QxItem *grabber = v->mouseGrabber;
        v->mouseFilters.clear();
        QEvent e(QEvent::UngrabMouse);
        grabber->sceneEvent(&e);
        v->mouseGrabber = 0;
    }
}

QxItem *QxItem::mouseGrabberItem() const
{
    Q_D(const QxItem);
    QxGraphicsView *view = d->view;
    if (!view && qApp)
        view = qobject_cast<QxGraphicsView *>(QApplication::focusWidget());
    return view ? QxGraphicsViewPrivate::get(view)->mouseGrabber : 0;
}

static void children_append(QDeclarativeListProperty<QxItem> *p, QxItem *o)
{
    if (o) o->setParentItem(static_cast<QxItem *>(p->object));
}

QDeclarativeListProperty<QxItem> QxItem::children()
{
    return QDeclarativeListProperty<QxItem>(this, 0, children_append);
}

bool QxItem::hasFocus() const
{
    //XXX finish implementation
    Q_D(const QxItem);
    if (!d->view)
        return false;

    if (QxGraphicsViewPrivate::get(d->view)->focusItem != this)
        return false;

    return true;     //panel() == d_ptr->scene->d_func()->activePanel;
}

void QxItem::setFocus(Qt::FocusReason)
{
    //XXX finish implementation
    Q_D(QxItem);
    if (!d->focusItem)
        d->focusItem = this;

    QxItem *p = d->parentItem;
    while (p) {
        if (p->d_func()->isFocusScope) {
            p->d_func()->focusItem = this;
            if (!p->d_func()->inActiveFocusChain) {
                // If you call setFocus on a child of a focus scope that
                // doesn't currently have a focus item, then stop.
                return;
            }
            break;
        }
        p = p->d_func()->parentItem;
    }

    QxGraphicsView *view = d->view;
    if (!view && qApp)
        view = qobject_cast<QxGraphicsView *>(QApplication::focusWidget());

    if (!view)
        return;

    QxGraphicsViewPrivate::get(view)->setFocusItem(this);
}

void QxItem::clearFocus()
{
    //XXX finish implementation
    Q_D(QxItem);
    if (d->focusItem == this)
        d->focusItem = 0;
    d->inActiveFocusChain = false;

    QxItem *p = d->parentItem;
    while (p) {
        if (p->d_func()->isFocusScope) {
            p->d_func()->focusItem = p;
            break;
        }
        p = p->d_func()->parentItem;
    }

    if (!d->view)
        return;

    QxGraphicsViewPrivate *priv = QxGraphicsViewPrivate::get(d->view);
    if (priv->focusItem == this) {
        priv->setFocusItem(p && p->d_func()->isFocusScope ? p : 0);
    }
}

QxItem *QxItem::focusItem() const
{
    Q_D(const QxItem);
    return d->focusItem;
}

bool QxItem::isActive() const
{
    return false;
}

void QxItem::setActive(bool)
{
}

bool QxItem::isFocusScope() const
{
    return false;
}

bool QxItem::acceptHoverEvents() const
{
    return false;
}

void QxItem::setAcceptHoverEvents(bool)
{
}

bool QxItem::isUnderMouse() const
{
    return false;
}

void QxItemPrivate::updateRenderOpacity()
{
    Q_Q(QxItem);

    qreal ro = (parentItem?parentItem->d_func()->renderOpacity:1.) * opacity;

    if (ro != renderOpacity) {
        qreal old = renderOpacity;
        renderOpacity = ro;

        QxItem *c = firstChild;
        while (c) {
            c->d_func()->updateRenderOpacity();
            c = c->d_func()->nextSibling;
        }

        q->renderOpacityChanged(renderOpacity, old);
    }
}

QMatrix4x4 QxItemPrivate::computeMatrixFromProperties() const
{
    QMatrix4x4 m;
    if (position.x() != 0. || position.y() != 0.)
        m.translate(position.x(), position.y());
    for (int i = m_transformations.size() - 1; i >= 0; --i)
        m_transformations.at(i)->applyTo(&m);
    if (scale != 1. || rotation != 0.) {
        QPointF p = transformOriginPoint();
        m.translate(p.x(), p.y());
        if (scale != 1.)
            m.scale(scale, scale);
        if (rotation != 0)
            m.rotate(rotation, 0, 0, 1);
        m.translate(-p.x(), -p.y());
    }
    return m;
}

void QxItemPrivate::updateGeometry()
{
    Q_Q(QxItem);

    QMatrix4x4 m = computeMatrixFromProperties();
    if (m != transform) {
        transform = m;
        if (!effectRefCount)
            transformNode.setMatrix(m);
        q->transformChanged(m.toTransform(), transform.toTransform());
    }
}

int QxItemPrivate::transform_count(QDeclarativeListProperty<QGraphicsTransform> *list)
{
    return QxItemPrivate::get(static_cast<QxItem *>(list->object))->m_transformations.count();
}

void QxItemPrivate::transform_append(QDeclarativeListProperty<QGraphicsTransform> *list, QGraphicsTransform *item)
{
    QxItem *q = static_cast<QxItem *>(list->object);
    QxItemPrivate *d = QxItemPrivate::get(q);
    d->m_transformations.append(item);
    d->updateGeometry();

    if (qobject_cast<QGraphicsRotation *>(item)) {
        QGraphicsRotation *rotation = static_cast<QGraphicsRotation *>(item);
        QObject::connect(rotation, SIGNAL(angleChanged()), q, SLOT(updateTransform()));
        QObject::connect(rotation, SIGNAL(axisChanged()), q, SLOT(updateTransform()));
        QObject::connect(rotation, SIGNAL(originChanged()), q, SLOT(updateTransform()));
    } else if (qobject_cast<QGraphicsScale *>(item)) {
        QGraphicsScale *scale = static_cast<QGraphicsScale *>(item);
        QObject::connect(scale, SIGNAL(scaleChanged()), q, SLOT(updateTransform()));
        QObject::connect(scale, SIGNAL(originChanged()), q, SLOT(updateTransform()));
    } else if (qobject_cast<QDeclarativeTranslate *>(item)) {
        QDeclarativeTranslate *translate = static_cast<QDeclarativeTranslate *>(item);
        QObject::connect(translate, SIGNAL(xChanged()), q, SLOT(updateTransform()));
        QObject::connect(translate, SIGNAL(yChanged()), q, SLOT(updateTransform()));
    } else {
        qWarning("QxItemPrivate::transform_append: Unknown transform type %s.", item->metaObject()->className());
    }
}

QGraphicsTransform *QxItemPrivate::transform_at(QDeclarativeListProperty<QGraphicsTransform> *list, int index)
{
    const QVector<QGraphicsTransform *> &v = QxItemPrivate::get(static_cast<QxItem *>(list->object))->m_transformations;
    if (uint(index) < uint(v.count()))
        return v.at(index);
    else
        return 0;
}

void QxItemPrivate::transform_clear(QDeclarativeListProperty<QGraphicsTransform> *list)
{
    QxItem *q = static_cast<QxItem *>(list->object);
    QxItemPrivate *d = QxItemPrivate::get(q);

    for (int i = 0; i < d->m_transformations.size(); ++i) {
        QGraphicsTransform *item = d->m_transformations.at(i);
        QObject::disconnect(item, 0, q, 0);
    }

    d->m_transformations.clear();
    d->updateGeometry();
}

QDeclarativeListProperty<QGraphicsTransform> QxItem::transformations()
{
    return QDeclarativeListProperty<QGraphicsTransform>(this, 0, QxItemPrivate::transform_append,
                                                        QxItemPrivate::transform_count,
                                                        QxItemPrivate::transform_at,
                                                        QxItemPrivate::transform_clear);
}

QRectF QxItemPrivate::renderOrderBoundingRect() const
{
    Q_Q(const QxItem);
    return QRectF(0, 0, q->width(), q->height());
}

void QxItem::mousePressEvent(QGraphicsSceneMouseEvent *)
{
}

void QxItem::mouseMoveEvent(QGraphicsSceneMouseEvent *)
{
}

void QxItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *)
{
}

void QxItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *)
{
}

void QxItem::wheelEvent(QGraphicsSceneWheelEvent *)
{
}

bool QxItem::sceneEventFilter(QxItem *, QEvent *)
{
    return false;
}

void QxItem::hoverEnterEvent(QGraphicsSceneHoverEvent *)
{
}

void QxItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *)
{
}

void QxItem::focusInEvent(QFocusEvent *)
{
}

void QxItem::focusOutEvent(QFocusEvent *)
{
}

Node *QxItem::paintNode() const
{
    Q_D(const QxItem);
    return d->localNode;
}

void QxItem::setPaintNode(Node *node)
{
    Q_D(QxItem);
    if (d->localNode != node) {
        if (d->localNode) {
            // Let the new node replace the old.
            if (node)
                d->groupNode.insertChildNodeAfter(node, d->localNode);
            d->groupNode.removeChildNode(d->localNode);
        } else if (node) {
            // Insert the node into the right location.

            Node *after = 0;
            qreal currentZ = 0;

            QxItem *child = d->lastChild;
            while (child) {
                QxItemPrivate *cd = QxItemPrivate::get(child);
                // Avoid comparing with items not currently in the scene graph.
                if (cd->transformNodeAttached) {
                    if (0 > cd->zValue && (after == 0 || cd->zValue >= currentZ)) {
                        after = &cd->transformNode;
                        currentZ = cd->zValue;
                    }
                }
                child = cd->prevSibling;
            }

            if (after)
                d->groupNode.insertChildNodeAfter(node, after);
            else
                d->groupNode.prependChildNode(node);
        }
        d->localNode = node;
    }
    if (d->clip && node)
        d->clipNode->setRect(node->subtreeBoundingRect());
}

bool QxItem::childrenDoNotOverlap() const
{
    Q_D(const QxItem);
    return d->childrenDoNotOverlap();
}

void QxItem::setChildrenDoNotOverlap(bool noOverlap)
{
    Q_D(QxItem);
    d->setChildrenDoNotOverlap(noOverlap);
}

void QxItem::updateTransform()
{
    Q_D(QxItem);
    d->updateGeometry();
}

void QxItemPrivate::setViewRecursive(QxGraphicsView *v)
{
    Q_Q(QxItem);
    view = v;
    dirtyInView = false;

    if (view && !QxGraphicsViewPrivate::get(view)->focusItem && focusItem)
        q->setFocus(Qt::OtherFocusReason);

    QxItem *c = firstChild;
    while (c) {
        QxItemPrivate *cp = QxItemPrivate::get(c);
        cp->setViewRecursive(v);
        c = cp->nextSibling;
    }
}

// Returns the transform that maps coordinates from local item space to the global space
QTransform QxItemPrivate::mapToGlobalTransform() const
{
    QTransform t = parentItem?QxItemPrivate::get(parentItem)->mapToGlobalTransform():QTransform();
    QMatrix4x4 m = computeMatrixFromProperties();

    return m.toTransform() * t;
}

// Returns the transform that maps coordinates from global space to local item space
QTransform QxItemPrivate::mapFromGlobalTransform(bool *ok) const
{
    QTransform t = mapToGlobalTransform();

    if (t.isInvertible()) {
        if (ok) *ok = true;
        return t.inverted();
    } else {
        if (ok) *ok = false;
        return QTransform();
    }
}

QPointF QxItemPrivate::transformOriginPoint() const
{
    qreal w = widthValid?size.width():implicitSize.width();
    qreal h = heightValid?size.height():implicitSize.height();

    switch(transformOrigin) {
    default:
    case QxItem::TopLeft:
        return QPointF(0, 0);
    case QxItem::Top:
        return QPointF(w / 2., 0);
    case QxItem::TopRight:
        return QPointF(w, 0);
    case QxItem::Left:
        return QPointF(0, h / 2.);
    case QxItem::Center:
        return QPointF(w / 2., h / 2.);
    case QxItem::Right:
        return QPointF(w, h / 2.);
    case QxItem::BottomLeft:
        return QPointF(0, h);
    case QxItem::Bottom:
        return QPointF(w / 2., h);
    case QxItem::BottomRight:
        return QPointF(w, h);
    }
}

void QxItemPrivate::attachTransformNode()
{
#if 0
    // Print QxItem tree.
    {
        QxItem *q = q_func();
        while (q->d_func()->parentItem)
            q = q->d_func()->parentItem;
        QByteArray spaces;
        while (q) {
            QxItemPrivate *d = q->d_func();
            qDebug("%s%s at %p", spaces.constData(), q->metaObject()->className(), q);
            if (d->lastChild) {
                spaces.append("  ");
                q = d->lastChild;
            } else {
                do {
                    d = q->d_func();
                    if (d->prevSibling) {
                        q = d->prevSibling;
                        break;
                    }
                    q = d->parentItem;
                    spaces.chop(2);
                } while (q);
            }
        }
    }
#endif

    if (parentItem && !transformNodeAttached) {
        QxItemPrivate *pd = QxItemPrivate::get(parentItem);

        Node *after = 0;
        qreal currentZ = 0;

        QxItem *child = nextSibling;
        while (child) {
            QxItemPrivate *cd = QxItemPrivate::get(child);
            // Avoid comparing items not currently in the scene graph.
            if (cd->transformNodeAttached) {
                if (zValue >= cd->zValue && (after == 0 || cd->zValue > currentZ)) {
                    after = &cd->transformNode;
                    currentZ = cd->zValue;
                }
            }
            child = cd->nextSibling;
        }

        if (pd->localNode && zValue >= 0 && (after == 0 || 0 > currentZ)) {
            after = pd->localNode;
            currentZ = 0;
        }

        child = prevSibling;
        while (child) {
            QxItemPrivate *cd = QxItemPrivate::get(child);
            // Avoid comparing items not currently in the scene graph.
            if (cd->transformNodeAttached) {
                if (zValue > cd->zValue && (after == 0 || cd->zValue >= currentZ)) {
                    after = &cd->transformNode;
                    currentZ = cd->zValue;
                }
            }
            child = cd->prevSibling;
        }

        if (after)
            pd->groupNode.insertChildNodeAfter(&transformNode, after);
        else
            pd->groupNode.prependChildNode(&transformNode);

        transformNodeAttached = true;
    }
}

void QxItemPrivate::detachTransformNode()
{
    if (parentItem && transformNodeAttached)
        QxItemPrivate::get(parentItem)->groupNode.removeChildNode(&transformNode);
    transformNodeAttached = false;
}

void QxItemPrivate::refFromEffectItem()
{
    Q_ASSERT((effectRefCount == 0) == (rootNode == 0));

    if (effectRefCount == 0) {
        detachTransformNode();

        Q_ASSERT(!transformNode.parent());

        rootNode = new RootNode;
        rootNode->appendChildNode(&transformNode);
        transformNode.setMatrix(QMatrix4x4());
    }
    ++effectRefCount;
}

void QxItemPrivate::derefFromEffectItem()
{
    Q_ASSERT(rootNode && effectRefCount);
    Q_ASSERT(rootNode->childCount() == 1 && rootNode->childAtIndex(0) == &transformNode);

    --effectRefCount;
    if (effectRefCount == 0) {
        rootNode->removeChildNode(&transformNode);
        delete rootNode;
        rootNode = 0;
        transformNode.setMatrix(transform);

        if (opacity != 0 && visible)
            attachTransformNode();
    }
}

void QxItemPrivate::setParentItem(QxItem *p)
{
    if (parentItem) {
        detachTransformNode();

        QxItem **prevsNext = prevSibling
            ? &QxItemPrivate::get(prevSibling)->nextSibling
            : &QxItemPrivate::get(parentItem)->firstChild;
        QxItem **nextsPrev = nextSibling
            ? &QxItemPrivate::get(nextSibling)->prevSibling
            : &QxItemPrivate::get(parentItem)->lastChild;

        *prevsNext = nextSibling;
        *nextsPrev = prevSibling;
        nextSibling = 0;
        prevSibling = 0;
    }

    parentItem = p;

    if (parentItem) {
        Q_Q(QxItem);
        if (!view && parentItem->d_func()->isFocusScope) {
            if ((!parentItem->d_func()->focusItem || parentItem->d_func()->focusItem == parentItem) && focusItem)
                parentItem->d_func()->focusItem = q;
        }

        nextSibling = QxItemPrivate::get(parentItem)->firstChild;
        QxItem **nextsPrev = nextSibling
            ? &QxItemPrivate::get(nextSibling)->prevSibling
            : &QxItemPrivate::get(parentItem)->lastChild;

        QxItemPrivate::get(parentItem)->firstChild = *nextsPrev = q;

        if (opacity != 0 && visible && !rootNode)
            attachTransformNode();
    }
}

void QxItemPrivate::geometryChanged(const QRectF &ng, const QRectF &og)
{
    Q_Q(QxItem);

    q->geometryChanged(ng, og);

    if (clipNode && !localNode)
        clipNode->setRect(QRectF(0, 0, ng.width(), ng.height()));
}

QxItem::QxItem(QxItem* parent)
: QObject(*(new QxItemPrivate), parent)
{
#ifndef QT_NO_DEBUG
    qt_inc_graphics_object_count();
#endif

    Q_D(QxItem);
    d->init(parent);
}

QxItem::QxItem(QxItemPrivate &dd, QxItem *parent)
: QObject(dd, parent)
{
#ifndef QT_NO_DEBUG
    qt_inc_graphics_object_count();
#endif

    Q_D(QxItem);
    d->init(parent);
}

QxItem::~QxItem()
{
#ifndef QT_NO_DEBUG
    --qt_graphics_object_count;
    if (qt_graphics_object_count < 0)
        qDebug("Graphics object destroyed after qt_print_graphics_object_count() was called.");
#endif

    Q_D(QxItem);
    for (int ii = 0; ii < d->changeListeners.count(); ++ii) {
        QxAnchorsPrivate *anchor = d->changeListeners.at(ii).listener->anchorPrivate();
        if (anchor)
            anchor->clearItem(this);
    }

    if (d->parentItem) {
        for (int ii = 0; ii < d->changeListeners.count(); ++ii) {
            QxAnchorsPrivate *anchor = d->changeListeners.at(ii).listener->anchorPrivate();
            if (anchor && anchor->item && anchor->item->parentItem() != this) //child will be deleted anyway
                anchor->updateOnComplete();
        }
    }
    for(int ii = 0; ii < d->changeListeners.count(); ++ii) {
        const QxItemPrivate::ChangeListener &change = d->changeListeners.at(ii);
        if (change.types & QxItemPrivate::Destroyed)
            change.listener->itemDestroyed(this);
    }

    d->changeListeners.clear();
    delete d->_anchorLines; d->_anchorLines = 0;
    delete d->_anchors; d->_anchors = 0;
    delete d->_stateGroup; d->_stateGroup = 0;
    delete d->_contents; d->_contents = 0;

    if (d->parentItem)
        d->setParentItem(0);
    QxItem *c = d->firstChild;
    while (c) {
        QxItem *next = c->d_func()->nextSibling;
        c->d_func()->parentItem = c->d_func()->nextSibling = c->d_func()->prevSibling = 0;
        c = next;
    }
    if (d->clipNode)
        delete d->clipNode;
    if (d->rootNode)
        delete d->rootNode;
}

bool QxItem::isComponentComplete() const
{
    Q_D(const QxItem);
    return d->_componentComplete;
}

void QxItemPrivate::data_append(QDeclarativeListProperty<QObject> *prop, QObject *o)
{
    if (!o)
        return;

    QxItem *that = static_cast<QxItem *>(prop->object);

    // This test is measurably (albeit only slightly) faster than qobject_cast<>()
    const QMetaObject *mo = o->metaObject();
    while (mo && mo != &QxItem::staticMetaObject) {
        // XXX akennedy
        if (mo == &QDeclarativeItem::staticMetaObject) {
            qWarning("Attempted to add a declarative item - %s", o->metaObject()->className());

            const QMetaObject *meta = o->metaObject();
            while (meta) {
                qWarning(" - %s", meta->className());
                meta = meta->d.superdata;
            }

        }
        mo = mo->d.superdata;
    }

    if (mo) {
        static_cast<QxItem *>(o)->setParentItem(that);
    } else {
        o->setParent(that);
    }
}

QObject *QxItemPrivate::resources_at(QDeclarativeListProperty<QObject> *prop, int index)
{
    QObjectList children = prop->object->children();
    if (index < children.count())
        return children.at(index);
    else
        return 0;
}

void QxItemPrivate::resources_append(QDeclarativeListProperty<QObject> *prop, QObject *o)
{
    o->setParent(prop->object);
}

int QxItemPrivate::resources_count(QDeclarativeListProperty<QObject> *prop)
{
    return prop->object->children().count();
}

void QxItemPrivate::parentProperty(QObject *o, void *rv, QDeclarativeNotifierEndpoint *e)
{
    QxItem *item = static_cast<QxItem*>(o);
    if (e)
        e->connect(&item->d_func()->parentNotifier);
    *((QxItem **)rv) = item->parentItem();
}

QDeclarativeListProperty<QObject> QxItemPrivate::data()
{
    return QDeclarativeListProperty<QObject>(q_func(), 0, QxItemPrivate::data_append);
}

QRectF QxItem::childrenRect()
{
    Q_D(QxItem);
    if (!d->_contents) {
        d->_contents = new QxContents;
        d->_contents->setItem(this);
    }
    return d->_contents->rectF();
}

void QxItem::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    Q_D(QxItem);

    if (d->_anchors)
        d->_anchors->d_func()->updateMe();

    // XXX akennedy
    /*
    if (transformOrigin() != QxItem::TopLeft
        && (newGeometry.width() != oldGeometry.width() || newGeometry.height() != oldGeometry.height())) {
        if (d->transformData) {
            QPointF origin = d->computeTransformOrigin();
            if (transformOriginPoint() != origin)
                setTransformOriginPoint(origin);
        } else {
            d->transformOriginDirty = true;
        }
    }
    */

    for(int ii = 0; ii < d->changeListeners.count(); ++ii) {
        const QxItemPrivate::ChangeListener &change = d->changeListeners.at(ii);
        if (change.types & QxItemPrivate::Geometry)
            change.listener->itemGeometryChanged(this, newGeometry, oldGeometry);
    }
}

void QxItem::renderOpacityChanged(qreal newOpacity, qreal oldOpacity)
{
    Q_UNUSED(newOpacity)
    Q_UNUSED(oldOpacity)
}

void QxItem::transformChanged(const QTransform &newTransform, const QTransform &oldTransform)
{
    Q_UNUSED(newTransform)
    Q_UNUSED(oldTransform)
}

void QxItemPrivate::removeItemChangeListener(QxItemChangeListener *listener, ChangeTypes types)
{
    ChangeListener change(listener, types);
    changeListeners.removeOne(change);
}

void QxItem::keyPressEvent(QKeyEvent *event)
{
    Q_D(QxItem);
    keyPressPreHandler(event);
    if (event->isAccepted())
        return;
    if (d->keyHandler)
        d->keyHandler->keyPressed(event, true);
    else
        event->ignore();
}

void QxItem::keyReleaseEvent(QKeyEvent *event)
{
    Q_D(QxItem);
    keyReleasePreHandler(event);
    if (event->isAccepted())
        return;
    if (d->keyHandler)
        d->keyHandler->keyReleased(event, true);
    else
        event->ignore();
}

void QxItem::inputMethodEvent(QInputMethodEvent *event)
{
    Q_D(QxItem);
    inputMethodPreHandler(event);
    if (event->isAccepted())
        return;
    if (d->keyHandler)
        d->keyHandler->inputMethodEvent(event, true);
    else
        event->ignore();
}

QVariant QxItem::inputMethodQuery(Qt::InputMethodQuery query) const
{
    Q_UNUSED(query)
    QVariant v;
    // XXX akennedy
    /*
    Q_D(const QxItem);
    if (d->keyHandler)
        v = d->keyHandler->inputMethodQuery(query);

    if (!v.isValid())
        v = QGraphicsObject::inputMethodQuery(query);
    */

    return v;
}

void QxItem::keyPressPreHandler(QKeyEvent *event)
{
    Q_D(QxItem);
    if (d->keyHandler && !d->doneEventPreHandler)
        d->keyHandler->keyPressed(event, false);
    else
        event->ignore();
    d->doneEventPreHandler = true;
}

void QxItem::keyReleasePreHandler(QKeyEvent *event)
{
    Q_D(QxItem);
    if (d->keyHandler && !d->doneEventPreHandler)
        d->keyHandler->keyReleased(event, false);
    else
        event->ignore();
    d->doneEventPreHandler = true;
}

void QxItem::inputMethodPreHandler(QInputMethodEvent *event)
{
    Q_D(QxItem);
    if (d->keyHandler && !d->doneEventPreHandler)
        d->keyHandler->inputMethodEvent(event, false);
    else
        event->ignore();
    d->doneEventPreHandler = true;
}

QxAnchorLine QxItemPrivate::left() const
{
    return anchorLines()->left;
}

QxAnchorLine QxItemPrivate::right() const
{
    return anchorLines()->right;
}

QxAnchorLine QxItemPrivate::horizontalCenter() const
{
    return anchorLines()->hCenter;
}

QxAnchorLine QxItemPrivate::top() const
{
    return anchorLines()->top;
}

QxAnchorLine QxItemPrivate::bottom() const
{
    return anchorLines()->bottom;
}

QxAnchorLine QxItemPrivate::verticalCenter() const
{
    return anchorLines()->vCenter;
}

QxAnchorLine QxItemPrivate::baseline() const
{
    return anchorLines()->baseline;
}

qreal QxItem::baselineOffset() const
{
    Q_D(const QxItem);
    if (!d->_baselineOffset.isValid()) {
        return 0.0;
    } else
        return d->_baselineOffset;
}

void QxItem::setBaselineOffset(qreal offset)
{
    Q_D(QxItem);
    if (offset == d->_baselineOffset)
        return;

    d->_baselineOffset = offset;

    for(int ii = 0; ii < d->changeListeners.count(); ++ii) {
        const QxItemPrivate::ChangeListener &change = d->changeListeners.at(ii);
        if (change.types & QxItemPrivate::Geometry) {
            QxAnchorsPrivate *anchor = change.listener->anchorPrivate();
            if (anchor)
                anchor->updateVerticalAnchors();
        }
    }
    emit baselineOffsetChanged(offset);
}

bool QxItem::keepMouseGrab() const
{
    Q_D(const QxItem);
    return d->_keepMouse;
}

void QxItem::setKeepMouseGrab(bool keep)
{
    Q_D(QxItem);
    d->_keepMouse = keep;
}

QScriptValue QxItem::mapFromItem(const QScriptValue &item, qreal x, qreal y) const
{
    QScriptValue sv = QDeclarativeEnginePrivate::getScriptEngine(qmlEngine(this))->newObject();
    QxItem *itemObj = qobject_cast<QxItem*>(item.toQObject());
    if (!itemObj && !item.isNull()) {
        qmlInfo(this) << "mapFromItem() given argument \"" << item.toString() << "\" which is neither null nor an Item";
        return 0;
    }

    // If QGraphicsItem::mapFromItem() is called with 0, behaves the same as mapFromScene()
    QPointF p = QxItem::mapFromItem(itemObj, QPointF(x, y));
    sv.setProperty(QLatin1String("x"), p.x());
    sv.setProperty(QLatin1String("y"), p.y());
    return sv;
}

QScriptValue QxItem::mapToItem(const QScriptValue &item, qreal x, qreal y) const
{
    QScriptValue sv = QDeclarativeEnginePrivate::getScriptEngine(qmlEngine(this))->newObject();
    QxItem *itemObj = qobject_cast<QxItem*>(item.toQObject());
    if (!itemObj && !item.isNull()) {
        qmlInfo(this) << "mapToItem() given argument \"" << item.toString() << "\" which is neither null nor an Item";
        return 0;
    }

    // If QGraphicsItem::mapToItem() is called with 0, behaves the same as mapToScene()
    QPointF p = QxItem::mapToItem(itemObj, QPointF(x, y));
    sv.setProperty(QLatin1String("x"), p.x());
    sv.setProperty(QLatin1String("y"), p.y());
    return sv;
}

QxItem *QxItem::childAt(qreal x, qreal y) const
{
    // Copied from QDeclarativeItem
    const QList<QxItem *> children = childItems();
    for (int i = children.count() - 1; i >= 0; --i) {
        if (QxItem *child = children.at(i)) {
            if (child->isVisible() && child->x() <= x
                && child->x() + child->width() >= x
                && child->y() <= y
                && child->y() + child->height() >= y)
                return child;
        }
    }
    return 0;
}

void QxItem::forceFocus()
{
    setFocus(true);
    QxItem *parent = parentItem();
    while (parent) {
        if (parent->d_func()->isFocusScope)
            parent->setFocus(true);
        parent = parent->parentItem();
    }
}

void QxItemPrivate::focusChanged(bool flag)
{
    Q_Q(QxItem);
    emit q->focusChanged(flag);
}

/*! \internal */
QDeclarativeListProperty<QObject> QxItemPrivate::resources()
{
    return QDeclarativeListProperty<QObject>(q_func(), 0, QxItemPrivate::resources_append,
                                             QxItemPrivate::resources_count,
                                             QxItemPrivate::resources_at);
}

QDeclarativeListProperty<QDeclarativeState> QxItemPrivate::states()
{
    return _states()->statesProperty();
}

QDeclarativeListProperty<QDeclarativeTransition> QxItemPrivate::transitions()
{
    return _states()->transitionsProperty();
}

QString QxItemPrivate::state() const
{
    if (!_stateGroup)
        return QString();
    else
        return _stateGroup->state();
}

void QxItemPrivate::setState(const QString &state)
{
    _states()->setState(state);
}

void QxItem::classBegin()
{
    Q_D(QxItem);
    d->_componentComplete = false;
    if (d->_stateGroup)
        d->_stateGroup->classBegin();
    if (d->_anchors)
        d->_anchors->classBegin();
}

void QxItem::componentComplete()
{
    Q_D(QxItem);
    d->_componentComplete = true;
    if (d->_stateGroup)
        d->_stateGroup->componentComplete();
    if (d->_anchors) {
        d->_anchors->componentComplete();
        d->_anchors->d_func()->updateOnComplete();
    }
    if (d->keyHandler)
        d->keyHandler->componentComplete();
    if (d->clip)
        d->updateClip();
}

QDeclarativeStateGroup *QxItemPrivate::_states()
{
    Q_Q(QxItem);
    if (!_stateGroup) {
        _stateGroup = new QDeclarativeStateGroup;
        if (!_componentComplete)
            _stateGroup->classBegin();
        QObject::connect(_stateGroup, SIGNAL(stateChanged(QString)),
                         q, SIGNAL(stateChanged(QString)));
    }

    return _stateGroup;
}

QxItemPrivate::AnchorLines::AnchorLines(QxItem *q)
{
    left.item = q;
    left.anchorLine = QxAnchorLine::Left;
    right.item = q;
    right.anchorLine = QxAnchorLine::Right;
    hCenter.item = q;
    hCenter.anchorLine = QxAnchorLine::HCenter;
    top.item = q;
    top.anchorLine = QxAnchorLine::Top;
    bottom.item = q;
    bottom.anchorLine = QxAnchorLine::Bottom;
    vCenter.item = q;
    vCenter.anchorLine = QxAnchorLine::VCenter;
    baseline.item = q;
    baseline.anchorLine = QxAnchorLine::Baseline;
}

bool QxItem::sceneEvent(QEvent *event)
{
    Q_UNUSED(event)
    // XXX akennedy
    /*
    Q_D(QxItem);
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *k = static_cast<QKeyEvent *>(event);
        if ((k->key() == Qt::Key_Tab || k->key() == Qt::Key_Backtab) &&
            !(k->modifiers() & (Qt::ControlModifier | Qt::AltModifier))) {
            keyPressEvent(static_cast<QKeyEvent *>(event));
            if (!event->isAccepted())
                return QGraphicsItem::sceneEvent(event);
            else
                return true;
        } else {
            return QGraphicsItem::sceneEvent(event);
        }
    }
    */
    return true;
}

QVariant QxItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
    Q_UNUSED(change)
    Q_UNUSED(value)
    // XXX akennedy
    /*
    Q_D(QxItem);
    switch (change) {
    case ItemParentHasChanged:
        emit parentChanged(parentItem());
        d->parentNotifier.notify();
        break;
    case ItemVisibleHasChanged: {
            for(int ii = 0; ii < d->changeListeners.count(); ++ii) {
                const QxItemPrivate::ChangeListener &change = d->changeListeners.at(ii);
                if (change.types & QxItemPrivate::Visibility) {
                    change.listener->itemVisibilityChanged(this);
                }
            }
        }
        break;
    case ItemOpacityHasChanged: {
            for(int ii = 0; ii < d->changeListeners.count(); ++ii) {
                const QxItemPrivate::ChangeListener &change = d->changeListeners.at(ii);
                if (change.types & QxItemPrivate::Opacity) {
                    change.listener->itemOpacityChanged(this);
                }
            }
        }
        break;
    case ItemChildAddedChange:
        if (d->_contents)
            d->_contents->childAdded(qobject_cast<QxItem*>(
                    value.value<QGraphicsItem*>()));
        break;
    case ItemChildRemovedChange:
        if (d->_contents)
            d->_contents->childRemoved(qobject_cast<QxItem*>(
                    value.value<QGraphicsItem*>()));
        break;
    default:
        break;
    }

    return QGraphicsItem::itemChange(change, value);
    */
    return QVariant();
}

void QxItem::itemParentHasChanged()
{
    emit parentChanged(parentItem());
}

void QxItem::itemOpacityHasChanged()
{
    Q_D(QxItem);
    for(int ii = 0; ii < d->changeListeners.count(); ++ii) {
        const QxItemPrivate::ChangeListener &change = d->changeListeners.at(ii);
        if (change.types & QxItemPrivate::Opacity) {
            change.listener->itemOpacityChanged(this);
        }
    }
}

void QxItem::itemVisibleHasChanged()
{
    Q_D(QxItem);
    for(int ii = 0; ii < d->changeListeners.count(); ++ii) {
        const QxItemPrivate::ChangeListener &change = d->changeListeners.at(ii);
        if (change.types & QxItemPrivate::Visibility) {
            change.listener->itemVisibilityChanged(this);
        }
    }
}

QRectF QxItem::boundingRect() const
{
    return QRectF(0, 0, width(), height());
}

bool QxItem::smooth() const
{
    Q_D(const QxItem);
    return d->smooth;
}

void QxItem::setSmooth(bool smooth)
{
    Q_D(QxItem);
    if (d->smooth == smooth)
        return;

    bool oldSmooth = d->smooth;

    d->smooth = smooth;

    smoothChange(d->smooth, oldSmooth);

    emit smoothChanged(smooth);
}

bool QxItem::wantsFocus() const
{
    Q_D(const QxItem);
    return d->inActiveFocusChain;
}

void QxItem::setFocus(bool focus)
{
    if (focus)
        setFocus(Qt::OtherFocusReason);
    else
        clearFocus();
}

bool QxItem::event(QEvent *ev)
{
    Q_D(QxItem);
    switch (ev->type()) {
    case QEvent::KeyPress:
        keyPressEvent((QKeyEvent*)ev);
        d->doneEventPreHandler = false;
        break;
    case QEvent::KeyRelease:
        keyReleaseEvent((QKeyEvent*)ev);
        d->doneEventPreHandler = false;
        break;
    case QEvent::InputMethod:
        d->doneEventPreHandler = false;
        break;
    case QEvent::FocusIn:
    case QEvent::FocusOut:
        d->focusChanged(hasFocus());
        break;
    case QEvent::GraphicsSceneMousePress:
        mousePressEvent(static_cast<QGraphicsSceneMouseEvent *>(ev));
        break;
    case QEvent::GraphicsSceneMouseRelease:
        mouseReleaseEvent(static_cast<QGraphicsSceneMouseEvent *>(ev));
        break;
    case QEvent::GraphicsSceneMouseMove:
        mouseMoveEvent(static_cast<QGraphicsSceneMouseEvent *>(ev));
        break;
    default:
        break;
    }

    return QObject::event(ev);
}

QDebug operator<<(QDebug debug, QxItem *item)
{
    if (!item) {
        debug << "QxItem(0)";
        return debug;
    }

    debug << item->metaObject()->className() << "(this =" << ((void*)item)
          << ", parent =" << ((void*)item->parentItem())
          << ", geometry =" << QRectF(item->pos(), QSizeF(item->width(), item->height()))
          << ", z =" << item->zValue() << ')';
    return debug;
}

qint64 QxItemPrivate::consistentTime = -1;
void QxItemPrivate::setConsistentTime(qint64 t)
{
    consistentTime = t;
}

class QElapsedTimerConsistentTimeHack
{
public:
    void start() {
        t1 = QxItemPrivate::consistentTime;
        t2 = 0;
    }
    qint64 elapsed() {
        return QxItemPrivate::consistentTime - t1;
    }
    qint64 restart() {
        qint64 val = QxItemPrivate::consistentTime - t1;
        t1 = QxItemPrivate::consistentTime;
        t2 = 0;
        return val;
    }

private:
    qint64 t1;
    qint64 t2;
};

void QxItemPrivate::start(QElapsedTimer &t)
{
    if (QxItemPrivate::consistentTime == -1)
        t.start();
    else
        ((QElapsedTimerConsistentTimeHack*)&t)->start();
}

qint64 QxItemPrivate::elapsed(QElapsedTimer &t)
{
    if (QxItemPrivate::consistentTime == -1)
        return t.elapsed();
    else
        return ((QElapsedTimerConsistentTimeHack*)&t)->elapsed();
}

qint64 QxItemPrivate::restart(QElapsedTimer &t)
{
    if (QxItemPrivate::consistentTime == -1)
        return t.restart();
    else
        return ((QElapsedTimerConsistentTimeHack*)&t)->restart();
}

void QxItem::smoothChange(bool newSmooth, bool oldSmooth)
{
    Q_UNUSED(newSmooth)
    Q_UNUSED(oldSmooth)
}

#include <moc_qxitem.cpp>
