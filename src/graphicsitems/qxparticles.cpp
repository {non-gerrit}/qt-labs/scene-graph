/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxparticles_p.h"
#include "nodes/qxparticlenode_p.h"

#include <qdeclarativeinfo.h>
#include "qxitem_p.h"

#include <private/qdeclarativepixmapcache_p.h>
#include "qxvisualitemmodel_p.h"
#include <QtCore/QAbstractAnimation>

#include <QPainter>
#include <QtGui/qdrawutil.h>
#include <QVarLengthArray>

#include <stdlib.h>
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#define M_PI_2 (M_PI / 2.)
#endif
#ifndef INT_MAX
#define INT_MAX 2147483647
#endif

#define PI_SQR 9.8696044
// parabolic approximation
inline qreal fastSin(qreal theta)
{
    const qreal b = 4 / M_PI;
    const qreal c = -4 / PI_SQR;

    qreal y = b * theta + c * theta * qAbs(theta);
    return y;
}

inline qreal fastCos(qreal theta)
{
    theta += M_PI_2;
    if (theta > M_PI)
        theta -= 2 * M_PI;

    return fastSin(theta);
}

//---------------------------------------------------------------------------

/*!
    \class QxParticleMotion
    \ingroup group_effects
    \brief The QxParticleMotion class is the base class for particle motion.
    \internal

    This class causes the particles to remain static.
*/

/*!
    Constructs a QxParticleMotion with parent object \a parent.
*/
QxParticleMotion::QxParticleMotion(QObject *parent) :
    QObject(parent)
{
}

/*!
    Move the \a particle to its new position.  \a interval is the number of
    milliseconds elapsed since it was last moved.
*/
void QxParticleMotion::advance(QxParticle &particle, int interval)
{
    Q_UNUSED(particle);
    Q_UNUSED(interval);
}

/*!
    The \a particle has just been created.  Some motion strategies require
    additional state information.  This can be allocated by this function.
*/
void QxParticleMotion::created(QxParticle &particle)
{
    Q_UNUSED(particle);
}

/*!
    The \a particle is about to be destroyed.  Any additional memory
    that has been allocated for the particle should be freed.
*/
void QxParticleMotion::destroy(QxParticle &particle)
{
    Q_UNUSED(particle);
}

/*!
    \qmlclass ParticleMotionLinear
    \since 4.7
    \brief The ParticleMotionLinear object moves particles linearly.

    \sa Particles
*/

/*!
    \internal
    \class QxParticleMotionLinear
    \ingroup group_effects
    \brief The QxParticleMotionLinear class moves the particles linearly.
*/

void QxParticleMotionLinear::advance(QxParticle &p, int interval)
{
    p.x += interval * p.x_velocity;
    p.y += interval * p.y_velocity;
    if (p.item) {
        qreal offX = p.item->parentItem()->x() + p.item->parentItem()->parentItem()->x();
        qreal offY = p.item->parentItem()->y() + p.item->parentItem()->parentItem()->y();
        p.item->setX(p.x - offX /*+ p.item->width()*/);
        p.item->setY(p.y - offY /*+ p.item->height()*/);
    }
}

///////////////////////////////////////////////////
void QxParticleMotionRandomDrift::advance(QxParticle &p, int interval)
{
    p.x_velocity += ( qreal(qrand())/RAND_MAX - 0.5 ) * _xDrift * qreal(interval) / 1000;
    p.y_velocity += ( qreal(qrand())/RAND_MAX - 0.5 ) * _yDrift * qreal(interval) / 1000;
}

void QxParticleMotionAcceleration::advance(QxParticle &p, int interval)
{
    p.x_velocity += _xAccel * qreal(interval) / 1000;
    p.y_velocity += _yAccel * qreal(interval) / 1000;
}

void QxParticleMotionFriction::advance(QxParticle &p, int interval)
{
    qreal len2 = p.x_velocity * p.x_velocity + p.y_velocity * p.y_velocity;
    if (qFuzzyIsNull(len2))
        return;
    qreal scale = 1 - (_friction * qreal(interval) / 1000) / ( ::sqrt( len2 ) /* * p.mass*/ );
    if (scale < 0) {
        p.x_velocity = 0;
        p.y_velocity = 0;
    } else {
        p.x_velocity *= scale;
        p.y_velocity *= scale;
    }
}

void QxParticleMotionGravityWell::advance(QxParticle &p, int interval)
{
    qreal xdiff = _xAttr - p.x;
    qreal ydiff = _yAttr - p.y;
    qreal diffsq = xdiff * xdiff + ydiff * ydiff;
    if (diffsq == 0)
        return;
    qreal diff = ::sqrt(diffsq);
    qreal epsq = _epsilon * _epsilon;
    if (diffsq < epsq)
        diffsq = epsq;
    qreal factor = (_force * interval) / (diffsq * diff);

    p.x_velocity += xdiff * factor;
    p.y_velocity += ydiff * factor;
}

void QxParticleKillZone::advance(QxParticle &p, int interval)
{
    Q_UNUSED(interval)
    if (!_area.isEmpty() && _area.contains(p.x, p.y))
        p.birthTime -= 100000;   //###
}
///////////////////////////////////////////////////

/*!
    \qmlclass ParticleMotionGravity
    \since 4.7
    \brief The ParticleMotionGravity object moves particles towards a point.

    \sa Particles
*/

/*!
    \internal
    \class QxParticleMotionGravity
    \ingroup group_effects
    \brief The QxParticleMotionGravity class moves the particles towards a point.
*/

/*!
    \qmlproperty real ParticleMotionGravity::xattractor
    \qmlproperty real ParticleMotionGravity::yattractor
    These properties hold the x and y coordinates of the point attracting the particles.
*/

/*!
    \qmlproperty real ParticleMotionGravity::acceleration
    This property holds the acceleration to apply to the particles.
*/

/*!
    \property QxParticleMotionGravity::xattractor
    \brief the x coordinate of the point attracting the particles.
*/

/*!
    \property QxParticleMotionGravity::yattractor
    \brief the y coordinate of the point attracting the particles.
*/

/*!
    \property QxParticleMotionGravity::acceleration
    \brief the acceleration to apply to the particles.
*/

void QxParticleMotionGravity::setXAttractor(qreal x)
{
    if (qFuzzyCompare(x, _xAttr))
        return;
    _xAttr = x;
    emit xattractorChanged();
}

void QxParticleMotionGravity::setYAttractor(qreal y)
{
    if (qFuzzyCompare(y, _yAttr))
        return;
    _yAttr = y;
    emit yattractorChanged();
}

void QxParticleMotionGravity::setAcceleration(qreal accel)
{
    qreal scaledAccel = accel/1000000.0;
    if (qFuzzyCompare(scaledAccel, _accel))
        return;
    _accel = scaledAccel;
    emit accelerationChanged();
}

void QxParticleMotionGravity::advance(QxParticle &p, int interval)
{
    qreal xdiff = _xAttr - p.x;
    qreal ydiff = _yAttr - p.y;
    qreal absXdiff = qAbs(xdiff);
    qreal absYdiff = qAbs(ydiff);

    qreal xcomp = xdiff / (absXdiff + absYdiff);
    qreal ycomp = ydiff / (absXdiff + absYdiff);

    p.x_velocity += xcomp * _accel * interval;
    p.y_velocity += ycomp * _accel * interval;
}

/*!
    \qmlclass ParticleMotionWander
    \since 4.7
    \brief The ParticleMotionWander object moves particles in a somewhat random fashion.

    The particles will continue roughly in the original direction, however will randomly
    drift to each side.

    The code below produces an effect similar to falling snow.

    \qml
Rectangle {
    width: 240
    height: 320
    color: "black"

    Particles {
        y: 0
        width: parent.width
        height: 30
        source: "star.png"
        lifeSpan: 5000
        count: 50
        angle: 70
        angleDeviation: 36
        velocity: 30
        velocityDeviation: 10
        ParticleMotionWander {
            xvariance: 30
            pace: 100
        }
    }
}
    \endqml

    \sa Particles
*/

/*!
    \internal
    \class QxParticleMotionWander
    \ingroup group_effects
    \brief The QxParticleMotionWander class moves particles in a somewhat random fashion.

    The particles will continue roughly in the original direction, however will randomly
    drift to each side.
*/

/*!
    \qmlproperty real QxParticleMotionWander::xvariance
    \qmlproperty real QxParticleMotionWander::yvariance

    These properties set the amount to wander in the x and y directions.
*/

/*!
    \qmlproperty real QxParticleMotionWander::pace
    This property holds how quickly the paricles will move from side to side.
*/

void QxParticleMotionWander::advance(QxParticle &p, int interval)
{
    if (!particles)
        particles = qobject_cast<QxParticles*>(parent());
    if (particles) {
        Data *d = (Data*)p.data;
        if (_xvariance != 0.) {
            qreal xdiff = p.x_velocity - d->x_targetV;
            if ((xdiff > d->x_peak && d->x_var > 0.0) || (xdiff < -d->x_peak && d->x_var < 0.0)) {
                d->x_var = -d->x_var;
                d->x_peak = _xvariance + _xvariance * qreal(qrand()) / RAND_MAX;
            }
            p.x_velocity += d->x_var * interval;
        }

        if (_yvariance != 0.) {
            qreal ydiff = p.y_velocity - d->y_targetV;
            if ((ydiff > d->y_peak && d->y_var > 0.0) || (ydiff < -d->y_peak && d->y_var < 0.0)) {
                d->y_var = -d->y_var;
                d->y_peak = _yvariance + _yvariance * qreal(qrand()) / RAND_MAX;
            }
            p.y_velocity += d->y_var * interval;
        }
    }
}

void QxParticleMotionWander::created(QxParticle &p)
{
    if (!p.data) {
        Data *d = new Data;
        p.data = (void*)d;
        d->x_targetV = p.x_velocity;
        d->y_targetV = p.y_velocity;
        d->x_peak = _xvariance;
        d->y_peak = _yvariance;
        d->x_var = _pace * qreal(qrand()) / RAND_MAX / 1000.0;
        d->y_var = _pace * qreal(qrand()) / RAND_MAX / 1000.0;
    }
}

void QxParticleMotionWander::destroy(QxParticle &p)
{
    if (p.data)
        delete (Data*)p.data;
}

void QxParticleMotionWander::setXVariance(qreal var)
{
    qreal scaledVar = var / 1000.0;
    if (qFuzzyCompare(scaledVar, _xvariance))
        return;
    _xvariance = scaledVar;
    emit xvarianceChanged();
}

void QxParticleMotionWander::setYVariance(qreal var)
{
    qreal scaledVar = var / 1000.0;
    if (qFuzzyCompare(scaledVar, _yvariance))
        return;
    _yvariance = scaledVar;
    emit yvarianceChanged();
}

void QxParticleMotionWander::setPace(qreal pace)
{
    qreal scaledPace = pace / 1000.0;
    if (qFuzzyCompare(scaledPace, _pace))
        return;
    _pace = scaledPace;
    emit paceChanged();
}

//---------------------------------------------------------------------------
//an animation that just gives a tick
template<class T, void (T::*method)(int)>
class TickAnimationProxy : public QAbstractAnimation
{
public:
    TickAnimationProxy(T *p, QObject *parent = 0) : QAbstractAnimation(parent), m_p(p) {}
    virtual int duration() const { return -1; }
protected:
    virtual void updateCurrentTime(int msec) { (m_p->*method)(msec); }

private:
    T *m_p;
};
//---------------------------------------------------------------------------
class QxParticlesPrivate : public QxItemPrivate
{
    Q_DECLARE_PUBLIC(QxParticles)
public:
    QxParticlesPrivate()
        : count(1), emissionRate(-1), emissionVariance(0.5), lifeSpan(1000)
        , lifeSpanDev(1000), fadeInDur(200), fadeOutDur(300)
        , angle(0), angleDev(0), velocity(0), velocityDev(0), emissionCarry(0.)
        , addParticleTime(0), addParticleCount(0), lastAdvTime(0)
        , field(0), model(0),  modelIndex(0), clock(this)
    {
    }

    ~QxParticlesPrivate()
    {
    }

    void init()
    {
    }

    void tick(int time);
    void createParticle(int time);
    void updateOpacity(QxParticle &p, int age);

    virtual QRectF renderOrderBoundingRect() const { return QRectF(0, 0, -1, -1); }

    QUrl mask;
    QImage maskImage;
    int count;
    int emissionRate;
    qreal emissionVariance;
    int lifeSpan;
    int lifeSpanDev;
    int fadeInDur;
    int fadeOutDur;
    qreal angle;
    qreal angleDev;
    qreal velocity;
    qreal velocityDev;
    qreal emissionCarry;
    int addParticleTime;
    int addParticleCount;
    int lastAdvTime;
    QList<QxParticleMotion *> motion;
    QxParticleField *field;
    mutable QxVisualDataModel *model;
    QVariant modelVariant;
    int modelIndex;

    QxVisualDataModel *getModel() const {
        Q_Q(const QxParticles);
        if (!model)
            model = new QxVisualDataModel(qmlContext(q), const_cast<QxParticles*>(q));
        return model;
    }

    void setField(QxParticleField *p) {
        field = p;
    }

    QList<QPair<int, int> > bursts;//countLeft, emissionRate pairs
    QList<QxParticle> particles;
    TickAnimationProxy<QxParticlesPrivate, &QxParticlesPrivate::tick> clock;

};

QxParticles *QxParticleField::emitter() const
{
    return m_emitter;
}

void QxParticleField::setEmitter(QxParticles *emitter)
{
    m_emitter = emitter;
    if (m_emitter) {
        m_emitter->setParentItem(this);
        m_emitter->d_func()->setField(this);
    }
}

QDeclarativeListProperty<QxParticlesPainter> QxParticleField::painters()
{
    return QDeclarativeListProperty<QxParticlesPainter>(this, &m_painters,
                                                      &QxParticleField::painters_append,
                                                      &QxParticleField::painters_count,
                                                      &QxParticleField::painters_at, 0);
}

void QxParticleField::painters_append(QDeclarativeListProperty<QxParticlesPainter> *list, QxParticlesPainter *painter)
{
    QxParticleField *_this = static_cast<QxParticleField *>(list->object);
    if (painter) {
        painter->field = _this;
        painter->setParentItem(_this);
        _this->m_painters.append(painter);
    }
}

int QxParticleField::painters_count(QDeclarativeListProperty<QxParticlesPainter> *list)
{
    QxParticleField *_this = static_cast<QxParticleField *>(list->object);
    return _this->m_painters.count();
}

QxParticlesPainter* QxParticleField::painters_at(QDeclarativeListProperty<QxParticlesPainter> *list, int index)
{
    QxParticleField *_this = static_cast<QxParticleField *>(list->object);
    return _this->m_painters.at(index);
}

QList<QxParticle> QxParticleField::particles()
{
    if (m_emitter)
        return m_emitter->d_func()->particles;
    return QList<QxParticle>();
}

void QxParticleField::update()
{
    for (int i = 0; i < m_painters.size(); ++i)
        m_painters.at(i)->update();
}

QVariant QxParticles::model() const
{
    Q_D(const QxParticles);
    return d->modelVariant;
}

void QxParticles::setModel(const QVariant &model)
{
    //### greatly simplified
    Q_D(QxParticles);
    if (d->modelVariant == model)
        return;

    d->modelVariant = model;
    d->getModel()->setModel(model);
}

QDeclarativeComponent *QxParticles::delegate() const
{
    Q_D(const QxParticles);
    return d->getModel()->delegate();
}

void QxParticles::setDelegate(QDeclarativeComponent *delegate)
{
    Q_D(QxParticles);
    if (delegate == this->delegate())
        return;

    //### greatly simplified
    d->getModel()->setDelegate(delegate);
}

void QxParticlesPrivate::tick(int time)
{
    int oldCount = particles.count();
    int removed = 0;
    int interval = time - lastAdvTime;
    for (int i = 0; i < particles.count(); ) {
        QxParticle &particle = particles[i];
        int age = time - particle.birthTime;
        if (age >= particle.lifeSpan)  {
            QxParticle part = particles.takeAt(i);
            if (part.item)
                model->release(part.item);
            for (int j = 0; j < motion.count(); ++j)
                motion.at(j)->destroy(part);
            ++removed;
        } else {
            updateOpacity(particle, age);
            for (int j = 0; j < motion.count(); ++j)
                motion.at(j)->advance(particle, interval);
            ++i;
        }
    }

    if(emissionRate == -1)//Otherwise leave emission to the emission rate
        while(removed-- && ((count == -1) || particles.count() < count))
            createParticle(time);

    if (!addParticleTime)
        addParticleTime = time;

    //Possibly emit new particles
    if (((count == -1) || particles.count() < count) && emissionRate
            && !(count==-1 && emissionRate==-1)) {
        int emissionCount = -1;
        if (emissionRate != -1){
            qreal variance = 1.;
            if (emissionVariance > 0.){
                variance += (qreal(qrand())/RAND_MAX) * emissionVariance * (qrand()%2?-1.:1.);
            }
            qreal emission = emissionRate * (qreal(interval)/1000.);
            emission = emission * variance + emissionCarry;
            double tmpDbl;
            emissionCarry = modf(emission, &tmpDbl);
            emissionCount = (int)tmpDbl;
            emissionCount = qMax(0,emissionCount);
        }
        while(((count == -1) || particles.count() < count) &&
                (emissionRate==-1 || emissionCount--))
            createParticle(time);
    }

    //Deal with emissions from requested bursts
    for(int i=0; i<bursts.size(); i++){
        int emission = 0;
        if(bursts[i].second == -1){
            emission = bursts[i].first;
        }else{
            qreal variance = 1.;
            if (emissionVariance > 0.){
                variance += (qreal(qrand())/RAND_MAX) * emissionVariance * (qrand()%2?-1.:1.);
            }
            qreal workingEmission = bursts[i].second * (qreal(interval)/1000.);
            workingEmission *= variance;
            emission = (int)workingEmission;
            emission = qMax(emission, 0);
        }
        emission = qMin(emission, bursts[i].first);
        bursts[i].first -= emission;
        while(emission--)
            createParticle(time);
    }
    for(int i=bursts.size()-1; i>=0; i--)
        if(bursts[i].first <= 0)
            bursts.removeAt(i);

    lastAdvTime = time;
    if (field)
        field->update();
    if (!(oldCount || particles.count()) && (!count || !emissionRate) && bursts.isEmpty()) {
        lastAdvTime = 0;
        clock.stop();
    }
}

void QxParticlesPrivate::createParticle(int time)
{
    Q_Q(QxParticles);
    QxParticle p(time);
    if (!maskImage.isNull()) {
        int width = maskImage.width();
        int height = maskImage.height();
        int count = 0;
        QRgb black = qRgb(0,0,0);
        while(count < 100000) { //### so we don't get stuck in an infinite loop
            int xpos = qrand() % width;
            int ypos = qrand() % height;
            QRgb rgb = maskImage.pixel(xpos, ypos);
            if (rgb == black) {
                p.x = q->x() + xpos;
                p.y = q->y() + ypos;
                break;
            }
        }
    } else {
        p.x = q->x() + q->width() * qreal(qrand()) / RAND_MAX;
        p.y = q->y() + q->height() * qreal(qrand()) / RAND_MAX;
    }
    p.lifeSpan = lifeSpan;
    if (lifeSpanDev)
        p.lifeSpan += int(lifeSpanDev/2 - lifeSpanDev * qreal(qrand()) / RAND_MAX);
    p.fadeOutAge = p.lifeSpan - fadeOutDur;
    if (fadeInDur == 0.) {
        p.state= QxParticle::Solid;
        p.opacity = 1.0;
    }
    qreal a = angle;
    if (angleDev)
        a += angleDev/2 - angleDev * qreal(qrand()) / RAND_MAX;
    if (a > M_PI)
        a = a - 2 * M_PI;
    qreal v = velocity;
    if (velocityDev)
        v += velocityDev/2 - velocityDev * qreal(qrand()) / RAND_MAX;
    p.x_velocity = v * fastCos(a);
    p.y_velocity = v * fastSin(a);
    if (model && modelVariant.isValid() && q->delegate()) {

        QxItem *item = 0;
        if (particles.count() < model->count()) {
            item = model->item(modelIndex);
            ++modelIndex;
            if (modelIndex == model->count())
                modelIndex = 0;
        }
        if (item) {
            //### x, y are center point
            item->setX(p.x);
            item->setY(p.y);
            item->setOpacity(p.opacity);
            //item->setScale(1.0 + (.5 - 1 * qreal(qrand()) / RAND_MAX));
            p.item = item;
            item->setParentItem(field);
        }
    }
    particles.append(p);

    for (int i = 0; i < motion.count(); ++i)
        motion.at(i)->created(particles.last());
}

void QxParticlesPrivate::updateOpacity(QxParticle &p, int age)
{
    switch (p.state) {
    case QxParticle::FadeIn:
        if (age <= fadeInDur) {
            p.opacity = qreal(age) / fadeInDur;
            break;
        } else {
            p.opacity = 1.0;
            p.state = QxParticle::Solid;
            // Fall through
        }
    case QxParticle::Solid:
        if (age <= p.fadeOutAge) {
            break;
        } else {
            p.state = QxParticle::FadeOut;
            // Fall through
        }
    case QxParticle::FadeOut:
        p.opacity = qreal(p.lifeSpan - age) / fadeOutDur;
        break;
    }
    if (p.item)
        p.item->setOpacity(p.opacity);
}

/*!
    \qmlclass Particles
    \since 4.7
    \brief The Particles object generates and moves particles.
    \inherits Item

    Particles are available in the \bold{Qt.labs.particles 1.0} module.
    \e {Elements in the Qt.labs module are not guaranteed to remain compatible
    in future versions.}

    This element provides preliminary support for particles in QML,
    and may be heavily changed or removed in later versions.

    The particles created by this object cannot be dealt with
    directly, they can only be controlled through the parameters of
    the Particles object. The particles are all the same pixmap,
    specified by the user.

    The particles are painted relative to the parent of the Particles
    object.  Moving the Particles object will not move the particles
    already emitted.

    The below example creates two differently behaving particle
    sources.  The top one has particles falling from the top like
    snow, the lower one has particles expelled up like a fountain.

    \qml
import Qt 4.7
import Qt.labs.particles 1.0

Rectangle {
    width: 240
    height: 320
    color: "black"
    Particles {
        y: 0
        width: parent.width
        height: 30
        source: "star.png"
        lifeSpan: 5000
        count: 50
        angle: 70
        angleDeviation: 36
        velocity: 30
        velocityDeviation: 10
        ParticleMotionWander {
            xvariance: 30
            pace: 100
        }
    }
    Particles {
        y: 300
        x: 120
        width: 1
        height: 1
        source: "star.png"
        lifeSpan: 5000
        count: 200
        angle: 270
        angleDeviation: 45
        velocity: 50
        velocityDeviation: 30
        ParticleMotionGravity {
            yattractor: 1000
            xattractor: 0
            acceleration: 25
        }
    }
}
    \endqml
    \image particles.gif
*/

/*!
    \internal
    \class QxParticles
    \ingroup group_effects
    \brief The QxParticles class generates and moves particles.
*/

QxParticles::QxParticles(QxItem *parent)
    : QxItem(*(new QxParticlesPrivate), parent)
{
    Q_D(QxParticles);
    d->init();
}

QxParticles::~QxParticles()
{
}

QxParticlesPainter::~QxParticlesPainter()
{
}


/*!
    \qmlproperty string Particles::source
    This property holds the URL of the particle image.
*/

/*!
    \property QxParticles::source
    \brief the URL of the particle image.
*/
QUrl QxParticlesPainter::source() const
{
    return url;
}

void QxParticlesPainter::imageLoaded()
{
    if (image.isError())
        qmlInfo(this) << image.error();
    update();
}

void QxParticlesPainter::setSource(const QUrl &name)
{
    if ((url.isEmpty() == name.isEmpty()) && name == url)
        return;

    if (name.isEmpty()) {
        url = name;
        image.clear(this);
        update();
    } else {
        url = name;
        Q_ASSERT(!name.isRelative());
        image.load(qmlEngine(this), url);
        if (image.isLoading()) {
            image.connectFinished(this, SLOT(imageLoaded()));
        } else {
            if (image.isError())
                qmlInfo(this) << image.error();
            //### unify with imageLoaded
            update();
        }
    }
    emit sourceChanged();
}

QUrl QxParticles::mask() const
{
    Q_D(const QxParticles);
    return d->mask;
}

void QxParticles::setMask(const QUrl &name)
{
    Q_D(QxParticles);

    //### super simplified
    if ((d->mask.isEmpty() == name.isEmpty()) && name == d->mask)
        return;

    d->maskImage.load(name.toLocalFile());
}

/*!
    \qmlproperty int Particles::count
    This property holds the maximum number of particles

    The particles element emits particles until it has count active
    particles. When this number is reached, new particles are not emitted until
    some of the current particles reach the end of their lifespan.

    If count is -1 then there is no maximum number of active particles, and
    particles will be constantly emitted at the rate specified by emissionRate.

    The default value for count is 1.

    If both count and emissionRate are set to -1, nothing will be emitted.

*/

/*!
    \property QxParticles::count
    \brief the maximum number of particles
*/
int QxParticles::count() const
{
    Q_D(const QxParticles);
    return d->count;
}

void QxParticles::setCount(int cnt)
{
    Q_D(QxParticles);
    if (cnt == d->count)
        return;

    int oldCount = d->count;
    d->count = cnt;
    d->addParticleTime = 0;
    d->addParticleCount = d->particles.count();
    if (!oldCount && d->clock.state() != QAbstractAnimation::Running && d->count && d->emissionRate) {
        d->clock.start();
    }
    if (d->field)
        d->field->update();
    emit countChanged();
}


/*!
    \qmlproperty int Particles::emissionRate
    This property holds the target number of particles to emit every second.

    The particles element will emit up to emissionRate particles every
    second. Fewer particles may be emitted per second if the maximum number of
    particles has been reached.

    If emissionRate is set to -1 there is no limit to the number of
    particles emitted per second, and particles will be instantly emitted to
    reach the maximum number of particles specified by count.

    The default value for emissionRate is -1.

    If both count and emissionRate are set to -1, nothing will be emitted.
*/

/*!
    \property QxParticles::emissionRate
    \brief the emission rate of particles
*/
int QxParticles::emissionRate() const
{
    Q_D(const QxParticles);
    return d->emissionRate;
}
void QxParticles::setEmissionRate(int er)
{
    Q_D(QxParticles);
    if(er == d->emissionRate)
        return;
    d->emissionRate = er;
    if (d->clock.state() != QAbstractAnimation::Running && d->count && d->emissionRate) {
            d->clock.start();
    }
    emit emissionRateChanged();
}

/*!
    \qmlproperty real Particles::emissionVariance
    This property holds how inconsistent the rate of particle emissions are.
    It is a number between 0 (no variance) and 1 (some variance).

    The expected number of particles emitted per second is emissionRate. If
    emissionVariance is 0 then particles will be emitted consistently throughout
    each second to reach that number. If emissionVariance is greater than 0 the
    rate of particle emission will vary randomly throughout the second, with the
    consequence that the actual number of particles emitted in one second will
    vary randomly as well.

    emissionVariance is the maximum deviation from emitting
    emissionRate particles per second. An emissionVariance of 0 means you should
    get exactly emissionRate particles emitted per second,
    and an emissionVariance of 1 means you will get between zero and two times
    emissionRate particles per second, but you should get emissionRate particles
    per second on average.

    Note that even with an emissionVariance of 0 there may be some variance due
    to performance and hardware constraints.

    The default value of emissionVariance is 0.5
*/

/*!
    \property QxParticles::emissionVariance
    \brief how much the particle emission amounts vary per tick
*/

qreal QxParticles::emissionVariance() const
{
    Q_D(const QxParticles);
    return d->emissionVariance;
}

void QxParticles::setEmissionVariance(qreal ev)
{
    Q_D(QxParticles);
    if(d->emissionVariance == ev)
        return;
    d->emissionVariance = ev;
    emit emissionVarianceChanged();
}

/*!
    \qmlproperty int Particles::lifeSpan
    \qmlproperty int Particles::lifeSpanDeviation

    These properties describe the life span of each particle.

    The default lifespan for a particle is 1000ms.

    lifeSpanDeviation randomly varies the lifeSpan up to the specified variation.  For
    example, the following creates particles whose lifeSpan will vary
    from 150ms to 250ms:

    \qml
Particles {
    source: "star.png"
    lifeSpan: 200
    lifeSpanDeviation: 100
}
    \endqml
*/

/*!
    \property QxParticles::lifeSpan
    \brief the life span of each particle.

    Default value is 1000ms.

    \sa QxParticles::lifeSpanDeviation
*/
int QxParticles::lifeSpan() const
{
    Q_D(const QxParticles);
    return d->lifeSpan;
}

void QxParticles::setLifeSpan(int ls)
{
    Q_D(QxParticles);
    if(d->lifeSpan == ls)
        return;
    d->lifeSpan = ls;
    emit lifeSpanChanged();
}

/*!
    \property QxParticles::lifeSpanDeviation
    \brief the maximum possible deviation from the set lifeSpan.

    Randomly varies the lifeSpan up to the specified variation.  For
    example, the following creates particles whose lifeSpan will vary
    from 150ms to 250ms:

\qml
Particles {
    source: "star.png"
    lifeSpan: 200
    lifeSpanDeviation: 100
}
\endqml

    \sa QxParticles::lifeSpan
*/
int QxParticles::lifeSpanDeviation() const
{
    Q_D(const QxParticles);
    return d->lifeSpanDev;
}

void QxParticles::setLifeSpanDeviation(int dev)
{
    Q_D(QxParticles);
    if(d->lifeSpanDev == dev)
        return;
    d->lifeSpanDev = dev;
    emit lifeSpanDeviationChanged();
}

/*!
    \qmlproperty int Particles::fadeInDuration
    \qmlproperty int Particles::fadeOutDuration
    These properties hold the time taken to fade the particles in and out.

    By default fade in is 200ms and fade out is 300ms.
*/

/*!
    \property QxParticles::fadeInDuration
    \brief the time taken to fade in the particles.

    Default value is 200ms.
*/
int QxParticles::fadeInDuration() const
{
    Q_D(const QxParticles);
    return d->fadeInDur;
}

void QxParticles::setFadeInDuration(int dur)
{
    Q_D(QxParticles);
    if (dur < 0.0 || dur == d->fadeInDur)
        return;
    d->fadeInDur = dur;
    emit fadeInDurationChanged();
}

/*!
    \property QxParticles::fadeOutDuration
    \brief the time taken to fade out the particles.

    Default value is 300ms.
*/
int QxParticles::fadeOutDuration() const
{
    Q_D(const QxParticles);
    return d->fadeOutDur;
}

void QxParticles::setFadeOutDuration(int dur)
{
    Q_D(QxParticles);
    if (dur < 0.0 || d->fadeOutDur == dur)
        return;
    d->fadeOutDur = dur;
    emit fadeOutDurationChanged();
}

/*!
    \qmlproperty real Particles::angle
    \qmlproperty real Particles::angleDeviation

    These properties control particle direction.

    angleDeviation randomly varies the direction up to the specified variation.  For
    example, the following creates particles whose initial direction will
    vary from 15 degrees to 105 degrees:

    \qml
Particles {
    source: "star.png"
    angle: 60
    angleDeviation: 90
}
    \endqml
*/

/*!
    \property QxParticles::angle
    \brief the initial angle of direction.

    \sa QxParticles::angleDeviation
*/
qreal QxParticles::angle() const
{
    Q_D(const QxParticles);
    return d->angle * 180.0 / M_PI;
}

void QxParticles::setAngle(qreal angle)
{
    Q_D(QxParticles);
    qreal radAngle = angle * M_PI / 180.0;
    if(radAngle == d->angle)
        return;
    d->angle = radAngle;
    emit angleChanged();
}

/*!
    \property QxParticles::angleDeviation
    \brief the maximum possible deviation from the set angle.

    Randomly varies the direction up to the specified variation.  For
    example, the following creates particles whose initial direction will
    vary from 15 degrees to 105 degrees:

\qml
Particles {
    source: "star.png"
    angle: 60
    angleDeviation: 90
}
\endqml

    \sa QxParticles::angle
*/
qreal QxParticles::angleDeviation() const
{
    Q_D(const QxParticles);
    return d->angleDev * 180.0 / M_PI;
}

void QxParticles::setAngleDeviation(qreal dev)
{
    Q_D(QxParticles);
    qreal radDev = dev * M_PI / 180.0;
    if(radDev == d->angleDev)
        return;
    d->angleDev = radDev;
    emit angleDeviationChanged();
}

/*!
    \qmlproperty real Particles::velocity
    \qmlproperty real Particles::velocityDeviation

    These properties control the velocity of the particles.

    velocityDeviation randomly varies the velocity up to the specified variation.  For
    example, the following creates particles whose initial velocity will
    vary from 40 to 60.

    \qml
Particles {
    source: "star.png"
    velocity: 50
    velocityDeviation: 20
}
    \endqml
*/

/*!
    \property QxParticles::velocity
    \brief the initial velocity of the particles.

    \sa QxParticles::velocityDeviation
*/
qreal QxParticles::velocity() const
{
    Q_D(const QxParticles);
    return d->velocity * 1000.0;
}

void QxParticles::setVelocity(qreal velocity)
{
    Q_D(QxParticles);
    qreal realVel = velocity / 1000.0;
    if(realVel == d->velocity)
        return;
    d->velocity = realVel;
    emit velocityChanged();
}

/*!
    \property QxParticles::velocityDeviation
    \brief the maximum possible deviation from the set velocity.

    Randomly varies the velocity up to the specified variation.  For
    example, the following creates particles whose initial velocity will
    vary from 40 to 60.

\qml
Particles {
    source: "star.png"
    velocity: 50
    velocityDeviation: 20
}
\endqml

    \sa QxParticles::velocity
*/
qreal QxParticles::velocityDeviation() const
{
    Q_D(const QxParticles);
    return d->velocityDev * 1000.0;
}

void QxParticles::setVelocityDeviation(qreal velocity)
{
    Q_D(QxParticles);
    qreal realDev = velocity / 1000.0;
    if(realDev == d->velocityDev)
        return;
    d->velocityDev = realDev;
    emit velocityDeviationChanged();
}

/*!
    \qmlproperty ParticleMotion Particles::motion
    This property sets the type of motion to apply to the particles.

    When a particle is created it will have an initial direction and velocity.
    The motion of the particle during its lifeSpan is then influenced by the
    motion property.

    Default motion is ParticleMotionLinear.
*/

/*!
    \property QxParticles::motion
    \brief sets the type of motion to apply to the particles.

    When a particle is created it will have an initial direction and velocity.
    The motion of the particle during its lifeSpan is then influenced by the
    motion property.

    Default motion is QxParticleMotionLinear.
*/
QDeclarativeListProperty<QxParticleMotion> QxParticles::motion()
{
    Q_D(QxParticles);
    return QDeclarativeListProperty<QxParticleMotion>(this, d->motion);
}

/*!
    \qmlmethod Particles::burst(int count, int emissionRate)

    Initiates a burst of particles.

    This method takes two arguments. The first argument is the number
    of particles to emit and the second argument is the emissionRate for the
    burst. If the second argument is omitted, it is treated as -1. The burst
    of particles has a separate emissionRate and count to the normal emission of
    particles. The burst uses the same values as normal emission for all other
    properties, including emissionVariance.

    The normal emission of particles will continue during the burst, however
    the particles created by the burst count towards the maximum number used by
    normal emission. To avoid this behavior, use two Particles elements.

*/
void QxParticles::burst(int count, int emissionRate)
{
    Q_D(QxParticles);
    d->bursts << qMakePair(count, emissionRate);
    if (d->clock.state() != QAbstractAnimation::Running)
        d->clock.start();
}

void QxParticles::fastForward(int ms)
{
    Q_D(QxParticles);
    int startTime = d->clock.currentTime();
    for (int offset = 0; offset <= ms; offset +=16) {
        d->clock.setCurrentTime(startTime + offset);
    }
}

void QxParticlesPainter::update()
{
    QList<QxParticle> particles = field ? field->particles() : QList<QxParticle>();
    if (image.isNull() || particles.isEmpty()) {
        setPaintNode(0);
        delete node;
        node = 0;
        return;
    }

    QRectF targetRect(0, 0, image.width(), image.height());
    QRect sourceRect(0, 0, image.width(), image.height());
    if (node) {
        node->setRects(targetRect, sourceRect);
        node->setOpacity(renderOpacity());
        node->setParticles(particles);
        node->setMask(maskImage);
        node->setLinearFiltering(smooth());
        node->update();
    } else {
        node = new QxParticleNode(renderOpacity(), targetRect, image, sourceRect, particles, smooth());
        node->setMask(maskImage);
        node->update();
        setPaintNode(node);
    }
}

void QxParticles::componentComplete()
{
    Q_D(QxParticles);
    QxItem::componentComplete();
    if (d->count && d->emissionRate) {
        d->clock.start();
    }
    if (d->lifeSpanDev > d->lifeSpan)
        d->lifeSpanDev = d->lifeSpan;
}

QUrl QxParticlesPainter::mask() const
{
    return maskUrl;
}

void QxParticlesPainter::setMask(const QUrl &name)
{
    //### super simplified
    if ((maskUrl.isEmpty() == name.isEmpty()) && name == maskUrl)
        return;

    maskUrl = name;
    maskImage.load(maskUrl.toLocalFile());
}

