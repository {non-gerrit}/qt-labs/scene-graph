/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXRECT_H
#define QXRECT_H

#include "qxitem.h"

#include <QtGui/qbrush.h>

class QxPen : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY penWidthChanged)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY penColorChanged)
public:
    QxPen(QObject *parent=0)
        : QObject(parent), _width(1), _color("#000000"), _valid(false)
    {}

    int width() const { return _width; }
    void setWidth(int w);

    QColor color() const { return _color; }
    void setColor(const QColor &c);

    bool isValid() { return _valid; };

Q_SIGNALS:
    void penWidthChanged();
    void penColorChanged();

private:
    int _width;
    QColor _color;
    bool _valid;
};

class QxGradientStop : public QObject
{
    Q_OBJECT

    Q_PROPERTY(qreal position READ position WRITE setPosition)
    Q_PROPERTY(QColor color READ color WRITE setColor)

public:
    QxGradientStop(QObject *parent=0) : QObject(parent) {}

    qreal position() const { return m_position; }
    void setPosition(qreal position) { m_position = position; updateGradient(); }

    QColor color() const { return m_color; }
    void setColor(const QColor &color) { m_color = color; updateGradient(); }

private:
    void updateGradient();

private:
    qreal m_position;
    QColor m_color;
};

class QxGradient : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QDeclarativeListProperty<QxGradientStop> stops READ declarativeStops)
    Q_CLASSINFO("DefaultProperty", "stops")

public:
    QxGradient(QObject *parent=0) : QObject(parent), m_gradient(0) {}
    ~QxGradient() { delete m_gradient; }

    QDeclarativeListProperty<QxGradientStop> declarativeStops() { return QDeclarativeListProperty<QxGradientStop>(this, m_stops); }
    QList<QxGradientStop *> stops() const { return m_stops; }

    const QGradient *gradient() const;

Q_SIGNALS:
    void updated();

private:
    void doUpdate();

private:
    QList<QxGradientStop *> m_stops;
    mutable QGradient *m_gradient;
    friend class QxGradientStop;
};

class QxRectanglePrivate;
class QxRectangle : public QxItem
{
    Q_OBJECT

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(QxGradient *gradient READ gradient WRITE setGradient)
    Q_PROPERTY(QxPen * border READ border CONSTANT)
    Q_PROPERTY(qreal radius READ radius WRITE setRadius NOTIFY radiusChanged)
public:
    QxRectangle(QxItem *parent=0);
    ~QxRectangle();

    QColor color() const;
    void setColor(const QColor &);

    QxPen *border();

    QxGradient *gradient() const;
    void setGradient(QxGradient *gradient);

    qreal radius() const;
    void setRadius(qreal radius);

protected:
    virtual void geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry);
    virtual void renderOpacityChanged(qreal newOpacity, qreal oldOpacity);
    virtual void componentComplete();

Q_SIGNALS:
    void colorChanged();
    void radiusChanged();

private Q_SLOTS:
    void doUpdateGradient();
    void doUpdateBorderWidth();
    void doUpdateBorderColor();

private:
    Q_DISABLE_COPY(QxRectangle)
    Q_DECLARE_PRIVATE(QxRectangle)
};

QML_DECLARE_TYPE(QxPen)
QML_DECLARE_TYPE(QxGradientStop)
QML_DECLARE_TYPE(QxGradient)
QML_DECLARE_TYPE(QxRectangle)

#endif // QXRECT_H
