/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qximagebase_p.h"
#include "qximagebase_p_p.h"

#include <QtDeclarative/qdeclarativeengine.h>
#include <QtDeclarative/qdeclarativeinfo.h>
#include <private/qdeclarativepixmapcache_p.h>

#include <QtCore/qfile.h>
#include <QTime>

QxImageBase::QxImageBase(QxImageBasePrivate &dd, QxItem *parent)
: QxItem(dd, parent)
{
}

QxImageBase::~QxImageBase()
{
}

QxImageBase::Status QxImageBase::status() const
{
    Q_D(const QxImageBase);
    return d->status;
}


qreal QxImageBase::progress() const
{
    Q_D(const QxImageBase);
    return d->progress;
}


bool QxImageBase::asynchronous() const
{
    Q_D(const QxImageBase);
    return d->async;
}

void QxImageBase::setAsynchronous(bool async)
{
    Q_D(QxImageBase);
    if (d->async != async) {
        d->async = async;
        emit asynchronousChanged();
    }
}

QUrl QxImageBase::source() const
{
    Q_D(const QxImageBase);
    return d->url;
}

void QxImageBase::setSource(const QUrl &url)
{
    Q_D(QxImageBase);
    //equality is fairly expensive, so we bypass for simple, common case
    if ((d->url.isEmpty() == url.isEmpty()) && url == d->url)
        return;

    d->url = url;
    emit sourceChanged(d->url);

    markDirty(QxImageBasePrivate::DirtySource);
}

void QxImageBase::setSourceSize(const QSize& size)
{
    Q_D(QxImageBase);
    if (d->sourcesize == size)
        return;

    d->sourcesize = size;
    d->explicitSourceSize = true;
    emit sourceSizeChanged();

    markDirty(QxImageBasePrivate::DirtySourceSize);
}

QSize QxImageBase::sourceSize() const
{
    Q_D(const QxImageBase);

    int width = d->sourcesize.width();
    int height = d->sourcesize.height();
    return QSize(width != -1 ? width : implicitWidth(), height != -1 ? height : implicitHeight());
}

void QxImageBase::load()
{
    Q_D(QxImageBase);

    if (d->url.isEmpty()) {
        d->pix.clear(this);
        d->status = Null;
        d->progress = 0.0;
        emit progressChanged(d->progress);
        emit statusChanged(d->status);
        pixmapChange();

    } else {
        d->pix.load(qmlEngine(this), d->url, d->explicitSourceSize ? sourceSize() : QSize(), d->async);
        if (d->pix.isLoading()) {
            d->progress = 0.0;
            d->status = Loading;
            emit progressChanged(d->progress);
            emit statusChanged(d->status);

            static int thisRequestProgress = -1;
            static int thisRequestFinished = -1;
            if (thisRequestProgress == -1) {
                thisRequestProgress =
                    QxImageBase::staticMetaObject.indexOfSlot("requestProgress(qint64,qint64)");
                thisRequestFinished =
                    QxImageBase::staticMetaObject.indexOfSlot("requestFinished()");
            }
            d->pix.connectFinished(this, thisRequestFinished);
            d->pix.connectDownloadProgress(this, thisRequestProgress);
        } else {
            requestFinished();
        }
    }
}

void QxImageBase::requestFinished()
{
    Q_D(QxImageBase);

    QxImageBase::Status oldStatus = d->status;
    qreal oldProgress = d->progress;

    if (d->pix.isError()) {
        d->status = Error;
        qmlInfo(this) << d->pix.error();
    } else {
        if (d->async)
            d->status = Uploading;
        else
            d->status = Ready;
    }

    d->progress = 1.0;

    if (d->sourcesize.width() != d->pix.width() || d->sourcesize.height() != d->pix.height())
        emit sourceSizeChanged();

    if (d->status != oldStatus)
        emit statusChanged(d->status);
    if (d->progress != oldProgress)
        emit progressChanged(d->progress);

    if (d->status != Error && d->status != Null) {
        QSGTextureManager *tm = QSGContext::current->textureManager();
        if (d->async) {
            QImage image = d->pix.pixmap().toImage();
            d->texture = tm->requestUpload(image,
                                           this,
                                           SLOT(textureStatusChanged(int)));

        } else {
            d->texture = tm->upload(d->pix.pixmap().toImage());
        }
        markDirty(QxImageBasePrivate::DirtyTextureReady);
    }
}

void QxImageBase::textureStatusChanged(int status)
{
    Q_D(QxImageBase);
    if (status == QSGTexture::Ready) {
        d->status = Ready;
        emit statusChanged(d->status);

        if (!d->texture.isNull())
            markDirty(QxImageBasePrivate::DirtyTextureReady);
    }
}

void QxImageBase::requestProgress(qint64 received, qint64 total)
{
    Q_D(QxImageBase);
    if (d->status == Loading && total > 0) {
        d->progress = qreal(received)/total;
        emit progressChanged(d->progress);
    }
}

void QxImageBase::pixmapChange()
{
}

void QxImageBase::markDirty(int flag)
{

    Q_D(QxImageBase);
    d->dirty |= flag;
    if (!d->connected) {
        connect(QSGContext::current, SIGNAL(aboutToRenderNextFrame()), this, SLOT(prepare()));
        d->connected = true;
    }
}

void QxImageBase::prepare()
{
    Q_D(QxImageBase);

    if (!isComponentComplete())
        return;

    if (d->dirty & (QxImageBasePrivate::DirtySource | QxImageBasePrivate::DirtySourceSize)
        && !d->url.isEmpty()
        && d->pix.isNull()) {
        load();
    }

    if ((d->dirty & QxImageBasePrivate::DirtyTextureReady) && d->texture.isReady()) {
        // Updating the stuff...
        pixmapChange();
    }

    disconnect(QSGContext::current, SIGNAL(aboutToRenderNextFrame()), this, SLOT(prepare()));
    d->connected = false;
    d->dirty = 0;
}
