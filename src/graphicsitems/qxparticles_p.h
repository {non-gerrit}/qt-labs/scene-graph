/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXPARTICLES_H
#define QXPARTICLES_H

#include "qxitem.h"

#include <private/qdeclarativepixmapcache_p.h>

class QxParticle;
class QxParticles;
class QxParticleMotion : public QObject
{
    Q_OBJECT
public:
    QxParticleMotion(QObject *parent=0);

    virtual void advance(QxParticle &, int interval);
    virtual void created(QxParticle &);
    virtual void destroy(QxParticle &);
};

class QxParticleMotionLinear : public QxParticleMotion
{
    Q_OBJECT
public:
    QxParticleMotionLinear(QObject *parent=0)
        : QxParticleMotion(parent) {}

    virtual void advance(QxParticle &, int interval);
};

class QxParticleMotionGravity : public QxParticleMotion
{
    Q_OBJECT

    Q_PROPERTY(qreal xattractor READ xAttractor WRITE setXAttractor NOTIFY xattractorChanged)
    Q_PROPERTY(qreal yattractor READ yAttractor WRITE setYAttractor NOTIFY yattractorChanged)
    Q_PROPERTY(qreal acceleration READ acceleration WRITE setAcceleration NOTIFY accelerationChanged)
public:
    QxParticleMotionGravity(QObject *parent=0)
        : QxParticleMotion(parent), _xAttr(0.0), _yAttr(0.0), _accel(0.00005) {}

    qreal xAttractor() const { return _xAttr; }
    void setXAttractor(qreal x);

    qreal yAttractor() const { return _yAttr; }
    void setYAttractor(qreal y);

    qreal acceleration() const { return _accel * 1000000; }
    void setAcceleration(qreal accel);

    virtual void advance(QxParticle &, int interval);

Q_SIGNALS:
    void xattractorChanged();
    void yattractorChanged();
    void accelerationChanged();

private:
    qreal _xAttr;
    qreal _yAttr;
    qreal _accel;
};

//////////////////////////////////////////////////////////////////////////////
class QxParticleMotionRandomDrift : public QxParticleMotion
{
    Q_OBJECT

    Q_PROPERTY(qreal xDrift READ xDrift WRITE setXDrift)
    Q_PROPERTY(qreal yDrift READ yDrift WRITE setYDrift)
public:
    QxParticleMotionRandomDrift(QObject *parent=0)
        : QxParticleMotion(parent), _xDrift(0.0), _yDrift(0.0) {}

    qreal xDrift() const { return _xDrift * 500.0; }
    void setXDrift(qreal x) { _xDrift = x / 500.0; }

    qreal yDrift() const { return _yDrift * 500.0; }
    void setYDrift(qreal y) { _yDrift = y / 500.0; }

    virtual void advance(QxParticle &, int interval);

private:
    qreal _xDrift;
    qreal _yDrift;
};

class QxParticleMotionAcceleration : public QxParticleMotion
{
    Q_OBJECT

    Q_PROPERTY(qreal xAccel READ xAccel WRITE setXAccel)
    Q_PROPERTY(qreal yAccel READ yAccel WRITE setYAccel)
public:
    QxParticleMotionAcceleration(QObject *parent=0)
        : QxParticleMotion(parent), _xAccel(0.0), _yAccel(0.0) {}

    qreal xAccel() const { return _xAccel * 1000.0; }
    void setXAccel(qreal x) { _xAccel = x / 1000.0; }

    qreal yAccel() const { return _yAccel * 1000.0; }
    void setYAccel(qreal y) { _yAccel = y / 1000.0; }

    virtual void advance(QxParticle &, int interval);

private:
    qreal _xAccel;
    qreal _yAccel;
};

class QxParticleMotionFriction : public QxParticleMotion
{
    Q_OBJECT

    Q_PROPERTY(qreal friction READ friction WRITE setFriction)
public:
    QxParticleMotionFriction(QObject *parent=0)
        : QxParticleMotion(parent), _friction(0.0) {}

    qreal friction() const { return _friction * 1000.0; }
    void setFriction(qreal friction) { _friction = friction / 1000.0; }

    virtual void advance(QxParticle &, int interval);

private:
    qreal _friction;
};

class QxParticleMotionGravityWell : public QxParticleMotion
{
    Q_OBJECT

    Q_PROPERTY(qreal xattractor READ xAttractor WRITE setXAttractor)
    Q_PROPERTY(qreal yattractor READ yAttractor WRITE setYAttractor)
    Q_PROPERTY(qreal force READ force WRITE setForce)
    Q_PROPERTY(qreal epsilon READ epsilon WRITE setEpsilon)
public:
    QxParticleMotionGravityWell(QObject *parent=0)
        : QxParticleMotion(parent), _xAttr(0.0), _yAttr(0.0), _force(1.0), _epsilon(100) {}

    qreal xAttractor() const { return _xAttr; }
    void setXAttractor(qreal x) { _xAttr = x; }

    qreal yAttractor() const { return _yAttr; }
    void setYAttractor(qreal y) { _yAttr = y; }

    qreal force() const { return _force; }
    virtual void setForce(qreal force) { _force = force; }

    qreal epsilon() const { return _epsilon; }
    void setEpsilon(qreal e) { _epsilon = e; }

    virtual void advance(QxParticle &, int interval);

private:
    qreal _xAttr;
    qreal _yAttr;
    qreal _force;
    qreal _epsilon;
};

class QxParticleMotionAntiGravity : public QxParticleMotionGravityWell
{
    Q_OBJECT

public:
    QxParticleMotionAntiGravity(QObject *parent=0)
        : QxParticleMotionGravityWell(parent) { QxParticleMotionGravityWell::setForce(-1.0); }

    virtual void setForce(qreal force) { QxParticleMotionGravityWell::setForce(-force); setEpsilon(1.0); }
};

class QxParticleKillZone : public QxParticleMotion
{
    Q_OBJECT

    Q_PROPERTY(QRectF area READ area WRITE setArea)
public:
    QxParticleKillZone(QObject *parent=0)
        : QxParticleMotion(parent) {}

    QRectF area() const { return _area; }
    void setArea(QRectF area) { _area = area; }

    virtual void advance(QxParticle &, int interval);

private:
    QRectF _area;
};
//////////////////////////////////////////////////////////////////////////////

class QxParticleMotionWander : public QxParticleMotion
{
    Q_OBJECT
public:
    QxParticleMotionWander()
        : QxParticleMotion(), particles(0), _xvariance(0), _yvariance(0), _pace(100) {}

    virtual void advance(QxParticle &, int interval);
    virtual void created(QxParticle &);
    virtual void destroy(QxParticle &);

    struct Data {
        qreal x_targetV;
        qreal y_targetV;
        qreal x_peak;
        qreal y_peak;
        qreal x_var;
        qreal y_var;
    };

    Q_PROPERTY(qreal xvariance READ xVariance WRITE setXVariance NOTIFY xvarianceChanged)
    qreal xVariance() const { return _xvariance * 1000.0; }
    void setXVariance(qreal var);

    Q_PROPERTY(qreal yvariance READ yVariance WRITE setYVariance NOTIFY yvarianceChanged)
    qreal yVariance() const { return _yvariance * 1000.0; }
    void setYVariance(qreal var);

    Q_PROPERTY(qreal pace READ pace WRITE setPace NOTIFY paceChanged)
    qreal pace() const { return _pace * 1000.0; }
    void setPace(qreal pace);

Q_SIGNALS:
    void xvarianceChanged();
    void yvarianceChanged();
    void paceChanged();

private:
    QxParticles *particles;
    qreal _xvariance;
    qreal _yvariance;
    qreal _pace;
};

class QxParticlesPrivate;
class QxParticles : public QxItem
{
    Q_OBJECT

    Q_PROPERTY(QUrl mask READ mask WRITE setMask)
    Q_PROPERTY(int count READ count WRITE setCount NOTIFY countChanged)
    Q_PROPERTY(int emissionRate READ emissionRate WRITE setEmissionRate NOTIFY emissionRateChanged)
    Q_PROPERTY(qreal emissionVariance READ emissionVariance WRITE setEmissionVariance NOTIFY emissionVarianceChanged)
    Q_PROPERTY(qreal angle READ angle WRITE setAngle NOTIFY angleChanged)
    Q_PROPERTY(qreal angleDeviation READ angleDeviation WRITE setAngleDeviation NOTIFY angleDeviationChanged)
    Q_PROPERTY(qreal velocity READ velocity WRITE setVelocity NOTIFY velocityChanged)
    Q_PROPERTY(qreal velocityDeviation READ velocityDeviation WRITE setVelocityDeviation NOTIFY velocityDeviationChanged)
    Q_PROPERTY(QVariant model READ model WRITE setModel)

    //### move
    Q_PROPERTY(int fadeInDuration READ fadeInDuration WRITE setFadeInDuration NOTIFY fadeInDurationChanged)
    Q_PROPERTY(int fadeOutDuration READ fadeOutDuration WRITE setFadeOutDuration NOTIFY fadeOutDurationChanged)
    Q_PROPERTY(int lifeSpan READ lifeSpan WRITE setLifeSpan NOTIFY lifeSpanChanged)
    Q_PROPERTY(int lifeSpanDeviation READ lifeSpanDeviation WRITE setLifeSpanDeviation NOTIFY lifeSpanDeviationChanged)
    Q_PROPERTY(QDeclarativeComponent *delegate READ delegate WRITE setDelegate)
    Q_PROPERTY(QDeclarativeListProperty<QxParticleMotion> motion READ motion)
    Q_CLASSINFO("DefaultProperty", "motion")

public:
    QxParticles(QxItem *parent=0);
    ~QxParticles();

    QUrl mask() const;
    void setMask(const QUrl &);

    int count() const;
    void setCount(int cnt);

    int emissionRate() const;
    void setEmissionRate(int);

    qreal emissionVariance() const;
    void setEmissionVariance(qreal);

    int lifeSpan() const;
    void setLifeSpan(int);

    int lifeSpanDeviation() const;
    void setLifeSpanDeviation(int);

    int fadeInDuration() const;
    void setFadeInDuration(int);

    int fadeOutDuration() const;
    void setFadeOutDuration(int);

    qreal angle() const;
    void setAngle(qreal);

    qreal angleDeviation() const;
    void setAngleDeviation(qreal);

    qreal velocity() const;
    void setVelocity(qreal);

    qreal velocityDeviation() const;
    void setVelocityDeviation(qreal);

    QVariant model() const;
    void setModel(const QVariant &);

    QDeclarativeComponent *delegate() const;
    void setDelegate(QDeclarativeComponent *);

    QDeclarativeListProperty<QxParticleMotion> motion();

public Q_SLOTS:
    void burst(int count, int emissionRate=-1);
    void fastForward(int ms);

protected:
    virtual void componentComplete();

Q_SIGNALS:
    void countChanged();
    void emissionRateChanged();
    void emissionVarianceChanged();
    void lifeSpanChanged();
    void lifeSpanDeviationChanged();
    void fadeInDurationChanged();
    void fadeOutDurationChanged();
    void angleChanged();
    void angleDeviationChanged();
    void velocityChanged();
    void velocityDeviationChanged();
    void emittingChanged();

private:
    Q_DISABLE_COPY(QxParticles)
    Q_DECLARE_PRIVATE(QxParticles)
    friend class QxParticleField;
};

class QxParticleNode;
class QxParticleField;
class QxParticlesPainter : public QxItem
{
    Q_OBJECT

    Q_PROPERTY(QUrl source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(QUrl mask READ mask WRITE setMask)
public:
    QxParticlesPainter(QxItem* parent = 0)
        : QxItem(parent), field(0), node(0)
    {
    }
    ~QxParticlesPainter();

    QUrl source() const;
    void setSource(const QUrl &);

    QUrl mask() const;
    void setMask(const QUrl &);

    virtual void update();

    QxParticleField *field;
    QxParticleNode *node;
    QUrl url;
    QDeclarativePixmap image;
    QUrl maskUrl;
    QImage maskImage;

Q_SIGNALS:
    void sourceChanged();

private Q_SLOTS:
    void imageLoaded();
};

class QxParticleField : public QxItem
{
    Q_OBJECT

    Q_PROPERTY(QxParticles *emitter READ emitter WRITE setEmitter)
    Q_PROPERTY(QDeclarativeListProperty<QxParticlesPainter> painters READ painters)
public:
    QxParticleField(QxItem* parent = 0)
        : QxItem(parent), m_emitter(0)
    {
    }

    QxParticles *emitter() const;
    void setEmitter(QxParticles *emitter);

    QDeclarativeListProperty<QxParticlesPainter> painters();

    void update();
    QList<QxParticle> particles();

private:
    QxParticles *m_emitter;
    QList<QxParticlesPainter*> m_painters;
    static void painters_append(QDeclarativeListProperty<QxParticlesPainter> *, QxParticlesPainter *);
    static int painters_count(QDeclarativeListProperty<QxParticlesPainter> *);
    static QxParticlesPainter* painters_at(QDeclarativeListProperty<QxParticlesPainter> *, int index);
};

QT_END_NAMESPACE

QML_DECLARE_TYPE(QxParticleMotion)
QML_DECLARE_TYPE(QxParticleMotionLinear)
QML_DECLARE_TYPE(QxParticleMotionGravity)
QML_DECLARE_TYPE(QxParticleMotionWander)
QML_DECLARE_TYPE(QxParticleMotionRandomDrift)
QML_DECLARE_TYPE(QxParticleMotionAcceleration)
QML_DECLARE_TYPE(QxParticleMotionGravityWell)
QML_DECLARE_TYPE(QxParticleMotionAntiGravity)
QML_DECLARE_TYPE(QxParticleKillZone)
QML_DECLARE_TYPE(QxParticles)
QML_DECLARE_TYPE(QxParticlesPainter)
QML_DECLARE_TYPE(QxParticleField)

QT_END_HEADER

#endif
