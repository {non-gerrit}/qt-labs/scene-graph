/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxtext_p.h"
#include "qxtext_p_p.h"

#include "textnode.h"

#include <QtDeclarative/qdeclarativeinfo.h>

#include <private/qdeclarativestyledtext_p.h>
#include <private/qdeclarativepixmapcache_p.h>
#include <private/qcssparser_p.h>

#include <QSet>
#include <QTextLayout>
#include <QTextLine>
#include <QTextDocument>
#include <QTextCursor>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QAbstractTextDocumentLayout>
#include <QTextBlock>
#include <QTextList>
#include <QTextFrame>
#include <QTextTable>
#include <qmath.h>
#include <QXmlStreamReader>

class QTextDocumentWithImageResources : public QTextDocument {
    Q_OBJECT

public:
    QTextDocumentWithImageResources(QxText *textItem);
    virtual ~QTextDocumentWithImageResources();

    void setText(const QString &);
    int resourcesLoading() const { return m_outstanding > 0; }

protected:
    QVariant loadResource(int type, const QUrl &name);

private slots:
    void requestFinished();

private:
    QHash<QUrl, QDeclarativePixmap *> m_resources;
    QxText *m_textItem;
    int m_outstanding;
    static QSet<QUrl> errors;
};

QSet<QUrl> QTextDocumentWithImageResources::errors;

QTextDocumentWithImageResources::QTextDocumentWithImageResources(QxText *textItem)
    : m_textItem(textItem), m_outstanding(0)
{
}

QTextDocumentWithImageResources::~QTextDocumentWithImageResources()
{
    if (!m_resources.isEmpty())
        qDeleteAll(m_resources);
}

void QTextDocumentWithImageResources::setText(const QString &text)
{
    if (!m_resources.isEmpty()) {
        qWarning("CLEAR");
        qDeleteAll(m_resources);
        m_resources.clear();
        m_outstanding = 0;
    }

#ifndef QT_NO_TEXTHTMLPARSER
    setHtml(text);
#else
    setPlainText(text);
#endif
}

QVariant QTextDocumentWithImageResources::loadResource(int type, const QUrl &name)
{
    QDeclarativeContext *context = qmlContext(m_textItem);
    Q_ASSERT(context != 0);

    QUrl url = context->resolvedUrl(name);

    if (type == QTextDocument::ImageResource) {
        QHash<QUrl, QDeclarativePixmap *>::Iterator iter = m_resources.find(url);

        if (iter == m_resources.end()) {
            QDeclarativePixmap *p = new QDeclarativePixmap(context->engine(), url);
            iter = m_resources.insert(name, p);

            if (p->isLoading()) {
                p->connectFinished(this, SLOT(requestFinished()));
                m_outstanding++;
            }
        }

        QDeclarativePixmap *p = *iter;
        if (p->isReady()) {
            return p->pixmap();
        } else if (p->isError()) {
            if (!errors.contains(url)) {
                errors.insert(url);
                qmlInfo(m_textItem) << p->error();
            }
        }
    }

    return QTextDocument::loadResource(type,url); // The *resolved* URL
}

void QTextDocumentWithImageResources::requestFinished()
{
    m_outstanding--;
    if (m_outstanding == 0) {
        QString text = m_textItem->text();
#ifndef QT_NO_TEXTHTMLPARSER
        setHtml(text);
#else
        setPlainText(text);
#endif
        QxTextPrivate *d = QxTextPrivate::get(m_textItem);
        d->node->setUpdateFlag(TextNode::UpdateAll);
    }
}

QxText::QxText(QxItem *parent)
: QxItem(*(new QxTextPrivate), parent)
{
    setAcceptedMouseButtons(Qt::LeftButton);
}

QxText::~QxText()
{
    Q_D(QxText);
    delete d->node;
}

void QxTextPrivate::ensureNodeHasTextDocument()
{
    Q_Q(QxText);
    if (node->isRichText() && node->textDocument() == 0)
        node->setTextDocument(new QTextDocumentWithImageResources(q));
}

QFont QxText::font() const
{
    Q_D(const QxText);
    return d->font;
}

void QxText::setFont(const QFont &font)
{
    Q_D(QxText);
    if (d->font == font)
        return;

    d->font = font;

    if (d->node) {
        d->node->setFont(font);
        if (!widthValid())
            d->setImplicitSizeBasedOnContents();
    }

    emit fontChanged(font);
}

void QxTextPrivate::setImplicitSizeBasedOnContents()
{
    Q_Q(QxText);
    if (!q->isComponentComplete())
        return;
    QSizeF implicitSize = node->contentsSize();
    q->setImplicitWidth(implicitSize.width());
    q->setImplicitHeight(implicitSize.height());
}

void QxText::setText(const QString &text)
{
    Q_D(QxText);
    if (d->text == text)
        return;

    d->text = text;

    if (d->node) {
        d->node->setText(text);
        d->ensureNodeHasTextDocument();
        if (!widthValid())
            d->setImplicitSizeBasedOnContents();
    }

    emit textChanged(text);
}

QString QxText::text() const
{
    Q_D(const QxText);
    return d->text;
}

void QxText::setColor(const QColor &color)
{
    Q_D(QxText);
    if (d->color == color)
        return;

    d->color = color;

    if (d->node)
        d->node->setColor(color);

    emit colorChanged(color);
}

QColor QxText::color() const
{
    Q_D(const QxText);
    return d->color;
}

QxText::TextStyle QxText::style() const
{
    Q_D(const QxText);
    return d->textStyle;
}

void QxText::setStyle(QxText::TextStyle style)
{
    Q_D(QxText);
    if (d->textStyle == style)
        return;

    d->textStyle = style;

    if (d->node) {
        d->node->setTextStyle(TextNode::TextStyle(style));
        if (!widthValid())
            d->setImplicitSizeBasedOnContents();
    }

    emit styleChanged(style);
}

void QxText::setStyleColor(const QColor &color)
{
    Q_D(QxText);
    if (d->styleColor == color)
        return;

    d->styleColor = color;

    if (d->node)
        d->node->setStyleColor(color);

    emit styleColorChanged(color);
}

QColor QxText::styleColor() const
{
    Q_D(const QxText);
    return d->styleColor;
}

QxText::HAlignment QxText::hAlign() const
{
    Q_D(const QxText);
    return QxText::HAlignment(int(d->alignment & Qt::AlignHorizontal_Mask));
}

void QxText::setHAlign(HAlignment align)
{
    Q_D(QxText);
    if (hAlign() == align)
        return;

    Qt::Alignment currentVerticalAlignment = d->alignment & Qt::AlignVertical_Mask;
    d->alignment = currentVerticalAlignment | Qt::AlignmentFlag(align);

    if (d->node)
        d->node->setAlignment(d->alignment);

    emit horizontalAlignmentChanged(align);
}

QxText::VAlignment QxText::vAlign() const
{
    Q_D(const QxText);
    return QxText::VAlignment(int(d->alignment & Qt::AlignVertical_Mask));
}

void QxText::setVAlign(VAlignment align)
{
    Q_D(QxText);
    if (vAlign() == align)
        return;

    Qt::Alignment currentHorizontalAlignment = d->alignment & Qt::AlignHorizontal_Mask;
    d->alignment = currentHorizontalAlignment | Qt::AlignmentFlag(align);

    if (d->node)
        d->node->setAlignment(d->alignment);

    emit verticalAlignmentChanged(align);
}

QxText::WrapMode QxText::wrapMode() const
{
    Q_D(const QxText);
    return d->wrapMode;
}

void QxText::setWrapMode(WrapMode mode)
{
    Q_D(QxText);
    if (mode == d->wrapMode)
        return;

    d->wrapMode = mode;

    if (d->node) {
        d->node->setWrapMode(QTextOption::WrapMode(mode));
        if (!widthValid())
            d->setImplicitSizeBasedOnContents();
    }

    emit wrapModeChanged();
}

QxText::TextFormat QxText::textFormat() const
{
    Q_D(const QxText);
    return d->textFormat;
}

void QxText::setTextFormat(TextFormat format)
{
    Q_D(QxText);
    if (format == d->textFormat)
        return;

    d->textFormat = format;

    if (d->node) {
        d->node->setTextFormat(Qt::TextFormat(format));
        d->ensureNodeHasTextDocument();
        if (!widthValid())
            d->setImplicitSizeBasedOnContents();
    }

    emit textFormatChanged(format);
}

void QxText::setUsePixmapCache(bool on)
{
    Q_D(QxText);
    if (on == usePixmapCache())
        return;

    d->usePixmapCache = on;

    if (d->node)
        d->node->setUsePixmapCache(on);

    emit usePixmapCacheChanged();
}

bool QxText::usePixmapCache() const
{
    Q_D(const QxText);
    return d->usePixmapCache;
}

QxText::TextElideMode QxText::elideMode() const
{
    Q_D(const QxText);
    return d->elideMode;
}

void QxText::setElideMode(QxText::TextElideMode mode)
{
    Q_D(QxText);
    if (mode == d->elideMode)
        return;

    d->elideMode = mode;

    if (d->node) {
        d->node->setElideMode(Qt::TextElideMode(mode));
        if (!widthValid())
            d->setImplicitSizeBasedOnContents();
    }

    emit elideModeChanged(mode);
}

void QxText::geometryChanged(const QRectF &newGeometry,
                              const QRectF &oldGeometry)
{
    Q_D(QxText);

    if (d->node) {
        d->node->setGeometry(newGeometry);
        if (widthValid())
            d->node->setWidthRestriction(width());
        if (heightValid())
            d->node->setHeightRestriction(height());
    }

    QxItem::geometryChanged(newGeometry, oldGeometry);
}

void QxText::renderOpacityChanged(qreal newOpacity, qreal oldOpacity)
{
    Q_D(QxText);

    QxItem::renderOpacityChanged(newOpacity, oldOpacity);
    if (d->node)
        d->node->setOpacity(newOpacity);
}

qreal QxText::paintedWidth() const
{
    return implicitWidth();
}

qreal QxText::paintedHeight() const
{
    return implicitHeight();
}

void QxText::componentComplete()
{
    Q_D(QxText);
    QxItem::componentComplete();

    d->node = new TextNode(QSGContext::current);
    d->node->setText(d->text);
    d->node->setFont(d->font);
    d->node->setColor(d->color);
    d->node->setTextStyle(TextNode::TextStyle(d->textStyle));
    d->node->setStyleColor(d->styleColor);
    d->node->setAlignment(d->alignment);
    d->node->setWrapMode(QTextOption::WrapMode(d->wrapMode));
    d->node->setTextFormat(Qt::TextFormat(d->textFormat));
    d->node->setElideMode(Qt::TextElideMode(d->elideMode));
    d->node->setUsePixmapCache(d->usePixmapCache);
    d->node->setLinearFiltering(d->smooth);
    d->node->setOpacity(d->renderOpacity);
    d->node->setGeometry(geometry());
    d->ensureNodeHasTextDocument();

    if (widthValid())
        d->node->setWidthRestriction(width());
    if (heightValid())
        d->node->setHeightRestriction(height());

    if (!widthValid() || !heightValid())
        d->setImplicitSizeBasedOnContents();

    setPaintNode(d->node);
}

void QxText::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_D(QxText);

    QAbstractTextDocumentLayout *documentLayout = d->node->isRichText()
                                                  ? d->node->textDocument()->documentLayout()
                                                  : 0;
    if (documentLayout == 0 || documentLayout->anchorAt(event->pos()).isEmpty()) {
        event->setAccepted(false);
        d->activeLink = QString();
    } else {
        d->activeLink = documentLayout->anchorAt(event->pos());
    }

    // ### may malfunction if two of the same links are clicked & dragged onto each other)
    if (!event->isAccepted())
        QxItem::mousePressEvent(event);
}

void QxText::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_D(QxText);

    // ### confirm the link, and send a signal out
    QAbstractTextDocumentLayout *documentLayout = d->node->isRichText()
                                                  ? d->node->textDocument()->documentLayout()
                                                  : 0;
    if (documentLayout != 0 && d->activeLink == documentLayout->anchorAt(event->pos()))
        emit linkActivated(d->activeLink);
    else
        event->setAccepted(false);

    if (!event->isAccepted())
        QxItem::mouseReleaseEvent(event);
}

void QxText::smoothChange(bool newSmooth, bool oldSmooth)
{
    Q_D(QxText);

    QxItem::smoothChange(newSmooth, oldSmooth);
    if (d->node)
        d->node->setLinearFiltering(newSmooth);
}

#include "qxtext.moc"
