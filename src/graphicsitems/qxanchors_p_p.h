/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXANCHORS_P_H
#define QXANCHORS_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "qxanchors_p.h"
#include "qxitemchangelistener_p.h"

#include <private/qobject_p.h>

class QxAnchorLine
{
public:
    QxAnchorLine() : item(0), anchorLine(Invalid) {}

    enum AnchorLine {
        Invalid = 0x0,
        Left = 0x01,
        Right = 0x02,
        Top = 0x04,
        Bottom = 0x08,
        HCenter = 0x10,
        VCenter = 0x20,
        Baseline = 0x40,
        Horizontal_Mask = Left | Right | HCenter,
        Vertical_Mask = Top | Bottom | VCenter | Baseline
    };

    QxItem *item;
    AnchorLine anchorLine;
};

inline bool operator==(const QxAnchorLine& a, const QxAnchorLine& b)
{
    return a.item == b.item && a.anchorLine == b.anchorLine;
}

class QxAnchorsPrivate : public QObjectPrivate, public QxItemChangeListener
{
    Q_DECLARE_PUBLIC(QxAnchors)
public:
    QxAnchorsPrivate(QxItem *i)
    : componentComplete(true), updatingMe(false), updatingHorizontalAnchor(0),
      updatingVerticalAnchor(0), updatingFill(0), updatingCenterIn(0), item(i), usedAnchors(0), fill(0),
      centerIn(0), leftMargin(0), rightMargin(0), topMargin(0), bottomMargin(0),
      margins(0), vCenterOffset(0), hCenterOffset(0), baselineOffset(0)
    {
    }

    void clearItem(QxItem *);

    void addDepend(QxItem *);
    void remDepend(QxItem *);
    bool isItemComplete() const;

    bool componentComplete:1;
    bool updatingMe:1;
    uint updatingHorizontalAnchor:2;
    uint updatingVerticalAnchor:2;
    uint updatingFill:2;
    uint updatingCenterIn:2;

    void setItemHeight(qreal);
    void setItemWidth(qreal);
    void setItemX(qreal);
    void setItemY(qreal);
    void setItemPos(const QPointF &);
    void setItemSize(const QSizeF &);

    void updateOnComplete();
    void updateMe();

    // QxItemGeometryListener interface
    void itemGeometryChanged(QxItem *, const QRectF &, const QRectF &);
    QxAnchorsPrivate *anchorPrivate() { return this; }

    bool checkHValid() const;
    bool checkVValid() const;
    bool checkHAnchorValid(QxAnchorLine anchor) const;
    bool checkVAnchorValid(QxAnchorLine anchor) const;
    bool calcStretch(const QxAnchorLine &edge1, const QxAnchorLine &edge2, qreal offset1, qreal offset2, QxAnchorLine::AnchorLine line, qreal &stretch);

    void updateHorizontalAnchors();
    void updateVerticalAnchors();
    void fillChanged();
    void centerInChanged();

    QxItem *item;
    QxAnchors::Anchors usedAnchors;

    QxItem *fill;
    QxItem *centerIn;

    QxAnchorLine left;
    QxAnchorLine right;
    QxAnchorLine top;
    QxAnchorLine bottom;
    QxAnchorLine vCenter;
    QxAnchorLine hCenter;
    QxAnchorLine baseline;

    qreal leftMargin;
    qreal rightMargin;
    qreal topMargin;
    qreal bottomMargin;
    qreal margins;
    qreal vCenterOffset;
    qreal hCenterOffset;
    qreal baselineOffset;
};

Q_DECLARE_METATYPE(QxAnchorLine)

#endif
