/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXBORDERIMAGE_H
#define QXBORDERIMAGE_H

#include "qximagebase_p.h"

#include <QtNetwork/qnetworkreply.h>

class QDeclarativeScaleGrid;
class QDeclarativeGridScaledImage;
class QxBorderImagePrivate;
class QxBorderImage : public QxImageBase
{
    Q_OBJECT
    Q_ENUMS(TileMode)

    Q_PROPERTY(QDeclarativeScaleGrid *border READ border CONSTANT)
    Q_PROPERTY(TileMode horizontalTileMode READ horizontalTileMode WRITE setHorizontalTileMode NOTIFY horizontalTileModeChanged)
    Q_PROPERTY(TileMode verticalTileMode READ verticalTileMode WRITE setVerticalTileMode NOTIFY verticalTileModeChanged)

public:
    QxBorderImage(QxItem *parent=0);
    ~QxBorderImage();

    QDeclarativeScaleGrid *border();

    enum TileMode { Stretch = Qt::StretchTile, Repeat = Qt::RepeatTile, Round = Qt::RoundTile };

    TileMode horizontalTileMode() const;
    void setHorizontalTileMode(TileMode);

    TileMode verticalTileMode() const;
    void setVerticalTileMode(TileMode);

    void setSource(const QUrl &url);

Q_SIGNALS:
    void horizontalTileModeChanged();
    void verticalTileModeChanged();

protected:
    virtual void load();
    virtual void componentComplete();
    virtual void renderOpacityChanged(qreal newOpacity, qreal oldOpacity);
    virtual void geometryChanged(const QRectF &, const QRectF &);
    virtual void smoothChange(bool newSmooth, bool oldSmooth);

private:
    void setGridScaledImage(const QDeclarativeGridScaledImage& sci);

private Q_SLOTS:
    void requestFinished();
    void sciRequestFinished();

private:
    Q_DISABLE_COPY(QxBorderImage)
    Q_DECLARE_PRIVATE(QxBorderImage)
};

QML_DECLARE_TYPE(QxBorderImage)

#endif // QXBORDERIMAGE_H
