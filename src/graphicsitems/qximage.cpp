/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qximage_p.h"
#include "qximage_p_p.h"

#include "adaptationlayer.h"

#include <QtGui/qevent.h>
#include <QtGui/qpainter.h>

QxImage::QxImage(QxItem *parent)
: QxImageBase(*(new QxImagePrivate), parent)
{
}

QxImage::QxImage(QxImagePrivate &dd, QxItem *parent)
: QxImageBase(dd, parent)
{
}

QxImage::~QxImage()
{
    Q_D(QxImage);
    delete d->node;
}

QxImage::FillMode QxImage::fillMode() const
{
    Q_D(const QxImage);
    return d->fillMode;
}

void QxImage::setFillMode(FillMode mode)
{
    Q_D(QxImage);
    if (d->fillMode == mode)
        return;
    d->fillMode = mode;

    if (d->node)
        d->update();

    emit fillModeChanged();
}

qreal QxImage::paintedWidth() const
{
    Q_D(const QxImage);
    return d->paintedWidth;
}

qreal QxImage::paintedHeight() const
{
    Q_D(const QxImage);
    return d->paintedHeight;
}

void QxImagePrivate::update()
{
    Q_Q(QxImage);

    if (node && (pixmapDirty || pix.isNull())) {
        q->setPaintNode(0);
        delete node;
        node = 0;
    }

    // Update painted geometry properties
    if (fillMode == QxImage::PreserveAspectFit) {
        if (!pix.width() || !pix.height())
            return;
        qreal widthScale = q->width() / qreal(pix.width());
        qreal heightScale = q->height() / qreal(pix.height());
        if (widthScale <= heightScale) {
            paintedWidth = q->width();
            paintedHeight = widthScale * qreal(pix.height());
        } else if(heightScale < widthScale) {
            paintedWidth = heightScale * qreal(pix.width());
            paintedHeight = q->height();
        }
        if (q->widthValid() && !q->heightValid()) {
            q->setImplicitHeight(paintedHeight);
        }
        if (q->heightValid() && !q->widthValid()) {
            q->setImplicitWidth(paintedWidth);
        }
    } else {
        paintedWidth = q->width();
        paintedHeight = q->height();
    }
    emit q->paintedGeometryChanged();

    // Regenerate node
    if (!pix.isNull()) {
        QRectF targetRect;
        QRectF sourceRect;
        bool clampToEdge = true;

        if (fillMode == QxImage::Stretch) {
            targetRect = QRectF(0, 0, q->width(), q->height());
            sourceRect = pix.rect();
        } else if (fillMode == QxImage::PreserveAspectFit) {
            targetRect = QRectF((q->width() - paintedWidth) / 2., (q->height() - paintedHeight) / 2.,
                                paintedWidth, paintedHeight);
            sourceRect = pix.rect();
        } else if (fillMode == QxImage::PreserveAspectCrop) {
            targetRect = QRect(0, 0, q->width(), q->height());
            qreal wscale = q->width() / qreal(pix.width());
            qreal hscale = q->height() / qreal(pix.height());

            if (wscale > hscale) {
                int src = (hscale / wscale) * qreal(pix.height());
                sourceRect = QRectF(0, (pix.height() - src) / 2, pix.width(), src);
            } else {
                int src = (wscale / hscale) * qreal(pix.width());
                sourceRect = QRectF((pix.width() - src) / 2, 0, src, pix.height());
            }

        } else if (fillMode == QxImage::Tile) {
            targetRect = QRectF(0, 0, q->width(), q->height());
            sourceRect = QRectF(0, 0, q->width(), q->height());
            clampToEdge = false;
        } else if (fillMode == QxImage::TileHorizontally) {
            targetRect = QRectF(0, 0, q->width(), q->height());
            sourceRect = QRectF(0, 0, q->width(), pix.height());
            clampToEdge = false;
        } else if (fillMode == QxImage::TileVertically) {
            targetRect = QRectF(0, 0, q->width(), q->height());
            sourceRect = QRectF(0, 0, pix.width(), q->height());
            clampToEdge = false;
        }

        if (!node) {
            node = QSGContext::current->createTextureNode();
        }

        QRectF nsrect(sourceRect.x() / pix.width(),
                      sourceRect.y() / pix.height(),
                      sourceRect.width() / pix.width(),
                      sourceRect.height() / pix.height());

        node->setTexture(texture);
        node->setRect(targetRect);
        node->setSourceRect(nsrect);
        node->setOpacity(renderOpacity);
        node->setClampToEdge(clampToEdge);
        node->setLinearFiltering(smooth);
        q->setPaintNode(node);
    }

    pixmapDirty = false;
}

void QxImagePrivate::updateOpacity()
{
    if (!node || pix.isNull())
        return;

    node->setOpacity(renderOpacity);
}

void QxImage::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    Q_D(QxImage);

    QxImageBase::geometryChanged(newGeometry, oldGeometry);

    if (d->node && !d->pix.isNull() && newGeometry.size() != oldGeometry.size())
        d->update();
}

void QxImage::pixmapChange()
{
    Q_D(QxImage);

    setImplicitWidth(d->pix.width());
    setImplicitHeight(d->pix.height());
    d->status = d->pix.isNull() ? QxImageBase::Null : QxImageBase::Ready;
    d->pixmapDirty = true;
    d->update();
}

void QxImage::renderOpacityChanged(qreal newOpacity, qreal oldOpacity)
{
    Q_D(QxImage);

    QxImageBase::renderOpacityChanged(newOpacity, oldOpacity);

    if (d->node && (newOpacity == 1.) != (oldOpacity == 1.)) {
        setPaintNode(0);
        delete d->node;
        d->node = 0;

        if (!d->pix.isNull())
            d->update();
        return;
    }

    d->updateOpacity();
}

void QxImage::smoothChange(bool newSmooth, bool oldSmooth)
{
    Q_D(QxImage);

    QxImageBase::smoothChange(newSmooth, oldSmooth);
    if (d->node)
        d->update();
}

void QxImage::componentComplete()
{
    //pixmapChange is called by QxImageBase
    QxImageBase::componentComplete();
}
