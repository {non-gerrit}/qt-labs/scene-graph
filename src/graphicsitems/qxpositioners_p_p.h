/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXPOSITIONERS_P_H
#define QXPOSITIONERS_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "qxpositioners_p.h"

#include "qxitem_p.h"

#include <private/qdeclarativestate_p.h>
#include <private/qdeclarativetransitionmanager_p_p.h>
#include <private/qdeclarativestateoperations_p.h>

#include <QtCore/qobject.h>
#include <QtCore/qstring.h>
#include <QtCore/qtimer.h>

class QxBasePositionerPrivate : public QxItemPrivate, public QxItemChangeListener
{
    Q_DECLARE_PUBLIC(QxBasePositioner)

public:
    QxBasePositionerPrivate()
        : spacing(0), type(QxBasePositioner::None)
        , moveTransition(0), addTransition(0), queuedPositioning(false)
        , doingPositioning(false), anchorConflict(false)
    {
        setChildrenDoNotOverlap(true);
    }

    void init(QxBasePositioner::PositionerType at)
    {
        type = at;
    }

    int spacing;

    QxBasePositioner::PositionerType type;
    QDeclarativeTransition *moveTransition;
    QDeclarativeTransition *addTransition;
    QDeclarativeStateOperation::ActionList addActions;
    QDeclarativeStateOperation::ActionList moveActions;
    QDeclarativeTransitionManager addTransitionManager;
    QDeclarativeTransitionManager moveTransitionManager;

    void watchChanges(QxItem *other);
    void unwatchChanges(QxItem* other);
    bool queuedPositioning : 1;
    bool doingPositioning : 1;
    bool anchorConflict : 1;

    virtual void itemSiblingOrderChanged(QxItem* other)
    {
        Q_Q(QxBasePositioner);
        Q_UNUSED(other);
        if(!queuedPositioning){
            //Delay is due to many children often being reordered at once
            //And we only want to reposition them all once
            QTimer::singleShot(0,q,SLOT(prePositioning()));
            queuedPositioning = true;
        }
    }

    void itemGeometryChanged(QxItem *, const QRectF &newGeometry, const QRectF &oldGeometry)
    {
        Q_Q(QxBasePositioner);
        if (newGeometry.size() != oldGeometry.size())
            q->prePositioning();
    }
    virtual void itemVisibilityChanged(QxItem *)
    {
        Q_Q(QxBasePositioner);
        q->prePositioning();
    }
    virtual void itemOpacityChanged(QxItem *)
    {
        Q_Q(QxBasePositioner);
        q->prePositioning();
    }

    void itemDestroyed(QxItem *item)
    {
        Q_Q(QxBasePositioner);
        q->positionedItems.removeOne(QxBasePositioner::PositionedItem(item));
    }
};

#endif
