/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxanchors_p_p.h"

#include "qxitem.h"
#include "qxitem_p.h"

#include <QtDeclarative/qdeclarativeinfo.h>

//TODO: should we cache relationships, so we don't have to check each time (parent-child or sibling)?
//TODO: support non-parent, non-sibling (need to find lowest common ancestor)

static qreal hcenter(QxItem *item)
{
    qreal width = item->width();
    int iw = width;
    if (iw % 2)
        return (width + 1) / 2;
    else
        return width / 2;
}

static qreal vcenter(QxItem *item)
{
    qreal height = item->height();
    int ih = height;
    if (ih % 2)
        return (height + 1) / 2;
    else
        return height / 2;
}

//### const item?
//local position
static qreal position(QxItem *item, QxAnchorLine::AnchorLine anchorLine)
{
    qreal ret = 0.0;
    switch(anchorLine) {
    case QxAnchorLine::Left:
        ret = item->x();
        break;
    case QxAnchorLine::Right:
        ret = item->x() + item->width();
        break;
    case QxAnchorLine::Top:
        ret = item->y();
        break;
    case QxAnchorLine::Bottom:
        ret = item->y() + item->height();
        break;
    case QxAnchorLine::HCenter:
        ret = item->x() + hcenter(item);
        break;
    case QxAnchorLine::VCenter:
        ret = item->y() + vcenter(item);
        break;
    case QxAnchorLine::Baseline:
        ret = item->y() + item->baselineOffset();
        break;
    default:
        break;
    }

    return ret;
}

//position when origin is 0,0
static qreal adjustedPosition(QxItem *item, QxAnchorLine::AnchorLine anchorLine)
{
    qreal ret = 0.0;
    switch(anchorLine) {
    case QxAnchorLine::Left:
        ret = 0.0;
        break;
    case QxAnchorLine::Right:
        ret = item->width();
        break;
    case QxAnchorLine::Top:
        ret = 0.0;
        break;
    case QxAnchorLine::Bottom:
        ret = item->height();
        break;
    case QxAnchorLine::HCenter:
        ret = hcenter(item);
        break;
    case QxAnchorLine::VCenter:
        ret = vcenter(item);
        break;
    case QxAnchorLine::Baseline:
        ret = item->baselineOffset();
        break;
    default:
        break;
    }

    return ret;
}

/*!
    \internal
    \class QxAnchors
    \since 4.7
    \brief The QxAnchors class provides a way to lay out items relative to other items.

    \warning Currently, only anchoring to siblings or parent is supported.
*/

QxAnchors::QxAnchors(QObject *parent)
  : QObject(*new QxAnchorsPrivate(0), parent)
{
    qFatal("QxAnchors::QxAnchors(QObject*) called");
}

QxAnchors::QxAnchors(QxItem *item, QObject *parent)
  : QObject(*new QxAnchorsPrivate(item), parent)
{
}

QxAnchors::~QxAnchors()
{
    Q_D(QxAnchors);
    d->remDepend(d->fill);
    d->remDepend(d->centerIn);
    d->remDepend(d->left.item);
    d->remDepend(d->right.item);
    d->remDepend(d->top.item);
    d->remDepend(d->bottom.item);
    d->remDepend(d->vCenter.item);
    d->remDepend(d->hCenter.item);
    d->remDepend(d->baseline.item);
}

void QxAnchorsPrivate::fillChanged()
{
    if (!fill || !isItemComplete())
        return;

    if (updatingFill < 2) {
        ++updatingFill;

        if (fill == item->parentItem()) {                         //child-parent
            setItemPos(QPointF(leftMargin, topMargin));
        } else if (fill->parentItem() == item->parentItem()) {   //siblings
            setItemPos(QPointF(fill->x()+leftMargin, fill->y()+topMargin));
        }
        setItemSize(QSizeF(fill->width()-leftMargin-rightMargin, fill->height()-topMargin-bottomMargin));

        --updatingFill;
    } else {
        // ### Make this certain :)
        qmlInfo(item) << QxAnchors::tr("Possible anchor loop detected on fill.");
    }

}

void QxAnchorsPrivate::centerInChanged()
{
    if (!centerIn || fill || !isItemComplete())
        return;

    if (updatingCenterIn < 2) {
        ++updatingCenterIn;
        if (centerIn == item->parentItem()) {
            QxItem *parent = item->parentItem();
            QPointF p(hcenter(parent) - hcenter(item) + hCenterOffset,
                      vcenter(parent) - vcenter(item) + vCenterOffset);
            setItemPos(p);

        } else if (centerIn->parentItem() == item->parentItem()) {
            QPointF p(centerIn->x() + hcenter(centerIn) - hcenter(item) + hCenterOffset,
                      centerIn->y() + vcenter(centerIn) - vcenter(item) + vCenterOffset);
            setItemPos(p);
        }

        --updatingCenterIn;
    } else {
        // ### Make this certain :)
        qmlInfo(item) << QxAnchors::tr("Possible anchor loop detected on centerIn.");
    }
}

void QxAnchorsPrivate::clearItem(QxItem *item)
{
    if (!item)
        return;
    if (fill == item)
        fill = 0;
    if (centerIn == item)
        centerIn = 0;
    if (left.item == item) {
        left.item = 0;
        usedAnchors &= ~QxAnchors::LeftAnchor;
    }
    if (right.item == item) {
        right.item = 0;
        usedAnchors &= ~QxAnchors::RightAnchor;
    }
    if (top.item == item) {
        top.item = 0;
        usedAnchors &= ~QxAnchors::TopAnchor;
    }
    if (bottom.item == item) {
        bottom.item = 0;
        usedAnchors &= ~QxAnchors::BottomAnchor;
    }
    if (vCenter.item == item) {
        vCenter.item = 0;
        usedAnchors &= ~QxAnchors::VCenterAnchor;
    }
    if (hCenter.item == item) {
        hCenter.item = 0;
        usedAnchors &= ~QxAnchors::HCenterAnchor;
    }
    if (baseline.item == item) {
        baseline.item = 0;
        usedAnchors &= ~QxAnchors::BaselineAnchor;
    }
}

void QxAnchorsPrivate::addDepend(QxItem *item)
{
    if (!item)
        return;
    QxItemPrivate * itemPrivate = QxItemPrivate::get(item);
    itemPrivate->addItemChangeListener(this, QxItemPrivate::Geometry);
}

void QxAnchorsPrivate::remDepend(QxItem *item)
{
    if (!item)
        return;
    QxItemPrivate * itemPrivate = QxItemPrivate::get(item);
    itemPrivate->removeItemChangeListener(this, QxItemPrivate::Geometry);
}

bool QxAnchorsPrivate::isItemComplete() const
{
    return componentComplete;
}

void QxAnchors::classBegin()
{
    Q_D(QxAnchors);
    d->componentComplete = false;
}

void QxAnchors::componentComplete()
{
    Q_D(QxAnchors);
    d->componentComplete = true;
}

void QxAnchorsPrivate::setItemHeight(qreal v)
{
    updatingMe = true;
    item->setHeight(v);
    updatingMe = false;
}

void QxAnchorsPrivate::setItemWidth(qreal v)
{
    updatingMe = true;
    item->setWidth(v);
    updatingMe = false;
}

void QxAnchorsPrivate::setItemX(qreal v)
{
    updatingMe = true;
    item->setX(v);
    updatingMe = false;
}

void QxAnchorsPrivate::setItemY(qreal v)
{
    updatingMe = true;
    item->setY(v);
    updatingMe = false;
}

void QxAnchorsPrivate::setItemPos(const QPointF &v)
{
    updatingMe = true;
    item->setPos(v);
    updatingMe = false;
}

void QxAnchorsPrivate::setItemSize(const QSizeF &v)
{
    updatingMe = true;
    item->setSize(v);
    updatingMe = false;
}

void QxAnchorsPrivate::updateMe()
{
    if (updatingMe) {
        updatingMe = false;
        return;
    }

    fillChanged();
    centerInChanged();
    updateHorizontalAnchors();
    updateVerticalAnchors();
}

void QxAnchorsPrivate::updateOnComplete()
{
    fillChanged();
    centerInChanged();
    updateHorizontalAnchors();
    updateVerticalAnchors();
}

void QxAnchorsPrivate::itemGeometryChanged(QxItem *, const QRectF &newG, const QRectF &oldG)
{
    fillChanged();
    centerInChanged();
    if (newG.x() != oldG.x() || newG.width() != oldG.width())
        updateHorizontalAnchors();
    if (newG.y() != oldG.y() || newG.height() != oldG.height())
        updateVerticalAnchors();
}

QxItem *QxAnchors::fill() const
{
    Q_D(const QxAnchors);
    return d->fill;
}

void QxAnchors::setFill(QxItem *f)
{
    Q_D(QxAnchors);
    if (d->fill == f)
        return;

    if (!f) {
        d->remDepend(d->fill);
        d->fill = f;
        emit fillChanged();
        return;
    }
    if (f != d->item->parentItem() && f->parentItem() != d->item->parentItem()){
        qmlInfo(d->item) << tr("Cannot anchor to an item that isn't a parent or sibling.");
        return;
    }
    d->remDepend(d->fill);
    d->fill = f;
    d->addDepend(d->fill);
    emit fillChanged();
    d->fillChanged();
}

void QxAnchors::resetFill()
{
    setFill(0);
}

QxItem *QxAnchors::centerIn() const
{
    Q_D(const QxAnchors);
    return d->centerIn;
}

void QxAnchors::setCenterIn(QxItem* c)
{
    Q_D(QxAnchors);
    if (d->centerIn == c)
        return;

    if (!c) {
        d->remDepend(d->centerIn);
        d->centerIn = c;
        emit centerInChanged();
        return;
    }
    if (c != d->item->parentItem() && c->parentItem() != d->item->parentItem()){
        qmlInfo(d->item) << tr("Cannot anchor to an item that isn't a parent or sibling.");
        return;
    }

    d->remDepend(d->centerIn);
    d->centerIn = c;
    d->addDepend(d->centerIn);
    emit centerInChanged();
    d->centerInChanged();
}

void QxAnchors::resetCenterIn()
{
    setCenterIn(0);
}

bool QxAnchorsPrivate::calcStretch(const QxAnchorLine &edge1,
                                    const QxAnchorLine &edge2,
                                    qreal offset1,
                                    qreal offset2,
                                    QxAnchorLine::AnchorLine line,
                                    qreal &stretch)
{
    bool edge1IsParent = (edge1.item == item->parentItem());
    bool edge2IsParent = (edge2.item == item->parentItem());
    bool edge1IsSibling = (edge1.item->parentItem() == item->parentItem());
    bool edge2IsSibling = (edge2.item->parentItem() == item->parentItem());

    bool invalid = false;
    if ((edge2IsParent && edge1IsParent) || (edge2IsSibling && edge1IsSibling)) {
        stretch = (position(edge2.item, edge2.anchorLine) + offset2)
                    - (position(edge1.item, edge1.anchorLine) + offset1);
    } else if (edge2IsParent && edge1IsSibling) {
        stretch = (position(edge2.item, edge2.anchorLine) + offset2)
                    - (position(item->parentItem(), line)
                    + position(edge1.item, edge1.anchorLine) + offset1);
    } else if (edge2IsSibling && edge1IsParent) {
        stretch = (position(item->parentItem(), line) + position(edge2.item, edge2.anchorLine) + offset2)
                    - (position(edge1.item, edge1.anchorLine) + offset1);
    } else
        invalid = true;

    return invalid;
}

void QxAnchorsPrivate::updateVerticalAnchors()
{
    if (fill || centerIn || !isItemComplete())
        return;

    if (updatingVerticalAnchor < 2) {
        ++updatingVerticalAnchor;
        if (usedAnchors & QxAnchors::TopAnchor) {
            //Handle stretching
            bool invalid = true;
            qreal height = 0.0;
            if (usedAnchors & QxAnchors::BottomAnchor) {
                invalid = calcStretch(top, bottom, topMargin, -bottomMargin, QxAnchorLine::Top, height);
            } else if (usedAnchors & QxAnchors::VCenterAnchor) {
                invalid = calcStretch(top, vCenter, topMargin, vCenterOffset, QxAnchorLine::Top, height);
                height *= 2;
            }
            if (!invalid)
                setItemHeight(height);

            //Handle top
            if (top.item == item->parentItem()) {
                setItemY(adjustedPosition(top.item, top.anchorLine) + topMargin);
            } else if (top.item->parentItem() == item->parentItem()) {
                setItemY(position(top.item, top.anchorLine) + topMargin);
            }
        } else if (usedAnchors & QxAnchors::BottomAnchor) {
            //Handle stretching (top + bottom case is handled above)
            if (usedAnchors & QxAnchors::VCenterAnchor) {
                qreal height = 0.0;
                bool invalid = calcStretch(vCenter, bottom, vCenterOffset, -bottomMargin,
                                              QxAnchorLine::Top, height);
                if (!invalid)
                    setItemHeight(height*2);
            }

            //Handle bottom
            if (bottom.item == item->parentItem()) {
                setItemY(adjustedPosition(bottom.item, bottom.anchorLine) - item->height() - bottomMargin);
            } else if (bottom.item->parentItem() == item->parentItem()) {
                setItemY(position(bottom.item, bottom.anchorLine) - item->height() - bottomMargin);
            }
        } else if (usedAnchors & QxAnchors::VCenterAnchor) {
            //(stetching handled above)

            //Handle vCenter
            if (vCenter.item == item->parentItem()) {
                setItemY(adjustedPosition(vCenter.item, vCenter.anchorLine) - vcenter(item) + vCenterOffset);
            } else if (vCenter.item->parentItem() == item->parentItem()) {
                setItemY(position(vCenter.item, vCenter.anchorLine) - vcenter(item) + vCenterOffset);
            }
        } else if (usedAnchors & QxAnchors::BaselineAnchor) {
            //Handle baseline
            if (baseline.item == item->parentItem()) {
                setItemY(adjustedPosition(baseline.item, baseline.anchorLine) - item->baselineOffset() + baselineOffset);
            } else if (baseline.item->parentItem() == item->parentItem()) {
                setItemY(position(baseline.item, baseline.anchorLine) - item->baselineOffset() + baselineOffset);
            }
        }
        --updatingVerticalAnchor;
    } else {
        // ### Make this certain :)
        qmlInfo(item) << QxAnchors::tr("Possible anchor loop detected on vertical anchor.");
    }
}

void QxAnchorsPrivate::updateHorizontalAnchors()
{
    if (fill || centerIn || !isItemComplete())
        return;

    if (updatingHorizontalAnchor < 2) {
        ++updatingHorizontalAnchor;
        if (usedAnchors & QxAnchors::LeftAnchor) {
            //Handle stretching
            bool invalid = true;
            qreal width = 0.0;
            if (usedAnchors & QxAnchors::RightAnchor) {
                invalid = calcStretch(left, right, leftMargin, -rightMargin, QxAnchorLine::Left, width);
            } else if (usedAnchors & QxAnchors::HCenterAnchor) {
                invalid = calcStretch(left, hCenter, leftMargin, hCenterOffset, QxAnchorLine::Left, width);
                width *= 2;
            }
            if (!invalid)
                setItemWidth(width);

            //Handle left
            if (left.item == item->parentItem()) {
                setItemX(adjustedPosition(left.item, left.anchorLine) + leftMargin);
            } else if (left.item->parentItem() == item->parentItem()) {
                setItemX(position(left.item, left.anchorLine) + leftMargin);
            }
        } else if (usedAnchors & QxAnchors::RightAnchor) {
            //Handle stretching (left + right case is handled in updateLeftAnchor)
            if (usedAnchors & QxAnchors::HCenterAnchor) {
                qreal width = 0.0;
                bool invalid = calcStretch(hCenter, right, hCenterOffset, -rightMargin,
                                              QxAnchorLine::Left, width);
                if (!invalid)
                    setItemWidth(width*2);
            }

            //Handle right
            if (right.item == item->parentItem()) {
                setItemX(adjustedPosition(right.item, right.anchorLine) - item->width() - rightMargin);
            } else if (right.item->parentItem() == item->parentItem()) {
                setItemX(position(right.item, right.anchorLine) - item->width() - rightMargin);
            }
        } else if (usedAnchors & QxAnchors::HCenterAnchor) {
            //Handle hCenter
            if (hCenter.item == item->parentItem()) {
                setItemX(adjustedPosition(hCenter.item, hCenter.anchorLine) - hcenter(item) + hCenterOffset);
            } else if (hCenter.item->parentItem() == item->parentItem()) {
                setItemX(position(hCenter.item, hCenter.anchorLine) - hcenter(item) + hCenterOffset);
            }
        }

        --updatingHorizontalAnchor;
    } else {
        // ### Make this certain :)
        qmlInfo(item) << QxAnchors::tr("Possible anchor loop detected on horizontal anchor.");
    }
}

QxAnchorLine QxAnchors::top() const
{
    Q_D(const QxAnchors);
    return d->top;
}

void QxAnchors::setTop(const QxAnchorLine &edge)
{
    Q_D(QxAnchors);
    if (!d->checkVAnchorValid(edge) || d->top == edge)
        return;

    d->usedAnchors |= TopAnchor;

    if (!d->checkVValid()) {
        d->usedAnchors &= ~TopAnchor;
        return;
    }

    d->remDepend(d->top.item);
    d->top = edge;
    d->addDepend(d->top.item);
    emit topChanged();
    d->updateVerticalAnchors();
}

void QxAnchors::resetTop()
{
    Q_D(QxAnchors);
    d->usedAnchors &= ~TopAnchor;
    d->remDepend(d->top.item);
    d->top = QxAnchorLine();
    emit topChanged();
    d->updateVerticalAnchors();
}

QxAnchorLine QxAnchors::bottom() const
{
    Q_D(const QxAnchors);
    return d->bottom;
}

void QxAnchors::setBottom(const QxAnchorLine &edge)
{
    Q_D(QxAnchors);
    if (!d->checkVAnchorValid(edge) || d->bottom == edge)
        return;

    d->usedAnchors |= BottomAnchor;

    if (!d->checkVValid()) {
        d->usedAnchors &= ~BottomAnchor;
        return;
    }

    d->remDepend(d->bottom.item);
    d->bottom = edge;
    d->addDepend(d->bottom.item);
    emit bottomChanged();
    d->updateVerticalAnchors();
}

void QxAnchors::resetBottom()
{
    Q_D(QxAnchors);
    d->usedAnchors &= ~BottomAnchor;
    d->remDepend(d->bottom.item);
    d->bottom = QxAnchorLine();
    emit bottomChanged();
    d->updateVerticalAnchors();
}

QxAnchorLine QxAnchors::verticalCenter() const
{
    Q_D(const QxAnchors);
    return d->vCenter;
}

void QxAnchors::setVerticalCenter(const QxAnchorLine &edge)
{
    Q_D(QxAnchors);
    if (!d->checkVAnchorValid(edge) || d->vCenter == edge)
        return;

    d->usedAnchors |= VCenterAnchor;

    if (!d->checkVValid()) {
        d->usedAnchors &= ~VCenterAnchor;
        return;
    }

    d->remDepend(d->vCenter.item);
    d->vCenter = edge;
    d->addDepend(d->vCenter.item);
    emit verticalCenterChanged();
    d->updateVerticalAnchors();
}

void QxAnchors::resetVerticalCenter()
{
    Q_D(QxAnchors);
    d->usedAnchors &= ~VCenterAnchor;
    d->remDepend(d->vCenter.item);
    d->vCenter = QxAnchorLine();
    emit verticalCenterChanged();
    d->updateVerticalAnchors();
}

QxAnchorLine QxAnchors::baseline() const
{
    Q_D(const QxAnchors);
    return d->baseline;
}

void QxAnchors::setBaseline(const QxAnchorLine &edge)
{
    Q_D(QxAnchors);
    if (!d->checkVAnchorValid(edge) || d->baseline == edge)
        return;

    d->usedAnchors |= BaselineAnchor;

    if (!d->checkVValid()) {
        d->usedAnchors &= ~BaselineAnchor;
        return;
    }

    d->remDepend(d->baseline.item);
    d->baseline = edge;
    d->addDepend(d->baseline.item);
    emit baselineChanged();
    d->updateVerticalAnchors();
}

void QxAnchors::resetBaseline()
{
    Q_D(QxAnchors);
    d->usedAnchors &= ~BaselineAnchor;
    d->remDepend(d->baseline.item);
    d->baseline = QxAnchorLine();
    emit baselineChanged();
    d->updateVerticalAnchors();
}

QxAnchorLine QxAnchors::left() const
{
    Q_D(const QxAnchors);
    return d->left;
}

void QxAnchors::setLeft(const QxAnchorLine &edge)
{
    Q_D(QxAnchors);
    if (!d->checkHAnchorValid(edge) || d->left == edge)
        return;

    d->usedAnchors |= LeftAnchor;

    if (!d->checkHValid()) {
        d->usedAnchors &= ~LeftAnchor;
        return;
    }

    d->remDepend(d->left.item);
    d->left = edge;
    d->addDepend(d->left.item);
    emit leftChanged();
    d->updateHorizontalAnchors();
}

void QxAnchors::resetLeft()
{
    Q_D(QxAnchors);
    d->usedAnchors &= ~LeftAnchor;
    d->remDepend(d->left.item);
    d->left = QxAnchorLine();
    emit leftChanged();
    d->updateHorizontalAnchors();
}

QxAnchorLine QxAnchors::right() const
{
    Q_D(const QxAnchors);
    return d->right;
}

void QxAnchors::setRight(const QxAnchorLine &edge)
{
    Q_D(QxAnchors);
    if (!d->checkHAnchorValid(edge) || d->right == edge)
        return;

    d->usedAnchors |= RightAnchor;

    if (!d->checkHValid()) {
        d->usedAnchors &= ~RightAnchor;
        return;
    }

    d->remDepend(d->right.item);
    d->right = edge;
    d->addDepend(d->right.item);
    emit rightChanged();
    d->updateHorizontalAnchors();
}

void QxAnchors::resetRight()
{
    Q_D(QxAnchors);
    d->usedAnchors &= ~RightAnchor;
    d->remDepend(d->right.item);
    d->right = QxAnchorLine();
    emit rightChanged();
    d->updateHorizontalAnchors();
}

QxAnchorLine QxAnchors::horizontalCenter() const
{
    Q_D(const QxAnchors);
    return d->hCenter;
}

void QxAnchors::setHorizontalCenter(const QxAnchorLine &edge)
{
    Q_D(QxAnchors);
    if (!d->checkHAnchorValid(edge) || d->hCenter == edge)
        return;

    d->usedAnchors |= HCenterAnchor;

    if (!d->checkHValid()) {
        d->usedAnchors &= ~HCenterAnchor;
        return;
    }

    d->remDepend(d->hCenter.item);
    d->hCenter = edge;
    d->addDepend(d->hCenter.item);
    emit horizontalCenterChanged();
    d->updateHorizontalAnchors();
}

void QxAnchors::resetHorizontalCenter()
{
    Q_D(QxAnchors);
    d->usedAnchors &= ~HCenterAnchor;
    d->remDepend(d->hCenter.item);
    d->hCenter = QxAnchorLine();
    emit horizontalCenterChanged();
    d->updateHorizontalAnchors();
}

qreal QxAnchors::leftMargin() const
{
    Q_D(const QxAnchors);
    return d->leftMargin;
}

void QxAnchors::setLeftMargin(qreal offset)
{
    Q_D(QxAnchors);
    if (d->leftMargin == offset)
        return;
    d->leftMargin = offset;
    if(d->fill)
        d->fillChanged();
    else
        d->updateHorizontalAnchors();
    emit leftMarginChanged();
}

qreal QxAnchors::rightMargin() const
{
    Q_D(const QxAnchors);
    return d->rightMargin;
}

void QxAnchors::setRightMargin(qreal offset)
{
    Q_D(QxAnchors);
    if (d->rightMargin == offset)
        return;
    d->rightMargin = offset;
    if(d->fill)
        d->fillChanged();
    else
        d->updateHorizontalAnchors();
    emit rightMarginChanged();
}

qreal QxAnchors::margins() const
{
    Q_D(const QxAnchors);
    return d->margins;
}

void QxAnchors::setMargins(qreal offset)
{
    Q_D(QxAnchors);
    if (d->margins == offset)
        return;
    //###Is it significantly faster to set them directly so we can call fillChanged only once?
    if(!d->rightMargin || d->rightMargin == d->margins)
        setRightMargin(offset);
    if(!d->leftMargin || d->leftMargin == d->margins)
        setLeftMargin(offset);
    if(!d->topMargin || d->topMargin == d->margins)
        setTopMargin(offset);
    if(!d->bottomMargin || d->bottomMargin == d->margins)
        setBottomMargin(offset);
    d->margins = offset;
    emit marginsChanged();

}

qreal QxAnchors::horizontalCenterOffset() const
{
    Q_D(const QxAnchors);
    return d->hCenterOffset;
}

void QxAnchors::setHorizontalCenterOffset(qreal offset)
{
    Q_D(QxAnchors);
    if (d->hCenterOffset == offset)
        return;
    d->hCenterOffset = offset;
    if(d->centerIn)
        d->centerInChanged();
    else
        d->updateHorizontalAnchors();
    emit horizontalCenterOffsetChanged();
}

qreal QxAnchors::topMargin() const
{
    Q_D(const QxAnchors);
    return d->topMargin;
}

void QxAnchors::setTopMargin(qreal offset)
{
    Q_D(QxAnchors);
    if (d->topMargin == offset)
        return;
    d->topMargin = offset;
    if(d->fill)
        d->fillChanged();
    else
        d->updateVerticalAnchors();
    emit topMarginChanged();
}

qreal QxAnchors::bottomMargin() const
{
    Q_D(const QxAnchors);
    return d->bottomMargin;
}

void QxAnchors::setBottomMargin(qreal offset)
{
    Q_D(QxAnchors);
    if (d->bottomMargin == offset)
        return;
    d->bottomMargin = offset;
    if(d->fill)
        d->fillChanged();
    else
        d->updateVerticalAnchors();
    emit bottomMarginChanged();
}

qreal QxAnchors::verticalCenterOffset() const
{
    Q_D(const QxAnchors);
    return d->vCenterOffset;
}

void QxAnchors::setVerticalCenterOffset(qreal offset)
{
    Q_D(QxAnchors);
    if (d->vCenterOffset == offset)
        return;
    d->vCenterOffset = offset;
    if(d->centerIn)
        d->centerInChanged();
    else
        d->updateVerticalAnchors();
    emit verticalCenterOffsetChanged();
}

qreal QxAnchors::baselineOffset() const
{
    Q_D(const QxAnchors);
    return d->baselineOffset;
}

void QxAnchors::setBaselineOffset(qreal offset)
{
    Q_D(QxAnchors);
    if (d->baselineOffset == offset)
        return;
    d->baselineOffset = offset;
    d->updateVerticalAnchors();
    emit baselineOffsetChanged();
}

QxAnchors::Anchors QxAnchors::usedAnchors() const
{
    Q_D(const QxAnchors);
    return d->usedAnchors;
}

bool QxAnchorsPrivate::checkHValid() const
{
    if (usedAnchors & QxAnchors::LeftAnchor &&
        usedAnchors & QxAnchors::RightAnchor &&
        usedAnchors & QxAnchors::HCenterAnchor) {
        qmlInfo(item) << QxAnchors::tr("Cannot specify left, right, and hcenter anchors.");
        return false;
    }

    return true;
}

bool QxAnchorsPrivate::checkHAnchorValid(QxAnchorLine anchor) const
{
    if (!anchor.item) {
        qmlInfo(item) << QxAnchors::tr("Cannot anchor to a null item.");
        return false;
    } else if (anchor.anchorLine & QxAnchorLine::Vertical_Mask) {
        qmlInfo(item) << QxAnchors::tr("Cannot anchor a horizontal edge to a vertical edge.");
        return false;
    } else if (anchor.item != item->parentItem() && anchor.item->parentItem() != item->parentItem()){
        qmlInfo(item) << QxAnchors::tr("Cannot anchor to an item that isn't a parent or sibling.");
        return false;
    } else if (anchor.item == item) {
        qmlInfo(item) << QxAnchors::tr("Cannot anchor item to self.");
        return false;
    }

    return true;
}

bool QxAnchorsPrivate::checkVValid() const
{
    if (usedAnchors & QxAnchors::TopAnchor &&
        usedAnchors & QxAnchors::BottomAnchor &&
        usedAnchors & QxAnchors::VCenterAnchor) {
        qmlInfo(item) << QxAnchors::tr("Cannot specify top, bottom, and vcenter anchors.");
        return false;
    } else if (usedAnchors & QxAnchors::BaselineAnchor &&
               (usedAnchors & QxAnchors::TopAnchor ||
                usedAnchors & QxAnchors::BottomAnchor ||
                usedAnchors & QxAnchors::VCenterAnchor)) {
        qmlInfo(item) << QxAnchors::tr("Baseline anchor cannot be used in conjunction with top, bottom, or vcenter anchors.");
        return false;
    }

    return true;
}

bool QxAnchorsPrivate::checkVAnchorValid(QxAnchorLine anchor) const
{
    if (!anchor.item) {
        qmlInfo(item) << QxAnchors::tr("Cannot anchor to a null item.");
        return false;
    } else if (anchor.anchorLine & QxAnchorLine::Horizontal_Mask) {
        qmlInfo(item) << QxAnchors::tr("Cannot anchor a vertical edge to a horizontal edge.");
        return false;
    } else if (anchor.item != item->parentItem() && anchor.item->parentItem() != item->parentItem()){
        qmlInfo(item) << QxAnchors::tr("Cannot anchor to an item that isn't a parent or sibling.");
        return false;
    } else if (anchor.item == item){
        qmlInfo(item) << QxAnchors::tr("Cannot anchor item to self.");
        return false;
    }

    return true;
}

#include <moc_qxanchors_p.cpp>

