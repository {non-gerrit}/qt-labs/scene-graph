/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxtextinput_p.h"
#include "qxtextinput_p_p.h"

#include <private/qdeclarativeglobal_p.h>
#include <qdeclarativeinfo.h>

#include <QValidator>
#include <QTextCursor>
#include <QApplication>
#include <QFontMetrics>
#include <QPainter>

#ifndef QT_NO_LINEEDIT

QT_BEGIN_NAMESPACE

/*!
    \qmlclass TextInput QxTextInput
    \since 4.7
    \brief The TextInput item allows you to add an editable line of text to a scene.
    \inherits Item

    TextInput can only display a single line of text, and can only display
    plain text. However it can provide addition input constraints on the text.

    Input constraints include setting a QValidator, an input mask, or a
    maximum input length.

    On Mac OS X, the Up/Down key bindings for Home/End are explicitly disabled.
    If you want such bindings (on any platform), you will need to construct them in QML.
*/
QxTextInput::QxTextInput(QxItem* parent)
    : QxPaintItem(*(new QxTextInputPrivate), parent)
{
    Q_D(QxTextInput);
    d->init();
}

QxTextInput::~QxTextInput()
{
}

/*!
    \qmlproperty string TextInput::text

    The text in the TextInput.
*/

QString QxTextInput::text() const
{
    Q_D(const QxTextInput);
    return d->control->text();
}

void QxTextInput::setText(const QString &s)
{
    Q_D(QxTextInput);
    if(s == text())
        return;
    d->control->setText(s);
}

/*!
    \qmlproperty string TextInput::font.family

    Sets the family name of the font.

    The family name is case insensitive and may optionally include a foundry name, e.g. "Helvetica [Cronyx]".
    If the family is available from more than one foundry and the foundry isn't specified, an arbitrary foundry is chosen.
    If the family isn't available a family will be set using the font matching algorithm.
*/

/*!
    \qmlproperty bool TextInput::font.bold

    Sets whether the font weight is bold.
*/

/*!
    \qmlproperty enumeration TextInput::font.weight

    Sets the font's weight.

    The weight can be one of:
    \list
    \o Font.Light
    \o Font.Normal - the default
    \o Font.DemiBold
    \o Font.Bold
    \o Font.Black
    \endlist

    \qml
    TextInput { text: "Hello"; font.weight: Font.DemiBold }
    \endqml
*/

/*!
    \qmlproperty bool TextInput::font.italic

    Sets whether the font has an italic style.
*/

/*!
    \qmlproperty bool TextInput::font.underline

    Sets whether the text is underlined.
*/

/*!
    \qmlproperty bool TextInput::font.outline

    Sets whether the font has an outline style.
*/

/*!
    \qmlproperty bool TextInput::font.strikeout

    Sets whether the font has a strikeout style.
*/

/*!
    \qmlproperty real TextInput::font.pointSize

    Sets the font size in points. The point size must be greater than zero.
*/

/*!
    \qmlproperty int TextInput::font.pixelSize

    Sets the font size in pixels.

    Using this function makes the font device dependent.
    Use \c pointSize to set the size of the font in a device independent manner.
*/

/*!
    \qmlproperty real TextInput::font.letterSpacing

    Sets the letter spacing for the font.

    Letter spacing changes the default spacing between individual letters in the font.
    A value of 100 will keep the spacing unchanged; a value of 200 will enlarge the spacing after a character by
    the width of the character itself.
*/

/*!
    \qmlproperty real TextInput::font.wordSpacing

    Sets the word spacing for the font.

    Word spacing changes the default spacing between individual words.
    A positive value increases the word spacing by a corresponding amount of pixels,
    while a negative value decreases the inter-word spacing accordingly.
*/

/*!
    \qmlproperty enumeration TextInput::font.capitalization

    Sets the capitalization for the text.

    \list
    \o Font.MixedCase - This is the normal text rendering option where no capitalization change is applied.
    \o Font.AllUppercase - This alters the text to be rendered in all uppercase type.
    \o Font.AllLowercase	 - This alters the text to be rendered in all lowercase type.
    \o Font.SmallCaps -	This alters the text to be rendered in small-caps type.
    \o Font.Capitalize - This alters the text to be rendered with the first character of each word as an uppercase character.
    \endlist

    \qml
    TextInput { text: "Hello"; font.capitalization: Font.AllLowercase }
    \endqml
*/

QFont QxTextInput::font() const
{
    Q_D(const QxTextInput);
    return d->font;
}

void QxTextInput::setFont(const QFont &font)
{
    Q_D(QxTextInput);
    if (d->font == font)
        return;

    d->font = font;

    d->control->setFont(d->font);
    if(d->cursorItem){
        d->cursorItem->setHeight(QFontMetrics(d->font).height());
        moveCursor();
    }
    updateSize();
    emit fontChanged(d->font);
}

/*!
    \qmlproperty color TextInput::color

    The text color.
*/
QColor QxTextInput::color() const
{
    Q_D(const QxTextInput);
    return d->color;
}

void QxTextInput::setColor(const QColor &c)
{
    Q_D(QxTextInput);
    if (c != d->color) {
        d->color = c;
        update();
        emit colorChanged(c);
    }
}


/*!
    \qmlproperty color TextInput::selectionColor

    The text highlight color, used behind selections.
*/
QColor QxTextInput::selectionColor() const
{
    Q_D(const QxTextInput);
    return d->selectionColor;
}

void QxTextInput::setSelectionColor(const QColor &color)
{
    Q_D(QxTextInput);
    if (d->selectionColor == color)
        return;

    d->selectionColor = color;
    QPalette p = d->control->palette();
    p.setColor(QPalette::Highlight, d->selectionColor);
    d->control->setPalette(p);
    if (d->control->hasSelectedText())
        update();
    emit selectionColorChanged(color);
}

/*!
    \qmlproperty color TextInput::selectedTextColor

    The highlighted text color, used in selections.
*/
QColor QxTextInput::selectedTextColor() const
{
    Q_D(const QxTextInput);
    return d->selectedTextColor;
}

void QxTextInput::setSelectedTextColor(const QColor &color)
{
    Q_D(QxTextInput);
    if (d->selectedTextColor == color)
        return;

    d->selectedTextColor = color;
    QPalette p = d->control->palette();
    p.setColor(QPalette::HighlightedText, d->selectedTextColor);
    d->control->setPalette(p);
    if (d->control->hasSelectedText())
        update();
    emit selectedTextColorChanged(color);
}

/*!
    \qmlproperty enumeration TextInput::horizontalAlignment

    Sets the horizontal alignment of the text within the TextInput item's
    width and height.  By default, the text is left aligned.

    TextInput does not have vertical alignment, as the natural height is
    exactly the height of the single line of text. If you set the height
    manually to something larger, TextInput will always be top aligned
    vertically. You can use anchors to align it however you want within
    another item.

    The valid values for \c horizontalAlignment are \c TextInput.AlignLeft, \c TextInput.AlignRight and
    \c TextInput.AlignHCenter.
*/
QxTextInput::HAlignment QxTextInput::hAlign() const
{
    Q_D(const QxTextInput);
    return d->hAlign;
}

void QxTextInput::setHAlign(HAlignment align)
{
    Q_D(QxTextInput);
    if(align == d->hAlign)
        return;
    d->hAlign = align;
    updateRect();
    d->updateHorizontalScroll();
    emit horizontalAlignmentChanged(d->hAlign);
}

bool QxTextInput::isReadOnly() const
{
    Q_D(const QxTextInput);
    return d->control->isReadOnly();
}

void QxTextInput::setReadOnly(bool ro)
{
    Q_D(QxTextInput);
    if (d->control->isReadOnly() == ro)
        return;

    d->control->setReadOnly(ro);

    emit readOnlyChanged(ro);
}

int QxTextInput::maxLength() const
{
    Q_D(const QxTextInput);
    return d->control->maxLength();
}

void QxTextInput::setMaxLength(int ml)
{
    Q_D(QxTextInput);
    if (d->control->maxLength() == ml)
        return;

    d->control->setMaxLength(ml);

    emit maximumLengthChanged(ml);
}

/*!
    \qmlproperty bool TextInput::cursorVisible
    Set to true when the TextInput shows a cursor.

    This property is set and unset when the TextInput gets focus, so that other
    properties can be bound to whether the cursor is currently showing. As it
    gets set and unset automatically, when you set the value yourself you must
    keep in mind that your value may be overwritten.

    It can be set directly in script, for example if a KeyProxy might
    forward keys to it and you desire it to look active when this happens
    (but without actually giving it the focus).

    It should not be set directly on the element, like in the below QML,
    as the specified value will be overridden an lost on focus changes.

    \code
    TextInput {
        text: "Text"
        cursorVisible: false
    }
    \endcode

    In the above snippet the cursor will still become visible when the
    TextInput gains focus.
*/
bool QxTextInput::isCursorVisible() const
{
    Q_D(const QxTextInput);
    return d->cursorVisible;
}

void QxTextInput::setCursorVisible(bool on)
{
    Q_D(QxTextInput);
    if (d->cursorVisible == on)
        return;
    d->cursorVisible = on;
    d->control->setCursorBlinkPeriod(on?QApplication::cursorFlashTime():0);
    QRect r = d->control->cursorRect();
    if (d->control->inputMask().isEmpty())
        updateRect(r);
    else
        updateRect();
    emit cursorVisibleChanged(d->cursorVisible);
}

/*!
    \qmlproperty int TextInput::cursorPosition
    The position of the cursor in the TextInput.
*/
int QxTextInput::cursorPosition() const
{
    Q_D(const QxTextInput);
    return d->control->cursor();
}
void QxTextInput::setCursorPosition(int cp)
{
    Q_D(QxTextInput);
    d->control->moveCursor(cp);
}

/*!
  \internal

  Returns a Rect which encompasses the cursor, but which may be larger than is
  required. Ignores custom cursor delegates.
*/
QRect QxTextInput::cursorRectangle() const
{
    Q_D(const QxTextInput);
    QRect r = d->control->cursorRect();
    r.setHeight(r.height()-1); // Make consistent with TextEdit (QLineControl inexplicably adds 1)
    return r;
}

/*!
    \qmlproperty int TextInput::selectionStart

    The cursor position before the first character in the current selection.
    Setting this and selectionEnd allows you to specify a selection in the
    text edit.

    Note that if selectionStart == selectionEnd then there is no current
    selection. If you attempt to set selectionStart to a value outside of
    the current text, selectionStart will not be changed.

    \sa selectionEnd, cursorPosition, selectedText
*/
int QxTextInput::selectionStart() const
{
    Q_D(const QxTextInput);
    return d->lastSelectionStart;
}

/*!
    \qmlproperty int TextInput::selectionEnd

    The cursor position after the last character in the current selection.
    Setting this and selectionStart allows you to specify a selection in the
    text edit.

    Note that if selectionStart == selectionEnd then there is no current
    selection. If you attempt to set selectionEnd to a value outside of
    the current text, selectionEnd will not be changed.

    \sa selectionStart, cursorPosition, selectedText
*/
int QxTextInput::selectionEnd() const
{
    Q_D(const QxTextInput);
    return d->lastSelectionEnd;
}

void QxTextInput::select(int start, int end)
{
    Q_D(QxTextInput);
    if (start < 0 || end < 0 || start > d->control->text().length() || end > d->control->text().length())
        return;
    d->control->setSelection(start, end-start);
}

/*!
    \qmlproperty string TextInput::selectedText

    This read-only property provides the text currently selected in the
    text input.

    It is equivalent to the following snippet, but is faster and easier
    to use.

    \qml
    myTextInput.text.toString().substring(myTextInput.selectionStart,
        myTextInput.selectionEnd);
    \endqml
*/
QString QxTextInput::selectedText() const
{
    Q_D(const QxTextInput);
    return d->control->selectedText();
}

/*!
    \qmlproperty bool TextInput::focusOnPress

    Whether the TextInput should gain focus on a mouse press. By default this is
    set to true.
*/
bool QxTextInput::focusOnPress() const
{
    Q_D(const QxTextInput);
    return d->focusOnPress;
}

void QxTextInput::setFocusOnPress(bool b)
{
    Q_D(QxTextInput);
    if (d->focusOnPress == b)
        return;

    d->focusOnPress = b;

    emit focusOnPressChanged(d->focusOnPress);
}

/*!
    \qmlproperty bool TextInput::autoScroll

    Whether the TextInput should scroll when the text is longer than the width. By default this is
    set to true.
*/
bool QxTextInput::autoScroll() const
{
    Q_D(const QxTextInput);
    return d->autoScroll;
}

void QxTextInput::setAutoScroll(bool b)
{
    Q_D(QxTextInput);
    if (d->autoScroll == b)
        return;

    d->autoScroll = b;
    //We need to repaint so that the scrolling is taking into account.
    updateSize(true);
    d->updateHorizontalScroll();
    emit autoScrollChanged(d->autoScroll);
}

/*!
    \qmlclass IntValidator QIntValidator

    This element provides a validator for integer values
*/
/*!
    \qmlproperty int IntValidator::top

    This property holds the validator's highest acceptable value.
    By default, this property's value is derived from the highest signed integer available (typically 2147483647).
*/
/*!
    \qmlproperty int IntValidator::bottom

    This property holds the validator's lowest acceptable value.
    By default, this property's value is derived from the lowest signed integer available (typically -2147483647).
*/

/*!
    \qmlclass DoubleValidator QDoubleValidator

    This element provides a validator for non-integer numbers.
*/

/*!
    \qmlproperty real DoubleValidator::top

    This property holds the validator's maximum acceptable value.
    By default, this property contains a value of infinity.
*/
/*!
    \qmlproperty real DoubleValidator::bottom

    This property holds the validator's minimum acceptable value.
    By default, this property contains a value of -infinity.
*/
/*!
    \qmlproperty int DoubleValidator::decimals

    This property holds the validator's maximum number of digits after the decimal point.
    By default, this property contains a value of 1000.
*/
/*!
    \qmlproperty enumeration DoubleValidator::notation
    This property holds the notation of how a string can describe a number.

    The values for this property are DoubleValidator.StandardNotation or DoubleValidator.ScientificNotation.
    If this property is set to DoubleValidator.ScientificNotation, the written number may have an exponent part(i.e. 1.5E-2).

    By default, this property is set to DoubleValidator.ScientificNotation.
*/

/*!
    \qmlclass RegExpValidator QRegExpValidator

    This element provides a validator, which counts as valid any string which
    matches a specified regular expression.
*/
/*!
   \qmlproperty regExp RegExpValidator::regExp

   This property holds the regular expression used for validation.

   Note that this property should be a regular expression in JS syntax, e.g /a/ for the regular expression
   matching "a".

   By default, this property contains a regular expression with the pattern .* that matches any string.
*/

/*!
    \qmlproperty Validator TextInput::validator

    Allows you to set a validator on the TextInput. When a validator is set
    the TextInput will only accept input which leaves the text property in
    an acceptable or intermediate state. The accepted signal will only be sent
    if the text is in an acceptable state when enter is pressed.

    Currently supported validators are IntValidator, DoubleValidator and
    RegExpValidator. An example of using validators is shown below, which allows
    input of integers between 11 and 31 into the text input:

    \code
    import Qt 4.7
    TextInput{
        validator: IntValidator{bottom: 11; top: 31;}
        focus: true
    }
    \endcode

    \sa acceptableInput, inputMask
*/
#ifndef QT_NO_VALIDATOR
QValidator* QxTextInput::validator() const
{
    Q_D(const QxTextInput);
    //###const cast isn't good, but needed for property system?
    return const_cast<QValidator*>(d->control->validator());
}

void QxTextInput::setValidator(QValidator* v)
{
    Q_D(QxTextInput);
    if (d->control->validator() == v)
        return;

    d->control->setValidator(v);
    if(!d->control->hasAcceptableInput()){
        d->oldValidity = false;
        emit acceptableInputChanged();
    }

    emit validatorChanged();
}
#endif // QT_NO_VALIDATOR

/*!
    \qmlproperty string TextInput::inputMask

    Allows you to set an input mask on the TextInput, restricting the allowable
    text inputs. See QLineEdit::inputMask for further details, as the exact
    same mask strings are used by TextInput.

    \sa acceptableInput, validator
*/
QString QxTextInput::inputMask() const
{
    Q_D(const QxTextInput);
    return d->control->inputMask();
}

void QxTextInput::setInputMask(const QString &im)
{
    Q_D(QxTextInput);
    if (d->control->inputMask() == im)
        return;

    d->control->setInputMask(im);
    emit inputMaskChanged(d->control->inputMask());
}

/*!
    \qmlproperty bool TextInput::acceptableInput

    This property is always true unless a validator or input mask has been set.
    If a validator or input mask has been set, this property will only be true
    if the current text is acceptable to the validator or input mask as a final
    string (not as an intermediate string).
*/
bool QxTextInput::hasAcceptableInput() const
{
    Q_D(const QxTextInput);
    return d->control->hasAcceptableInput();
}

/*!
    \qmlproperty enumeration TextInput::echoMode

    Specifies how the text should be displayed in the TextInput.
    \list
    \o TextInput.Normal - Displays the text as it is. (Default)
    \o TextInput.Password - Displays asterixes instead of characters.
    \o TextInput.NoEcho - Displays nothing.
    \o TextInput.PasswordEchoOnEdit - Displays all but the current character as asterixes.
    \endlist
*/
QxTextInput::EchoMode QxTextInput::echoMode() const
{
    Q_D(const QxTextInput);
    return (QxTextInput::EchoMode)d->control->echoMode();
}

void QxTextInput::setEchoMode(QxTextInput::EchoMode echo)
{
    Q_D(QxTextInput);
    if (echoMode() == echo)
        return;
    /*
    Qt::InputMethodHints imHints = inputMethodHints();
    if (echo == Password || echo == NoEcho)
        imHints |= Qt::ImhHiddenText;
    else
        imHints &= ~Qt::ImhHiddenText;
    if (echo != Normal)
        imHints |= (Qt::ImhNoAutoUppercase | Qt::ImhNoPredictiveText);
    else
        imHints &= ~(Qt::ImhNoAutoUppercase | Qt::ImhNoPredictiveText);
    setInputMethodHints(imHints);
    */
    d->control->setEchoMode((uint)echo);
    update();
    emit echoModeChanged(echoMode());
}

/*!
    \qmlproperty Component TextInput::cursorDelegate
    The delegate for the cursor in the TextInput.

    If you set a cursorDelegate for a TextInput, this delegate will be used for
    drawing the cursor instead of the standard cursor. An instance of the
    delegate will be created and managed by the TextInput when a cursor is
    needed, and the x property of delegate instance will be set so as
    to be one pixel before the top left of the current character.

    Note that the root item of the delegate component must be a QxItem or
    QxItem derived item.
*/
QDeclarativeComponent* QxTextInput::cursorDelegate() const
{
    Q_D(const QxTextInput);
    return d->cursorComponent;
}

void QxTextInput::setCursorDelegate(QDeclarativeComponent* c)
{
    Q_D(QxTextInput);
    if (d->cursorComponent == c)
        return;

    d->cursorComponent = c;
    if(!c){
        //note that the components are owned by something else
        disconnect(d->control, SIGNAL(cursorPositionChanged(int, int)),
                this, SLOT(moveCursor()));
        delete d->cursorItem;
    }else{
        d->startCreatingCursor();
    }

    emit cursorDelegateChanged();
}

void QxTextInputPrivate::startCreatingCursor()
{
    Q_Q(QxTextInput);
    q->connect(control, SIGNAL(cursorPositionChanged(int, int)),
            q, SLOT(moveCursor()));
    if(cursorComponent->isReady()){
        q->createCursor();
    }else if(cursorComponent->isLoading()){
        q->connect(cursorComponent, SIGNAL(statusChanged(int)),
                q, SLOT(createCursor()));
    }else {//isError
        qmlInfo(q, cursorComponent->errors()) << QxTextInput::tr("Could not load cursor delegate");
    }
}

void QxTextInput::createCursor()
{
    Q_D(QxTextInput);
    if(d->cursorComponent->isError()){
        qmlInfo(this, d->cursorComponent->errors()) << tr("Could not load cursor delegate");
        return;
    }

    if(!d->cursorComponent->isReady())
        return;

    if(d->cursorItem)
        delete d->cursorItem;
    d->cursorItem = qobject_cast<QxItem*>(d->cursorComponent->create());
    if(!d->cursorItem){
        qmlInfo(this, d->cursorComponent->errors()) << tr("Could not instantiate cursor delegate");
        return;
    }

    QDeclarative_setParent_noEvent(d->cursorItem, this);
    d->cursorItem->setParentItem(this);
    d->cursorItem->setX(d->control->cursorToX());
    d->cursorItem->setHeight(d->control->height());
}

void QxTextInput::moveCursor()
{
    Q_D(QxTextInput);
    if(!d->cursorItem)
        return;
    d->cursorItem->setX(d->control->cursorToX() - d->hscroll);
}

/*!
    \qmlmethod rect TextInput::positionToRectangle(int x)
*/
QRectF QxTextInput::positionToRectangle(int x) const
{
    Q_D(const QxTextInput);
    return QRectF(d->control->cursorToX(x)-d->hscroll,
        0.0,
        d->control->cursorWidth(),
        cursorRectangle().height());
}

/*!
    \qmlmethod int TextInput::positionAt(int x)

    This function returns the character position at
    x pixels from the left of the textInput. Position 0 is before the
    first character, position 1 is after the first character but before the second,
    and so on until position text.length, which is after all characters.

    This means that for all x values before the first character this function returns 0,
    and for all x values after the last character this function returns text.length.
*/
int QxTextInput::positionAt(int x) const
{
    Q_D(const QxTextInput);
    return d->control->xToPos(x + d->hscroll);
}

void QxTextInputPrivate::focusChanged(bool hasFocus)
{
    Q_Q(QxTextInput);
    focused = hasFocus;
    q->setCursorVisible(hasFocus);
    if(q->echoMode() == QxTextInput::PasswordEchoOnEdit && !hasFocus)
        control->updatePasswordEchoEditing(false);//QLineControl sets it on key events, but doesn't deal with focus events
    if (!hasFocus)
        control->deselect();
    QxItemPrivate::focusChanged(hasFocus);
}

void QxTextInput::keyPressEvent(QKeyEvent* ev)
{
    Q_D(QxTextInput);
    keyPressPreHandler(ev);
    if (ev->isAccepted())
        return;
    if (((ev->key() == Qt::Key_Up || ev->key() == Qt::Key_Down) && ev->modifiers() == Qt::NoModifier) // Don't allow MacOSX up/down support, and we don't allow a completer.
        || (((d->control->cursor() == 0 && ev->key() == Qt::Key_Left)
            || (d->control->cursor() == d->control->text().length()
                && ev->key() == Qt::Key_Right))
            && (d->lastSelectionStart == d->lastSelectionEnd)))
    {
        //ignore when moving off the end
        //unless there is a selection, because then moving will do something (deselect)
        ev->ignore();
    }else{
        d->control->processKeyEvent(ev);
    }
    if (!ev->isAccepted())
        QxPaintItem::keyPressEvent(ev);
}

/*!
\overload
Handles the given mouse \a event.
*/
void QxTextInput::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_D(QxTextInput);
    if (d->selectByMouse) {
        int cursor = d->xToPos(event->pos().x());
        d->control->selectWordAtPos(cursor);
        event->setAccepted(true);
    } else {
        QxPaintItem::mouseDoubleClickEvent(event);
    }
}

void QxTextInput::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_D(QxTextInput);
    if(d->focusOnPress){
        bool hadFocus = hasFocus();
        forceFocus();
        if (d->showInputPanelOnFocus) {
            if (hasFocus() && hadFocus && !isReadOnly()) {
                // re-open input panel on press if already focused
                openSoftwareInputPanel();
            }
        } else { // show input panel on click
            if (hasFocus() && !hadFocus) {
                d->clickCausedFocus = true;
            }
        }
    }
    bool mark = event->modifiers() & Qt::ShiftModifier;
    int cursor = d->xToPos(event->pos().x());
    d->control->moveCursor(cursor, mark);
    event->setAccepted(true);
}

void QxTextInput::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    Q_D(QxTextInput);
    if (d->selectByMouse) {
        d->control->moveCursor(d->xToPos(event->pos().x()), true);
        event->setAccepted(true);
    } else {
        QxPaintItem::mouseMoveEvent(event);
    }
}

/*!
\overload
Handles the given mouse \a event.
*/
void QxTextInput::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    Q_D(QxTextInput);
    /* XXX
    if (!d->showInputPanelOnFocus) { // input panel on click
        if (d->focusOnPress && !isReadOnly() && boundingRect().contains(event->pos())) {
            if (QGraphicsView * view = qobject_cast<QGraphicsView*>(qApp->focusWidget())) {
                if (view->scene() && view->scene() == scene()) {
                    qt_widget_private(view)->handleSoftwareInputPanel(event->button(), d->clickCausedFocus);
                }
            }
        }
    }
    */
    d->clickCausedFocus = false;
    d->control->processEvent(event);
    if (!event->isAccepted())
        QxPaintItem::mouseReleaseEvent(event);
}

bool QxTextInput::event(QEvent* ev)
{
    Q_D(QxTextInput);
    //Anything we don't deal with ourselves, pass to the control
    bool handled = false;
    switch(ev->type()){
        case QEvent::KeyPress:
        case QEvent::KeyRelease://###Should the control be doing anything with release?
        case QEvent::GraphicsSceneMousePress:
        case QEvent::GraphicsSceneMouseMove:
        case QEvent::GraphicsSceneMouseRelease:
        case QEvent::GraphicsSceneMouseDoubleClick:
            break;
        default:
            handled = d->control->processEvent(ev);
    }
    if(!handled)
        handled = QxPaintItem::event(ev);
    return handled;
}

void QxTextInput::geometryChanged(const QRectF &newGeometry,
                                  const QRectF &oldGeometry)
{
    Q_D(QxTextInput);
    if (newGeometry.width() != oldGeometry.width()) {
        updateSize();
        d->updateHorizontalScroll();
    }
    QxPaintItem::geometryChanged(newGeometry, oldGeometry);
}

int QxTextInputPrivate::calculateTextWidth()
{
    return qRound(control->naturalTextWidth());
}

void QxTextInputPrivate::updateHorizontalScroll()
{
    Q_Q(QxTextInput);
    int cix = qRound(control->cursorToX());
    QRect br(q->boundingRect().toRect());
    int widthUsed = calculateTextWidth();
    if (autoScroll) {
        if (widthUsed <=  br.width()) {
            // text fits in br; use hscroll for alignment
            switch (hAlign & ~(Qt::AlignAbsolute|Qt::AlignVertical_Mask)) {
            case Qt::AlignRight:
                hscroll = widthUsed - br.width() - 1;
                break;
            case Qt::AlignHCenter:
                hscroll = (widthUsed - br.width()) / 2;
                break;
            default:
                // Left
                hscroll = 0;
                break;
            }
        } else if (cix - hscroll >= br.width()) {
            // text doesn't fit, cursor is to the right of br (scroll right)
            hscroll = cix - br.width() + 1;
        } else if (cix - hscroll < 0 && hscroll < widthUsed) {
            // text doesn't fit, cursor is to the left of br (scroll left)
            hscroll = cix;
        } else if (widthUsed - hscroll < br.width()) {
            // text doesn't fit, text document is to the left of br; align
            // right
            hscroll = widthUsed - br.width() + 1;
        }
    } else {
        if(hAlign == QxTextInput::AlignRight){
            hscroll = q->width() - widthUsed;
        }else if(hAlign == QxTextInput::AlignHCenter){
            hscroll = (q->width() - widthUsed) / 2;
        } else {
            hscroll = 0;
        }
    }
}

void QxTextInput::paint(QPainter *p)
{
    Q_D(QxTextInput);
    p->setRenderHint(QPainter::TextAntialiasing, true);
    p->save();
    p->setPen(QPen(d->color));
    int flags = QLineControl::DrawText;
    if(!isReadOnly() && d->cursorVisible && !d->cursorItem)
        flags |= QLineControl::DrawCursor;
    if (d->control->hasSelectedText())
            flags |= QLineControl::DrawSelections;
    QPoint offset = QPoint(0,0);
    QFontMetrics fm = QFontMetrics(d->font);
    QRect br(boundingRect().toRect());
    if (d->autoScroll) {
        // the y offset is there to keep the baseline constant in case we have script changes in the text.
        offset = br.topLeft() - QPoint(d->hscroll, d->control->ascent() - fm.ascent());
    } else {
        offset = QPoint(d->hscroll, 0);
    }
    d->control->draw(p, offset, br, flags);

    p->restore();
}

/*!
\overload
Returns the value of the given \a property.
*/
QVariant QxTextInput::inputMethodQuery(Qt::InputMethodQuery property) const
{
    Q_D(const QxTextInput);
    switch(property) {
    case Qt::ImMicroFocus:
        return d->control->cursorRect();
    case Qt::ImFont:
        return font();
    case Qt::ImCursorPosition:
        return QVariant(d->control->cursor());
    case Qt::ImSurroundingText:
        return QVariant(text());
    case Qt::ImCurrentSelection:
        return QVariant(selectedText());
    case Qt::ImMaximumTextLength:
        return QVariant(maxLength());
    case Qt::ImAnchorPosition:
        if (d->control->selectionStart() == d->control->selectionEnd())
            return QVariant(d->control->cursor());
        else if (d->control->selectionStart() == d->control->cursor())
            return QVariant(d->control->selectionEnd());
        else
            return QVariant(d->control->selectionStart());
    default:
        return QVariant();
    }
}

/*!
    \qmlmethod void TextInput::selectAll()

    Causes all text to be selected.
*/
void QxTextInput::selectAll()
{
    Q_D(QxTextInput);
    d->control->setSelection(0, d->control->text().length());
}

#ifndef QT_NO_CLIPBOARD
/*!
    \qmlmethod TextInput::cut()

    Moves the currently selected text to the system clipboard.
*/
void QxTextInput::cut()
{
    Q_D(QxTextInput);
    d->control->copy();
    d->control->del();
}

/*!
    \qmlmethod TextInput::copy()

    Copies the currently selected text to the system clipboard.
*/
void QxTextInput::copy()
{
    Q_D(QxTextInput);
    d->control->copy();
}

/*!
    \qmlmethod TextInput::paste()

    Replaces the currently selected text by the contents of the system clipboard.
*/
void QxTextInput::paste()
{
    Q_D(QxTextInput);
    d->control->paste();
}
#endif // QT_NO_CLIPBOARD

/*!
    \qmlmethod void TextInput::selectWord()

    Causes the word closest to the current cursor position to be selected.
*/
void QxTextInput::selectWord()
{
    Q_D(QxTextInput);
    d->control->selectWordAtPos(d->control->cursor());
}

/*!
    \qmlproperty bool TextInput::smooth

    This property holds whether the text is smoothly scaled or transformed.

    Smooth filtering gives better visual quality, but is slower.  If
    the item is displayed at its natural size, this property has no visual or
    performance effect.

    \note Generally scaling artifacts are only visible if the item is stationary on
    the screen.  A common pattern when animating an item is to disable smooth
    filtering at the beginning of the animation and reenable it at the conclusion.
*/

/*!
   \qmlproperty string TextInput::passwordCharacter

   This is the character displayed when echoMode is set to Password or
   PasswordEchoOnEdit. By default it is an asterisk.

   If this property is set to a string with more than one character,
   the first character is used. If the string is empty, the value
   is ignored and the property is not set.
*/
QString QxTextInput::passwordCharacter() const
{
    Q_D(const QxTextInput);
    return QString(d->control->passwordCharacter());
}

void QxTextInput::setPasswordCharacter(const QString &str)
{
    Q_D(QxTextInput);
    if(str.length() < 1)
        return;
    d->control->setPasswordCharacter(str.constData()[0]);
    EchoMode echoMode_ = echoMode();
    if (echoMode_ == Password || echoMode_ == PasswordEchoOnEdit) {
        updateSize();
    }
    emit passwordCharacterChanged();
}

/*!
   \qmlproperty string TextInput::displayText

   This is the text displayed in the TextInput.

   If \l echoMode is set to TextInput::Normal, this holds the
   same value as the TextInput::text property. Otherwise,
   this property holds the text visible to the user, while
   the \l text property holds the actual entered text.
*/
QString QxTextInput::displayText() const
{
    Q_D(const QxTextInput);
    return d->control->displayText();
}

/*!
    \qmlproperty bool TextInput::selectByMouse

    Defaults to false.

    If true, the user can use the mouse to select text in some
    platform-specific way. Note that for some platforms this may
    not be an appropriate interaction (eg. may conflict with how
    the text needs to behave inside a Flickable.
*/
bool QxTextInput::selectByMouse() const
{
    Q_D(const QxTextInput);
    return d->selectByMouse;
}

void QxTextInput::setSelectByMouse(bool on)
{
    Q_D(QxTextInput);
    if (d->selectByMouse != on) {
        d->selectByMouse = on;
        emit selectByMouseChanged(on);
    }
}


/*!
    \qmlmethod void TextInput::moveCursorSelection(int position)

    Moves the cursor to \a position and updates the selection accordingly.
    (To only move the cursor, set the \l cursorPosition property.)

    When this method is called it additionally sets either the
    selectionStart or the selectionEnd (whichever was at the previous cursor position)
    to the specified position. This allows you to easily extend and contract the selected
    text range.

    For example, take this sequence of calls:

    \code
        cursorPosition = 5
        moveCursorSelection(9)
        moveCursorSelection(7)
    \endcode

    This moves the cursor to position 5, extend the selection end from 5 to 9
    and then retract the selection end from 9 to 7, leaving the text from position 5 to 7
    selected (the 6th and 7th characters).
*/
void QxTextInput::moveCursorSelection(int position)
{
    Q_D(QxTextInput);
    d->control->moveCursor(position, true);
    d->updateHorizontalScroll();
}

/*!
    \qmlmethod void TextInput::openSoftwareInputPanel()

    Opens software input panels like virtual keyboards for typing, useful for
    customizing when you want the input keyboard to be shown and hidden in
    your application.

    By default the opening of input panels follows the platform style. On Symbian^1 and
    Symbian^3 -based devices the panels are opened by clicking TextInput. On other platforms
    the panels are automatically opened when TextInput element gains focus. Input panels are
    always closed if no editor owns focus.

  . You can disable the automatic behavior by setting the property \c focusOnPress to false
    and use functions openSoftwareInputPanel() and closeSoftwareInputPanel() to implement
    the behavior you want.

    Only relevant on platforms, which provide virtual keyboards.

    \qml
        import Qt 4.7
        TextInput {
            id: textInput
            text: "Hello world!"
            focusOnPress: false
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (!textInput.focus) {
                        textInput.focus = true;
                        textInput.openSoftwareInputPanel();
                    } else {
                        textInput.focus = false;
                    }
                }
                onPressAndHold: textInput.closeSoftwareInputPanel();
            }
        }
    \endqml
*/
void QxTextInput::openSoftwareInputPanel()
{
    QEvent event(QEvent::RequestSoftwareInputPanel);
    if (qApp) {
        if (QxGraphicsView *view = qobject_cast<QxGraphicsView *>(qApp->focusWidget()))
            QApplication::sendEvent(view, &event);
    }
}

/*!
    \qmlmethod void TextInput::closeSoftwareInputPanel()

    Closes a software input panel like a virtual keyboard shown on the screen, useful
    for customizing when you want the input keyboard to be shown and hidden in
    your application.

    By default the opening of input panels follows the platform style. On Symbian^1 and
    Symbian^3 -based devices the panels are opened by clicking TextInput. On other platforms
    the panels are automatically opened when TextInput element gains focus. Input panels are
    always closed if no editor owns focus.

  . You can disable the automatic behavior by setting the property \c focusOnPress to false
    and use functions openSoftwareInputPanel() and closeSoftwareInputPanel() to implement
    the behavior you want.

    Only relevant on platforms, which provide virtual keyboards.

    \qml
        import Qt 4.7
        TextInput {
            id: textInput
            text: "Hello world!"
            focusOnPress: false
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (!textInput.focus) {
                        textInput.focus = true;
                        textInput.openSoftwareInputPanel();
                    } else {
                        textInput.focus = false;
                    }
                }
                onPressAndHold: textInput.closeSoftwareInputPanel();
            }
        }
    \endqml
*/
void QxTextInput::closeSoftwareInputPanel()
{
    QEvent event(QEvent::CloseSoftwareInputPanel);
    if (qApp) {
        if (QxGraphicsView *view = qobject_cast<QxGraphicsView *>(qApp->focusWidget()))
            QApplication::sendEvent(view, &event);
    }
}

void QxTextInput::focusInEvent(QFocusEvent *event)
{
    Q_UNUSED(event)
    /* XXX
    Q_D(const QxTextInput);
    if (d->showInputPanelOnFocus) {
        if (d->focusOnPress && !isReadOnly()) {
            openSoftwareInputPanel();
        }
    }
    QxPaintedItem::focusInEvent(event);
    */
}

void QxTextInputPrivate::init()
{
    Q_Q(QxTextInput);
    control->setCursorWidth(1);
    control->setPasswordCharacter(QLatin1Char('*'));
    q->setSmooth(smooth);
    q->setAcceptedMouseButtons(Qt::LeftButton);
    //q->setFlag(QGraphicsItem::ItemHasNoContents, false);
    //q->setFlag(QGraphicsItem::ItemAcceptsInputMethod);
    q->connect(control, SIGNAL(cursorPositionChanged(int,int)),
               q, SLOT(cursorPosChanged()));
    q->connect(control, SIGNAL(selectionChanged()),
               q, SLOT(selectionChanged()));
    q->connect(control, SIGNAL(textChanged(const QString &)),
               q, SLOT(q_textChanged()));
    q->connect(control, SIGNAL(accepted()),
               q, SIGNAL(accepted()));
    q->connect(control, SIGNAL(updateNeeded(QRect)),
               q, SLOT(updateRect(QRect)));
    q->updateSize();
    oldValidity = control->hasAcceptableInput();
    lastSelectionStart = 0;
    lastSelectionEnd = 0;
    QPalette p = control->palette();
    selectedTextColor = p.color(QPalette::HighlightedText);
    selectionColor = p.color(QPalette::Highlight);
}

void QxTextInput::cursorPosChanged()
{
    Q_D(QxTextInput);
    updateRect();//TODO: Only update rect between pos's
    //updateMicroFocus(); //XXX
    emit cursorPositionChanged();

    if(!d->control->hasSelectedText()){
        if(d->lastSelectionStart != d->control->cursor()){
            d->lastSelectionStart = d->control->cursor();
            emit selectionStartChanged();
        }
        if(d->lastSelectionEnd != d->control->cursor()){
            d->lastSelectionEnd = d->control->cursor();
            emit selectionEndChanged();
        }
    }
}

void QxTextInput::selectionChanged()
{
    Q_D(QxTextInput);
    updateRect();//TODO: Only update rect in selection
    emit selectedTextChanged();

    if(d->lastSelectionStart != d->control->selectionStart()){
        d->lastSelectionStart = d->control->selectionStart();
        if(d->lastSelectionStart == -1)
            d->lastSelectionStart = d->control->cursor();
        emit selectionStartChanged();
    }
    if(d->lastSelectionEnd != d->control->selectionEnd()){
        d->lastSelectionEnd = d->control->selectionEnd();
        if(d->lastSelectionEnd == -1)
            d->lastSelectionEnd = d->control->cursor();
        emit selectionEndChanged();
    }
}

void QxTextInput::q_textChanged()
{
    Q_D(QxTextInput);
    updateSize();
    d->updateHorizontalScroll();
    //updateMicroFocus();//XXX
    emit textChanged();
    emit displayTextChanged();
    if(hasAcceptableInput() != d->oldValidity){
        d->oldValidity = hasAcceptableInput();
        emit acceptableInputChanged();
    }
}

void QxTextInput::updateRect(const QRect &r)
{
    Q_UNUSED(r)
    update();
}

void QxTextInput::updateSize(bool needsRedraw)
{
    Q_D(QxTextInput);
    int w = width();
    int h = height();
    setImplicitHeight(d->control->height()-1); // -1 to counter QLineControl's +1 which is not consistent with Text.
    //XXX declarative version does NOT include cursor width here
    int cursorWidth = d->cursorItem ? d->cursorItem->width() : d->control->cursorWidth();
    setImplicitWidth(d->control->naturalTextWidth() + cursorWidth);
    if(w==width() && h==height() && needsRedraw)
        update();
}

QT_END_NAMESPACE

#endif // QT_NO_LINEEDIT
