/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXRECT_P_H
#define QXRECT_P_H

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail.  This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

#include "qxitem_p.h"
#include "adaptationlayer.h"

class QxGradient;
class QxRectangle;
class QxRectanglePrivate : public QxItemPrivate
{
    Q_DECLARE_PUBLIC(QxRectangle)

public:
    QxRectanglePrivate()
    : color(Qt::white)
    , gradient(0)
    , pen(0)
    , radius(0)
    , node(0)
    {
    }

    ~QxRectanglePrivate()
    {
        delete pen;
    }

    QColor color;
    QxGradient *gradient;
    QxPen *pen;
    qreal radius;
    static int doUpdateGradientSlotIdx;
    static int doUpdateBorderWidthSlotIdx;
    static int doUpdateBorderColorSlotIdx;

    QxPen *getPen() {
        if (!pen) {
            Q_Q(QxRectangle);
            pen = new QxPen;
            static int penWidthChangedSignalIdx = -1;
            static int penColorChangedSignalIdx = -1;
            if (penWidthChangedSignalIdx < 0) {
                penWidthChangedSignalIdx = QxPen::staticMetaObject.indexOfSignal("penWidthChanged()");
                penColorChangedSignalIdx = QxPen::staticMetaObject.indexOfSignal("penColorChanged()");
            }
            if (doUpdateBorderWidthSlotIdx < 0) {
                doUpdateBorderWidthSlotIdx = QxRectangle::staticMetaObject.indexOfSlot("doUpdateBorderWidth()");
                doUpdateBorderColorSlotIdx = QxRectangle::staticMetaObject.indexOfSlot("doUpdateBorderColor()");
            }
            QMetaObject::connect(pen, penWidthChangedSignalIdx, q, doUpdateBorderWidthSlotIdx);
            QMetaObject::connect(pen, penColorChangedSignalIdx, q, doUpdateBorderColorSlotIdx);
        }
        return pen;
    }

    void updateGradient();
    void updateBorderWidth();
    void updateBorderColor();

    virtual void clipChanged(bool);

    RectangleNodeInterface *node;
};

#endif // QXRECT_P_H
