/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxanchoranimation_p.h"
#include "qxanchoranimation_p_p.h"

#include "qxstateoperations_p.h"

#include <private/qdeclarativebehavior_p.h>
#include <private/qdeclarativestateoperations_p.h>
#include <private/qdeclarativecontext_p.h>

#include <QtDeclarative/qdeclarativepropertyvaluesource.h>
#include <QtDeclarative/qdeclarative.h>
#include <QtDeclarative/qdeclarativeinfo.h>
#include <QtDeclarative/qdeclarativeexpression.h>
#include <private/qdeclarativestringconverters_p.h>
#include <private/qdeclarativeglobal_p.h>
#include <private/qdeclarativemetatype_p.h>
#include <private/qdeclarativevaluetype_p.h>
#include <private/qdeclarativeproperty_p.h>
#include <private/qdeclarativeengine_p.h>

#include <QtGui/qcolor.h>
#include <QtCore/qvariant.h>
#include <QtCore/qfile.h>
#include <QtCore/qparallelanimationgroup.h>
#include <QtCore/qsequentialanimationgroup.h>
#include <QtCore/qset.h>
#include <QtCore/qrect.h>
#include <QtCore/qpoint.h>
#include <QtCore/qsize.h>
#include <QtCore/qmath.h>

#include <private/qvariantanimation_p.h>

QxAnchorAnimation::QxAnchorAnimation(QObject *parent)
: QDeclarativeAbstractAnimation(*(new QxAnchorAnimationPrivate), parent)
{
    Q_D(QxAnchorAnimation);
    d->va = new QDeclarativeBulkValueAnimator;
    QDeclarative_setParent_noEvent(d->va, this);
}

QxAnchorAnimation::~QxAnchorAnimation()
{
}

QAbstractAnimation *QxAnchorAnimation::qtAnimation()
{
    Q_D(QxAnchorAnimation);
    return d->va;
}

QDeclarativeListProperty<QxItem> QxAnchorAnimation::targets()
{
    Q_D(QxAnchorAnimation);
    return QDeclarativeListProperty<QxItem>(this, d->targets);
}

int QxAnchorAnimation::duration() const
{
    Q_D(const QxAnchorAnimation);
    return d->va->duration();
}

void QxAnchorAnimation::setDuration(int duration)
{
    if (duration < 0) {
        qmlInfo(this) << tr("Cannot set a duration of < 0");
        return;
    }

    Q_D(QxAnchorAnimation);
    if (d->va->duration() == duration)
        return;
    d->va->setDuration(duration);
    emit durationChanged(duration);
}

QEasingCurve QxAnchorAnimation::easing() const
{
    Q_D(const QxAnchorAnimation);
    return d->va->easingCurve();
}

void QxAnchorAnimation::setEasing(const QEasingCurve &e)
{
    Q_D(QxAnchorAnimation);
    if (d->va->easingCurve() == e)
        return;

    d->va->setEasingCurve(e);
    emit easingChanged(e);
}

void QxAnchorAnimation::transition(QDeclarativeStateActions &actions,
                                   QDeclarativeProperties &modified,
                                   TransitionDirection direction)
{
    Q_UNUSED(modified);
    Q_D(QxAnchorAnimation);
    QDeclarativeAnimationPropertyUpdater *data = new QDeclarativeAnimationPropertyUpdater;
    data->interpolatorType = QMetaType::QReal;
    data->interpolator = d->interpolator;

    data->reverse = direction == Backward ? true : false;
    data->fromSourced = false;
    data->fromDefined = false;

    for (int ii = 0; ii < actions.count(); ++ii) {
        QDeclarativeAction &action = actions[ii];
        if (action.event && action.event->typeName() == QLatin1String("AnchorChanges")
            && (d->targets.isEmpty() || d->targets.contains(static_cast<QxAnchorChanges*>(action.event)->object()))) {
            data->actions << static_cast<QxAnchorChanges*>(action.event)->additionalActions();
        }
    }

    if (data->actions.count()) {
        if (!d->rangeIsSet) {
            d->va->setStartValue(qreal(0));
            d->va->setEndValue(qreal(1));
            d->rangeIsSet = true;
        }
        d->va->setAnimValue(data, QAbstractAnimation::DeleteWhenStopped);
        d->va->setFromSourcedValue(&data->fromSourced);
    } else {
        delete data;
    }
}

