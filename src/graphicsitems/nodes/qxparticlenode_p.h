/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXPARTICLENODE_P_H
#define QXPARTICLENODE_P_H

#include "node.h"
#include "texturematerial.h"

class QxItem;
class QxParticle
{
public:
    QxParticle(int time) : lifeSpan(1000), fadeOutAge(800)
        , opacity(0), birthTime(time), x_velocity(0), y_velocity(0)
        , state(FadeIn), data(0), item(0)
    {
    }

    int lifeSpan;
    int fadeOutAge;
    qreal x;
    qreal y;
    qreal opacity;
    int birthTime;
    qreal x_velocity;
    qreal y_velocity;
    enum State { FadeIn, Solid, FadeOut };
    State state;
    void *data;
    QxItem *item;
};

class QxParticleNode : public GeometryNode
{
public:
    QxParticleNode(qreal opacity, const QRectF &targetRect, const QPixmap &pixmap, const QRectF &sourceRect, const QList<QxParticle> &particles, bool linearFiltering = false);

    void setRects(const QRectF &targetRect, const QRectF &sourceRect);

    void setOpacity(qreal);
    qreal opacity() const { return m_opacity; }

    void setParticles(const QList<QxParticle> &particles);
    void setMask(const QImage &pixmap);

    bool linearFIltering() const { return m_linearFiltering; }
    void setLinearFiltering(bool);

    void update() { updateGeometry(); }

private:
    void updateGeometry();

    qreal m_opacity;
    QRectF m_target_rect;
    QPixmap m_pixmap;
    QImage m_mask_pixmap;
    QRectF m_source_rect;
    bool m_linearFiltering;
    QList<QxParticle> m_particles;
    TextureMaterialWithOpacity m_material;
    SubTexture2DPtr m_texture;
};

#endif // PIXMAPNODE_H
