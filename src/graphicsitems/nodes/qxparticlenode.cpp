/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxparticlenode_p.h"
#include "utilities.h"
#include "texturematerial.h"
#include "adaptationlayer.h"

QxParticleNode::QxParticleNode(qreal opacity, const QRectF &t, const QPixmap &pixmap, const QRectF &sourceRect, const QList<QxParticle> &particles, bool linearFiltering)
    : m_opacity(opacity)
    , m_target_rect(t)
    , m_pixmap(pixmap)
    , m_source_rect(sourceRect)
    , m_linearFiltering(linearFiltering)
    , m_particles(particles)
{
    m_texture = Utilities::getTextureForPixmap(pixmap, true);
    m_material.setTexture(m_texture->texture(), !pixmap.hasAlphaChannel());
    m_material.setLinearFiltering(linearFiltering);
    setMaterial(&m_material);
}

void QxParticleNode::setRects(const QRectF &targetRect, const QRectF &sourceRect)
{
    m_target_rect = targetRect;
    m_source_rect = sourceRect;
}

void QxParticleNode::setOpacity(qreal o)
{
    if (m_opacity == o)
        return;

    m_opacity = o;
    m_material.setOpacity(o);
    setMaterial(&m_material); // Indicate that the material has changed.
}

void QxParticleNode::setParticles(const QList<QxParticle> &particles)
{
    m_particles = particles;
}

void QxParticleNode::setMask(const QImage &pixmap)
{
    m_mask_pixmap = pixmap;
}

void QxParticleNode::setLinearFiltering(bool linearFiltering)
{
    if (m_linearFiltering == linearFiltering)
        return;

    m_material.setLinearFiltering(linearFiltering);
    setMaterial(&m_material);
}

void QxParticleNode::updateGeometry()
{
    if (m_source_rect.isEmpty())
        m_source_rect = m_pixmap.rect();

    Geometry *g = geometry();
    if (g->isNull()) {
        QVector<QGLAttributeDescription> desc;
        desc << QGLAttributeDescription(QGL::Position, 2, GL_FLOAT, 5 * sizeof(float));
        desc << QGLAttributeDescription(QGL::TextureCoord0, 2, GL_FLOAT, 5 * sizeof(float));
        desc << QGLAttributeDescription(QGL::CustomVertex0, 1, GL_FLOAT, 5 * sizeof(float));
        updateGeometryDescription(desc, GL_UNSIGNED_SHORT);
        g->setDrawingMode(QGL::Triangles);
    }
    g->setVertexCount(4 * m_particles.count());
    g->setIndexCount(6 * m_particles.count());

    struct V
    {
        V(float x, float y, float u, float v, float o) : x(x), y(y), u(u), v(v), o(o) { }
        float x, y, u, v, o;
    };

    V *vertices = (V *)g->vertexData();
    ushort *indices = g->ushortIndexData();

    QRgb black = qRgb(0,0,0);

    int imod = 0;
    for (int i = 0; i < m_particles.count(); ++i, ++imod) {
        const QxParticle &particle = m_particles.at(i);
        float px = particle.x;
        float py = particle.y;
        //### big hack. if this filters everything out, we crash
        if (!m_mask_pixmap.isNull()) {
            if (px > m_mask_pixmap.width() || py > m_mask_pixmap.height()) {
                --imod;
                continue;
            }
            QRgb rgb = m_mask_pixmap.pixel(px, py);
            if (rgb != black) {
                --imod;
                continue;
            }
        }
        float pw = m_source_rect.width()/2;
        float ph = m_source_rect.height()/2;

        QRectF src = m_texture->sourceRect();

        int i4 = imod * 4;
        vertices[i4 + 0] = V(px - pw, py - ph, src.left(), src.top(), particle.opacity * m_opacity);
        vertices[i4 + 1] = V(px + pw, py - ph, src.right(), src.top(), particle.opacity * m_opacity);
        vertices[i4 + 2] = V(px - pw, py + ph, src.left(), src.bottom(), particle.opacity * m_opacity);
        vertices[i4 + 3] = V(px + pw, py + ph, src.right(), src.bottom(), particle.opacity * m_opacity);

        int i6 = imod * 6;
        indices[i6 + 0] = i4 + 0;
        indices[i6 + 1] = i4 + 2;
        indices[i6 + 2] = i4 + 3;
        indices[i6 + 3] = i4 + 3;
        indices[i6 + 4] = i4 + 1;
        indices[i6 + 5] = i4 + 0;
    }

    setEndIndex(6 * imod);
    markDirty(Node::DirtyGeometry);

    //setBoundingRect(m_target_rect); //### wrong
}
