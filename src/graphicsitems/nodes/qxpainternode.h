/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef QXPAINTERNODE_H
#define QXPAINTERNODE_H

#include "node.h"
#include "texturematerial.h"

class QxPaintItem;
class QGLFramebufferObject;

class QxPainterNode : public GeometryNode
{
public:
    QxPainterNode(QxPaintItem *item = 0);
    virtual ~QxPainterNode();

    void setPaintItem(QxPaintItem *item) { m_item = item; }

    void preprocess();

    void setSize(const QSize &size);
    QSize size() const { return m_size; }

    void setOpacity(qreal opacity) ;
    qreal opacity() const { return m_opacity; }

    void setOpaquePainting(bool opaque);
    bool opaquePainting() const { return m_opaquePainting; }

    void setLinearFiltering(bool linearFiltering);
    bool linearFiltering() const { return m_linear_filtering; }

    void setSmoothPainting(bool s);
    bool smoothPainting() const { return m_smoothPainting; }

    virtual void update(uint updateFlags);

private:
    enum UpdateFlag
    {
        UpdateTexture = 0x01,
        UpdateGeometry = 0x02,
        UpdateFBO = 0x03
    };

    void updateTexture();
    void updateGeometry();
    void updateFBO();

    QxPaintItem *m_item;

    QGLFramebufferObject *m_fbo;
    QGLFramebufferObject *m_multisampledFbo;
    TextureMaterial m_material;
    TextureMaterialWithOpacity m_materialO;
    QSGTextureRef m_texture;

    QSize m_size;
    qreal m_opacity;
    bool m_opaquePainting;
    bool m_linear_filtering;
    bool m_smoothPainting;
    bool m_extensionsChecked;
    bool m_multisamplingSupported;
};

#endif // QXPAINTERNODE_H
