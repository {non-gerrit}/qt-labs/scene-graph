/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxrectangle_p.h"
#include "qxrectangle_p_p.h"
#include "qxclipnode_p.h"

#include "geometry.h"
#include "vertexcolormaterial.h"

#include <QtGui/qpixmapcache.h>
#include <QtGui/qpainter.h>
#include <QtCore/qstringbuilder.h>
#include <QtCore/qmath.h>
#include <QtDeclarative/qdeclarativeinfo.h>

//#define ROUNDEDRECT_CLIP

void QxPen::setColor(const QColor &c)
{
    _color = c;
    _valid = _color.alpha() ? true : false;
    emit penColorChanged();
}

void QxPen::setWidth(int w)
{
    if (_width == w && _valid)
        return;

    _width = w;
    _valid = (_width < 1) ? false : true;
    emit penWidthChanged();
}

void QxGradientStop::updateGradient()
{
    if (QxGradient *grad = qobject_cast<QxGradient*>(parent()))
        grad->doUpdate();
}

const QGradient *QxGradient::gradient() const
{
    if (!m_gradient && !m_stops.isEmpty()) {
        m_gradient = new QLinearGradient(0,0,0,1.0);
        for (int i = 0; i < m_stops.count(); ++i) {
            const QxGradientStop *stop = m_stops.at(i);
            m_gradient->setCoordinateMode(QGradient::ObjectBoundingMode);
            m_gradient->setColorAt(stop->position(), stop->color());
        }
    }

    return m_gradient;
}

void QxGradient::doUpdate()
{
    delete m_gradient;
    m_gradient = 0;
    emit updated();
}

int QxRectanglePrivate::doUpdateGradientSlotIdx = -1;
int QxRectanglePrivate::doUpdateBorderWidthSlotIdx = -1;
int QxRectanglePrivate::doUpdateBorderColorSlotIdx = -1;

QxRectangle::QxRectangle(QxItem *parent)
: QxItem(*(new QxRectanglePrivate), parent)
{
}

QxRectangle::~QxRectangle()
{
    Q_D(QxRectangle);

    delete d->node;
}

void QxRectangle::doUpdateGradient()
{
    Q_D(QxRectangle);
    d->updateGradient();
}

void QxRectangle::doUpdateBorderWidth()
{
    Q_D(QxRectangle);
    d->updateBorderWidth();
}

void QxRectangle::doUpdateBorderColor()
{
    Q_D(QxRectangle);
    d->updateBorderColor();
}

QxPen *QxRectangle::border()
{
    Q_D(QxRectangle);
    return d->getPen();
}

QxGradient *QxRectangle::gradient() const
{
    Q_D(const QxRectangle);
    return d->gradient;
}

void QxRectangle::setGradient(QxGradient *gradient)
{
    Q_D(QxRectangle);
    if (d->gradient == gradient)
        return;
    static int updatedSignalIdx = -1;
    if (updatedSignalIdx < 0)
        updatedSignalIdx = QxGradient::staticMetaObject.indexOfSignal("updated()");
    if (d->doUpdateGradientSlotIdx < 0)
        d->doUpdateGradientSlotIdx = QxRectangle::staticMetaObject.indexOfSlot("doUpdateGradient()");
    if (d->gradient)
        QMetaObject::disconnect(d->gradient, updatedSignalIdx, this, d->doUpdateGradientSlotIdx);
    d->gradient = gradient;
    if (d->gradient)
        QMetaObject::connect(d->gradient, updatedSignalIdx, this, d->doUpdateGradientSlotIdx);
    d->updateGradient();
}

qreal QxRectangle::radius() const
{
    Q_D(const QxRectangle);
    return d->radius;
}

void QxRectangle::setRadius(qreal radius)
{
    Q_D(QxRectangle);
    if (d->radius == radius)
        return;

    d->radius = radius;
    if (d->node)
        d->node->setRadius(radius);

    if (d->clipNode)
        d->clipNode->setRadius(d->radius);

    emit radiusChanged();
}

QColor QxRectangle::color() const
{
    Q_D(const QxRectangle);
    return d->color;
}

void QxRectangle::setColor(const QColor &c)
{
    Q_D(QxRectangle);
    if (d->color == c)
        return;

    d->color = c;
    if (d->node)
        d->node->setColor(c);

    emit colorChanged();
}

void QxRectanglePrivate::updateGradient()
{
    if (!node)
        return;

    QGradientStops stops;
    if (gradient) {
        QList<QxGradientStop *> qxstops = gradient->stops();
        for (int i = 0; i < qxstops.size(); ++i)
            stops.append(QGradientStop(qxstops.at(i)->position(), qxstops.at(i)->color()));
    }
    node->setGradientStops(stops);
}

void QxRectanglePrivate::updateBorderWidth()
{
    Q_Q(QxRectangle);

    if (pen && node) {
        node->setPenWidth(pen->width());
        q->setPaintNode(node); // Update clip.
    }
}

void QxRectanglePrivate::updateBorderColor()
{
    if (pen && node)
        node->setPenColor(pen->color());
}

void QxRectangle::componentComplete()
{
    Q_D(QxRectangle);

    QxItem::componentComplete();
    Q_ASSERT(d->node == 0);
    d->node = QSGContext::current->createRectangleNode();
    d->node->setColor(d->color);
    d->node->setRadius(d->radius);
    d->node->setRect(QRectF(0, 0, width(), height()));
    d->node->setOpacity(d->renderOpacity);
    d->updateGradient();
    d->updateBorderWidth();
    d->updateBorderColor();
    if (d->clipNode)
        d->clipNode->setRadius(d->radius);
    setPaintNode(d->node); // Must call setRect() before calling setPaintNode() for clipping to be correct.
}

void QxRectangle::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    Q_D(QxRectangle);

    if (d->node && newGeometry.size() != oldGeometry.size()) {
        d->node->setRect(QRectF(0, 0, width(), height()));
        setPaintNode(d->node); // Update clip.
    }

    QxItem::geometryChanged(newGeometry, oldGeometry);
}

void QxRectangle::renderOpacityChanged(qreal newOpacity, qreal oldOpacity)
{
    Q_D(QxRectangle);

    QxItem::renderOpacityChanged(newOpacity, oldOpacity);

    if (d->node)
        d->node->setOpacity(newOpacity);
}

void QxRectanglePrivate::clipChanged(bool newClip)
{
#ifdef ROUNDEDRECT_CLIP
    if (newClip && radius > 0) {
        clipNode->setRadius(radius);
    }
#endif
    QxItemPrivate::clipChanged(newClip);
}

