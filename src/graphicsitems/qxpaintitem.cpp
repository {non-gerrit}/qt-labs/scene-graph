/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qxpaintitem.h"
#include "qxpaintitem_p.h"
#include "utilities.h"

/*!
    \class QxPaintItem

    The QxPaintItem makes it possible to use the QPainter API in a scene graph.
    It sets up a textured rectangle in the scene graph and uses
    a QPainter to paint onto the texture. Call update() to trigger a repaint.

    QxPaintItem is meant to make it easier to port old code that is using the
    QPainter API to the scene graph API.
*/

QxPaintItemPrivate::QxPaintItemPrivate()
    : QxItemPrivate()
    , m_node(0)
    , m_opaque_painting(false)
    , m_dirty(false)
{
}

QxPaintItemPrivate::~QxPaintItemPrivate()
{
    delete m_node;
}

/*!
    Constructs a QxPaintItem with \a parent.
*/
QxPaintItem::QxPaintItem(QxItem *parent)
    : QxItem(*(new QxPaintItemPrivate), parent)
{
}

/*!
    \internal
*/
QxPaintItem::QxPaintItem(QxPaintItemPrivate &dd, QxItem *parent)
  : QxItem(dd, parent)
{
}

/*!
    Destroys the QxPaintItem.
*/
QxPaintItem::~QxPaintItem()
{
}

/*!
    Causes a repaint of the item and the scene graph.
*/
void QxPaintItem::update()
{
    Q_D(QxPaintItem);
    d->m_dirty = true;
    if (d->m_node)
        d->m_node->markDirty(Node::DirtyMaterial);
}

/*!
    Initializes the item and triggers the first paint.
*/
void QxPaintItem::componentComplete()
{
    Q_D(QxPaintItem);
    QxItem::componentComplete();
    Q_ASSERT(d->m_node == 0);
    d->m_node = new QxPainterNode(this);
    d->m_node->setSize(size().toSize());
    d->m_node->setOpacity(renderOpacity());
    d->m_node->setSmoothPainting(smooth());
    d->m_node->setLinearFiltering(smooth());
    d->m_node->setOpaquePainting(d->m_opaque_painting);
    setPaintNode(d->m_node);
    update();
}

/*!
    Updates the item geometry.
*/
void QxPaintItem::geometryChanged(const QRectF &newGeometry, const QRectF &oldGeometry)
{
    Q_D(QxPaintItem);
    if (d->m_node && newGeometry.size() != oldGeometry.size()) {
        d->m_node->setSize(newGeometry.size().toSize());
        update();
    }
    QxItem::geometryChanged(newGeometry, oldGeometry);
}

/*!
    Updates the item opacity.
*/
void QxPaintItem::renderOpacityChanged(qreal newOpacity, qreal oldOpacity)
{
    Q_D(QxPaintItem);
    if (d->m_node && newOpacity != oldOpacity)
        d->m_node->setOpacity(newOpacity);
    QxItem::renderOpacityChanged(newOpacity, oldOpacity);
}

/*!
    Updates the item's texture filtering.
*/
void QxPaintItem::smoothChange(bool newSmooth, bool oldSmooth)
{
    Q_D(QxPaintItem);
    if (d->m_node && newSmooth != oldSmooth) {
        d->m_node->setSmoothPainting(newSmooth);
        d->m_node->setLinearFiltering(newSmooth);
        update();
    }
    QxItem::smoothChange(newSmooth, oldSmooth);
}

/*!
    Sets a hint saying that the item is painted opaque.

    Set \a opaque to true if paint() fills the entire item with opaque pixels.
    Setting \a opaque to true can improve performance. This property is
    independent of opacity.

    \sa opaquePainting()
*/
void QxPaintItem::setOpaquePainting(bool opaque)
{
    Q_D(QxPaintItem);
    if (opaque == d->m_opaque_painting)
        return;
    d->m_opaque_painting = opaque;
    if (d->m_node)
        d->m_node->setOpaquePainting(d->m_opaque_painting);
}

/*!
    Returns the hint saying that the item is painted opaque.

    \sa opaquePainting()
*/
bool QxPaintItem::opaquePainting() const
{
    Q_D(const QxPaintItem);
    return d->m_opaque_painting;
}

void QxPaintItem::setDirty(bool dirty)
{
    Q_D(QxPaintItem);
    d->m_dirty = dirty;
}

bool QxPaintItem::isDirty() const
{
    Q_D(const QxPaintItem);
    return d->m_dirty;
}
