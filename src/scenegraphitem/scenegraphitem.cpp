/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "scenegraphitem.h"
#include "scenegraphitem_p.h"
#include "qxitem.h"
#include "qxitem_p.h"

#include <QtCore/qcoreapplication.h>
#include <QtGui/qpainter.h>
#include <QtGui/qevent.h>
#include <QtGui/qgraphicssceneevent.h>
#include <QtOpenGL/qglframebufferobject.h>
#include <QtDeclarative/qdeclarativecomponent.h>
#include <private/qmath_p.h>

#include <private/qdeclarativedebugtrace_p.h>

#ifdef Q_WS_QPA
#include <qplatformglcontext_qpa.h>
#endif

#include "utilities.h"

// ### gunnar: SceneGraphItem needs to be ported to QSGContext too

/*!
    \class SceneGraphItem

    The SceneGraphItem is used for embedding a scene graph based QML document
    in a QGraphicsScene. Specify the QML document with setSource(). The
    SceneGraphItem will have the same size as the root item of the QML
    document. Override the default size with setSize().
*/

/*!
    \fn SceneGraphItem::frameStarted()

    This signal is emitted right before the scene graph is painted.

    \sa frameEnded()
*/

/*!
    \fn SceneGraphItem::frameEnded()

    This signal is emitted right after the scene graph was painted.

    \sa frameStarted()
*/

SceneGraphItemPrivate::SceneGraphItemPrivate()
    : root(0)
    , component(0)
    , mouseGrabber(0)
    , focusItem(0)
    , fbo(0)
    , multisampledFbo(0)
    , canUseMultisampledFbo(false)
{
    renderer.setClearColor(Qt::transparent);
}

SceneGraphItemPrivate::~SceneGraphItemPrivate()
{
    delete fbo;
    delete multisampledFbo;
    delete root;
}

void SceneGraphItemPrivate::execute()
{
    Q_Q(SceneGraphItem);

    if (component) {
        delete component;
        component = 0;
    }

    if (!source.isEmpty()) {
        component = new QDeclarativeComponent(&engine, source, q);
        if (!component->isReady()) {
            QList<QDeclarativeError> errors = component->errors();
            foreach (const QDeclarativeError &error, errors)
                qWarning() << error;
            return;
        }

        QObject *o = component->create();

        QxItem *go = qobject_cast<QxItem *>(o);
        if (go) {

            root = go;
            QxItemPrivate *op = QxItemPrivate::get(root);
            //op->view = q;
            rootNode.appendChildNode(&op->transformNode);
        } else {
            qWarning("Failed to create a proper QxItem from component...");
        }
    }
}

QSize SceneGraphItemPrivate::rootObjectSize() const
{
    QSize size = root ? root->size().toSize() : QSize(0, 0);

    if (size.width() < 0)
        size.setWidth(0);
    if (size.height() < 0)
        size.setHeight(0);

    return size;
}

/*!
    Constructs a SceneGraphItem with \a parent.
*/
SceneGraphItem::SceneGraphItem(QGraphicsItem *parent)
    : QGraphicsObject(*new SceneGraphItemPrivate, parent, 0)
{
    Q_D(SceneGraphItem);

    extern void qt_scenegraph_register_types(); // in qmlscene_global.cpp
    qt_scenegraph_register_types();

    d->renderer.setClearColor(Qt::white);

    d->renderer.setRootNode(&d->rootNode);
    connect(&d->renderer, SIGNAL(sceneGraphChanged()), this, SLOT(repaint()));

    setFlag(QGraphicsItem::ItemHasNoContents, false);
    setAcceptedMouseButtons(Qt::LeftButton | Qt::MiddleButton | Qt::RightButton | Qt::XButton1 | Qt::XButton2);
}

/*!
    Destroys the SceneGraphItem.
*/
SceneGraphItem::~SceneGraphItem()
{
}

/*!
    Returns the root item of the QML document. The root item is a
    QxItem, which is the scene graph equivalent of QGraphicsObject in
    graphics view.
*/
QxItem *SceneGraphItem::root()
{
    Q_D(SceneGraphItem);
    return d->root;
}

/*!
    Returns the size of the root item.

    \sa setSize(), root()
*/
QSizeF SceneGraphItem::size() const
{
    Q_D(const SceneGraphItem);
    return d->root ? d->root->size() : QSizeF(0, 0);
}

/*!
    Sets the size of the root item.

    \sa size(), root()
*/
void SceneGraphItem::setSize(const QSizeF &size)
{
    Q_D(SceneGraphItem);
    if (d->root)
        d->root->setSize(size);
}

void SceneGraphItem::repaint()
{
    update();
}

/*!
    Paints the scene graph based QML document using the transformation set on \a painter.
*/
void SceneGraphItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    if (painter->paintEngine()->type() != QPaintEngine::OpenGL2) {
        qWarning("SceneGraphItem requires the OpenGL2 paint engine to work.");
        return;
    }

    Q_D(SceneGraphItem);
    painter->setRenderHint(QPainter::Antialiasing, true); // Enable multisampling.
    painter->beginNativePainting();
    QGLContext *ctx = const_cast<QGLContext *>(QGLContext::currentContext());

    QDeclarativeDebugTrace::addEvent(QDeclarativeDebugTrace::FramePaint);
    QDeclarativeDebugTrace::startRange(QDeclarativeDebugTrace::Painting);

    emit frameStarted();

    bool paintViaFbo = painter->hasClipping();
    if (paintViaFbo) {
        if (!d->fbo) {
            // If no FBO has been allocated, it is the first time we reach here. Check for extensions.
            QList<QByteArray> extensions = QByteArray((const char *)glGetString(GL_EXTENSIONS)).split(' ');
            d->canUseMultisampledFbo = extensions.contains("GL_EXT_framebuffer_multisample")
                                    && extensions.contains("GL_EXT_framebuffer_blit");
        }

        if (!d->fbo || d->fbo->width() != qCeil(d->root->width()) || d->fbo->height() != qCeil(d->root->height())) {
            delete d->fbo;
            delete d->multisampledFbo;
            // If the backbuffer has multisampling, try to use it for the FBO as well.
            const QGLFormat ctxFormat = ctx->format();
            if (d->canUseMultisampledFbo && ctxFormat.sampleBuffers()) {
                d->fbo = new QGLFramebufferObject(qCeil(d->root->width()), qCeil(d->root->height()), QGLFramebufferObject::NoAttachment);

                QGLFramebufferObjectFormat format;
                format.setAttachment(QGLFramebufferObject::CombinedDepthStencil);
                format.setSamples(ctxFormat.samples());
                d->multisampledFbo = new QGLFramebufferObject(d->fbo->size(), format);
            } else {
                d->fbo = new QGLFramebufferObject(qCeil(d->root->width()), qCeil(d->root->height()), QGLFramebufferObject::CombinedDepthStencil);
                d->multisampledFbo = 0;
            }
        }

        d->renderer.setDeviceRect(QRect(0, 0, d->fbo->width(), d->fbo->height()));
        // TODO: Fix sub-pixel positioning.
        d->renderer.setProjectMatrixToDeviceRect();
        if (d->multisampledFbo) {
            QRect rect(0, 0, d->fbo->width(), d->fbo->height());
            d->renderer.renderScene(BindableFbo(ctx, d->multisampledFbo));
            QGLFramebufferObject::blitFramebuffer(d->fbo, rect, d->multisampledFbo, rect);
        } else {
            d->renderer.renderScene(BindableFbo(ctx, d->fbo));
        }
        QGLFramebufferObject::bindDefault();
    } else {
        d->renderer.setDeviceRect(QRect(0, 0, painter->device()->width(), painter->device()->height()));
        d->renderer.setProjectMatrixToDeviceRect();
        QMatrix4x4 m = d->renderer.projectMatrix();
        m *= painter->worldTransform();
        d->renderer.setProjectMatrix(m);
        d->renderer.renderScene();
    }

    QDeclarativeDebugTrace::endRange(QDeclarativeDebugTrace::Painting);

    emit frameEnded();

    // Reset some states.
    glDisable(GL_DEPTH_TEST);
    //glDepthFunc(GL_LEQUAL);
    //glDisable(GL_STENCIL_TEST);
    //glDepthMask(true);

    QGLBuffer::release(QGLBuffer::VertexBuffer);
    QGLBuffer::release(QGLBuffer::IndexBuffer);

    painter->endNativePainting();
    if (paintViaFbo)
        drawTexture(d->fbo->texture());
}

/*!
    Renders a rectangle the size of this item using \a texture and the
    currently active painter's transformation. This function is called from
    paint() when drawing the scene graph via an FBO. Override this function
    to set texture states like filtering.
*/
void SceneGraphItem::drawTexture(uint texture)
{
    Q_D(SceneGraphItem);
    const_cast<QGLContext *>(QGLContext::currentContext())->drawTexture(QRectF(0, 0, d->root->width(), d->root->height()), texture);
}

/*!
    Returns the bounding rectangle of the root item.

    \sa root()
*/
QRectF SceneGraphItem::boundingRect() const
{
    Q_D(const SceneGraphItem);
    return d->root ? QRectF(0, 0, d->root->width(), d->root->height()) : QRectF();
}

QGraphicsSceneMouseEvent *SceneGraphItemPrivate::sceneMouseEvent(QGraphicsSceneMouseEvent *e, const QPointF &local)
{
    QGraphicsSceneMouseEvent *g = new QGraphicsSceneMouseEvent(e->type());
    g->setPos(local);
    g->setScenePos(e->pos());
    g->setScreenPos(e->screenPos());
    g->setButtons(e->buttons());
    g->setButton(e->button());
    g->setModifiers(e->modifiers());
    return g;
}

bool SceneGraphItemPrivate::deliverInitialMouseEvent(QxItem *o, QGraphicsSceneMouseEvent *e)
{
    QxItemPrivate *op = QxItemPrivate::get(o);
    if (op->opacity == 0. || !op->visible || op->scale == 0.)
        return false;
    if (op->clip) {
        bool ok = false;
        QTransform t = op->mapFromGlobalTransform(&ok);
        if (!ok)
            return false;

        QPointF local = t.map(e->pos());
        QRectF bounds(qreal(0) ,qreal(0) , o->width(), o->height());
        if (!bounds.contains(local))
            return false;
    }

    bool filter = op->filtersChildMouse;
    if (filter) mouseFilters.append(o);

    QxItem *children = op->firstChild;
    while (children) {
        if (deliverInitialMouseEvent(children, e))
            return true;
        children = QxItemPrivate::get(children)->nextSibling;
    }

    if (filter) mouseFilters.removeLast();

    if (op->acceptedButtons & e->button()) {
        bool ok = false;
        QTransform t = op->mapFromGlobalTransform(&ok);
        if (!ok)
            return false;

        QPointF local = t.map(e->pos());
        QRectF bounds(qreal(0) ,qreal(0) , o->width(), o->height());
        if (bounds.contains(local)) {
            QGraphicsSceneMouseEvent *m = sceneMouseEvent(e, local);

            mouseGrabber = o;

            for (int ii = mouseFilters.count() - 1; ii >= 0; --ii) {
                if (mouseFilters.at(ii)->sceneEventFilter(o, m)) {
                    delete m;
                    return false;
                }
            }

            o->mousePressEvent(m);

            if (m->isAccepted()) {
                delete m;
                return true;
            } else {
                mouseGrabber = 0;
                delete m;
                return false;
            }
        }
    }

    return false;
}

/*!
    Delivers a mouse press event to the scene graph.
*/
void SceneGraphItem::mousePressEvent(QGraphicsSceneMouseEvent *e)
{
    Q_D(SceneGraphItem);
    if (d->deliverInitialMouseEvent(d->root, e)) {
        e->accept();
    } else {
        QGraphicsItem::mousePressEvent(e);
    }
}

/*!
    Delivers a mouse move event to the scene graph.
*/
void SceneGraphItem::mouseMoveEvent(QGraphicsSceneMouseEvent *e)
{
    Q_D(SceneGraphItem);
    if (d->mouseGrabber) {
        QTransform t = QxItemPrivate::get(d->mouseGrabber)->mapFromGlobalTransform();
        QPointF local = t.map(e->pos());
        QGraphicsSceneMouseEvent *m = d->sceneMouseEvent(e, local);

        for (int ii = d->mouseFilters.count() - 1; ii >= 0; --ii) {
            if (d->mouseFilters.at(ii)->sceneEventFilter(d->mouseGrabber, m)) {
                delete m;
                return;
            }
        }

        d->mouseGrabber->mouseMoveEvent(m);

        bool wasAccepted = m->isAccepted();

        delete m;

        if (wasAccepted) {
            e->accept();
            return;
        }
    }
    QGraphicsItem::mouseMoveEvent(e);
}

/*!
    Delivers a mouse release event to the scene graph.
*/
void SceneGraphItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *e)
{
    Q_D(SceneGraphItem);
    if (d->mouseGrabber) {
        QTransform t = QxItemPrivate::get(d->mouseGrabber)->mapFromGlobalTransform();
        QPointF local = t.map(e->pos());
        QGraphicsSceneMouseEvent *m = d->sceneMouseEvent(e, local);

        for (int ii = d->mouseFilters.count() - 1; ii >= 0; --ii) {
            if (d->mouseFilters.at(ii)->sceneEventFilter(d->mouseGrabber, m)) {
                delete m;
                d->mouseFilters.clear();
                d->mouseGrabber = 0;
                return;
            }
        }
        d->mouseFilters.clear();

        d->mouseGrabber->mouseReleaseEvent(m);
        d->mouseGrabber = 0;

        bool wasAccepted = m->isAccepted();

        delete m;

        if (wasAccepted) {
            e->accept();
            return;
        }
    }
    QGraphicsItem::mouseReleaseEvent(e);
}

/*!
    Delivers a key press event to the scene graph.
*/
void SceneGraphItem::keyPressEvent(QKeyEvent *e)
{
    Q_D(SceneGraphItem);
    QxItem *item = d->focusItem;
    if (item) {
        QxItem *p = item;
        do {
            // Accept the event by default
            e->accept();
            // Send it; QxItem::keyPressEvent ignores it.  If the event
            // is filtered out, stop propagating it.
            //if (p->isBlockedByModalPanel())
            //    break;
            QCoreApplication::sendEvent(p, e);
        } while (!e->isAccepted() /*&& !p->isPanel()*/ && (p = p->parentItem()));
    } else {
        e->ignore();
    }

    QGraphicsItem::keyPressEvent(e);
}

/*!
    Delivers a key release event to the scene graph.
*/
void SceneGraphItem::keyReleaseEvent(QKeyEvent *e)
{
    Q_D(SceneGraphItem);
    QxItem *item = d->focusItem;
    if (item) {
        QxItem *p = item;
        do {
            // Accept the event by default
            e->accept();
            // Send it; QxItem::keyReleaseEvent ignores it.  If the event
            // is filtered out, stop propagating it.
            //if (p->isBlockedByModalPanel())
            //    break;
            QCoreApplication::sendEvent(p, e);
        } while (!e->isAccepted() /*&& !p->isPanel()*/ && (p = p->parentItem()));
    } else {
        e->ignore();
    }
    QGraphicsItem::keyReleaseEvent(e);
}

void SceneGraphItemPrivate::setFocusItem(QxItem *item)
{
    //### should never enter this function if we aren't in an active focus chain
    //XXX finish implementation
    if (focusItem == item)
        return;

    //remove focus from previous focus item
    if (focusItem) {
        QxItem *prevFocusItem = focusItem;
        focusItem = 0;
        QFocusEvent event(QEvent::FocusOut, Qt::OtherFocusReason);
        QCoreApplication::sendEvent(prevFocusItem, &event);
    }

    QxItem *fi = item;
    while (fi && fi->d_func()->isFocusScope) {
        fi->d_func()->inActiveFocusChain = true;
        if (fi == fi->d_func()->focusItem)
            break;
        if (fi->d_func()->focusItem)
            fi = fi->d_func()->focusItem;
    }

    focusItem = fi;

    //give focus to new focus item
    if (focusItem) {
        focusItem->d_func()->inActiveFocusChain = true;
        QFocusEvent event(QEvent::FocusOut, Qt::OtherFocusReason);
        QCoreApplication::sendEvent(focusItem, &event);
    }
}

/*!
    Returns the scene graph item that is currently receiving mouse events.
*/
QxItem *SceneGraphItem::mouseGrabberItem() const
{
    Q_D(const SceneGraphItem);
    return d->mouseGrabber;
}

/*!
    Returns engine used when interpreting the QML document.
*/
QDeclarativeEngine *SceneGraphItem::engine()
{
    Q_D(SceneGraphItem);
    return &d->engine;
}

/*!
    Returns the engine's root context.

    \sa engine()
*/
QDeclarativeContext *SceneGraphItem::rootContext() const
{
    Q_D(const SceneGraphItem);
    return d->engine.rootContext();
}

/*!
    Returns the URL pointing to the QML document.

    \sa setSource()
*/
QUrl SceneGraphItem::source() const
{
    Q_D(const SceneGraphItem);
    return d->source;
}

/*!
    Sets the URL pointing to the QML document.

    \sa source()
*/
void SceneGraphItem::setSource(const QUrl &url)
{
    Q_D(SceneGraphItem);
    d->source = url;
    d->execute();
}

/*!
    Returns the integer size of the root item.

    \sa size(), root()
*/
QSize SceneGraphItem::sizeHint() const
{
    Q_D(const SceneGraphItem);
    return d->rootObjectSize();
}
