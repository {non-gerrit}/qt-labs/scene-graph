/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/


#ifndef SCENEGRAPHITEM_P_H
#define SCENEGRAPHITEM_P_H

#include "scenegraphitem.h"

#include "qmlrenderer.h"
#include "node.h"

#include <QtDeclarative/qdeclarativeengine.h>
#include <private/qgraphicsitem_p.h>
#include <QtCore/qlist.h>
#include <QtCore/qsize.h>
#include <QtCore/qurl.h>

class QDeclarativeComponent;

class SceneGraphItemPrivate : public QGraphicsItemPrivate
{
    Q_DECLARE_PUBLIC(SceneGraphItem)
public:
    SceneGraphItemPrivate();
    ~SceneGraphItemPrivate();

    QxItem *root;
    QDeclarativeComponent *component;

    QDeclarativeEngine engine;

    QxItem *mouseGrabber;
    QList<QxItem *> mouseFilters;
    QxItem *focusItem;

    void setFocusItem(QxItem *item);

    bool deliverInitialMouseEvent(QxItem *, QGraphicsSceneMouseEvent *);
    QGraphicsSceneMouseEvent *sceneMouseEvent(QGraphicsSceneMouseEvent *, const QPointF &);

    static inline SceneGraphItemPrivate *get(SceneGraphItem *v) {
        return v->d_func();
    }

    void execute();
    QSize rootObjectSize() const;

    QMLRenderer renderer;
    RootNode rootNode;

    QUrl source;
    QGLFramebufferObject *fbo;
    QGLFramebufferObject *multisampledFbo;
    bool canUseMultisampledFbo;
};

#endif
