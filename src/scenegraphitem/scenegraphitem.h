/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef SCENEGRAPHITEM_H
#define SCENEGRAPHITEM_H

#include "qmlscene_global.h"

#include <QtGui/qgraphicsitem.h>

class QxItem;
class QDeclarativeEngine;
class QDeclarativeContext;
class QUrl;
class QSize;
class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class QGraphicsSceneMouseEvent;
class QKeyEvent;
class SceneGraphItemPrivate;

class QT_SCENEGRAPH_EXPORT SceneGraphItem : public QGraphicsObject
{
    Q_OBJECT
public:
    SceneGraphItem(QGraphicsItem *parent = 0);
    ~SceneGraphItem();

    QxItem *root();

    QDeclarativeEngine* engine();
    QDeclarativeContext* rootContext() const;

    QUrl source() const;
    void setSource(const QUrl &url);

    QxItem *mouseGrabberItem() const;

    QSize sizeHint() const;

    QSizeF size() const;
    void setSize(const QSizeF &size);

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);
    virtual QRectF boundingRect() const;

Q_SIGNALS:
    void frameStarted();
    void frameEnded();

protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *);
    virtual void keyPressEvent(QKeyEvent *);
    virtual void keyReleaseEvent(QKeyEvent *);

    virtual void drawTexture(uint texture);

private Q_SLOTS:
    void repaint();

private:
    Q_DISABLE_COPY(SceneGraphItem)
    Q_DECLARE_PRIVATE_D(QGraphicsItem::d_ptr.data(), SceneGraphItem)
};

#endif
