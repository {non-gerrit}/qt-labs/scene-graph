/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef SHADEREFFECTITEM_H
#define SHADEREFFECTITEM_H

#include "qxitem.h"
#include "effectnode.h"
#include "material.h"
#include "adaptationlayer.h"

#include <QtCore/qpointer.h>

class QSGContext;
class QSignalMapper;
class CustomShaderMaterialData;
class QGLFramebufferObject;

// TODO: Implement async loading and loading over network.
// TODO: Implement support for multisampling.
class ShaderEffectSource : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QxItem *sourceItem READ sourceItem WRITE setSourceItem NOTIFY sourceItemChanged)
    Q_PROPERTY(QUrl sourceImage READ sourceImage WRITE setSourceImage NOTIFY sourceImageChanged)
    Q_PROPERTY(FilterMode mipmap READ mipmap WRITE setMipmap NOTIFY mipmapChanged)
    Q_PROPERTY(FilterMode filtering READ filtering WRITE setFiltering NOTIFY filteringChanged)
    Q_PROPERTY(WrapMode horizontalWrap READ horizontalWrap WRITE setHorizontalWrap NOTIFY horizontalWrapChanged)
    Q_PROPERTY(WrapMode verticalWrap READ verticalWrap WRITE setVerticalWrap NOTIFY verticalWrapChanged)
    Q_PROPERTY(QSize margins READ margins WRITE setMargins NOTIFY marginsChanged)
    Q_PROPERTY(QSize textureSize READ textureSize WRITE setTextureSize NOTIFY textureSizeChanged)
    Q_PROPERTY(int width READ width NOTIFY widthChanged)
    Q_PROPERTY(int height READ height NOTIFY heightChanged)
    Q_PROPERTY(bool live READ isLive WRITE setLive NOTIFY liveChanged)
    Q_PROPERTY(bool hideOriginal READ hideOriginal WRITE setHideOriginal NOTIFY hideOriginalChanged)
    Q_PROPERTY(bool active READ isActive NOTIFY activeChanged)
    Q_ENUMS(FilterMode)
    Q_ENUMS(WrapMode)

public:
    enum FilterMode
    {
        None,
        Nearest,
        Linear
    };

    enum WrapMode
    {
        Repeat,
        ClampToEdge
    };

    ShaderEffectSource(QObject *parent = 0);
    virtual ~ShaderEffectSource();

    QSGContext *sceneGraphContext() const { return m_context; }
    void setSceneGraphContext(QSGContext *context) { m_context = context; }

    QxItem *sourceItem() const { return m_sourceItem.data(); }
    void setSourceItem(QxItem *item);

    QUrl sourceImage() const { return m_sourceImage; }
    void setSourceImage(const QUrl &url);

    FilterMode mipmap() const { return m_mipmap; }
    void setMipmap(FilterMode mode);

    FilterMode filtering() const { return m_filtering; }
    void setFiltering(FilterMode mode);

    WrapMode horizontalWrap() const { return m_horizontalWrap; }
    void setHorizontalWrap(WrapMode mode);

    WrapMode verticalWrap() const { return m_verticalWrap; }
    void setVerticalWrap(WrapMode mode);

    QSize margins() const { return m_margins; }
    void setMargins(const QSize &size);

    QSize textureSize() const { return m_textureSize; }
    void setTextureSize(const QSize &size);

    int width() const { return m_size.width(); }
    int height() const { return m_size.height(); }

    bool isLive() const { return m_live; }
    void setLive(bool s);

    bool hideOriginal() const { return m_hideOriginal; }
    void setHideOriginal(bool hide);

    bool isActive() const { return m_refs; }

    void bind() const;

    void refFromEffectItem();
    void derefFromEffectItem();
    void update();

    Q_INVOKABLE void grab();

Q_SIGNALS:
    void sourceItemChanged();
    void sourceImageChanged();
    void mipmapChanged();
    void filteringChanged();
    void horizontalWrapChanged();
    void verticalWrapChanged();
    void marginsChanged();
    void textureSizeChanged();
    void widthChanged();
    void heightChanged();
    void liveChanged();
    void hideOriginalChanged();
    void activeChanged();

    void repaintRequired();

private Q_SLOTS:
    void markSceneGraphDirty();
    void markSourceSizeDirty();

private:
    void updateSizeAndTexture();

    QPointer<QxItem> m_sourceItem;
    QUrl m_sourceImage;
    FilterMode m_mipmap;
    FilterMode m_filtering;
    WrapMode m_horizontalWrap;
    WrapMode m_verticalWrap;
    QSize m_margins;
    QSize m_textureSize;
    QSize m_size;

    QSGTextureRef m_texture;
    QGLFramebufferObject *m_fbo;
    QGLFramebufferObject *m_multisampledFbo;
    Renderer *m_renderer;
    QSGContext *m_context;
    int m_refs;
    uint m_dirtyTexture : 1; // Causes update no matter what.
    uint m_dirtySceneGraph : 1; // Causes update if not static.
    uint m_multisamplingSupported : 1;
    uint m_checkedForMultisamplingSupport : 1;
    uint m_live : 1;
    uint m_hideOriginal : 1;
};

class ShaderEffectNode : public GeometryNode
{
public:
    ShaderEffectNode();

    void setRect(const QRectF &rect);
    QRectF rect() const;

    void setResolution(const QSize &res);
    QSize resolution() const;

    virtual void preprocess();
    virtual void update(uint updateFlags);

private:
    enum UpdateFlag
    {
        UpdateGeometry = 0x01
    };

    void updateGeometry();

    QSize m_meshResolution;
};

class ShaderEffectItem : public QxItem, public AbstractEffect
{
    Q_OBJECT
    Q_PROPERTY(QString fragmentShader READ fragmentShader WRITE setFragmentShader NOTIFY fragmentShaderChanged)
    Q_PROPERTY(QString vertexShader READ vertexShader WRITE setVertexShader NOTIFY vertexShaderChanged)
    Q_PROPERTY(bool blending READ blending WRITE setBlending NOTIFY blendingChanged)
    Q_PROPERTY(bool active READ active WRITE setActive NOTIFY activeChanged)
    Q_PROPERTY(QSize meshResolution READ meshResolution WRITE setMeshResolution NOTIFY meshResolutionChanged)

public:
    ShaderEffectItem(QxItem *parent = 0);
    ~ShaderEffectItem();

    virtual void componentComplete();

    virtual AbstractEffectType *type() const;
    virtual AbstractEffectProgram *createProgram() const;

    QString fragmentShader() const { return m_fragment_code; }
    void setFragmentShader(const QString &code);
    
    QString vertexShader() const { return m_vertex_code; }
    void setVertexShader(const QString &code);

    bool blending() const { return flags() == Blending; }
    void setBlending(bool enable);

    bool active() const { return m_active; }
    void setActive(bool enable);

    QSize meshResolution() const { return m_node.resolution(); }
    void setMeshResolution(const QSize &size);

    void preprocess();

Q_SIGNALS:
    void fragmentShaderChanged();
    void vertexShaderChanged();
    void blendingChanged();
    void activeChanged();
    void marginsChanged();
    void meshResolutionChanged();

protected:
    virtual void geometryChanged(const QRectF &newGeometry,
                                 const QRectF &oldGeometry);

private Q_SLOTS:
    void changeSource(int index);
    void markDirty();

private:
    friend class CustomShaderMaterialData;

    void setSource(QVariant var, int index);
    void disconnectPropertySignals();
    void connectPropertySignals();
    void reset();
    void updateProperties();
    void updateShaderProgram();
    void lookThroughShaderCode(const QString &code);

    ShaderEffectNode m_node;
    AbstractEffectType m_type;
    QGLShaderProgram m_program;
    QString m_fragment_code;
    QString m_vertex_code;
    QVector<QGL::VertexAttribute> m_attributes;
    QVector<QByteArray> m_attributeNames;
    QSet<QByteArray> m_uniformNames;
    QSize m_mesh_resolution;

    struct SourceData
    {
        QSignalMapper *mapper;
        QPointer<ShaderEffectSource> source;
        QByteArray name;
        bool ownedByEffect;
    };

    QVector<SourceData> m_sources;

    uint m_program_dirty : 1;
    uint m_active : 1;

    uint m_respects_matrix : 1;
    uint m_respects_opacity : 1;
};

#endif // SHADEREFFECTITEM_H
