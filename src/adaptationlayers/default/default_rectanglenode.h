/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/


#ifndef DEFAULT_RECTANGLENODE_H
#define DEFAULT_RECTANGLENODE_H

#include "adaptationlayer.h"

#include "flatcolormaterial.h"
#include "utilities.h"

class AbstractEffect;
class QSGContext;

class DefaultRectangleNode : public RectangleNodeInterface
{
public:
    enum MaterialPreference
    {
        PreferTextureMaterial,
        PreferVertexColorMaterial
    };
    enum UpdateFlags
    {
        UpdateGeometry = 0x01,
        UpdateGradientTexture = 0x02
    };

    DefaultRectangleNode(MaterialPreference preference, QSGContext *context);
    ~DefaultRectangleNode();

    virtual void setRect(const QRectF &rect);
    virtual void setColor(const QColor &color);
    virtual void setPenColor(const QColor &color);
    virtual void setPenWidth(int width);
    virtual void setOpacity(qreal opacity);
    virtual void setGradientStops(const QGradientStops &stops);
    virtual void setRadius(qreal radius);
    virtual void update(uint updateFlags);
private:
    QRectF calculateBoundingRect();
    void updateGeometry();
    void updateGradientTexture();

    MaterialPreference m_material_preference;
    GeometryNode m_border;
    AbstractEffect *m_fill_material; // Can be FlatColorMaterial, VertexColorMaterial, TextureMaterial or TextureMaterialWithOpacity.
    FlatColorMaterial m_border_material;
    QSGTextureRef m_gradient_texture;
    bool m_gradient_is_opaque;

    QSGContext *m_context;
};

#endif
