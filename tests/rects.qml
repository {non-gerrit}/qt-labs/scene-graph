/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0

Rectangle {
    width: 800
    height: 400

    property Gradient simpleGradient: Gradient { GradientStop { position: 0.1; color: "orange" } }
    property Gradient complexGradient: Gradient {
        GradientStop { position: 0.0; color: "red" }
        GradientStop { position: 0.1; color: "orange" }
        GradientStop { position: 0.3; color: "yellow" }
        GradientStop { position: 0.5; color: "green" }
        GradientStop { position: 0.7; color: "cyan" }
        GradientStop { position: 0.9; color: "blue" }
        GradientStop { position: 1.0; color: "magenta" }
    }

    Rectangle {
        clip: true
        x: 20; y: 20
        width: parent.width / 4 - 40; height: width
        color: "red"
        opacity: 0.5
        radius: 40
        Rectangle { x: -20; y: -20; width: 200; height: 200; color: "black" }
    }
    Rectangle {
        clip: true
        x: parent.width * 0.25 + 20; y: 20
        width: parent.width / 4 - 40; height: width
        color: "blue"
        opacity: 0.5
        radius: 40
        border.color: "cyan"
        border.width: 25
        Rectangle { x: -20; y: -20; width: 200; height: 200; color: "black" }
    }
    Rectangle {
        clip: true
        x: parent.width * 0.5 + 20; y: 20
        width: parent.width / 4 - 40; height: width
        color: "red"
        opacity: 0.5
        radius: 40
        gradient: simpleGradient
        Rectangle { x: -20; y: -20; width: 200; height: 200; color: "black" }
    }
    Rectangle {
        clip: true
        x: parent.width * 0.75 + 20; y: 20
        width: parent.width / 4 - 40; height: width
        color: "red"
        opacity: 0.5
        radius: 40
        border.color: "black"
        border.width: 25
        gradient: complexGradient
        SequentialAnimation on border.width {
            loops: Animation.Infinite
            NumberAnimation { to: 0; easing.type: Easing.Linear; duration: 1000 }
            PauseAnimation { duration: 1000 }
            NumberAnimation { to: 25; easing.type: Easing.Linear; duration: 1000 }
            PauseAnimation { duration: 1000 }
        }
        Rectangle { x: -20; y: -20; width: 200; height: 200; color: "black" }
    }
    
    
    Rectangle {
        clip: true
        x: 20; y: parent.height / 2 + 20
        width: parent.width / 4 - 40; height: width
        color: "red"
        opacity: 0.5
        Rectangle { x: -20; y: -20; width: 200; height: 200; color: "black" }
    }
    Rectangle {
        clip: true
        x: parent.width * 0.25 + 20; y: parent.height / 2 + 20
        width: parent.width / 4 - 40; height: width
        color: "blue"
        opacity: 0.5
        border.color: "cyan"
        border.width: 25
        Rectangle { x: -20; y: -20; width: 200; height: 200; color: "black" }
    }
    Rectangle {
        clip: true
        x: parent.width * 0.5 + 20; y: parent.height / 2 + 20
        width: parent.width / 4 - 40; height: width
        color: "red"
        opacity: 0.5
        gradient: simpleGradient
        Rectangle { x: -20; y: -20; width: 200; height: 200; color: "black" }
    }
    Rectangle {
        clip: true
        x: parent.width * 0.75 + 20; y: parent.height / 2 + 20
        width: parent.width / 4 - 40; height: width
        color: "red"
        opacity: 0.5
        border.color: "black"
        border.width: 25
        gradient: complexGradient
        SequentialAnimation on border.width {
            loops: Animation.Infinite
            NumberAnimation { to: 0; easing.type: Easing.Linear; duration: 1000 }
            PauseAnimation { duration: 1000 }
            NumberAnimation { to: 25; easing.type: Easing.Linear; duration: 1000 }
            PauseAnimation { duration: 1000 }
        }
        Rectangle { x: -20; y: -20; width: 200; height: 200; color: "black" }
    }
}
