/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0

Rectangle {
    width: 640
    height: 640

    Text {
        id: regularText
        text: "Regular text (Arial, 36 pt)"
        font.family: "Arial"
        font.pointSize: 36
    }

    Text {
        id: strikeOutText
        anchors.top: regularText.bottom
        text: "Struck out, right-aligned"
        font.family: "Courier"
        font.pointSize: 22
        font.strikeout: true
        font.overline: true
        font.underline: true
        width: parent.width;
        horizontalAlignment: Text.AlignRight
    }

    Text {
        id: boldText
        anchors.top: strikeOutText.bottom
        text: "Bold, centered"
        font.pointSize: 22
        font.bold: true
        width: parent.width;
        horizontalAlignment: Text.AlignHCenter
    }

    Text {
        id: nonOpaqueText
        anchors.top:  strikeOutText.bottom
        text: "Text which is not completely opaaaaaaque"
        font.pointSize: 22
        width: parent.width
        color: "red"
        opacity: 0.2
    }

    Text {
        id: italicText
        anchors.top: boldText.bottom
        text: "Italic (Arial 32 pt)"
        font.family: "Arial"
        font.pointSize: 32
        font.italic: true
    }

    Text {
        id: arabicText
        anchors.top: italicText.bottom
        text: "كافة قطاعات"
        font.pointSize: 32
        font.italic: true
    }

    Text {
        id: bidiText
        anchors.top: arabicText.bottom
        text: "Conference)، كافة قطاعات"
        font.pointSize: 32
        style: Text.Outline
        styleColor: "green"
        // font.strikeout: true
        font.italic: true
    }

    Text {
        id: redText
        anchors.top: bidiText.bottom
        text: "Red text"
        font.pointSize: 25
        color: "red"
    }

    Text {
        id: greenText
        anchors.top: redText.bottom
        text: "Green text"
        font.underline: true
        font.pointSize: 25
        color: "green"
    }

    Text {
        id: raisedText
        anchors.top: greenText.bottom
        text: "Raised text"
        font.pointSize: 24
        font.underline: true
        style: Text.Raised
        styleColor: "red"
    }

    Text {
        x: 5;
        id: outlinedText
        anchors.top: raisedText.bottom
        text: "Outlined text"
        font.pointSize: 24
        font.strikeout: true
        style: Text.Outline
        styleColor: "red"
    }

    Text {
        id: sunkenText
        anchors.top: outlinedText.bottom
        text: "Sunken text"
        font.pointSize: 24
        font.overline: true
        style: Text.Sunken        
        styleColor: "red"
    }

    Text {
        id: richText
        anchors.top: sunkenText.bottom
        text: "<p><b><u>Bold</u><img src=\"http://codepaster-nokia.troll.no/paster_resources/newlogo.png\" /></b><font color=\"red\">Red</font></p><p>Some other paragraph</p><p><ul><li>Abc</li><li>Def</li></ul><table border=\"1\"><tr><td>A</td><td>B</td></tr><tr><td>C</td><td>D</td></tr></table></p>"
        font.strikeout: true
        style: Text.Outline
        styleColor: "green"
        font.pointSize: 10
    }

}
