/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0

Rectangle {
    width: 360
    height: 540

    gradient: Gradient {
        GradientStop { position: 0; color: "steelblue" }
        GradientStop { position: 1; color: "black" }
    }

    Item {
        id: theItem
        anchors.fill: parent
        Rectangle {
            x: 0;
            y: 0;
            width: 100;
            height: parent.height;
            color: "#ffffff";

            NumberAnimation on width { from: 0; to: effect.width; loops: Animation.Infinite; duration: 1000 }
        }

        Rectangle {
            x: 100;
            y: 0;
            width: 100;
            height: 100;
            color: "red";

            NumberAnimation on y { from: 0; to: effect.height; loops: Animation.Infinite; duration: 2000 }
        }
    }

    ShaderEffectSource {
        id: theSource
        sourceItem: theItem
        //sourceImage: "face-smile.png"
        filtering: ShaderEffectSource.Linear
        horizontalWrap: ShaderEffectSource.ClampToEdge
        verticalWrap: ShaderEffectSource.ClampToEdge
    }

    ShaderEffectItem {
        id: effect
        anchors.fill: parent

        fragmentShader: "varying highp vec2 qt_TexCoord0;                                   \n" +
                        "uniform sampler2D source;                                          \n" +
                        "uniform highp float qt_Opacity;                                    \n" +
                        "void main() {                                                      \n" +
                        "    gl_FragColor = texture2D(source, qt_TexCoord0) * qt_Opacity;   \n" +
                        "}"

        //property url source: "face-smile.png"
        //property variant source: theItem
        property variant source: theSource
        active: opacity < 0.99
        blending: true

        SequentialAnimation on opacity {
            NumberAnimation { to: 0; duration: 1000 }
            NumberAnimation { to: 1; duration: 1000 }
            loops: Animation.Infinite
        }
    }
}
