/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0

Rectangle {
    id: main
    width: 800
    height: 600
    focus: true

    Item {
        anchors.centerIn: parent
        id: text
        width: 1024
        height: 1024
        Text {
            anchors.centerIn: parent
            text: "a"
            font.pixelSize: 1024
        }
    }

    Text {
        id: reference
        text: "a"
        font.pixelSize: 64
        x: (main.width / 4) * 3 - width / 2; y: main.height / 2 - height / 2 - scale * 4
        smooth: true
    }

    ShaderEffectSource {
        id: theDistTransSource
        sourceItem: text
        filtering: ShaderEffectSource.Nearest
        horizontalWrap: ShaderEffectSource.ClampToEdge
        verticalWrap: ShaderEffectSource.ClampToEdge
    }

    // Shader generating the distance transform of the glyph
    ShaderEffectItem {
        id: distanceTransform
        property variant source: theDistTransSource
        property real psize: 1 / text.width
        property real spread: 30
        anchors.centerIn: text
        width: 64
        height: width
        smooth: true

        fragmentShader: "varying highp vec2 qt_TexCoord0;                                                          \n" +
                        "uniform sampler2D source;                                                                \n" +
                        "uniform highp float psize;                                                               \n" +
                        "uniform lowp float spread;                                                               \n" +
                        "void main() {                                                                            \n" +
                        "    highp vec4 p = texture2D(source, qt_TexCoord0);                                       \n" +
                        "    lowp float sp = step(0.5, p.a);                                                      \n" +
                        "    mediump float dist = spread * psize;                                                 \n" +
                        "    for (mediump float sy = clamp(qt_TexCoord0.y - spread * psize, 0.0, 1.0); sy < clamp(qt_TexCoord0.y + spread * psize, 0.0, 1.0); sy += psize) { \n" +
                        "        for (mediump float sx = clamp(qt_TexCoord0.x - spread * psize, 0.0, 1.0); sx < clamp(qt_TexCoord0.x + spread * psize, 0.0, 1.0); sx += psize) { \n" +
                        "            lowp float t = step(0.5, texture2D(source, vec2(sx, sy)).a);               \n" +
                        "            dist = mix(dist, min(dist, length(vec2(sx - qt_TexCoord0.x,sy - qt_TexCoord0.y))), abs(sp - t));  \n" +
                        "        }                                                                                \n" +
                        "    }                                                                                    \n" +
                        "    mediump float a = ((dist - psize) * (0.0 - 0.50)) / (spread * psize - psize) + 0.50; \n" +
                        "    mediump float b = ((dist - psize) * (1.0 - 0.51)) / (spread * psize - psize) + 0.51; \n" +
                        "    mediump float alpha = mix(a, b, sp);                                                 \n" +
                        "    gl_FragColor = vec4(0.0, 0.0, 0.0, alpha);                                           \n" +
                        "}"
    }

    ShaderEffectSource {
        id: theAlphaTestSource
        sourceItem: distanceTransform
        filtering: ShaderEffectSource.Linear
        horizontalWrap: ShaderEffectSource.ClampToEdge
        verticalWrap: ShaderEffectSource.ClampToEdge
    }

    // Shader doing the alpha test on the distance transform
    ShaderEffectItem {
        id: alphaTest
        property variant source: theAlphaTestSource
        property real eps: 0.2
        property real scalep: alphaTest.scale
        width: distanceTransform.width; height: distanceTransform.height
        x: main.width / 4 - width / 2; y: main.height / 2 - height / 2 - scale * 4
        smooth: true

        fragmentShader: "varying highp vec2 qt_TexCoord0;                                         \n" +
                        "uniform sampler2D source;                                               \n" +
                        "uniform lowp float scalep;                                              \n" +
                        "uniform lowp float eps;                                                 \n" +
                        "void main() {                                                           \n" +
                        "    gl_FragColor = vec4(0.0, 0.0, 0.0,                                  \n" +
                        "                        smoothstep(clamp(0.5 - eps / scalep, 0.0, 0.5), \n" +
                        "                                   clamp(0.5 + eps / scalep, 0.5, 1.0), \n" +
                        "                                   texture2D(source, qt_TexCoord0).a));  \n" +
                        "}"
    }

    Rectangle {
        id: plus
        width: 40; height: 40
        color: "lightgray"
        radius: width / 2
        border.color: "black"
        border.width: 3
        anchors.right: parent.right; anchors.rightMargin: 30
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: -40
        Text {
            text: "+"
            font.family: "Arial"
            font.pointSize: 38
            anchors.centerIn: parent
            anchors.verticalCenterOffset: 1.5
            anchors.horizontalCenterOffset: 0.5
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                alphaTest.scale *= 1.5;
                reference.scale *= 1.5
            }
        }
    }

    Rectangle {
        width: 40; height: 40
        color: "lightgray"
        radius: width / 2
        border.color: "black"
        border.width: 3
        anchors.horizontalCenter: plus.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.verticalCenterOffset: 40
        Text {
            text: "-"
            font.family: "Arial"
            font.pointSize: 42
            anchors.centerIn: parent
            anchors.verticalCenterOffset: -4
            anchors.horizontalCenterOffset: 1
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                alphaTest.scale /= 1.5;
                reference.scale /= 1.5
            }
        }
    }
}
