import QtQuick 2.0

Item {
    width: 400
    height: 300

    ShaderEffectItem {
        anchors.fill: parent;
        fragmentShader: "
        varying highp vec2 qt_TexCoord0;
        void main() {
            gl_FragColor = vec4(qt_TexCoord0.x, qt_TexCoord0.y, 1, 1);
        }
        "
    }
}
