import QtQuick 2.0

Item {
    width: 360
    height: 640

    Flickable {
        id: flick
        anchors.fill: parent

    contentWidth: 4000
    contentHeight: 3000

    Image {
        source: "img.png"
        width: flick.contentWidth
        height: flick.contentHeight
        }
    }
}
