import QtQuick 2.0

Rectangle {
    width: 640
    height: 480
    id: main

    Text {
        id: regularText
        text: "Regular text (Arial, 1 pt)"
        font.family: "Arial"
        font.pointSize: 1

    }

   Text {
        id: regularText2
        anchors.top: regularText.bottom
        text: "Regular text (Arial, 2 pt)"
        font.pointSize: 2

        font.family: "Arial"
    }
   Text {
        id: regularText3
        anchors.top: regularText2.bottom
        text: "Regular text (Arial, 3 pt)"
        font.pointSize: 3
        font.family: "Arial"

    }
   Text {
        id: regularText4
        anchors.top: regularText3.bottom
        text: "Regular text (Arial, 4 pt)"
        font.pointSize: 4
        font.family: "Arial"

    }

   Text {
        id: regularText5
        anchors.top: regularText4.bottom
        text: "Regular text (Arial, 5 pt)"
        font.pointSize: 5
        font.family: "Arial"

    }

   Text {
        id: regularText6
        anchors.top: regularText5.bottom
        text: "Regular text (Arial, 6 pt)"
        font.pointSize: 6
        font.family: "Arial"

    }

   Text {
        id: regularText7
        anchors.top: regularText6.bottom
        text: "Regular text (Arial, 7 pt)"
        font.pointSize: 7
        font.family: "Arial"

    }

   Text {
        id: regularText8
        anchors.top: regularText7.bottom
        text: "Regular text (Arial, 8 pt)"
        font.pointSize: 8
        font.family: "Arial"

    }

   Text {
        id: regularText9
        anchors.top: regularText8.bottom
        text: "Regular text (Arial, 9 pt)"
        font.pointSize: 9
        font.family: "Arial"

    }

   Text {
        id: regularText10
        anchors.top: regularText9.bottom
        text: "Regular text (Arial, 10 pt)"
        font.pointSize: 10
        font.family: "Arial"

    }

   Text {
        id: regularText11
        anchors.top: regularText10.bottom
        text: "Regular text (Arial, 11 pt)"
        font.pointSize: 11
        font.family: "Arial"

    }

   Text {
        id: regularText12
        anchors.top: regularText11.bottom
        text: "Regular text (Arial, 12 pt)"
        font.pointSize: 12
        font.family: "Arial"

    }

   Text {
        id: regularText13
        anchors.top: regularText12.bottom
        text: "Regular text (Arial, 13 pt)"
        font.pointSize: 13
        font.family: "Arial"

    }

   Text {
        id: regularText14
        anchors.top: regularText13.bottom
        text: "Regular text (Arial, 14 pt)"
        font.pointSize: 14
        font.family: "Arial"

    }

   Text {
        id: regularText15
        anchors.top: regularText14.bottom
        text: "Regular text (Arial, 15 pt)"
        font.pointSize: 15
        font.family: "Arial"

    }
   Text {
        id: regularText16
        anchors.top: regularText15.bottom
        text: "Regular text (Arial, 16 pt)"
        font.pointSize: 16
        font.family: "Arial"

    }
   Text {
        id: regularText16_and_a_half
        anchors.top: regularText16.bottom
        text: "Regular text (Arial, 17 pt)"
        font.pointSize: 17
        font.family: "Arial"

    }

   Text {
        id: regularText17
        anchors.top: regularText16_and_a_half.bottom
        text: "Regular text (Times New Roman, 1 pt)"
        font.pointSize: 1
        font.family: "Times New Roman"

    }
   Text {
        id: regularText18
        anchors.top: regularText17.bottom
        text: "Regular text (Times New Roman, 2 pt)"
        font.pointSize: 2
        font.family: "Times New Roman"

    }
   Text {
        id: regularText19
        anchors.top: regularText18.bottom
        text: "Regular text (Times New Roman, 3 pt)"
        font.pointSize: 3
        font.family: "Times New Roman"

    }
   Text {
        id: regularText20
        anchors.top: regularText19.bottom
        text: "Regular text (Times New Roman, 4 pt)"
        font.pointSize: 4
        font.family: "Times New Roman"

    }

   Text {
        id: regularText21
        anchors.top: regularText20.bottom
        text: "Regular text (Times New Roman, 5 pt)"
        font.pointSize: 5
        font.family: "Times New Roman"

    }

   Text {
        id: regularText22
        anchors.top: regularText21.bottom
        text: "Regular text (Times New Roman, 6 pt)"
        font.pointSize: 6
        font.family: "Times New Roman"

    }
   Text {
        id: regularText23
        anchors.top: regularText22.bottom
        text: "Regular text (Times New Roman, 7 pt)"
        font.pointSize: 7
        font.family: "Times New Roman"

    }
   Text {
        id: regularText24
        anchors.top: regularText23.bottom
        text: "Regular text (Times New Roman, 8 pt)"
        font.pointSize: 8
        font.family: "Times New Roman"

    }
   Text {
        id: regularText25
        anchors.top: regularText24.bottom
        text: "Regular text (Times New Roman, 9 pt)"
        font.pointSize: 9
        font.family: "Times New Roman"

    }
   Text {
        id: regularText26
        anchors.top: regularText25.bottom
        text: "Regular text (Times New Roman, 10 pt)"
        font.pointSize: 10
        font.family: "Times New Roman"

    }
   Text {
        id: regularText27
        anchors.top: regularText26.bottom
        text: "Regular text (Times New Roman, 11 pt)"
        font.pointSize: 11
        font.family: "Times New Roman"

    }
   Text {
        id: regularText28
        anchors.top: regularText27.bottom
        text: "Regular text (Times New Roman, 12 pt)"
        font.pointSize: 12
        font.family: "Times New Roman"

    }
   Text {
        id: regularText29
        anchors.top: regularText28.bottom
        text: "Regular text (Times New Roman, 13 pt)"
        font.pointSize: 13
        font.family: "Times New Roman"

    }

   Text {
        id: regularText30
        anchors.top: regularText29.bottom
        text: "Regular text (Times New Roman, 14 pt)"
        font.pointSize: 14
        font.family: "Times New Roman"

    }
   Text {
        id: regularText31
        anchors.top: regularText30.bottom
        text: "Regular text (Times New Roman, 15 pt)"
        font.pointSize: 15
        font.family: "Times New Roman"

    }

   Text {
        id: regularText32
        anchors.top: regularText31.bottom
        text: "Regular text (Times New Roman, 16 pt)"
        font.pointSize: 16
        font.family: "Times New Roman"

    }

   Text {
        id: regularText33
        anchors.top: regularText32.bottom
        text: "Regular text (Times New Roman, 17 pt)"
        font.pointSize: 17
        font.family: "Times New Roman"

    }

   SequentialAnimation on y {
           loops: Animation.Infinite
           NumberAnimation { to: 200 - main.height; easing.type: Easing.OutBounce; duration: 2000 }
           NumberAnimation { to: 0; easing.type: Easing.OutQuad; duration: 1000 }
       }

}
