import QtQuick 2.0

Rectangle {
    width: 800
    height: 480
    color: "white"

    Flickable {
        anchors.fill: parent
        contentWidth: 1000
        contentHeight: 700

        MouseArea {
            width: 100; height: 100
            onClicked: print("Hello, you've clicked on a little MouseArea on a Flickable!");
        }

        Item {
            width: 100; height: 100

            Rectangle {
                color: "green"
                anchors.fill: parent
            }
            NumberAnimation on x { from: 0; to: 700; duration: 500; loops: -1 }
        }

        Rectangle {
            radius: 15
            y: 100
            width: 100
            height: 100
            color: "blue"
            border.width:2 
            NumberAnimation on x { from: 0; to: 700; duration: 3000; loops: -1 }
        }

        Image {
            y: 300
            width: 100
            height: 50
            fillMode: Image.Tile
            source: "face-smile.png"
            NumberAnimation on width { from: 100; to: 800; duration: 4000; loops: -1 }
            NumberAnimation on height { from: 50; to: 200; duration: 4000; loops: -1 }
                NumberAnimation on opacity { from: 0; to: 1; duration: 500; loops: -1 }
        }


        Repeater {
            model: 10
            Rectangle {
                y: 500
                x: index * 110
                color: "blue"
                width: 100; height: 90
                border.width: 2
                border.color: "pink"
                radius: 5
            }
        }

        Item {
            width: 150;
            height: 100
            clip: true
            Row {
                NumberAnimation on opacity { from: 0; to: 1; duration: 500; loops: -1 }
                y: 200
                width: 800
                Rectangle {
                    NumberAnimation on scale { from: 1; to: 2; duration: 2000; loops: -1 }
                    NumberAnimation on rotation { from: 0; to: 10; duration: 2000; loops: -1 }
                    width: 100; height: 100;
                    color: "pink"
                    radius: 2
                    border.color: "black"
                    border.width: 1
                }
                Rectangle {
                    width: 100; height: 100;
                    color: "orange"
                }
                Rectangle {
                    width: 100; height: 100;
                    color: "purple"
                }
            }
        }
    }

    Text {
        id: mytext
        text: "QML SceneGraph Test - How do you like dem apples?"
        NumberAnimation on width { from: 0; to: 700; duration: 1000; loops: -1 }
    }
}
