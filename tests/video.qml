import QtQuick 2.0
import Qt.labs.PhononVideo 1.0
Rectangle {
    id: toplevel
    width: 1280
    height: 720
    color: "white"

    function millisToFormattedTime(millis) {
        var str = ""
        var s = Math.floor(millis / 1000)
        var m = Math.floor(s / 60)
        var h = Math.floor(m / 60)
        m %= 60
        s %= 60
        if (h < 10)
            str += "0"
        str += h
        str += ":"
        if (m < 10)
            str += "0"
        str += m
        str += ":"
        if (s < 10)
            str += "0"
        str += s
        return str
    }

    Rectangle {
        x: parent.width - 70
        y: parent.height - 70
        width: 50
        height: 50
        color: "blue"
        NumberAnimation on rotation {
            loops: Animation.Infinite
            from: 360
            to: 0
            duration: 1000
        }
    }

    Rectangle {
        id: window
        width: 640 + 10
        height: 360 + 35
        gradient: Gradient {
            GradientStop { position: 0; color: "lightblue" }
            GradientStop { position: 1; color: "white" }
        }
        border.width: 2
        border.color: "black"
        radius: 5

        MouseArea {
            anchors.fill: window
            property bool resizeLeft: false
            property bool resizeRight: false
            property bool resizeTop: false
            property bool resizeBottom: false
            onPressed: {
                resizeLeft = (mouse.x < 5)
                resizeRight = (mouse.x > window.width - 5)
                resizeTop = (mouse.y < 5)
                resizeBottom = (mouse.y > window.height - 5)
                mouse.accepted = true
            }

            onPositionChanged: {
                if (resizeLeft) {
                    var delta = Math.min(mouse.x - 2, window.width - 100)
                    window.width -= delta
                    window.x += delta
                }
                if (resizeRight) {
                    window.width = Math.max(mouse.x + 2, 100)
                }
                if (resizeTop) {
                    var delta = Math.min(mouse.y - 2, window.height - 100)
                    window.height -= delta
                    window.y += delta
                }
                if (resizeBottom) {
                    window.height = Math.max(mouse.y + 2, 100)
                }
            }
        }

        MouseArea {
            id: titlebar
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.margins: 5
            height: 20
            drag.target: window
            drag.axis: Drag.XandYAxis
            drag.maximumX: toplevel.width - window.width
            drag.maximumY: toplevel.height - window.height
        }

        Item {
            anchors.fill: titlebar
            anchors.margins: 3
            clip: true
            Text {
                anchors.centerIn: parent
                text: player.source + " (" + millisToFormattedTime(player.time) + "/" + millisToFormattedTime(player.length) + ")"
                font.family: "Arial"
                font.pixelSize: titlebar.height * 0.7
            }
        }

        PhononVideo {
            id: player
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.top: titlebar.bottom
            anchors.margins: 5
            playing: true
            sourceWidth: nativeWidth
            sourceHeight: nativeHeight
            fillMode: PhononVideo.PreserveAspectFit

            // Windows 7 Sample video.
            source: "C:/Users/Public/Videos/Sample Videos/Wildlife.wmv"
            smooth: true

            property real t: 0
            property real w: Math.max(0, parent.width - width)
            property real h: Math.max(0, parent.height - height)

            NumberAnimation on t {
                loops: Animation.Infinite
                from: 0
                to: 10000
                duration: 300000
            }

            onStateChanged: {
                if (time >= length) {
                    time = 0
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    player.playing = !player.playing
                }
            }

            Rectangle {
                id: progressbar
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins: 20
                height: 10
                opacity: 0.5
                color: "black"
                border.width: 2
                border.color: "white"
                radius: 5
            }

            Rectangle {
                anchors.top: progressbar.top
                anchors.bottom: progressbar.bottom
                width: 20
                color: "white"
                radius: 5
                x: progressbar.x + (player.length == 0 ? 0 : (progressbar.width - width) * player.time / player.length)
                MouseArea {
                    anchors.fill: parent
                    drag.target: parent
                    drag.axis: Drag.XAxis
                    drag.minimumX: progressbar.x
                    drag.maximumX: progressbar.x + progressbar.width - parent.width
                    property bool wasPlaying: false
                    onPositionChanged: {
                        // Only called when dragging.
                        player.time = player.length * (parent.x - progressbar.x) / (progressbar.width - parent.width)
                    }
                    onPressed: {
                        wasPlaying = player.playing
                        player.playing = false
                        mouse.accepted = true
                    }
                    onReleased: {
                        player.playing = wasPlaying
                    }
                }
            }
        }
    }

    Rectangle {
        x: 20
        y: 20
        width: 50
        height: 50
        color: "red"
        NumberAnimation on rotation {
            loops: Animation.Infinite
            from: 0
            to: 360
            duration: 1000
        }
    }
}
