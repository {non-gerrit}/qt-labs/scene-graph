import QtQuick 2.0

Item
{
    width: 800
    height: 600

    Rectangle {
        width: 100
        height: 100
        radius: 50
        color: "black"

        property real t;

        x: parent.width / 2 + parent.width / 3 * Math.sin(t * Math.PI * 2) - width / 2.;
        y: parent.height / 2 + parent.height / 3 * Math.cos(t * Math.PI * 2) - height / 2.;

    NumberAnimation on t { from: 0; to: 1; duration: 5000; loops: Animation.Infinite }
    }
}
