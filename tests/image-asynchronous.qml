import QtQuick 2.0

Rectangle {
    width: 800
    height: 600

    color: "red"

    Image {
        id: image
        source: state == 1 || state == 3 ? "/Users/gunnar/Pictures/big.jpg" : ""
        width: 300
        height: 200
        asynchronous: true

        opacity: source == "" ? 0 : 1

        property int state: 0

        Behavior on opacity {
            NumberAnimation { duration: 500 }
        }


    }

    Rectangle {
        anchors.fill:  text
        anchors.margins: -10
        color: "black"
        radius: 5
    }

    Text {
        id: text
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 20
        color: "white"
        text: image.asynchronous ? "Loading in a thread" : "Not loading in a thread"
    }


    Rectangle {

        anchors.centerIn: parent

        width: 200
        height: 200

        border.width:  4
        border.color:  "black"
        color: "white"

        NumberAnimation on rotation {
            from: 0
            to: 360
            duration: 2500
            loops: Animation.Infinite
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            image.state = (image.state + 1) % 4;
        }
    }



}
