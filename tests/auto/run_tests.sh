#!/bin/sh

function execute_test()
{
    cd $1
    qmake
    make
    ./tst_$1
    cd ..
}

for val in *
do
    if [ -d "$val" ]
    then
        execute_test $val
    fi
done
