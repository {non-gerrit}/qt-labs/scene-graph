#-------------------------------------------------
#
# Project created by QtCreator 2010-12-16T06:09:44
#
#-------------------------------------------------

QT       += opengl testlib

TARGET = tst_texturemanager
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += tst_texturemanagertest.cpp

include(../../../src/scenegraph_include.pri)
