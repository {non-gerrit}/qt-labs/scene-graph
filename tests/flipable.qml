import QtQuick 1.0

Item {
    width: 600
    height: 400

    Flipable {
        width: 100
        height: 100
        front: Rectangle {
            anchors.fill: parent
            color: "red";
            Text { anchors.centerIn: parent; text: "static front" }
        }

        back: Rectangle {
            anchors.fill:  parent
            color: "blue"
            Text { anchors.centerIn: parent; text: "static back" }
        }
    }

    Flipable {
        x: 100
        width: 100
        height: 100
        front: Rectangle {
            anchors.fill: parent
            color: "red";
            Text { anchors.centerIn: parent; text: "animated front" }
        }

        back: Rectangle {
            anchors.fill:  parent
            color: "blue"
            Text { anchors.centerIn: parent; text: "animated back" }
        }

        transform: Rotation {
            origin.x: 50
            origin.y: 50
            axis: Qt.vector3d(0, 1, 0);
            NumberAnimation on angle {
                from: 0
                to: 360
                duration: 1000 + Math.random() * 2000;
                loops: Animation.Infinite
            }
        }
    }

    Flipable {
        y: 100
        width: 100
        height: 100
        front: Rectangle {
            clip: true
            anchors.fill: parent
            color: "red";
            Text { anchors.centerIn: parent; text: "clipped front" }
        }

        back: Rectangle {
            clip: true
            anchors.fill:  parent
            color: "blue"
            Text { anchors.centerIn: parent; text: "clipped back" }
        }
    }

    Flipable {
        x: 100
        y: 100
        width: 100
        height: 100
        front: Rectangle {
            clip: true
            anchors.fill: parent
            color: "red";
            Text {
                width: parent.width
                anchors.centerIn: parent;
                text: "clipped and animated front";
                wrapMode: Text.Wrap;
            }
        }

        back: Rectangle {
            clip: true
            anchors.fill:  parent
            color: "blue"
            Text {
                width: parent.width
                anchors.centerIn: parent;
                text: "clipped and animated back";
                wrapMode: Text.Wrap;
            }
        }

        transform: Rotation {
            origin.x: 50
            origin.y: 50
            axis: Qt.vector3d(0, 1, 0);
            NumberAnimation on angle {
                from: 0
                to: 360
                duration: 1000 + Math.random() * 2000;
                loops: Animation.Infinite
            }
        }

    }

}
