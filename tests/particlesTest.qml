/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the Qt scene graph research project.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the Technology Preview License Agreement accompanying
** this package.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
**
**
**
**
**
**
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0
import Qt.particles 1.0

Rectangle {
    width: 800
    height: 600
    color: "black"

    //control
    ParticleField {
        painters: ParticlePainter { source: "star.png" }
        emitter: ParticleEmitter {
            id: control
            y: 50;

            lifeSpan: 10000
            count: 500
            velocity: 65
            emissionRate: 5
            ParticleMotionLinear {}
        }
    }

    //random drift
    ParticleField {
        painters: ParticlePainter { source: "star.png" }
        emitter: ParticleEmitter {
        id: drift
        y: 100;

        lifeSpan: 10000
        count: 500
        velocity: 65
        emissionRate: 5
        ParticleMotionRandomDrift {
            yDrift: 50
        }
        ParticleMotionLinear {}
    }
    }
    Component.onCompleted: drift.fastForward(5000)

    //acceleration
    ParticleField {
        painters: ParticlePainter { source: "star.png" }
        emitter: ParticleEmitter {
        y: 150;

        lifeSpan: 10000
        count: 500
        velocity: 65
        emissionRate: 5
        ParticleMotionAcceleration {
            xAccel: 25
        }
        ParticleMotionLinear {}
    }
    }

    //kill zone
    ParticleField {
        painters: ParticlePainter { source: "star.png" }
        emitter: ParticleEmitter {
        y: 200;

        lifeSpan: 10000
        count: 500
        velocity: 65
        emissionRate: 5
        ParticleMotionLinear {}
        ParticleKillZone { area: "100,180,40x40" }
    }
    }

    //friction
    ParticleField {
        painters: ParticlePainter { source: "star.png" }
        emitter: ParticleEmitter {
        y: 250;

        lifeSpan: 10000
        count: 500
        velocity: 65
        emissionRate: 5
        ParticleMotionFriction {
            friction: 10
        }
        ParticleMotionLinear {}
    }
    }

    //gravity
    ParticleField {
        painters: ParticlePainter { source: "star.png" }
        emitter: ParticleEmitter {
        y: 300;

        lifeSpan: 50000
        count: 500
        velocity: 65
        emissionRate: 5
        ParticleMotionGravityWell {
            xattractor: 400
            yattractor: 300
            force: 5
        }
        ParticleMotionLinear {}
    }
    }

    //anti-gravity
    ParticleField {
        painters: ParticlePainter { source: "star.png" }
        emitter: ParticleEmitter {
        y: 350;

        lifeSpan: 50000
        count: 500
        velocity: 100
        emissionRate: 5
        ParticleMotionAntiGravity {
            xattractor: 600
            yattractor: 400
            force: 1
        }
        ParticleMotionAntiGravity {
            xattractor: 300
            yattractor: 0
            force: .1
        }
        ParticleMotionLinear {}
    }
    }

    //mask
    ParticleField {
        width: parent.width
        painters: [
            ParticlePainter { source: "star.png" },
            ParticlePainter { source: "redstar.png"; mask: "helloMaskBig.png" }
        ]
        emitter: ParticleEmitter {
            y: 400
            anchors.horizontalCenter: parent.horizontalCenter
            width: 400
            mask: "helloMask.png"

            lifeSpan: 10000
            count: 600
            emissionRate: 600
            angleDeviation: 360
            velocity: 5
            ParticleMotionLinear {}
        }
    }
}
